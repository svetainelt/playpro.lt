<?php

namespace unisend_shipping\services;


use NAWebCo\BoxPacker\GenericPackable;
use NAWebCo\BoxPacker\Packer;
use unisend_shipping\cons\UnisendShippingConst;
use unisend_shipping\context\UnisendShippingContextHolder;
require_once(DIR_SYSTEM . 'library/unisend_shipping/vendor/autoload.php');

/**
 * Singleton class
 */
class UnisendShippingSizeService
{

    private static $instance = null;

    public function __construct()
    {
    }

    public static function resolveSize($orderData)
    {
        $products = $orderData['products'] ?? null;
        if (empty($products)) return null;
        $packages = [];
        $defaultWidthCfg = UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_DIMENSION_WIDTH);
        $defaultHeightCfg = UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_DIMENSION_HEIGHT);
        $defaultLengthCfg = UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_DIMENSION_LENGTH);
        $defaultWidth = $defaultWidthCfg && is_numeric($defaultWidthCfg) ? (float)$defaultWidthCfg : 10;
        $defaultHeight = $defaultHeightCfg && is_numeric($defaultHeightCfg) ? (float)$defaultHeightCfg : 10;
        $defaultLength = $defaultLengthCfg && is_numeric($defaultLengthCfg) ? (float)$defaultLengthCfg : 10;

        $lengthClass = UnisendShippingContextHolder::getInstance()->getLength();
        $unisendLengthClassId = UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_LENGTH_CLASS_ID) ?: 1;
        foreach ($products as $product) {
            $virtual = isset($product['shipping']) && $product['shipping'] != true;
            $length = (float)$lengthClass->convert($product['length'], $product['length_class_id'], $unisendLengthClassId);
            $width = (float)$lengthClass->convert($product['width'], $product['length_class_id'], $unisendLengthClassId);
            $height = (float)$lengthClass->convert($product['height'], $product['length_class_id'], $unisendLengthClassId);
            if (!$virtual) {
                $packages[] = [$width > 0 ? $width : $defaultWidth, $height > 0 ? $height : $defaultHeight, $length > 0 ? $length : $defaultLength, $product['quantity'] ?? 1];
            }
        }
        $packages = self::packageSize($packages, $orderData);
        return !empty($packages) ? strtoupper($packages[0]) : UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_BOX_SIZE);
    }

    private static function getAvailableSizes($orderData)
    {
        if (!isset($orderData['unisendCarrier'])) {
            return [];
        }
        $planCode = $orderData['unisendCarrier']['plan_code'];
        if ($planCode === 'HANDS' || $planCode === 'TERMINAL') {
            return self::getBpSizes($orderData);
        }
        return self::getLpSizes($orderData);
    }

    private static function getLpSizes($orderData)
    {
        if (isset($orderData['orderInfo'])) {
            $weight = $orderData['orderInfo']['weight'] ?? 0;
            if ($weight <= 500) {
                $sizes['s'] = [2, 38, 30];
            }
            if ($weight <= 2000) {
                $sizes['m'] = [60, 60, 60];
            }
        }
        $sizes['l'] = [105, 105, 105];
        return $sizes;
    }

    private static function getBpSizes($orderData)
    {
        return [
            'xs' => [18, 61, 8],
            's' => [35, 61, 8],
            'm' => [35, 61, 17],
            'l' => [35, 61, 36],
            'xl' => [35, 61, 74],
        ];
    }

    private static function packageSize($packages, $orderData)
    {
        $sizes = self::getAvailableSizes($orderData);
        $possible_sizes = [];
        foreach ($sizes as $name => $size) {
            $packer = new Packer();
            $packer->addBox(new GenericPackable($size[0], $size[1], $size[2], $name));
            foreach ($packages as $k => $package) {
                for ($i = 0; $i < $package[3]; $i++) {
                    $packer->addItem(new GenericPackable($package[0], $package[1], $package[2], $k . $i));
                }
            }

            $result = $packer->pack();

            if ($result->success()) {
                $possible_sizes[] = $name;
            }
        }
        return $possible_sizes;
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new UnisendShippingSizeService();
        }
        return self::$instance;
    }
}
