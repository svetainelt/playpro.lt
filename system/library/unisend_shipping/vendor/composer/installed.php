<?php return array(
    'root' => array(
        'name' => 'oc/unisendshipping',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => null,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'composer/pcre' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '67a32d7d6f9f560b726ab25a061b38ff3a80c560',
            'type' => 'library',
            'install_path' => __DIR__ . '/./pcre',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'composer/semver' => array(
            'pretty_version' => '3.4.3',
            'version' => '3.4.3.0',
            'reference' => '4313d26ada5e0c4edfbd1dc481a92ff7bff91f12',
            'type' => 'library',
            'install_path' => __DIR__ . '/./semver',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'composer/xdebug-handler' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'reference' => '9e36aeed4616366d2b690bdce11f71e9178c579a',
            'type' => 'library',
            'install_path' => __DIR__ . '/./xdebug-handler',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => 'v1.4.0',
            'version' => '1.4.0.0',
            'reference' => '54cacc9b81758b14e3ce750f205a393d52339e97',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => '1febd6c3ef84253d7c815bed85fc622ad207a9f8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'friendsofphp/php-cs-fixer' => array(
            'pretty_version' => 'v2.19.3',
            'version' => '2.19.3.0',
            'reference' => '75ac86f33fab4714ea5a39a396784d83ae3b5ed8',
            'type' => 'application',
            'install_path' => __DIR__ . '/../friendsofphp/php-cs-fixer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'iio/libmergepdf' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'reference' => '77059d0f5d9c5ebfe6df2d094109e6aee90ae647',
            'type' => 'library',
            'install_path' => __DIR__ . '/../iio/libmergepdf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nawebco/box-packer' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'reference' => '82dfa0817b41c03e7adc7ebd25145a8621b52dcf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nawebco/box-packer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/php-parser' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'reference' => 'bb87e28e7d7b8d9a7fda231d37457c9210faf6ce',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/php-parser',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'oc/unisendshipping' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => null,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v2.0.21',
            'version' => '2.0.21.0',
            'reference' => '96c132c7f2f7bc3230723b66e89f8f150b29d5ae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'php-cs-fixer/diff' => array(
            'pretty_version' => 'v1.3.1',
            'version' => '1.3.1.0',
            'reference' => 'dbd31aeb251639ac0b9e7e29405c1441907f5759',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-cs-fixer/diff',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'prestashop/autoindex' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'reference' => '92e10242f94a99163dece280f6bd7b7c2b79c158',
            'type' => 'library',
            'install_path' => __DIR__ . '/../prestashop/autoindex',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'prestashop/header-stamp' => array(
            'pretty_version' => 'v1.7',
            'version' => '1.7.0.0',
            'reference' => 'd77ce6d0a7f066670a4774be88f05e5f07b4b6fc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../prestashop/header-stamp',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'prestashop/php-dev-tools' => array(
            'pretty_version' => 'v3.16.1',
            'version' => '3.16.1.0',
            'reference' => '785108c29ef6f580930372d88b8f551740fdee98',
            'type' => 'library',
            'install_path' => __DIR__ . '/../prestashop/php-dev-tools',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'roave/security-advisories' => array(
            'pretty_version' => 'dev-latest',
            'version' => 'dev-latest',
            'reference' => '5bd374d4b964c449fc99b871b6d9f139d0b41502',
            'type' => 'metapackage',
            'install_path' => null,
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'dev_requirement' => true,
        ),
        'setasign/fpdf' => array(
            'pretty_version' => '1.8.2',
            'version' => '1.8.2.0',
            'reference' => 'd77904018090c17dc9f3ab6e944679a7a47e710a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../setasign/fpdf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'setasign/fpdi' => array(
            'pretty_version' => 'v2.6.1',
            'version' => '2.6.1.0',
            'reference' => '09a816004fcee9ed3405bd164147e3fdbb79a56f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../setasign/fpdi',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'setasign/fpdi-fpdf' => array(
            'pretty_version' => 'v2.3.0',
            'version' => '2.3.0.0',
            'reference' => 'f2fdc44e4d5247a3bb55ed2c2c1396ef05c02357',
            'type' => 'library',
            'install_path' => __DIR__ . '/../setasign/fpdi-fpdf',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'squizlabs/php_codesniffer' => array(
            'pretty_version' => '3.10.3',
            'version' => '3.10.3.0',
            'reference' => '62d32998e820bddc40f99f8251958aed187a5c9c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../squizlabs/php_codesniffer',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'a10b1da6fc93080c180bba7219b5ff5b7518fe81',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/debug' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'ab42889de57fdfcfcc0759ab102e2fd4ea72dcae',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/debug',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => '31fde73757b6bad247c54597beef974919ec6860',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'e58d7841cddfed6e846829040dca2cca0ebbbbb3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'b6b6ad3db3edb1b4b1c1896b1975fb684994de6e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'c7efc97a47b2ebaabc19d5b6c6b50f5c37c92744',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'reference' => 'aed596913b70fae57be53d86faa2e9ef85a2297b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'reference' => 'b5f7b932ee6fa802fc792eabd77c4c88084517ce',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php70' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'reference' => '3fe414077251a81a1b15b1c709faf5c2fbae3d4e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php70',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'reference' => 'beecef6b463b06954638f02378f52496cb84bacc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'b8648cf1d5af12a44a51d07ef9bf980921f15fca',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'symfony/stopwatch' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => '298b81faad4ce60e94466226b2abbb8c9bca7462',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/stopwatch',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
        'viison/address-splitter' => array(
            'pretty_version' => '0.3.4',
            'version' => '0.3.4.0',
            'reference' => 'ebad709276aaadce94a3a1fe2507aa38a467a94a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../viison/address-splitter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
