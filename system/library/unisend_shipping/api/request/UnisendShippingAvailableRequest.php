<?php

namespace unisend_shipping\api\request;


class UnisendParcelRequest
{
    public $id;
    public $receiverCountryCode;
    public $planCode;
    public $parcelType;
    public $size;
    public $weight;

    public static function fromCart(Cart $cart, Carrier $carrier): UnisendParcelRequest
    {
        return new UnisendParcelRequest();//TODO implement
    }
}
