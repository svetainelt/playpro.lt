<?php
namespace vendor\isenselabs\giftteaser\conditions;
use \vendor\isenselabs\giftteaser\conditions\condition as Condition;

class RewardPoints extends Condition {

	public function checkCondition(&$giftCondProps, &$productData) {
		if (empty($giftCondProps['customer_group']) || count($productData) == 0) {
			return false;
		}

		$customer_group_id = 0;
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getGroupId();
		}
		if (!in_array($customer_group_id, $giftCondProps['customer_group'] )) {
			return false;
		}

		$totalRewardPoints = 0;
		foreach ($productData as &$pd) {
			$totalRewardPoints += (int)$pd['reward'];
		}
		$min = null;
		$max = null;
		if (isset($giftCondProps['reward_points_min'])) $min = floatval($giftCondProps['reward_points_min']);
		if (isset($giftCondProps['reward_points_max']) && $giftCondProps['reward_points_max'] != 0) $max = floatval($giftCondProps['reward_points_max']);
		if ($totalRewardPoints < $min) return false;
		if ($max !== null && $totalRewardPoints > $max) return false;
		return true;
	}

	public function buildProductData(&$gift) {
		$pd = parent::buildProductData($gift);
		$price = is_numeric($gift['condition_properties']['reward_points_price']) ? $gift['condition_properties']['reward_points_price'] : 0;
		$pd['reward'] = -1 * $price;
		return $pd;
	}

}
