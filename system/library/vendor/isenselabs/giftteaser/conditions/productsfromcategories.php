<?php
namespace vendor\isenselabs\giftteaser\conditions;
use \vendor\isenselabs\giftteaser\conditions\condition as Condition;

class ProductsFromCategories extends Condition {

	public function checkCondition(&$giftCondProps, &$productData) {
		if (empty($giftCondProps['customer_group']) || empty($giftCondProps['categories'])) {
			return false;
		}

        $customer_group_id = 0;
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getGroupId();
        }
        if (!in_array($customer_group_id, $giftCondProps['customer_group'] )) {
            return false;
        }

		$giftCondProps['category_include_exclude'] = isset($giftCondProps['category_include_exclude']) ? $giftCondProps['category_include_exclude'] : 'include';
		$giftCondProps['category_quantity']        = isset($giftCondProps['category_quantity']) ? $giftCondProps['category_quantity'] : 1;
		$giftCondProps['category_qty_sum']         = isset($giftCondProps['category_qty_sum']) ? $giftCondProps['category_qty_sum'] : 0;
		$giftCondProps['category_multiple_gift']   = isset($giftCondProps['category_multiple_gift']) ? $giftCondProps['category_multiple_gift'] : 0;

		$gift_total_qty   = 0;
		$gift_qty_to_cart = 0;
		$gift_total_price = 0;
		$validQuantity    = false;
		$productIds       = array();
		$products         = array();

		foreach ($productData as &$p) {
			$productIds[] = $p['product_id'];
		}
		$giftProducts = $this->model_module_giftteaser->cartProductsInGiftCategories($productIds, $giftCondProps['categories'], $giftCondProps['category_include_exclude']);

		if ($giftProducts) {
			foreach ($productData as &$p) {
				if (in_array($p['product_id'], $giftProducts)) {
					$gift_total_qty += $p['quantity'];
					$gift_total_price += $p['total'];

					if (!isset($products[$p['product_id']])) {
						$products[$p['product_id']] = $p['quantity'];
					} else {
						$products[$p['product_id']] += $p['quantity'];
					}
				}
			}

			// Validate Quantity
			$_gift_valid_in_cart = 0;
			foreach ($products as $productId => $productQuantity) {
				if ($productQuantity >= $giftCondProps['category_quantity']) {
					$_gift_valid_in_cart += 1;
				}
			}

			if (!$giftCondProps['category_qty_sum'] && $_gift_valid_in_cart) {
				$validQuantity = true; // each selected product
			} elseif ($giftCondProps['category_qty_sum'] && $gift_total_qty >= $giftCondProps['category_quantity']) {
				$validQuantity = true; // sum all selected product
			}

			$min_total = !empty($giftCondProps['category_min_total_price']) ? (float)$giftCondProps['category_min_total_price'] : 0;
			$currency_value = (float)$this->currency->getValue($this->session->data['currency']);
			$amount = $currency_value ? $min_total * $currency_value : $min_total;

			if ($validQuantity && $gift_total_price >= $amount) {
				$gift_qty_to_cart = 1;

				// Multiple Gift
				if ($giftCondProps['category_multiple_gift']) {
					if (!$giftCondProps['category_qty_sum']) {
						$gift_qty_to_cart = 0;
						foreach ($products as $productId => $productQuantity) {
							if ($productQuantity >= $giftCondProps['category_quantity']) {
								$gift_qty_to_cart += floor((int)$productQuantity / (int)$giftCondProps['category_quantity']);
							}
						}
					} elseif ($giftCondProps['category_qty_sum']) {
						$gift_qty_to_cart = floor((int)$gift_total_qty / (int)$giftCondProps['category_quantity']);
					}
				}
			}
		}

		$giftCondProps['gift_qty_to_cart'] = $gift_qty_to_cart;

		return $gift_qty_to_cart > 0;
	}
}
