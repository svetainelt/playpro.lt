<?php
namespace vendor\isenselabs\giftteaser\conditions;
use \vendor\isenselabs\giftteaser\conditions\condition as Condition;

class CartTotal extends Condition {

	public function __construct($registry) {
		parent::__construct($registry);
	}

	public function checkCondition(&$giftCondProps, &$productData) {
		if (empty($giftCondProps['customer_group']) || count($productData) == 0) {
			return false;
		}

		$customer_group_id = 0;
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getGroupId();
		}
		if (!in_array($customer_group_id, $giftCondProps['customer_group'] )) {
			return false;
		}

		if ($giftCondProps['select_total'] == 'total') {
			$cartPrice = $this->giftteaserlib->getCartLib()->getTotal();
		} else if ($giftCondProps['select_total'] == 'subtotal') {
			$cartPrice = $this->giftteaserlib->getCartLib()->getSubTotal();
		}
		$min = 0;
		$max = null;
		if (isset($giftCondProps['total'])) $min = floatval($giftCondProps['total']);
		if (isset($giftCondProps['total_max']) && $giftCondProps['total_max'] != 0) $max = floatval($giftCondProps['total_max']);
		if ($cartPrice < $min) return false;
		if ($max !== null && $cartPrice > $max) return false;

		$giftCondProps['gift_qty_to_cart'] = 1;

		return true;
	}

}
