<?php
namespace vendor\isenselabs\giftteaser\conditions;
use \vendor\isenselabs\giftteaser\conditions\condition as Condition;

class AllProducts extends Condition {

	public function checkCondition(&$giftCondProps, &$productData) {
		if (empty($giftCondProps['customer_group']) || empty($giftCondProps['certain'])) {
			return false;
		}

		$customer_group_id = 0;
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getGroupId();
		}
		if (!in_array($customer_group_id, $giftCondProps['customer_group'] )) {
			return false;
		}

		$gift_total_qty = 0;
		$gift_qty_to_cart = 0;
		$validQuantity = false;
		$productIds = array();
		$products = array();

		foreach ($productData as &$p) {
			$productIds[] = $p['product_id'];
			// track quantities per product, since OC creates different cart entries for each combination of options
			if (in_array($p['product_id'], $giftCondProps['certain'])) {
				$gift_total_qty += $p['quantity'];

				if (!isset($products[$p['product_id']])) {
					$products[$p['product_id']] = $p['quantity'];
				} else {
					$products[$p['product_id']] += $p['quantity'];
				}
			}
		}

		$allProductInCart = !array_diff($giftCondProps['certain'], array_keys($products));

		if ($allProductInCart) {
			$_gift_valid_in_cart = 0;
			foreach ($products as $productId => $productQuantity) {
				if ($productQuantity >= $giftCondProps['certain_product_quantity']) {
					$_gift_valid_in_cart += 1;
				}
			}

			// Validate Quantity
			if (empty($giftCondProps['certain_product_qty_sum']) && $_gift_valid_in_cart == count($giftCondProps['certain'])) {
				$validQuantity = true; // each selected product
			} elseif (!empty($giftCondProps['certain_product_qty_sum']) && $gift_total_qty >= $giftCondProps['certain_product_quantity']) {
				$validQuantity = true; // sum all selected product
			}
			
			if ($validQuantity) {
				$gift_qty_to_cart = 1;

				// Multiple Gift
				if (!empty($giftCondProps['certain_product_multiple_gift'])) {
					if (empty($giftCondProps['certain_product_qty_sum'])) {
						$gift_qty_to_cart = floor((int)min($products) / (int)$giftCondProps['certain_product_quantity']);
					} elseif (!empty($giftCondProps['certain_product_qty_sum'])) {
						$gift_qty_to_cart = floor((int)$gift_total_qty / (int)$giftCondProps['certain_product_quantity']);
					}
				}
			}
		}

		$giftCondProps['gift_qty_to_cart'] = $gift_qty_to_cart;

		return ($allProductInCart && $validQuantity && $gift_qty_to_cart > 0);
	}
}
