<?php
namespace vendor\isenselabs\giftteaser\conditions;
use \vendor\isenselabs\giftteaser\conditions\condition as Condition;

class ProductsFromManufacturers extends Condition {

	public function checkCondition(&$giftCondProps, &$productData) {
		if (empty($giftCondProps['customer_group']) || empty($giftCondProps['manufacturer'])) {
			return false;
		}

        $customer_group_id = 0;
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getGroupId();
        }
        if (!in_array($customer_group_id, $giftCondProps['customer_group'] )) {
            return false;
        }

		$giftCondProps['manufacturer_include_exclude'] = isset($giftCondProps['manufacturer_include_exclude']) ? $giftCondProps['manufacturer_include_exclude'] : 'include';
		$giftCondProps['manufacturer_quantity']        = isset($giftCondProps['manufacturer_quantity']) ? $giftCondProps['manufacturer_quantity'] : 1;
		$giftCondProps['manufacturer_qty_sum']         = isset($giftCondProps['manufacturer_qty_sum']) ? $giftCondProps['manufacturer_qty_sum'] : 0;
		$giftCondProps['manufacturer_multiple_gift']   = isset($giftCondProps['manufacturer_multiple_gift']) ? $giftCondProps['manufacturer_multiple_gift'] : 0;

		$gift_total_qty   = 0;
		$gift_qty_to_cart = 0;
		$gift_total_price = 0;
		$validQuantity    = false;
		$productIds       = array();
		$products         = array();

		foreach ($productData as &$p) {
			$productIds[] = $p['product_id'];
		}
		$giftProducts = $this->model_module_giftteaser->cartProductsInGiftManufacturers($productIds, $giftCondProps['manufacturer'], $giftCondProps['manufacturer_include_exclude']);

		if ($giftProducts) {
			foreach ($productData as &$p) {
				if (in_array($p['product_id'], $giftProducts)) {
					$gift_total_qty += $p['quantity'];
					$gift_total_price += $p['total'];

					if (!isset($products[$p['product_id']])) {
						$products[$p['product_id']] = $p['quantity'];
					} else {
						$products[$p['product_id']] += $p['quantity'];
					}
				}
			}

			// Validate Quantity
			$_gift_valid_in_cart = 0;
			foreach ($products as $productId => $productQuantity) {
				if ($productQuantity >= $giftCondProps['manufacturer_quantity']) {
					$_gift_valid_in_cart += 1;
				}
			}

			if (!$giftCondProps['manufacturer_qty_sum'] && $_gift_valid_in_cart) {
				$validQuantity = true; // each selected product
			} elseif ($giftCondProps['manufacturer_qty_sum'] && $gift_total_qty >= $giftCondProps['manufacturer_quantity']) {
				$validQuantity = true; // sum all selected product
			}

			$min_total = !empty($giftCondProps['manufacturer_min_total_price']) ? (float)$giftCondProps['manufacturer_min_total_price'] : 0;
			$currency_value = (float)$this->currency->getValue($this->session->data['currency']);
			$amount = $currency_value ? $min_total * $currency_value : $min_total;

			if ($validQuantity && $gift_total_price >= $amount) {
				$gift_qty_to_cart = 1;

				// Multiple Gift
				if ($giftCondProps['manufacturer_multiple_gift']) {
					if (!$giftCondProps['manufacturer_qty_sum']) {
						$gift_qty_to_cart = 0;
						foreach ($products as $productId => $productQuantity) {
							if ($productQuantity >= $giftCondProps['manufacturer_quantity']) {
								$gift_qty_to_cart += floor((int)$productQuantity / (int)$giftCondProps['manufacturer_quantity']);
							}
						}
					} elseif ($giftCondProps['manufacturer_qty_sum']) {
						$gift_qty_to_cart = floor((int)$gift_total_qty / (int)$giftCondProps['manufacturer_quantity']);
					}
				}
			}
		}

		$giftCondProps['gift_qty_to_cart'] = $gift_qty_to_cart;

		return $gift_qty_to_cart > 0;
	}
}
