<?php
namespace vendor\isenselabs\giftteaser\conditions;

abstract class Condition {
	protected $registry;

	// child classes shouldn't need to redefine the constructor, but if they do, they need to call the parent one for registry access
	// children are built lazily, and only instantiated once
	public function __construct($registry) {
		$this->registry = $registry;
	}

	// magic method for registry access
	public function __get($key) {
		return $this->registry->get($key);
	}

	public function buildProductData(&$gift) {
		$pd = $this->model_catalog_product->getProduct($gift['product_id']);
		$pos = $this->model_catalog_product->getProductOptions($gift['product_id']);
		$settings = $this->giftteaserlib->getSettings();

		$arr = array(
			'gift_teaser' => true,
			'real_price' => $pd['price'],
			'real_tax_class_id' => $pd['tax_class_id'],
			'cart_id' => 'gift_teaser_' . $gift['gift_id'],
			'product_id' => $gift['product_id'],
			'name'            => $pd['name'] . ' (' . $settings['FreeGiftLabel'][(int)$this->config->get('config_language_id')] . ')',
			'model'           => $pd['model'],
			'shipping'        => 0,
			'image'           => $pd['image'],
			'option'          => (is_array($gift['option']) ? $this->buildProductOptionsData($gift['product_id'], $gift['option']) : array()),
			'download'        => array(), // TODO255 support downloadable gifts
			'quantity'        => $gift['quantity'],
			'minimum'         => $pd['minimum'],
			'subtract'        => $pd['subtract'],
			'stock'           => true, // TODO255 take into account product stock, consider the case of having the same product as the gift in the cart
			'price'           => 0,
			'total'           => 0,
			'reward'          => 0,
			'points'          => 0,
			'tax_class_id'    => 0, // TODO255 $pd['tax_class_id'] or a new module setting if we want taxes applied
			'weight'          => ($pd['weight']) * $gift['quantity'], // TODO255 add product option weight
			'weight_class_id' => $pd['weight_class_id'],
			'length'          => $pd['length'],
			'width'           => $pd['width'],
			'height'          => $pd['height'],
			'length_class_id' => $pd['length_class_id'],
			'recurring'       => false, //$pd['recurring'] // TODO255 support for recurring products as gifts
		);

		// PreOrder compatibility
		if (!empty($pd['stock_status_id'])) {
			$arr['stock_status_id'] = $pd['stock_status_id'];
		}

		if (!empty($settings['gift_price_in_cart'])) {
			$arr['price'] = $pd['price'];
			$arr['total'] = $pd['price'] * $gift['quantity'];
			$arr['tax_class_id'] = $pd['tax_class_id'];
		}

		if (VERSION < '2.1.0.1') {
			$arr['key'] = $arr['cart_id'];
			unset($arr['cart_id']);
		}

		return $arr;
	}

	protected function buildProductOptionsData($productId, $opts) {
		$option_data = array();
		foreach ($opts as $product_option_id => $value) {
			$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$productId . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

			if ($option_query->num_rows) {
				if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio') {
					$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($option_value_query->num_rows) {
						// if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
						// 	$stock = false;
						// }
						$option_data[] = array(
							'product_option_id'       => $product_option_id,
							'product_option_value_id' => $value,
							'option_id'               => $option_query->row['option_id'],
							'option_value_id'         => $option_value_query->row['option_value_id'],
							'name'                    => $option_query->row['name'],
							'value'                   => $option_value_query->row['name'],
							'type'                    => $option_query->row['type'],
							'quantity'                => $option_value_query->row['quantity'],
							'subtract'                => $option_value_query->row['subtract'],
							'price'                   => 0,
							'price_prefix'            => $option_value_query->row['price_prefix'],
							'points'                  => 0,
							'points_prefix'           => $option_value_query->row['points_prefix'],
							'weight'                  => $option_value_query->row['weight'],
							'weight_prefix'           => $option_value_query->row['weight_prefix']
						);
					}
				} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
					foreach ($value as $product_option_value_id) {
						$option_value_query = $this->db->query("SELECT pov.option_value_id, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix, ovd.name FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (pov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

						if ($option_value_query->num_rows) {
							if ($option_value_query->row['price_prefix'] == '+') {
								$option_price += $option_value_query->row['price'];
							} elseif ($option_value_query->row['price_prefix'] == '-') {
								$option_price -= $option_value_query->row['price'];
							}

							if ($option_value_query->row['points_prefix'] == '+') {
								$option_points += $option_value_query->row['points'];
							} elseif ($option_value_query->row['points_prefix'] == '-') {
								$option_points -= $option_value_query->row['points'];
							}

							if ($option_value_query->row['weight_prefix'] == '+') {
								$option_weight += $option_value_query->row['weight'];
							} elseif ($option_value_query->row['weight_prefix'] == '-') {
								$option_weight -= $option_value_query->row['weight'];
							}

							// if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
							// 	$stock = false;
							// }

							$option_data[] = array(
								'product_option_id'       => $product_option_id,
								'product_option_value_id' => $product_option_value_id,
								'option_id'               => $option_query->row['option_id'],
								'option_value_id'         => $option_value_query->row['option_value_id'],
								'name'                    => $option_query->row['name'],
								'value'                   => $option_value_query->row['name'],
								'type'                    => $option_query->row['type'],
								'quantity'                => $option_value_query->row['quantity'],
								'subtract'                => $option_value_query->row['subtract'],
								'price'                   => $option_value_query->row['price'],
								'price_prefix'            => $option_value_query->row['price_prefix'],
								'points'                  => $option_value_query->row['points'],
								'points_prefix'           => $option_value_query->row['points_prefix'],
								'weight'                  => $option_value_query->row['weight'],
								'weight_prefix'           => $option_value_query->row['weight_prefix']
							);
						}
					}
				} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
					$option_data[] = array(
						'product_option_id'       => $product_option_id,
						'product_option_value_id' => '',
						'option_id'               => $option_query->row['option_id'],
						'option_value_id'         => '',
						'name'                    => $option_query->row['name'],
						'value'                   => $value,
						'type'                    => $option_query->row['type'],
						'quantity'                => '',
						'subtract'                => '',
						'price'                   => '',
						'price_prefix'            => '',
						'points'                  => '',
						'points_prefix'           => '',
						'weight'                  => '',
						'weight_prefix'           => ''
					);
				}
			}
		}
		return $option_data;
	}

	abstract public function checkCondition(&$gift, &$product_data);

}
