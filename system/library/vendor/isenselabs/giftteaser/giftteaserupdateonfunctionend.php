<?php
namespace vendor\isenselabs\giftteaser;

class GiftTeaserUpdateOnFunctionEnd {

	private $gtlib;

	public function __construct($giftTeaserLib) {
		$this->gtlib = $giftTeaserLib;
	}

	// destructor updates the cart gifts table so we can avoid trying to hook onto the end of a function or modify it
	public function __destruct() {
		$this->gtlib->updateCartGiftsTable();
	}

}