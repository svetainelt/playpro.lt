<?php
namespace vendor\isenselabs\giftteaser;

class GiftTeaserSkipMods {

	private $gtlib;
	private $originalSkip;

	public function __construct($giftTeaserLib) {
		$this->gtlib = $giftTeaserLib;
		$this->originalSkip = $this->gtlib->skipCartMods(true);
	}

	// destructor updates the cart gifts table so we can avoid trying to hook onto the end of a function or modify it
	public function __destruct() {
		$this->gtlib->skipCartMods($this->originalSkip);
	}

}