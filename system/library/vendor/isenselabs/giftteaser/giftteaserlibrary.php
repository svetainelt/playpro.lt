<?php
namespace vendor\isenselabs\giftteaser;
use \vendor\isenselabs\giftteaser\giftteaserupdateonfunctionend as GiftTeaserUpdateOnFunctionEnd;
use \vendor\isenselabs\giftteaser\giftteaserskipmods as GiftTeaserSkipMods;

class GiftTeaserLibrary {
	protected $registry;
	protected $cartLib; // inserted to keep a reference to the cart library during its construction
	protected $settings;
	// skipCartDataModifications is used internally to allow this class to call cart library functions without recursing back into its own modifications
	protected $skipCartDataModifications;
	protected $conditionCheckers;
	public $moduleModel;
	protected static $conditionCheckerTypeMap = array(
		1 => 'CartTotal',
		2 => 'AllProducts',
		3 => 'SomeProducts',
		4 => 'ProductsFromCategories',
		5 => 'ProductsFromManufacturers',
		6 => 'RewardPoints'
	);

	// created (and added to the registry) in the cart library's constructor
	public function __construct($registry, $cartLib) {
		$this->registry = $registry;
		$this->cartLib = $cartLib;
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');
		$this->settings = $this->model_setting_setting->getSetting('giftteaser', (int)$this->config->get('config_store_id'));
		if (isset($this->settings['giftteaser'])) {
			$this->settings = $this->settings['giftteaser'];
		} else {
			$this->settings = array('Enabled' => 0);
		}
		$this->load->model('module/giftteaser');
		$this->moduleModel = $this->model_module_giftteaser;
		$this->conditionCheckers = array();
		$this->skipCartDataModifications = false;
	}

	// magic method for registry access
	public function __get($key) {
		return $this->registry->get($key);
	}

	// ready only access to module settings, available from the registry
	public function getSettings() {
		return $this->settings;
	}

	// updates the internal skip variable and returns the old value for resetting purposes
	public function skipCartMods($toSkipOrNot) {
		$old = $this->skipCartDataModifications;
		$this->skipCartDataModifications = $toSkipOrNot;
		return $old;
	}

	// modifies product_data before the cart library's getProducts() method returns
	public function modifyCartData(&$productData) {
		if ($this->settings['Enabled'] != 'yes') return;
		if (count($productData) == 0) {
			$this->moduleModel->deleteStoredCartGifts();
			if (isset($this->session->data['giftteaser_ignoreGifts'])) {
				unset($this->session->data['giftteaser_ignoreGifts']);
			}
			if (isset($this->session->data['giftteaser_gifts'])) {
				unset($this->session->data['giftteaser_gifts']);
			}
			if (isset($this->session->data['giftteaser_notify'])) {
				unset($this->session->data['giftteaser_notify']);
			}
			return;
		}
		// skip modifying if preset to do so, used so we can call methods like cart->getTotal, getSubtotal, getTaxes, hasProducts, etc without recursing back here when they in turn call getProducts
		if ($this->skipCartDataModifications) return;
		// possibly also add a skip check if we're in the admin
		$gifts = $this->moduleModel->fetchStoredCartGifts();

		foreach ($gifts as &$gift) {
			if ($giftProductData = $this->buildProductDataForGift($gift)) {
				$productData[] = $this->buildProductDataForGift($gift);
			}
		}
	}

	// cleans out gifts that were in expired non-logged in sessions
	public function clearGiftsFromExpiredCarts() {
		// note that since opencart uses the cart library's add function to re-add cart products after login, we don't need to do anything special to have the gifts recalculated
		// $this->moduleModel->clearGiftsFromExpiredCarts();
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'cart_gift WHERE (api_id > "0" OR customer_id = "0") AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)');
	}

	// update the gifts after a customer log in / log out (returns an auto update object)
	public function updateGiftsOnLogin() {
		if ($this->customer->getId() && !isset($this->session->data['giftteaser_loggedin'])) {
			$this->session->data['giftteaser_loggedin'] = true;
			if (isset($this->session->data['giftteaser_ignoreGifts'])) {
				unset($this->session->data['giftteaser_ignoreGifts']);
			}
			if (isset($this->session->data['giftteaser_gifts'])) {
				unset($this->session->data['giftteaser_gifts']);
			}
			if (isset($this->session->data['giftteaser_notify'])) {
				unset($this->session->data['giftteaser_notify']);
			}
			return $this->getAutoUpdateObject();
		}
		return null;
	}

	// destroys all gifts, rechecks gift conditions and rebuilds the gifts for this order
	// public because of GiftTeaserUpdateOnFunctionEnd
	public function updateCartGiftsTable() {
		if ($this->settings['Enabled'] != 'yes') return;
		// skip modifying if preset to do so, used so we can call methods like cart->getTotal, getSubtotal, getTaxes, hasProducts, etc without recursing back here when they in turn call getProducts
		if ($this->skipCartDataModifications) return;
		// possibly also add a skip check if we're in the admin
		$oldSkip = $this->skipCartMods(true);
		$oldGifts = $this->moduleModel->fetchStoredCartGifts();
		$productData = $this->cartLib->getProducts(); // raw cart, no gifts, thanks to skipCartMods
		$giftProducts = $this->getGiftsForCartProducts($productData);

		$reindexedOldGifts = array();
		foreach ($oldGifts as &$gift) {
			$reindexedOldGifts[$gift['gift_id']] = $gift;
		}
		foreach ($giftProducts as &$gift) {
			if (isset($reindexedOldGifts[$gift['gift_id']])) {
				$gift['quantity'] = $this->getGiftEligibleQty($gift['gift_id'], $reindexedOldGifts[$gift['gift_id']]['quantity']);
				$gift['option'] = json_encode($reindexedOldGifts[$gift['gift_id']]['option']);
			}
		}
		$this->moduleModel->deleteStoredCartGifts();
		$this->moduleModel->storeCartGifts($giftProducts);
		$this->skipCartMods($oldSkip);
	}

	// updates the quantity of a gift, returns true/false whether it was an allowed change
	public function updateCartGiftQuantity($cartId, $newQuantity) {
		// convert to a cart gift id
		$cartGiftId = $this->extractCartGiftId($cartId);
		$applyQuantity = $this->getGiftEligibleQty($cartGiftId);
		
		if ($newQuantity == $applyQuantity) {
			$this->moduleModel->updateCartGiftQuantity($cartGiftId, $applyQuantity);
			return true;
		}
		return false;
	}

	// removes a gift and marks it in the session so we don't readd it
	public function removeGift($cartId) {
		$cartGiftId = $this->extractCartGiftId($cartId);
		if (!isset($this->session->data['giftteaser_ignoreGifts'])) {
			$this->session->data['giftteaser_ignoreGifts'] = array();
		}
		$this->session->data['giftteaser_ignoreGifts'][] = $cartGiftId;
		$this->moduleModel->deleteStoredGift($cartGiftId);
	}

	// checks the custom format that replaces the cart_id in the product data of cart->getProducts()
	public function isACartGiftId($cart_id) {
		return (preg_match('/gift_teaser_[0-9]+/', $cart_id) == 1);
	}

	// extract a gift id from the custom format that replaces the cart_id in the product data of cart->getProducts()
	public function extractCartGiftId($cart_id) {
		return (int)str_replace('gift_teaser_', '', $cart_id);
	}

	public function getGiftEligibleQty($gift_id, $default = 0) {
		$quantity = $default;
		if (!empty($this->session->data['giftteaser_gifts'][$gift_id]['condition_properties']['gift_qty_to_cart'])) {
			$quantity = $this->session->data['giftteaser_gifts'][$gift_id]['condition_properties']['gift_qty_to_cart'];
		}

		return $quantity;
	}

	public function clearGifts() {
		$this->moduleModel->deleteStoredCartGifts();
	}

	// returns an object that calls updateCartGiftsTable when destroyed
	public function getAutoUpdateObject() {
		return new GiftTeaserUpdateOnFunctionEnd($this);
	}

	// returns an object that enables the internal skip condition for admins, then resets it when destroyed
	public function getSkipObject() {
		return new GiftTeaserSkipMods($this);
	}

	// inserts the required css and js files for the catalog
	public function injectGiftTeaserResources() {
		$this->document->addStyle('catalog/view/javascript/giftteaser/fancybox/jquery.fancybox.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/giftteaser.css');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->document->addScript('catalog/view/javascript/giftteaser/fancybox/jquery.fancybox.pack.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addScript('catalog/view/javascript/giftteaser/main.js');
	}

	// access to cart lib during its construction
	public function getCartLib() {
		return $this->cartLib;
	}

	// expects an array formatted as that returned by cart->getProducts()
	protected function getGiftsForCartProducts(&$productData) {
		$sessId = $this->session->getId();
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = $this->customer->getId();
		$now = date('Y-m-d H:i:s');

		$configuredGifts = $this->moduleModel->getCurrentGifts();
		$giftProducts = array();

		if (!isset($this->session->data['giftteaser_gifts'])) {
			$this->session->data['giftteaser_gifts'] = array();
		}
		if (!isset($this->session->data['giftteaser_notify'])) {
			$this->session->data['giftteaser_notify'] = array();
		}
		// Will be updated per gift eligible quantity change at buildCartGiftData()
		$this->session->data['giftteaser_notify']['status'] = false;

		$oldSkip = $this->skipCartMods(true); // ensure the conditions do not recurse
		foreach ($configuredGifts as &$gift) {
			if ($this->checkGiftCondition($gift, $productData)) {
				$giftProducts[] = $this->buildCartGiftData($gift, $apiId, $customerId, $sessId, $now);
			}
		}
		$this->skipCartMods($oldSkip);
		return $giftProducts;
	}

	// checks gift conditions regardless of gift condition type (internally uses the factory method for specific gift conditions)
	protected function checkGiftCondition(&$gift, &$productData) {
		if (isset($this->session->data['giftteaser_ignoreGifts'])) {
			if (in_array($gift['gift_id'], $this->session->data['giftteaser_ignoreGifts'])) {
				return false; // user removed the gift manually
			}
		}
		if ($condition = $this->buildCondition($gift['condition_type'])) {
			return $condition->checkCondition($gift['condition_properties'], $productData);
		}

		return false;
	}

	// factory method for condition checkers
	protected function buildCondition($conditionType) {
		if (!isset($this->conditionCheckers[$conditionType])) {
			if (!isset(static::$conditionCheckerTypeMap[$conditionType])) {
				$this->updateCartGiftsTable();
			} else {
				$className = '\\vendor\\isenselabs\\giftteaser\\conditions\\' . strtolower(static::$conditionCheckerTypeMap[$conditionType]);
				$this->conditionCheckers[$conditionType] = new $className($this->registry);
			}
		}
		return isset($this->conditionCheckers[$conditionType]) ? $this->conditionCheckers[$conditionType] : false;
	}

	// builds the array that gets inserted into the cart library's product data
	protected function buildProductDataForGift(&$gift) {
		if ($condition = $this->buildCondition($gift['condition_type'])) {
			return $condition->buildProductData($gift);
		}

		return false;
	}

	protected function buildCartGiftData(&$gift, $apiId, $customerId, $sessId, $dateAdded) {
		// check if product requires options
		$productOptions = $this->model_catalog_product->getProductOptions($gift['product_id']);

		$this->session->data['giftteaser_gifts'][$gift['gift_id']] = $gift;
		$eligibleQuantity = $this->getGiftEligibleQty($gift['gift_id']);

		if (
			!isset($this->session->data['giftteaser_notify']['monitor'][$gift['gift_id']])
			|| $this->session->data['giftteaser_notify']['monitor'][$gift['gift_id']] != $eligibleQuantity
		) {
			$this->session->data['giftteaser_notify']['status'] = true;
			$this->session->data['giftteaser_notify']['monitor'][$gift['gift_id']] = $eligibleQuantity;
		}

		return array(
			'api_id' => $apiId,
			'customer_id' => $customerId,
			'session_id' => $sessId,
			'product_id' => $gift['product_id'],
			'recurring' => false, // TODO255 recurring products as gifts
			'requires_options' => (is_array($productOptions) && !empty($productOptions)),
			'option' => json_encode(array()), // options get populated afterwards
			'quantity' => $eligibleQuantity, // 1, //$gift['quantity']
			'date_added' => $dateAdded,
			// gift teaser specific
			'gift_id' => $gift['gift_id']
		);
	}

}
