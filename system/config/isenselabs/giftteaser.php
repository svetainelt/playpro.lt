<?php
$_['giftteaser_name']              = 'giftteaser';
$_['giftteaser_total2']            = 'gift_teaser';
$_['giftteaser_model']             = 'model_module_giftteaser';
$_['giftteaser_path']              = 'module/giftteaser';
$_['giftteaser_total_path2']       = 'total/gift_teaser';
$_['giftteaser_version']           = '2.6.0';
$_['giftteaser_module_data']       = 'giftTeaser_module';
$_['giftteaser_link']              = 'extension/module';
$_['giftteaser_total_link']        = 'extension/total';
$_['giftteaser_link_params']       = '&type=module';
$_['giftteaser_total_link_params'] = '&type=total';
