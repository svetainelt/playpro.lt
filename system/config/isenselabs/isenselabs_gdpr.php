<?php
$_['isenselabs_gdpr_name']               = 'isenselabs_gdpr';
$_['isenselabs_gdpr_model']              = 'model_module_isenselabs_gdpr';
$_['isenselabs_gdpr_path']               = 'module/isenselabs_gdpr';
$_['isenselabs_gdpr_version']            = '2.9.2';

$_['isenselabs_gdpr_link']               = 'extension/module';
$_['isenselabs_gdpr_link_params']        = '&type=module';

$_['isenselabs_gdpr_token_string']       = 'token';

$_['isenselabs_gdpr_model_editor']       = 'model_module_isenselabs_gdpr_editor';

$_['isenselabs_gdpr_mid']	  			 = 'R7H4X4O2L5';