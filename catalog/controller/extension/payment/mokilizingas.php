<?php
class ControllerExtensionPaymentMokilizingas extends Controller {
    
    /**
     * @var int Order ID
     */
    private $orderId;
    
    public function index() {
        $order_info = $this->getOrderInfo();
        
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
            $routeAction = 'extension/payment/mokilizingas/confirm';
        } else {
            $this->load->language('payment/mokilizingas');
            $routeAction = 'payment/mokilizingas/confirm';
        }
        
        $data['button_confirm'] = $this->language->get('button_confirm');
        
        $data['action'] = $this->url->link($routeAction, '', true);
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['order_id'] = $order_info['order_id'];
        $data['min_price'] = (float)$this->config->get('mokilizingas_minimal_price');
        $data['max_price'] = (float)$this->config->get('mokilizingas_max_price');
        $data['total'] = $order_info['total'];
        
        $this->template = $this->getTemplate('mokilizingas.tpl');
        
        if (version_compare(VERSION, '2', '<')) {
            $this->data = array_merge($this->data, $data);
            $this->render();
        } else {
            return $this->load->view($this->template, $data);
        }
    }
    
    public function checkString($string)
    {
        $bad_chars = array("&");
        $good_chars = array(" ");
        
        return str_replace($bad_chars, $good_chars, $string);
    }
    
    public function confirm() {
        // Get order id;
        if (isset($_POST['order_id'])) {
            $this->orderId = $_POST['order_id'];
        }
        
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
        } else {
            $this->load->language('payment/mokilizingas');
        }
        
        $this->load->model('payment/mokilizingas');
        $this->load->model('catalog/product');
        $this->load->model('account/order');
        
        // Load var;
        //$order_info = $this->model_checkout_order->getOrder($this->orderId);
        $order_info = $this->getOrderInfo();
        
        $order_json = array();
        $order_products = $this->model_account_order->getOrderProducts($this->orderId);
        
        if (isset($this->session->data['shipping_method'])) {
            $shipping_data = $this->session->data['shipping_method'];
        } else {
            $shipping_data = false;
        }
        
        $i = 0;
        foreach ($order_products as $productLine) {
            $order_json[$i]['name'] = $this->checkString($productLine['name']);
            $order_json[$i]['category'] = '';
            $price = $productLine['quantity'] * $productLine['price'];
            $order_json[$i]['price'] = (float)$this->currency->format($price, $order_info['currency_code'], $order_info['currency_value'], false);
            $order_json[$i]['sale'] = false;
            $i++;
        }
        
        if (isset($shipping_data['cost']) && $shipping_data['cost'] > 0) {
            
            $order_json[$i]['name'] = 'Pristatymas';
            $order_json[$i]['category'] = '';
            $order_json[$i]['price'] = (float)$this->currency->format((float)$shipping_data['cost'], $order_info['currency_code'], $order_info['currency_value'], false);
            $order_json[$i]['sale'] = false;
        }
        
        if (version_compare(VERSION, '2.3', '>=')) {
            $return_url   = $this->url->link('extension/payment/mokilizingas/callback', '&callback_type=return');
            $alert_url = $this->url->link('extension/payment/mokilizingas/callback', '&callback_type=response');
        } else {
            $return_url   = $this->url->link('payment/mokilizingas/callback', '&callback_type=return');
            $alert_url = $this->url->link('payment/mokilizingas/callback', '&callback_type=response');
        }
        
        $order_json = json_encode($order_json);
        $args = array(
            'user_name' => $this->config->get('mokilizingas_user'),
            'user_psw' => $this->config->get('mokilizingas_password'),
            'return_url' => $return_url,
            'alert_url' => $alert_url,
            'testing' => ((int)$this->config->get('mokilizingas_mode') === 1) ? '' : 'Y',
            'order_id' => $order_info['order_id'],
            'order_amount' => (float)$this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false),
            'order_currency' => 'EUR',
            'order_info' => $order_info['order_id'],
            'first_name' => $order_info['payment_firstname'],
            'last_name' => $order_info['payment_lastname'],
            'client_phone' => $order_info['telephone'],
            'client_email' => $order_info['email'],
            'client_address' => $order_info['payment_address_1'].', '.$order_info['payment_city'],
            'pers_code' => '',
            'doc_type' => '',
            'doc_no' => '',
            'vendor_email' => $this->config->get('mokilizingas_email'),
            'vendor_phone' => $this->config->get('mokilizingas_phone'),
            'vendor_name' => $order_info['store_name'],
            'vendor_img' => $this->config->get('mokilizingas_logo_url'),
            'order_json' => $order_json,
        );
        
        $data['leasing'] = $this->model_payment_mokilizingas->getLeasingSessionId($args, $order_info['order_id']);
        $data['leasing_url'] = 'https://api.mokilizingas.lt/api2/eshop/post';
        
        if (isset($data['leasing']['sessionId']) && !empty($data['leasing']['sessionId'])) {
            $this->finalizeOrder();
        } else {
            $this->failedOrder();
        }
        
        $this->template = $this->getTemplate('mokilizingas_processing.tpl');
        
        if (!$this->orderId || $this->orderId <= 0) {
            $this->redirectMe($this->url->link('checkout/checkout'));
        } else {
            
            $this->template = $this->getTemplate('mokilizingas_processing.tpl');
            
            if (version_compare(VERSION, '2', "<")) {
                $this->data = array_merge($this->data, $data);
                $this->response->setOutput($this->render(TRUE));
            } else {
                $this->response->setOutput($this->load->view($this->template, $data));
            }
        }
    }
    
    private function redirectMe($url) {
        if (version_compare(VERSION, '2', '<')) {
            $this->redirect($url);
        } else {
            $this->response->redirect($url);
        }
    }
    
    private function getTemplate($name)
    {
        $template = '';
        
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/' . $name)) {
            $template = $this->config->get('config_template') . '/template/payment/' . $name;
        } else {
            if (version_compare(VERSION, '2.2', '>=')) {
                $template = 'payment/' . $name;
            } else {
                $template = 'default/template/payment/' . $name;
            }
        }
        return $template;
    }
    
    private function getOrderInfo()
    {
        if (!isset($this->orderInfo)) {
            if (!isset($this->orderId)) {
                $this->orderId = $this->session->data['order_id'];
            }
            $this->load->model('checkout/order');
            $this->orderInfo = $this->model_checkout_order->getOrder($this->orderId);
        }
        return $this->orderInfo;
    }
    
    public function getValue($key, $default = 0) {
        return (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default));
    }
    
    public function callback() {
        
        $id_session = $this->getValue('session_id');
        $order_id = $this->getValue('order_id');
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);
        
        if ($order_info) {
            $this->load->model('payment/mokilizingas');
            
            $queryData = array(
                'stamp' => microtime(true),
                'id_order' => $order_id,
                'sessionId' => $id_session,
                'errorMessage' => '',
                'errorCode' => '',
                'resultCode' => '',
                'resultInfo' => 'Callback response from leasing 2',
                'resultMsg' => '',
                'advance' => '',
                'currency' => '',
                'contractNo' => '',
            );
            
            $this->model_payment_mokilizingas->addQueryRecord($queryData);
            $base = 'https://secure.mokilizingas.lt/online/api/check';
            $args = array(
                'session_id' => $id_session,
                'order_id' => $order_id,
                'user_name' => $this->config->get('mokilizingas_user'),
                'user_psw' => $this->config->get('mokilizingas_password'),
            );
            
            $leasingResponse = $this->model_payment_mokilizingas->getLeasingResponse($base, $args);
            // Update log array;
            $queryData['resultCode'] = $leasingResponse['result'];
            $queryData['resultInfo'] = $leasingResponse['resultInfo'];
            $queryData['advance'] = $leasingResponse['advance'];
            $queryData['currency'] = $leasingResponse['currency'];
            $queryData['contractNo'] = $leasingResponse['contractNo'];
            
            if ($leasingResponse['result'] == 'SIGNED_DEAL') {
                $queryData['resultMsg'] = 'Signed: '.$leasingResponse['resultMsg'];
    
                if (version_compare(VERSION, '2', '<')) {
                    $this->model_checkout_order->update($order_id, $this->config->get('mokilizingas_accepted_order_status_id'), '', false);
                } else {
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('mokilizingas_accepted_order_status_id'), '', false);
                }
            } elseif ($leasingResponse['result'] == 'WAITING_FOR_PERMISSION') {
                $queryData['resultMsg'] = 'Waiting response: ' . $leasingResponse['resultMsg'];
            } elseif ($leasingResponse['result'] == 'NO_LOAN_POSSIBLE' || $leasingResponse['result'] == 'CLIENT_CANCELED') {
                $queryData['resultMsg'] = 'Cancelled: '.$leasingResponse['resultMsg'];
    
                if (version_compare(VERSION, '2', '<')) {
                    $this->model_checkout_order->update($order_id, $this->config->get('mokilizingas_canceled_order_status_id'), '', false);
                } else {
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('mokilizingas_canceled_order_status_id'), '', false);
                }
            } else {
                $queryData['resultMsg'] = $leasingResponse['resultMsg'];
            }
            
            $this->model_payment_mokilizingas->addQueryRecord($queryData);
            
            $callbackType = $this->getValue('callback_type');
            //if (isset($callbackType) && $callbackType === 'return') {
            $this->response->redirect($this->url->link('checkout/success'));
            //}
        }
    }
    
    private function finalizeOrder() {
        try {
            $this->load->model('checkout/order');
            
            if (version_compare(VERSION, '2.3', '>=')) {
                $this->load->language('extension/payment/mokilizingas');
            } else {
                $this->load->language('payment/mokilizingas');
            }
            
            if (version_compare(VERSION, '2', '<')) {
                $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('mokilizingas_waiting_order_status_id'), '', false);
            } else {
                $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('mokilizingas_waiting_order_status_id'), '', false, false);
            }
        } catch (Exception $e) {
            // echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    
    private function failedOrder($log = null, $alert = null) {
        $this->load->model('checkout/order');
        
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
        } else {
            $this->load->language('payment/mokilizingas');
        }
    
        if (version_compare(VERSION, '2', '<')) {
            $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('mokilizingas_canceled_order_status_id'), '', false);
        } else {
            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('mokilizingas_canceled_order_status_id'), '', false);
        }
        
        if ($alert) {
            $this->session->data['error'] = $alert;
        } else {
            $this->session->data['error'] = $this->language->get('error_process_order');
        }
    }
    
    private function validate() {
        $this->load->model('checkout/order');
        $this->load->model('payment/mokilizingas');
        
        $error = array();
        if (!$this->session->data['order_id']) {
            $error['warning'] = $this->language->get('error_process_order');
        }
        
        if (!$error) {
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
            if (!$order_info) {
                $error['warning'] = $this->language->get('error_process_order');
            }
        }
        
        if (!in_array($order_info['currency_code'], $this->model_payment_mokilizingas->getSupportedCurrencies())) {
            $error['warning'] = $this->language->get('error_invalid_currency');
        }
        
        return $error;
    }
}