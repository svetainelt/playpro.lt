<?php
/******************************************************
 * @package Google Tag Manager for OC1.5x, OC2x,3x
 * @version 9.3
 * @author Muhammad Akram
 * @link https://aits.xyz
 * @copyright Copyright (C)2021 aits.xyz All rights reserved.
 * @email:info@aits.pk. 
 * $date: 17 DEC 2021
*******************************************************/
class ControllerExtensionAnalyticsTagmanager extends Controller {
    public function index() {
    	$customcode = '';
    	$PREFIX = '';
    	if(substr(VERSION,0,1)=='3' ) {
			$PREFIX = 'analytics_';
		}
		$tagmanager = $this->gtm->settings;
		if (isset($tagmanager['customcode']) && !empty($tagmanager['customcode'])) {
			$customcode = html_entity_decode($tagmanager['customcode'], ENT_QUOTES, 'UTF-8');
		}
		
		return $customcode;
	}
	public function sendorder() {
		$json['error'] = true ;
		$json['message'] = 'error';
		
		$tagmanager = $this->gtm->settings;

		if (isset($this->request->get['oid'])) {
			$order_id = $this->request->get['oid'];
		}

		if (isset($this->request->get['v'])) {
			$v= $this->request->get['v'];
		}

		if (!$this->validate($v)) {
			$json['message'] = 'error processing -> key not verified';
		} else {
			if (isset($order_id) && !empty($order_id)) {
				$json = $this->gtm->apiOrderSend($order_id) ;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
	
	public function refund() {
		$json['error'] = true ;
		$json['message'] = 'error';
		$tagmanager = $this->gtm->config();

		if (isset($this->request->get['oid'])) {
			$order_id = $this->request->get['oid'];
		}

		if (isset($this->request->get['v'])) {
			$v= $this->request->get['v'];
		}

		if (!$this->validate($v)) {
			$json['message'] = 'error processing -> key not verified';
		} else { 
			if (isset($this->request->get['order_status_id'])) {
				$order_status_id = $this->request->get['order_status_id'];
	
			} else {
				$json['message'] = 'error processing order status missing';
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput($json);
			}
	
			$status = $tagmanager['return_status'];
			
			if (in_array($order_status_id, $status, true)) {
				$json['error'] = true;
				if (isset($this->request->get['oid'])) {
					$order_id = $this->request->get['oid'];
					$json = $this->model_extension_module_tagmanager->apiOrderRefund($order_id);
				}
			} else {
				$json['message'] = 'error processing couldnt validate order status check return status id in config';
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function hitorder() {
		$json['error'] = true ;
		$json['message'] = 'error';
		$tagmanager = $this->gtm->settings;
		if (isset($this->request->get['v'])) {
			$v= $this->request->get['v'];
		}

		if (!$this->validate($v)) {
			$json['message'] = 'error processing -> key not verified ';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput($json);
		} 

		if ($this->validate($v)) { 
			if (isset($this->request->get['oid'])) {
				$order_id = $this->request->get['oid'];
				$json = $this->gtm->GAupdateorder($order_id);
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function validate($str) {
		$tagmanager = $this->gtm->config();
		if (strtolower($str) != strtolower($tagmanager['vs'])) {
		  $this->model_extension_module_tagmanager->tmerror('Tagmanager API couldnt verify key '. $str.' vs ' . $tagmanager['vs']);
		  return false;
		} else {
		  return true;
		}
	}
}
