<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once DIR_SYSTEM . 'library/vendor/webtopay/libwebtopay/WebToPay.php';

class ControllerPaymentPaysera extends Controller {

    public function index() {
        $data['action'] = $this->url->link('payment/paysera/confirm', '', 'SSL');
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['button_back'] = $this->language->get('button_back');

        $this->language->load('payment/paysera');
        $data['text_chosen'] = $this->language->get('text_chosen');
        $data['text_paycountry'] = $this->language->get('text_paycountry');

        $data['default_country'] = $this->config->get('default_payment_country');
        $data['paysera_display_payments'] = $this->config->get('paysera_display_payments_list');

        // CUSTOM START
        $data['use_filters'] = $this->config->get('paysera_use_filters');
        $payment_countries = $this->config->get('paysera_payment_countries');
        $payment_methods = $this->config->get('paysera_payment_methods');
        $data['allowed_countries'] = empty($payment_countries) ? array() : $payment_countries;
        $data['allowed_payments'] = empty($payment_methods) ? array() : $payment_methods;
        // CUSTOM END
        
        if ($this->request->get['route'] != 'checkout/guest/confirm') {
            $data['back'] = $this->url->link('checkout/payment', '', 'SSL');
        } else {
            $data['back'] = $this->url->link('checkout/guest', '', 'SSL');
        }

//countries
        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $data['country_iso'] = strtolower($order['payment_iso_code_2']);
        $data['countries'] = $this->getPaymentList($order);
        if($data['country_iso']) {
            if(!isset($data['countries'][$data['country_iso']])) {
                $data['country_iso'] = $data['default_country'];
            }
        }
//end countries

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paysera.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/payment/paysera.tpl', $data);
        } else {
            return $this->load->view('default/template/payment/paysera.tpl', $data);
        }
    }

    public function confirm() {
        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $language = strtolower($order['language_code']);
//        $language = $this->config->get('paysera_lang');

        // CUSTOM START
        $payment_methods = $this->config->get('paysera_payment_methods');
        if(!empty($payment_methods)) {
            foreach($payment_methods as &$payment_method) {
                $payment_method = substr($payment_method, 0, -3);
            }
            $payment_methods = array_unique($payment_methods);
        }
        // CUSTOM START
        
        if (!isset($_SERVER['HTTPS'])) {
            $_SERVER['HTTPS'] = false;
        }

        $lng = array('lt' => 'LIT', 'lv' => 'LAV', 'ee' => 'EST', 'en' => 'ENG', 'ru' => 'RUS', 'de' => 'GER', 'pl' => 'POL');
        
        $info = array(
            'projectid'     => $this->config->get('paysera_project'),
            'sign_password' => $this->config->get('paysera_sign'),

            'orderid'       => $order['order_id'],
            'amount'        => intval(number_format($order['total'] * $this->currency->getvalue($order['currency_code']), 2, '', '')), //ceil($order['total'] * $this->currency->getvalue($order['currency_code']) * 100)
            'currency'      => $order['currency_code'],
            'lang'          => $lng[$language], //$language,

            'accepturl'     => $this->url->link('payment/paysera/accept', '', 'SSL'),
            'cancelurl'     => $this->url->link('payment/paysera/cancel', '', 'SSL'),
            'callbackurl'   => $this->url->link('payment/paysera/callback', '', 'SSL'),
            'payment'       => (isset($_REQUEST['payment'])) ? $_REQUEST['payment'] : '',
            
            // CUSTOM START
            'only_payments' => empty($payment_methods) ? '' : implode(',', $payment_methods),
            // CUSTOM END
            
            'country'       => $order['payment_iso_code_2'],

            'logo'          => '',
            'p_firstname'   => $order['payment_firstname'],
            'p_lastname'    => $order['payment_lastname'],
            'p_email'       => $order['email'],
            'p_street'      => $order['payment_address_1'] . ' ' . $order['payment_address_2'],
            'p_city'        => $order['payment_city'],
            'p_state'       => '',
            'p_zip'         => $order['payment_postcode'],
            'p_countrycode' => $order['payment_iso_code_2'],
            'test'          => ($this->config->get('paysera_test') != 0 ? 1 : 0),
        );

        try {
            $request = WebToPay::redirectToPayment($info);
        } catch (WebToPayException $e) {
            exit($e->getMessage());
        }
        
        if($this->config->get('paysera_test') != 0) {
            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($order['order_id'], $this->config->get('paysera_new_order_status_id'));
        }
        
        $data['request'] = $request;
        $data['requestUrl'] = WebToPay::PAY_URL;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paysera_redirect.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/payment/paysera_redirect.tpl', $data);
        } else {
            return $this->load->view('default/template/payment/paysera_redirect.tpl', $data);
        }
    }

    private function getPaymentList($order = array()) {
        if(empty($order)) {
            $this->load->model('checkout/order');
            $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        }

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $amount = ceil($order['total'] * $this->currency->getvalue($order['currency_code']) * 100);
        $amount = $amount < 1 ? 100 : $amount;

        // CUSTOM START
        $language  = strtolower($this->language->get('code'));
        $lng = array('lt'=>'lt', 'lv'=>'lv', 'ee'=>'ee', 'ru'=>'ru', 'pl'=>'pl');
        $language = isset($lng[$language]) ? $lng[$language] : 'en';
        // CUSTOM END
        
        $projectId = $this->config->get('paysera_project');

        $methods = WebToPay::getPaymentMethodList($projectId, $order['currency_code'])
                ->filterForAmount($amount, $order['currency_code'])
                ->setDefaultLanguage($language);

        return $methods->getCountries();
    }

    public function accept() {
        if (isset($this->session->data['token'])) {
            $this->response->redirect($this->url->link('checkout/success', 'token=' . $this->session->data['token'], 'SSL'));
        } else {
            $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
        }
    }

    public function cancel() {
        $this->language->load('payment/paysera');

        $data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_store'));

        if (isset($this->request->server['HTTPS']) and $this->request->server['HTTPS'] == 'on') {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['charset'] = $this->language->get('charset');
        $data['language'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_store'));

        $data['text_response'] = $this->language->get('text_response');
        $data['text_success'] = $this->language->get('text_success');
        $data['text_success_wait'] = sprintf($this->language->get('text_success_wait'), $this->data['base'] . 'index.php?route=checkout/success');
        $data['text_failure'] = $this->language->get('text_failure');
        $data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->data['base'] . 'index.php?route=checkout/cart');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['continue'] = $this->url->link('checkout/cart', '', 'SSL');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paysera_failure.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/paysera_failure.tpl';
        } else {
            $this->template = 'default/template/payment/paysera_failure.tpl';
        }
        
        $this->response->setOutput($this->load->view('default/template/payment/paysera_failure.tpl', $data));
    }

    public function callback() {
        $project_id = $this->config->get('paysera_project');
        $sign_password = $this->config->get('paysera_sign');
        $this->load->model('checkout/order');

        try {
            $response = WebToPay::validateAndParseData($_REQUEST, $project_id, $sign_password);

            if ($response['status'] == 1) {
                $orderId = isset($response['orderid']) ? $response['orderid'] : null;

                $order = $this->model_checkout_order->getOrder($orderId);

                $amount = intval(number_format($order['total'] * $this->currency->getvalue($order['currency_code']), 2, '', ''));

                if (empty($order)) {
                    throw new Exception('Order with this ID not found');
                }

                if ($response['amount'] < $amount) {
                    throw new Exception('Bad amount: ' . $response['amount'] . ', expected: ' . ceil($order['total'] * 100));
                }

                if ($response['currency'] != $order['currency_code']) {
                    throw new Exception('Bad currency: ' . $response['currency'] . ', expected: ' . $order['currency_code']);
                }

                $message = '';

                $this->model_checkout_order->addOrderHistory($orderId, $this->config->get('paysera_order_status_id'));

                exit('OK');
            }
            exit('OK');
        } catch (Exception $e) {
            exit(get_class($e) . ': ' . $e->getMessage());
        }
    }

}