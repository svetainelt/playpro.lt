<?php
class ControllerApiQuantity extends Controller {
    public function get()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $ids = json_decode(file_get_contents('php://input'), true);

            $this->load->model("catalog/product");

            $response = array();
            foreach ($ids as $id){
                $product = $this->model_catalog_product->getProduct($id);
                $response[] = array(
                    'product_id' => $id,
                    'quantity' => $product['quantity'] + $product['reserve']
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($response));
        }
        else {
            $id = $this->request->get['itemId'];

            $this->load->model("catalog/product");

            $product = $this->model_catalog_product->getProduct($id);
            $response = array(
                'product_id' => $id,
                'quantity' => $product['quantity'] + $product['reserve']
            );

            $this->load->model("catalog/product");
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($response));
        }

    }
}
