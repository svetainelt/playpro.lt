<?php
class ControllerModuleMailSender extends Controller {
    public function index() {
        $this->load->model('checkout/order');
        $i = 0;
        $rows = $this->db->query("SELECT * FROM " . DB_PREFIX . "mail_queue LIMIT 30")->rows;
        if ($rows){
            foreach ($rows as $row){
                $order_info = $this->model_checkout_order->getOrder($row['order_id']);
                if ($order_info) {
                    $language = new Language($order_info['language_directory']);
                    $language->load($order_info['language_directory']);
                    $language->load('mail/order');

                    $subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $row['order_id']);

                    $message  = $language->get('text_update_order') . ' ' . $row['order_id'] . "\n";
                    $message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

                    $order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '5' AND language_id = '" . (int)$order_info['language_id'] . "'");

                    if ($order_status_query->num_rows) {
                        $message .= $language->get('text_update_order_status') . "\n\n";
                        $message .= $order_status_query->row['name'] . "\n\n";
                    }

                    if ($order_info['customer_id']) {
                        $message .= $language->get('text_update_link') . "\n";
                        $message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $row['order_id'] . "\n\n";
                    }


                    if ($order_info['order_status_id'] == 5) {

                        $tracking_omniva_info = $this->model_checkout_order->getOmnivaTrackingId($row['order_id']);

                        if ($tracking_omniva_info) {

                            $tracking_numbers = json_decode($tracking_omniva_info, true);
                            $tracking_number_list = '';

                            foreach ($tracking_numbers as $tracking_number)
                            {

                                $tracking_number_list .= $tracking_number;

                                if (next($tracking_numbers )) {
                                    $tracking_number_list .= ', ';
                                }

                            }

                            $date_current = date("Y-m-d");
                            $date_future = date("Y-m-d", time() + 172800);
                            $date_shipping_message = "Jūsų prekė išsiųsta. Sąskaitą rasite šio laiško priede. Numatomas pristatymas į paštomatą $date_current - $date_future www.omniva.lt";
                            $omniva_number = "Siuntos numeris: $tracking_number_list";

                            $message .= $omniva_number . "\n\n";
                            $message .= $date_shipping_message . "\n\n";

                        }

                        else {
                            $venipak_shipping = $this->db->query("SELECT * FROM `" . DB_PREFIX . "venipak_shipping` WHERE order_id = '" . $row['order_id'] . "'")->row;

                            if ($venipak_shipping && !empty(trim($venipak_shipping['tracking']))){
                                $tracking_number = $venipak_shipping['tracking'];
                                $shipping_message = "Jūsų prekė išsiųsta. Sąskaitą rasite šio laiško priede.";
                                $venipak_number = "www.venipak.lt, pakuotės nr.: $tracking_number";

                                $message .= $shipping_message . "\n\n";
                                $message .= $venipak_number . "\n\n";
                            }

                        }

                        $extra_message = "
Sveiki,

Dar kartą dėkojame už Jūsų užsakymą.

SĄSKAITĄ FAKTŪRĄ - GARANTINĮ DOKUMENTĄ RASITE PRISEGTUKE.
Prietaiso garantinio laikotarpio metu būtina išsaugoti pirkimo dokumentą.

Lauksime sugrįžtant!

P.S. Jei iškiltų kokių nors klausimų, visada prašome kreiptis.

Turite nusiskundimų, pastabų, patarimų mums? Parašykite, mums labai
svarbi Jūsų nuomonė.

Patiko mūsų aptarnavimas? Skleiskite apie mus žinią, būsime labai dėkingi!
				";

                        $message .= $extra_message . "\n\n";

                    }

                    $message .= $language->get('text_update_footer');

                    $mail = new Mail();
                    $mail->protocol = $this->config->get('config_mail_protocol');
                    $mail->parameter = $this->config->get('config_mail_parameter');
                    $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                    $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                    $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

                    $mail->setTo($order_info['email']);
                    $mail->setFrom($this->config->get('config_email'));
                    $mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
                    $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                    $mail->setText($message);

                    $this->load->language('mail/invoice');

                    $data['title'] = $this->language->get('text_invoice');

                    if ($this->request->server['HTTPS']) {
                        $data['base'] = HTTPS_SERVER;
                    } else {
                        $data['base'] = HTTP_SERVER;
                    }

                    $data['direction'] = $this->language->get('direction');
                    $data['lang'] = $this->language->get('code');

                    $data['text_invoice'] = $this->language->get('text_invoice');
                    $data['text_order_detail'] = $this->language->get('text_order_detail');
                    $data['text_order_id'] = $this->language->get('text_order_id');
                    $data['text_invoice_no'] = $this->language->get('text_invoice_no');
                    $data['text_invoice_date'] = $this->language->get('text_invoice_date');
                    $data['text_date_added'] = $this->language->get('text_date_added');
                    $data['text_telephone'] = $this->language->get('text_telephone');
                    $data['text_fax'] = $this->language->get('text_fax');
                    $data['text_email'] = $this->language->get('text_email');
                    $data['text_website'] = $this->language->get('text_website');
                    $data['text_payment_address'] = $this->language->get('text_payment_address');
                    $data['text_shipping_address'] = $this->language->get('text_shipping_address');
                    $data['text_payment_method'] = $this->language->get('text_payment_method');
                    $data['text_shipping_method'] = $this->language->get('text_shipping_method');
                    $data['text_comment'] = $this->language->get('text_comment');

                    $data['column_product'] = $this->language->get('column_product');
                    $data['column_model'] = $this->language->get('column_model');
                    $data['column_quantity'] = $this->language->get('column_quantity');
                    $data['column_price'] = $this->language->get('column_price');
                    $data['column_total'] = $this->language->get('column_total');

                    $data['text_guarantee'] = $this->language->get('text_guarantee');
                    $data['text_guarantee_client'] = $this->language->get('text_guarantee_client');
                    $data['text_guarantee_company'] = $this->language->get('text_guarantee_company');

                    $data['orders'] = array();

                    //$order_info = $this->model_checkout_order->getOrder($row['order_id']);

                    if ($order_info['customer_group_id'] == 2) // 2 group is company  1 - is client
                    {

                        $data['guarantee_data'] = $data['text_guarantee_company'];

                    }
                    else
                    {

                        $data['guarantee_data'] = $data['text_guarantee_client'];

                    }

                    if ($order_info) {
                        $store_info = $this->model_checkout_order->getSetting('config', $order_info['store_id']);

                        if ($store_info) {
                            $store_address = $store_info['config_address'];
                            $store_email = $store_info['config_email'];
                            $store_telephone = $store_info['config_telephone'];
                            $store_fax = $store_info['config_fax'];
                        } else {
                            $store_address = $this->config->get('config_address');
                            $store_email = $this->config->get('config_email');
                            $store_telephone = $this->config->get('config_telephone');
                            $store_fax = $this->config->get('config_fax');
                        }

                        // Custom Fields
                        $this->load->model('customer/custom_field');

                        $data['account_custom_fields'] = array();

                        $filter_data = array(
                            'sort'  => 'cf.sort_order',
                            'order' => 'ASC',
                        );

                        $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

                        foreach ($custom_fields as $custom_field) {
                            if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
                                if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                                    $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

                                    if ($custom_field_value_info) {
                                        $data['account_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $custom_field_value_info['name']
                                        );
                                    }
                                }

                                if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
                                    foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                                        if ($custom_field_value_info) {
                                            $data['account_custom_fields'][] = array(
                                                'name'  => $custom_field['name'],
                                                'value' => $custom_field_value_info['name']
                                            );
                                        }
                                    }
                                }

                                if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                                    $data['account_custom_fields'][] = array(
                                        'name'  => $custom_field['name'],
                                        'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
                                    );
                                }

                                if ($custom_field['type'] == 'file') {
                                    $upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

                                    if ($upload_info) {
                                        $data['account_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $upload_info['name']
                                        );
                                    }
                                }
                            }
                        }

                        // Custom fields
                        $data['payment_custom_fields'] = array();

                        foreach ($custom_fields as $custom_field) {
                            if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                                if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                                    $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                                    if ($custom_field_value_info) {
                                        $data['payment_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $custom_field_value_info['name'],
                                            'sort_order' => $custom_field['sort_order']
                                        );
                                    }
                                }

                                if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                                    foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                                        if ($custom_field_value_info) {
                                            $data['payment_custom_fields'][] = array(
                                                'name'  => $custom_field['name'],
                                                'value' => $custom_field_value_info['name'],
                                                'sort_order' => $custom_field['sort_order']
                                            );
                                        }
                                    }
                                }

                                if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                                    $data['payment_custom_fields'][] = array(
                                        'name'  => $custom_field['name'],
                                        'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
                                        'sort_order' => $custom_field['sort_order']
                                    );
                                }

                                if ($custom_field['type'] == 'file') {
                                    $upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                                    if ($upload_info) {
                                        $data['payment_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $upload_info['name'],
                                            'sort_order' => $custom_field['sort_order']
                                        );
                                    }
                                }
                            }
                        }

                        // Shipping
                        $data['shipping_custom_fields'] = array();

                        foreach ($custom_fields as $custom_field) {
                            if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                                if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                                    $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                                    if ($custom_field_value_info) {
                                        $data['shipping_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $custom_field_value_info['name'],
                                            'sort_order' => $custom_field['sort_order']
                                        );
                                    }
                                }

                                if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                                    foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                                        if ($custom_field_value_info) {
                                            $data['shipping_custom_fields'][] = array(
                                                'name'  => $custom_field['name'],
                                                'value' => $custom_field_value_info['name'],
                                                'sort_order' => $custom_field['sort_order']
                                            );
                                        }
                                    }
                                }

                                if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                                    $data['shipping_custom_fields'][] = array(
                                        'name'  => $custom_field['name'],
                                        'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
                                        'sort_order' => $custom_field['sort_order']
                                    );
                                }

                                if ($custom_field['type'] == 'file') {
                                    $upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                                    if ($upload_info) {
                                        $data['shipping_custom_fields'][] = array(
                                            'name'  => $custom_field['name'],
                                            'value' => $upload_info['name'],
                                            'sort_order' => $custom_field['sort_order']
                                        );
                                    }
                                }
                            }
                        }

                        if (isset($data['payment_custom_fields'][0]['value']) && $data['payment_custom_fields'][0]['value'] == 'Įmonės kodas')
                        {

                            $data['payment_custom_fields'][0]['value'] = '';

                        }

                        if (isset($data['payment_custom_fields'][1]['value']) && $data['payment_custom_fields'][1]['value'] == 'PVM mokėtojo kodas')
                        {

                            $data['payment_custom_fields'][1]['value'] = '';

                        }

                        if (isset($data['shipping_custom_fields'][0]['value']) && $data['shipping_custom_fields'][0]['value'] == 'Įmonės kodas')
                        {

                            $data['shipping_custom_fields'][0]['value'] = '';

                        }

                        if (isset($data['shipping_custom_fields'][1]['value']) && $data['shipping_custom_fields'][1]['value'] == 'PVM mokėtojo kodas')
                        {

                            $data['shipping_custom_fields'][1]['value'] = '';

                        }




                        if ($order_info['invoice_no']) {
                            $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                        } else {
                            $invoice_no = '';
                        }

                        if ($order_info['payment_address_format']) {
                            $format = $order_info['payment_address_format'];
                        } else {
                            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{company_code}' . "\n" . '{pvm_code}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                        }

                        $find = array(
                            '{firstname}',
                            '{lastname}',
                            '{company}',
                            '{company_code}',
                            '{pvm_code}',
                            '{address_1}',
                            '{address_2}',
                            '{city}',
                            '{postcode}',
                            '{zone}',
                            '{zone_code}',
                            '{country}'
                        );

                        $replace = array(
                            'firstname'     => $order_info['payment_firstname'],
                            'lastname'      => $order_info['payment_lastname'],
                            'company'       => $order_info['payment_company'],
                            'company_code'  => isset($data['payment_custom_fields'][0]) ? $data['payment_custom_fields'][0]['value'] : '',
                            'pvm_code'      => isset($data['payment_custom_fields'][1]) ? $data['payment_custom_fields'][1]['value'] : '',
                            'address_1'     => $order_info['payment_address_1'],
                            'address_2'     => $order_info['payment_address_2'],
                            'city'          => $order_info['payment_city'],
                            'postcode'      => $order_info['payment_postcode'],
                            'zone'          => $order_info['payment_zone'],
                            'zone_code'     => $order_info['payment_zone_code'],
                            'country'       => $order_info['payment_country']
                        );

                        $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                        if ($order_info['shipping_address_format']) {
                            $format = $order_info['shipping_address_format'];
                        } else {
                            $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{company_code}' . "\n" . '{pvm_code}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                        }

                        $find = array(
                            '{firstname}',
                            '{lastname}',
                            '{company}',
                            '{company_code}',
                            '{pvm_code}',
                            '{address_1}',
                            '{address_2}',
                            '{city}',
                            '{postcode}',
                            '{zone}',
                            '{zone_code}',
                            '{country}'
                        );

                        $replace = array(
                            'firstname'     => $order_info['shipping_firstname'],
                            'lastname'      => $order_info['shipping_lastname'],
                            'company'       => $order_info['shipping_company'],
                            'company_code'  => isset($data['payment_custom_fields'][0]) ? $data['payment_custom_fields'][0]['value'] : '',
                            'pvm_code'      => isset($data['payment_custom_fields'][1]) ? $data['payment_custom_fields'][1]['value'] : '',
                            'address_1'     => $order_info['shipping_address_1'],
                            'address_2'     => $order_info['shipping_address_2'],
                            'city'          => $order_info['shipping_city'],
                            'postcode'      => $order_info['shipping_postcode'],
                            'zone'          => $order_info['shipping_zone'],
                            'zone_code'     => $order_info['shipping_zone_code'],
                            'country'       => $order_info['shipping_country']
                        );

                        $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                        $this->load->model('tool/upload');

                        $product_data = array();

                        $products = $this->model_checkout_order->getOrderProducts($row['order_id']);

                        foreach ($products as $product) {
                            $option_data = array();

                            $options = $this->model_checkout_order->getOrderOptions($row['order_id'], $product['order_product_id']);

                            foreach ($options as $option) {
                                if ($option['type'] != 'file') {
                                    $value = $option['value'];
                                } else {
                                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                    if ($upload_info) {
                                        $value = $upload_info['name'];
                                    } else {
                                        $value = '';
                                    }
                                }

                                $option_data[] = array(
                                    'name'  => $option['name'],
                                    'value' => $value
                                );
                            }

                            $product_data[] = array(
                                'name'     => $product['name'],
                                'model'    => $product['model'],
                                'option'   => $option_data,
                                'quantity' => $product['quantity'],
                                'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                                'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                            );
                        }

                        $voucher_data = array();

                        $vouchers = $this->model_checkout_order->getOrderVouchers($row['order_id']);

                        foreach ($vouchers as $voucher) {
                            $voucher_data[] = array(
                                'description' => $voucher['description'],
                                'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                            );
                        }

                        $total_data = array();

                        $totals = $this->model_checkout_order->getOrderTotals($row['order_id']);

                        foreach ($totals as $total) {
                            $total_data[] = array(
                                'title' => $total['title'],
                                'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                            );
                        }

                        $data['orders'][] = array(
                            'text_invoice' => $order_info['invoice_no'] ? $this->language->get('text_invoice') : $this->language->get('text_prepay'),
                            'order_id'	         => $row['order_id'],
                            'invoice_no'         => $invoice_no,
                            'date_added'         => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                            'store_name'         => $order_info['store_name'],
                            'store_url'          => rtrim($order_info['store_url'], '/'),
                            'store_address'      => nl2br($store_address),
                            'store_email'        => $store_email,
                            'store_telephone'    => $store_telephone,
                            'store_fax'          => $store_fax,
                            'email'              => $order_info['email'],
                            'telephone'          => $order_info['telephone'],
                            'shipping_address'   => $shipping_address,
                            'shipping_method'    => $order_info['shipping_method'],
                            'payment_address'    => $payment_address,
                            'payment_method'     => $order_info['payment_method'],
                            'product'            => $product_data,
                            'voucher'            => $voucher_data,
                            'total'              => $total_data,
                            'comment'            => nl2br($order_info['comment'])
                        );
                    }


                    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order_invoice.tpl')) {
                        $html = $this->load->view($this->config->get('config_template') . '/template/mail/order_invoice.tpl', $data);
                    } else {
                        $html = $this->load->view('default/template/mail/order_invoice.tpl', $data);
                    }

                    if ($order_info['order_status_id'] == 5) {

                        $html = preg_replace('/>\s+</', "><", $html);

                        require_once("pdf/dompdf_config.inc.php");
                        $pdf = $html;

                        if (get_magic_quotes_gpc())
                            $pdf = stripslashes($html);

                        $dompdf = new DOMPDF();
                        $dompdf->load_html($pdf);
                        $dompdf->set_paper('letter', 'portrait');
                        $dompdf->render();
                        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
                        $mail->addAttachment('attachment.pdf');
                    }

                    //$mail->setHtml($html);
                    $mail->send();
                }
                $this->db->query("DELETE FROM " . DB_PREFIX . "mail_queue WHERE mail_queue_id = '" . $row['mail_queue_id'] . "'");
                $i = $i + 1;
            }
        }
        echo "Number of mails sent: " . $i;
        die;
    }
}
