<?php
class ControllerModuleNotifyWhenAvailable extends Controller {
	
	public function index() {
	
	}

	public function popup(){
		$this->load->language('module/notify_when_available');
            $data['notify_heading'] = $this->language->get('notify_heading');
            $data['entry_name']     = $this->language->get('entry_name');
            $data['entry_email']    = $this->language->get('entry_email');
            $data['entry_message']      = $this->language->get('entry_message');
            $data['button_notify_send']  = $this->language->get('button_notify_send');
            $data['text_loading']        = $this->language->get('text_loading');

            $data['product_id']        = $this->request->get['product_id'];

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status')) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'), $this->error);
            } else {
                $data['captcha'] = '';
            }

            if ($this->customer->isLogged()) {
                $data['name']           = $this->customer->getFirstName().''.$this->customer->getLastName();
                $data['email']          = $this->customer->getEmail();
            }else{
                $data['name']           = '';
                $data['email']          = '';
            }
		$this->response->setOutput( $this->load->view('default/template/module/notify/popup.tpl', $data));
	}

	public function notify_when_available(){
	    if (!$this->customer->isLogged()) {
	      $this->session->data['redirect'] = $this->url->link('account/account/notify_when_available', '', true);
	      $this->response->redirect($this->url->link('account/login', '', true));
	    }

	    $this->load->language('module/notify_when_available');
	    $this->document->setTitle($this->language->get('text_notify_when_available'));
	    $data['heading_title']              = $this->language->get('text_notify_when_available');
	    $data['column_date_added']         	= $this->language->get('column_date_added');
	    $data['text_expire']                = $this->language->get('text_expire');
	    $data['breadcrumbs']                = array();

	    $data['column_message']         	= $this->language->get('column_message');
	    $data['column_product']         	= $this->language->get('column_product');
	    $data['column_action'] 				= $this->language->get('column_action');


	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('text_home'),
	      'href' => $this->url->link('common/home')
	    );

	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('text_account'),
	      'href' => $this->url->link('account/account', '', true)
	    );

	    $data['breadcrumbs'][] = array(
	      'text' => $this->language->get('text_notify_when_available'),
	      'href' => $this->url->link('account/account/notify_when_available', '', true)
	    );

	    $this->load->model('module/notify_when_available');
	    
	    $data['button_continue'] = $this->language->get('button_continue');
	    $data['button_remove'] = $this->language->get('button_remove');
	    $data['text_empty'] = $this->language->get('text_empty');
	     

	    if (isset($this->request->get['page'])) {
	      $page = $this->request->get['page'];
	    } else {
	      $page = 1;
	    }

	    $data['notify_products'] = array();

	    $filter_data = array(
	      'sort'  => 'date_added',
	      'order' => 'DESC',
	      'start' => ($page - 1) * 10,
	      'limit' => 10
	    );

	    $this->load->model('catalog/product');
	    $notify_total = $this->model_module_notify_when_available->getTotalNotifyProducts();

	    $results = $this->model_module_notify_when_available->getNotifyProducts($filter_data);

	    foreach ($results as $result) {
	    	
	    	$results = $this->model_catalog_product->getProduct($result['product_id']);

	        $data['notify_products'][] = array(
	        	'product_id'		=> $result['product_id'],
	            'name' 				=> $result['name'],
	            'email'       		=> $result['email'],
	            'product_name'     	=> $results['name'],
	            'product_href'     	=> $this->url->link('product/product','product_id='.$result['product_id']),
	            'message'         	=> $result['message'],
	            'date_added'        => date($this->language->get('date_format_short'), strtotime($result['date_added']))
	        );
	    }

	    $pagination = new Pagination();
	    $pagination->total = $notify_total;
	    $pagination->page = $page;
	    $pagination->limit = 10;
	    $pagination->url = $this->url->link('account/account/notify_when_available', 'page={page}', true);

	    $data['pagination'] = $pagination->render();

	    $data['results'] = sprintf($this->language->get('text_pagination'), ($notify_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($notify_total - 10)) ? $notify_total : ((($page - 1) * 10) + 10), $notify_total, ceil($notify_total / 10));

	    $data['continue'] = $this->url->link('account/account', '', true);

	    $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('default/template/module/notify/notify_when_available.tpl', $data));
	  }

	public function add_notify_request(){

		$this->load->language('module/notify_when_available');
		$this->load->model('module/notify_when_available');

		$json = array();

		// Captcha
//		if ($this->config->get($this->config->get('config_captcha') . '_status')) {
//			$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');
//
//			if ($captcha) {
//				$json['error'] = $captcha;
//			}
//		}

		if ((utf8_strlen(trim($this->request->post['name'])) < 1) || (utf8_strlen(trim($this->request->post['name'])) > 32)) {
			$json['error'] = $this->language->get('error_name');
		}

		if (utf8_strlen(trim($this->request->post['product_id'])) < 1) {
			$json['error'] = $this->language->get('error_warning');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$json['error'] = $this->language->get('error_email');
		}

		


		if(!$json){
			$data = [
				'name' 			=> $this->request->post['name'],
				'email' 		=> $this->request->post['email'],
				'message' 		=> $this->request->post['message'],
				'product_id' 	=> $this->request->post['product_id'],
			];
			$customer_id = 0;
			if ($this->customer->isLogged()) {
				$customer_id = $this->customer->getId();
			}
			$data = array_merge($data,['customer_id' => $customer_id]);

			$this->model_module_notify_when_available->add_notify_request($data);
			$json['success'] = $this->language->get('text_success');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function remove_notify_request(){
		$this->load->model('module/notify_when_available');
		$this->model_module_notify_when_available->remove_notify_request($this->request->post['product_id']);
		$json['success'] = $this->language->get('text_success');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

}