<?php
class ControllerModuleGiftTeaser extends Controller  {
	private $moduleName;
	private $moduleTotal;
	private $modulePath;
	private $moduleModel;
	private $moduleVersion;
	private $extensionsLink;
	private $callModel;
	private $error = array();
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);

		// Config Loader
		$this->config->load('isenselabs/giftteaser');

		// Module Constants
		$this->moduleName           = $this->config->get('giftteaser_name');
		$this->moduleTotal          = $this->config->get('giftteaser_total');

		$this->callModel            = $this->config->get('giftteaser_model');
		$this->modulePath           = $this->config->get('giftteaser_path');
		$this->moduleVersion        = $this->config->get('giftteaser_version');
		$this->moduleData_module    = $this->config->get('giftteaser_module_data');

		// Load Language
		$this->load->language($this->modulePath);
		$this->load->language('common/cart');

		// Load Model
		$this->load->model($this->modulePath);

		// Model Instance
		$this->moduleModel          = $this->{$this->callModel};

		// Global Variables
		$this->data['moduleName']        = $this->moduleName;
		$this->data['modulePath']        = $this->modulePath;
		$this->data['moduleData_module'] = $this->moduleData_module;
		$this->data['moduleModel']       = $this->moduleModel;
	}

	public function index() {
		$setting = $this->moduleModel->getGiftsSettings();

		if (!empty($setting)) {
			$setting = $setting[$this->moduleName];

			if ($setting['Enabled'] && $setting['Enabled'] == 'yes' ) {
				if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/giftteaser.css')) {
					$this->document->addStyle('catalog/view/theme/'.$this->config->get('config_template').'/stylesheet/giftteaser.css');
				} else {
					$this->document->addStyle('catalog/view/theme/default/stylesheet/giftteaser.css');
				}

				$gifts = $this->moduleModel->getCurrentGifts();

				foreach ($gifts as &$gift) {
					if (empty($gift['description'])) {
						$gift['description'] = $this->language->get('no_information');
					}
				}

				$data['gifts'] = $gifts;
				$data['language'] = $this->config->get('config_language');
				$data['current_template'] = $this->config->get('config_template');

				$data['cart_total'] = $this->cart->getTotal();
				$data['data'] = $setting;

				return $this->load->view($this->template('giftteaser'), $data);
			}
		}
	}

	// Notice that product is a gift, fetched with ajax
	public function showGiftBox() {
		$json = array();
		$setting = $this->moduleModel->getGiftsSettings();

		if (!empty($setting)) {
			$setting = $setting[$this->moduleName];

			if ($setting['Enabled'] && $setting['Enabled'] == 'yes' && isset($this->request->post['product_id']) ) {
				if (isset($setting['showFreeGift']) && $setting['showFreeGift'] == 'yes') {
					$json['GiftNote'] = html_entity_decode($setting['notification_freeGift_'.$this->config->get('config_language')]);
					$json['NoteFontColor'] = $setting['NoteFontColor'];
					$json['NoteBackgroundColor'] = $setting['NoteBackgroundColor'];
					$json['NoteBorderColor'] = $setting['NoteBorderColor'];

					$active_gift = $this->moduleModel->checkGift($this->request->post['product_id']);

					if (!empty($active_gift)) {
						$json['ActiveGift'] = true;

						$json['GiftNoteDescription'] = $this->moduleModel->buildLocalizedDescription($active_gift);
					}

					$json['custom_css'] = $setting['customCss'];
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// returns gifts that still require options + whether a new one has been added
	public function optionGifts() {
		$jsonResp = array(
			'receivedGift' => isset($this->session->data['giftteaser_notify']['status']) ? $this->session->data['giftteaser_notify']['status'] : false,
			'notificationText' => $this->language->get('gift_added_to_cart'),
			'withOptions' => array()
		);

		if ($jsonResp['receivedGift']) {
			$giftsNeedingOptions = $this->moduleModel->getGiftsThatStillRequireOptions();

			foreach ($giftsNeedingOptions as $gift) {
				$jsonResp['withOptions'][] = $gift['gift_id'];
			}
		}

		$this->response->addHeader('Content-Type: application/json');	
		$this->response->setOutput(json_encode($jsonResp));
	}

	// renders popup form for product options
	public function giftTeaserOptionsDialog() {
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$gifts = $this->moduleModel->getGiftsThatStillRequireOptions();
		$data = array();
		$data['renders'] = array();
		foreach ($gifts as $gift) {
			$data['renders'][$gift['gift_id']] = $this->singleGiftOptions($gift['gift_id'], $gift['product_id']);
		}

		$this->response->setOutput($this->load->view($this->template('giftteaser_options'), $data));

	}

	protected function singleGiftOptions($giftId, $productId) {
		$product_info= $this->model_catalog_product->getProduct($productId);
		$product_options = $this->model_catalog_product->getProductOptions($productId);

		if ($product_info['image']) {
			$image = $this->model_tool_image->resize($product_info['image'], 80, 80);
		} else {
			$image = false;
		}

		if ($this->config->get('config_review_status')) {
			$rating = (int)$product_info['rating'];
		} else {
			$rating = false;
		}

		$product_options = $this->model_catalog_product->getProductOptions($product_info['product_id']);
		$data = array();
		$data['options'] = array();

		foreach ($product_options as $option) {
			if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
				$option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax')),$this->session->data['currency']);
						} else {
							$price = false;
						}

						$option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
						);
					}
				}

				$data['options'][] = array(
					'product_option_id' => $option['product_option_id'],
					'option_id'         => $option['option_id'],
					'name'              => $option['name'],
					'type'              => $option['type'],
					'option_value'      => $option_value_data,
					'required'          => $option['required']
				);
			} elseif ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
				$data['options'][] = array(
					'product_option_id' => $option['product_option_id'],
					'option_id'         => $option['option_id'],
					'name'              => $option['name'],
					'type'              => $option['type'],
					'option_value'      => '',
					'required'          => $option['required']
				);
			}
		}
		$data['gift'] = array(
			'gift_id'    => $giftId,
			'product_id' => $product_info['product_id'],
			'thumb'      => $image,
			'name'       => $product_info['name'],
			'rating'     => $rating,
			'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
			'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
			'options' => $data['options']
		);

		$data['text_select'] = $this->language->get('text_select');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_heading'] =  $this->language->get('gift_added_to_cart');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['Continue'] = $this->language->get('Continue');

		$this->response->setOutput($this->load->view($this->template('giftteaser_option_pane'), $data));
	}

	public function updateGiftOptions() {
		$this->load->model('catalog/product');
		$this->load->language('checkout/cart');
		$giftId = $this->request->post['gift_id'];
		$gift = $this->moduleModel->getGift($giftId);
		$json = array();

		if (isset($this->request->post['option'])) {
			$option = array_filter($this->request->post['option']);
		} else {
			$option = array();
		}

		$product_options = $this->model_catalog_product->getProductOptions($gift['item_id']);

		foreach ($product_options as $product_option) {
			if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
				$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
			}
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		$recurrings = $this->model_catalog_product->getProfiles($gift['item_id']);

		if ($recurrings) {
			$recurring_ids = array();

			foreach ($recurrings as $recurring) {
				$recurring_ids[] = $recurring['recurring_id'];
			}

			if (!in_array($recurring_id, $recurring_ids)) {
				$json['error']['recurring'] = $this->language->get('error_recurring_required');
			}
		}

		if (!isset($json['error'])) {
			$this->moduleModel->updateCartGiftOptions($giftId, $option);
			$json['success'] = true;
		}

		$this->response->setOutput(json_encode($json));
	}

	private function template($tplFile = 'module')
    {
		$template = $this->modulePath . '/' . $tplFile . '.tpl';
    	
    	if (version_compare(VERSION, '2.2.0.0', '<')) {
	        $template = 'default/template/' . $this->modulePath . '/' . $tplFile . '.tpl';
	        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->modulePath . '/' . $tplFile . '.tpl')) {
	            $template = $this->config->get('config_template') . '/template/' . $this->modulePath . '/' . $tplFile . '.tpl';
	        }
    	}

        return $template;
    }
}
