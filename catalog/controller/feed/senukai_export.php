<?php
class ControllerFeedSenukaiExport extends Controller {
    public function index() {

        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<items>';
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $products = $this->model_catalog_product->getProductsPigu();


        $domain =  $_SERVER['HTTP_HOST'];
        $full_domain = "http://$domain/image/";


        foreach ($products as $product) {
            if(empty($product['image'])){
                continue;
            }
            if($product['image'] == 'no_image.jpg'){
                continue;
            }

            $categories = $this->model_catalog_product->getCategories($product['product_id']);

            /*if ($categories[0]['category_id'] == 68 || $categories[0]['category_id'] == 640){
                continue;
            }*/
            $custom_options_color = [];
            $custom_options = [];
            foreach ($this->model_catalog_product->getProductOptions($product['product_id'], true) as $option) {
                if ($option['name'] == "Dydis" || $option['name'] == "Spalva") {
                    if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
                        foreach ($option['option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if($option['name'] == "Dydis"){
                                    $custom_options[] = array(
                                        'name' => $option_value['name']
                                    );
                                }
                                if($option['name'] == "Spalva"){
                                    $custom_options_color[] = array(
                                        'name' => $option_value['name']
                                    );
                                }
                            }
                        }

                    } elseif ($option['type'] == 'text' || $option['type'] == 'hide' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
                        foreach ($option['option_value'] as $option_name) {
                            if($option['name'] == "Dydis") {
                                $custom_options[] =  array(
                                    'name' => $option_name['name']

                                );
                            }
                            if($option['name'] == "Spalva") {
                                $custom_options_color[] = array(
                                    'name' => $option_name['name']
                                );
                            }
                        }
                    }
                }

            }

            $c_s = '<![' . 'CDATA[';
            $c_e = ']' . ']>';
            $manufacturer = "manufacturer";
            $empty = "";
            $category_indicator = " > ";
            $output .= '<item>';

            if (!empty($categories) && isset($categories[0]['category_id'])) {
                $category_infos = $this->model_catalog_category->getCategoryPath($categories[0]['category_id']);
                // Adding this thing because they asked for it
                // Unfortunately there's literally no standardized method to check for
                // what category it should fall under, so I'll have to hope this sticks.
                // First, check all tags for one of the keywords
                $tags = explode(',', $product['tag']);
                if ($category_infos['name'] == 'AUDIO') {
                    foreach ($tags as $tag) {
                        if (trim(strtolower($tag)) == 'ausinės') {
                            $category_infos['name'] = 'AUSINĖS';
                            $categories[0]['category_id'] = 2001;
                            break;
                        }
                        if (trim(strtolower($tag)) == 'mikrofonas') {
                            $category_infos['name'] = 'MIKROFONAI';
                            $categories[0]['category_id'] = 2002;
                            break;
                        }
                    }
                }
                // If it didn't find anything, try to use the name
                if ($category_infos['name'] == 'AUDIO'){
                    if (strpos(strtolower($product['name']), 'ausinės') !== false) {
                        $category_infos['name'] = 'AUSINĖS';
                        $categories[0]['category_id'] = 2001;
                    }
                    if (strpos(strtolower($product['name']), 'mikrofonas') !== false) {
                        $category_infos['name'] = 'MIKROFONAI';
                        $categories[0]['category_id'] = 2002;
                    }
                }
//                $output .= '<category-id>' . $c_s . $categories[0]['category_id'] . $c_e . '</category-id>';
                if (isset($category_infos['path']) && $category_infos['path'] != '') {
                    $output .= '<category>' . $c_s . htmlspecialchars_decode($category_infos['path']) . $category_indicator . htmlspecialchars_decode($category_infos['name']) . $c_e . '</category>';
                } else {
                    if (isset($category_infos['name'])) {
                        $output .= '<category>' . $c_s . htmlspecialchars_decode($category_infos['name']) . $c_e . '</category>';
                    }
                }
            } else {
                $output .= '<category>' . $c_s . $empty . $c_e . '</category>';
            }
            unset($categories);
            $output .= '<name>' . $c_s . $product['name'] . $c_e . '</name>';
            $output .= '<id>' . $c_s . $product['product_id'] . $c_e . '</id>';
            $output .= '<origin>' . $c_s . 'LT'. $c_e . '</origin>';
            $output .= '<instruction>' . $c_s . 'No'. $c_e . '</instruction>';
            $output .= '<warranty>' . $c_s . '24'. $c_e . '</warranty>';
            $output .= '<cold_resistant>' . $c_s . 'Yes'. $c_e . '</cold_resistant>';
            $output .= '<expiration>' . $c_s . 'No'. $c_e . '</expiration>';
            if($product['stock_status_id'] == 10) {
                $output .= '<shipment_time>' . $c_s . '0-2 d.d.' . $c_e . '</shipment_time>';
            } else {
                $output .= '<shipment_time>' . $c_s . '1-3 d.d.' . $c_e . '</shipment_time>';
            }

            if(empty($product['description'])){
                $output .= '<description>' . $c_s . strip_tags(htmlspecialchars_decode($product['name'])) . $c_e . '</description>';
            }else{
                $output .= '<description>' . $c_s . strip_tags(htmlspecialchars_decode($product['description'])) . $c_e . '</description>';
            }
            if(!empty($product['manufacturer'])) {
                $output .= '<brand>'  . $c_s . $product['manufacturer']  . $c_e . '</brand>';
            }
            $output .= '<manufacturer>' . $c_s . $product['manufacturer'] . $c_e . '</manufacturer>';
            if(!empty($product['special'])) {
                $output .= '<price>' . $c_s . $product['special'] . $c_e . '</price>';
            } else {
                $output .= '<price>' . $c_s . $product['price'] . $c_e . '</price>';
            }
            if(!empty($product['quantity'])) {
                $output .= '<quantity>' . $c_s . $product['quantity'] . $c_e . '</quantity>';
            }
            if(!empty($product['price'])) {
                $output .= '<code>'  . $c_s . $product['model'] . $c_e . '</code>';
            }
            if(!empty($product['ean'])) {
                $output .= '<ean>' . $c_s . $product['ean'] . $c_e . '</ean>';
            }
            $output .= '<attributes>';

            if( $product['length_class_id'] == 1) {
                $product['length'] = $product['length'] / 100;
                $product['width'] = $product['width'] / 100;
                $product['height'] = $product['height'] / 100;
            }
            if( $product['length_class_id'] == 2) {
                $product['length'] = $product['length'] / 1000;
                $product['width'] = $product['width'] / 1000;
                $product['height'] = $product['height'] / 1000;
            }
            if( $product['length_class_id'] == 3) {
                $product['length'] = round($product['length'] / 39.370, 2);
                $product['width'] = round($product['width'] / 39.370, 2);
                $product['height'] = round($product['height'] / 39.370, 2);
            }
            if( $product['weight_class_id'] == 1) {
                $product['weight'] = round($product['weight'] * 1000);
            }
            if( $product['weight_class_id'] == 6) {
                $product['weight'] = round($product['weight'] * 28.3495);
            }
            if( $product['weight_class_id'] == 5) {
                $product['weight'] = round($product['weight'] * 453.592, 2);
            }
            $output .= '<length>'  . $c_s . $product['length']. $c_e . '</length>';
            $output .= '<width>'  . $c_s . $product['width'] . $c_e . '</width>';
            $output .= '<height>'  . $c_s . $product['height'] . $c_e . '</height>';
            if(!empty($product['length']) && !empty($product['width']) && !empty($product['height'])) {
                $output .= '<volume>'  . $c_s . $product['height'] * $product['length'] * $product['width'] . $c_e . '</volume>';
            }
            $output .= '<weight_netto>' . $c_s . $product['weight'] . $c_e . '</weight_netto>';
            $output .= '<weight_brutto>' . $c_s . $product['weight'] . $c_e . '</weight_brutto>';

            $output .= '</attributes>';

            $output .= '<images>';

            $product_images = $this->model_catalog_product->getProductImages($product['product_id']);


            if ($product_images) {
                foreach ($product_images as $product_image) {
                    $output .= '<image>';
                    $output .= '<url>' . $c_s . $full_domain . $product_image['image'] . $c_e . '</url>';
                    $output .= '</image>';
                }
            } else {
                $output .= '<image>';
                $output .= '<url>' . $c_s . $full_domain . $product['image'] . $c_e . '</url>';
                $output .= '</image>';
            }
            $output .= '</images>';

            $output .= '</item>';

        }
        $output .= '</items>';
        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);

    }
    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);
        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }
            $path = $this->getPath($category_info['parent_id'], $new_path);
            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }
}
