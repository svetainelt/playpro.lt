<?php
class ControllerFeedOCPC extends Controller {
    public function index() {



        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<products>';
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

      /* konkreciai pagal name
      $manufacturer_names = "'EMU','Karactermania'";
        $manufacturers = $this->model_catalog_product->getManufacturer($manufacturer_names);
        $manufacturer_ids='(';
        $first = 0;
        foreach ($manufacturers as $manufacturer){
            if($first == 0){
                $manufacturer_ids .= $manufacturer['manufacturer_id'];
                $first++;
            }else{
                $manufacturer_ids .= ','.$manufacturer['manufacturer_id'];
            }
        }
        $manufacturer_ids.=')';
        $data_for_getProducts['manufacturer_ids'] = $manufacturer_ids;*/

        $products = $this->model_catalog_product->getProductsPigu();


        $domain =  $_SERVER['HTTP_HOST'];
        $full_domain = "http://$domain/image/";


        foreach ($products as $product) {
            if(empty($product['image'])){
                continue;
            }
            if($product['image'] == 'no_image.jpg'){
                continue;
            }

            $categories = $this->model_catalog_product->getCategories($product['product_id']);

            if ($categories[0]['category_id'] == 68 || $categories[0]['category_id'] == 640){
                continue;
            }

            $custom_options_color = [];

            $categories = $this->model_catalog_product->getCategories($product['product_id']);
            $skip = false;
            foreach($categories as $category_product) {
                $category = $this->model_catalog_category->getCategory($category_product['category_id']);
                if ($category['category_id'] == 671 || $category['parent_id'] == 671){
                    $skip = true;
                }
            }
            if ($skip)
                continue;

            $custom_options = [];
            foreach ($this->model_catalog_product->getProductOptions($product['product_id'], true) as $option) {
                if ($option['name'] == "Dydis" || $option['name'] == "Spalva") {
                    if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
                        foreach ($option['option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if($option['name'] == "Dydis"){
                                    $custom_options[] = array(
                                        'name' => $option_value['name']
                                    );
                                }
                                if($option['name'] == "Spalva"){
                                    $custom_options_color[] = array(
                                        'name' => $option_value['name']
                                    );
                                }
                            }
                        }

                    } elseif ($option['type'] == 'text' || $option['type'] == 'hide' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
                        foreach ($option['option_value'] as $option_name) {
                            if($option['name'] == "Dydis") {
                                $custom_options[] =  array(
                                    'name' => $option_name['name']

                                );
                            }
                            if($option['name'] == "Spalva") {
                                $custom_options_color[] = array(
                                    'name' => $option_name['name']
                                );
                            }
                        }
                    }
                }

            }

            $c_s = '<![' . 'CDATA[';
            $c_e = ']' . ']>';
            $manufacturer = "manufacturer";
            $empty = "";
            $category_indicator = " > ";
            $output .= '<product>';

            if (!empty($categories) && isset($categories[0]['category_id'])) {
                $category_infos = $this->model_catalog_category->getCategoryPath($categories[0]['category_id']);
                // Adding this thing because they asked for it
                // Unfortunately there's literally no standardized method to check for
                // what category it should fall under, so I'll have to hope this sticks.
                // First, check all tags for one of the keywords
                $tags = explode(',', $product['tag']);
                if ($category_infos['name'] == 'AUDIO') {
                    foreach ($tags as $tag) {
                        if (trim(strtolower($tag)) == 'ausinės') {
                            $category_infos['name'] = 'AUSINĖS';
                            $categories[0]['category_id'] = 2001;
                            break;
                        }
                        if (trim(strtolower($tag)) == 'mikrofonas') {
                            $category_infos['name'] = 'MIKROFONAI';
                            $categories[0]['category_id'] = 2002;
                            break;
                        }
                    }
                }
                // If it didn't find anything, try to use the name
                if ($category_infos['name'] == 'AUDIO'){
                    if (strpos(strtolower($product['name']), 'ausinės') !== false) {
                        $category_infos['name'] = 'AUSINĖS';
                        $categories[0]['category_id'] = 2001;
                    }
                    if (strpos(strtolower($product['name']), 'mikrofonas') !== false) {
                        $category_infos['name'] = 'MIKROFONAI';
                        $categories[0]['category_id'] = 2002;
                    }
                }
                $output .= '<category-id>' . $c_s . $categories[0]['category_id'] . $c_e . '</category-id>';
                if (isset($category_infos['path']) && $category_infos['path'] != '') {
                    $output .= '<category-name>' . $c_s . htmlspecialchars_decode($category_infos['path']) . $category_indicator . htmlspecialchars_decode($category_infos['name']) . $c_e . '</category-name>';
                } else {
                    if (isset($category_infos['name'])) {
                        $output .= '<category-name>' . $c_s . htmlspecialchars_decode($category_infos['name']) . $c_e . '</category-name>';
                    }
                }
            } else {
                $output .= '<category-id>' . $c_s . $empty . $c_e . '</category-id>';
                $output .= '<category-name>' . $c_s . $empty . $c_e . '</category-name>';
            }
            unset($categories);
            $output .= '<title>' . $c_s . $product['name'] . $c_e . '</title>';

            if(empty($product['description'])){
                $output .= '<long-description>' . $c_s . strip_tags(htmlspecialchars_decode($product['name'])) . $c_e . '</long-description>';
            }else{
                $output .= '<long-description>' . $c_s . strip_tags(htmlspecialchars_decode($product['description'])) . $c_e . '</long-description>';
            }

            $output .= '<properties>';
            $output .= '<property>';
            $output .= '<id>' . $c_s . $manufacturer . $c_e . '</id>';
            $output .= '<values>';
            $output .= '<value>' . $c_s . $product['manufacturer'] . $c_e . '</value>';
            $output .= '</values>';
            $output .= '</property>';
            $output .= '</properties>';

            $output .= '<colours>';
            $product_images = $this->model_catalog_product->getProductImages($product['product_id']);
            if (!empty($custom_options_color)) {
                /*Jei yra spalvos*/
                $counter_custom_option_color = 0;
                foreach ( $custom_options_color as $color) {
                    $counter_custom_option_color = $counter_custom_option_color + 1;
                    $output .= '<colour>';
                    $output .= '<color-title>' . $c_s . $color['name'] . $c_e . '</color-title>';
                    $output .= '<images>';

                    $output .= '<image>';
                    $output .= '<url>' . $c_s . $full_domain . $product['image'] . $c_e . '</url>';
                    $output .= '</image>';

                    if ($product_images) {
                        foreach ($product_images as $product_image) {
                            $output .= '<image>';
                            $output .= '<url>' . $c_s . $full_domain . $product_image['image'] . $c_e . '</url>';
                            $output .= '</image>';
                        }
                    }

                    $output .= '</images>';

                    $output .= '<modifications>';
                    if (!empty($custom_options)) {
                        $counter_custom_option = 0;
                        foreach ($custom_options as $custom_option) {
                            $counter_custom_option = $counter_custom_option + 1;
                            $output .= '<modification>';
                            $output .= '<modification-title>' . $c_s . $custom_option['name'] . $c_e . '</modification-title>';
                            if ($product['weight'] > 0) {
                                $output .= '<weight>'. $c_s . $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $c_e .'</weight>';
                            }
                            else {
                                $output .= '<weight>'. $c_s . $empty . $c_e .'</weight>';
                            }

                            if($product['length'] == '0.00000000' && $product['height'] == '0.00000000' && $product['width'] == '0.00000000'){
                                $output .= '<length>'. $c_s . $empty . $c_e .'</length>';
                                $output .= '<height>'. $c_s . $empty . $c_e .'</height>';
                                $output .= '<width>'. $c_s . $empty . $c_e .'</width>';
                            }else{
                                $output .= '<length>'. $c_s . $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</length>';
                                $output .= '<height>'. $c_s . $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</height>';
                                $output .= '<width>'. $c_s .  $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</width>';
                            }


                            $output .= '<attributes>';
                            $output .= '<ean>' . $c_s . $product['ean'] . $c_e . '</ean>';
                            $output .= '<ean>' . $c_s . $product['model'] . $c_e . '</ean>';
                            $output .= '<supplier-code>' . $c_s . $product['product_id'] . $counter_custom_option . $counter_custom_option_color . $c_e . '</supplier-code>';
                            $output .= '<manufacturer-code>' . $c_s . $c_e . '</manufacturer-code>';
                            $output .= '</attributes>';
                            $output .= '</modification>';
                        }
                    }else{
                        $output .= '<modification>';
                        $output .= '<modification-title>' . $c_s .  $product['name'] . $c_e . '</modification-title>';
                        if ($product['weight'] > 0) {
                            $output .= '<weight>'. $c_s . $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $c_e .'</weight>';
                        }
                        else {
                            $output .= '<weight>'. $c_s . $empty . $c_e .'</weight>';
                        }
                        if($product['length'] == '0.00000000' && $product['height'] == '0.00000000' && $product['width'] == '0.00000000'){
                            $output .= '<length>'. $c_s . $empty . $c_e .'</length>';
                            $output .= '<height>'. $c_s . $empty . $c_e .'</height>';
                            $output .= '<width>'. $c_s . $empty . $c_e .'</width>';
                        }else{
                            $output .= '<length>'. $c_s . $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</length>';
                            $output .= '<height>'. $c_s . $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</height>';
                            $output .= '<width>'. $c_s .  $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</width>';
                        }


                        $output .= '<attributes>';
                        $output .= '<barcodes>';
                        $output .= '<barcode>' . $c_s . $product['ean'] . $c_e . '</barcode>';
                        $output .= '</barcodes>';
                        $output .= '<supplier-code>' . $c_s . $product['product_id'] . $c_e . '</supplier-code>';
                        $output .= '<manufacturer-code>' . $c_s . $c_e . '</manufacturer-code>';
                        $output .= '</attributes>';
                        $output .= '</modification>';
                    }

                    $output .= '</modifications>';
                    $output .= '</colour>';

                }

            }else{

                    $output .= '<colour>';
                    $output .= '<color-title>'. $c_s . $empty . $c_e .'</color-title>';
                    $output .= '<images>';

                    if ($product_images) {
                        foreach ($product_images as $product_image) {
                            $output .= '<image>';
                            $output .= '<url>' . $c_s . $full_domain . $product_image['image'] . $c_e . '</url>';
                            $output .= '</image>';
                        }
                    } else {
                        $output .= '<image>';
                        $output .= '<url>' . $c_s . $full_domain . $product['image'] . $c_e . '</url>';
                        $output .= '</image>';
                    }
                    $output .= '</images>';

                    $output .= '<modifications>';
                    if (!empty($custom_options)) {
                        $counter_custom_option_second = 0;
                        foreach ($custom_options as $custom_option) {
                            $counter_custom_option_second = $counter_custom_option_second + 1;
                            $output .= '<modification>';
                            $output .= '<modification-title>' . $c_s . $custom_option['name'] . $c_e . '</modification-title>';
                            if ($product['weight'] > 0) {
                                $output .= '<weight>'. $c_s . $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $c_e .'</weight>';
                            }
                            else {
                                $output .= '<weight>'. $c_s . $empty . $c_e .'</weight>';
                            }

                            if($product['length'] == '0.00000000' && $product['height'] == '0.00000000' && $product['width'] == '0.00000000'){
                                $output .= '<length>'. $c_s . $empty . $c_e .'</length>';
                                $output .= '<height>'. $c_s . $empty . $c_e .'</height>';
                                $output .= '<width>'. $c_s . $empty . $c_e .'</width>';
                            }else{
                                $output .= '<length>'. $c_s . $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</length>';
                                $output .= '<height>'. $c_s . $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</height>';
                                $output .= '<width>'. $c_s .  $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</width>';
                            }

                            $output .= '<attributes>';
                            $output .= '<barcodes>';
                            $output .= '<barcode>' . $c_s . $product['ean'] . $c_e . '</barcode>';
                            $output .= '</barcodes>';
                            $output .= '<supplier-code>' . $c_s . $product['product_id'] . $counter_custom_option_second . $c_e . '</supplier-code>';
                            $output .= '<manufacturer-code>' . $c_s . $c_e . '</manufacturer-code>';
                            $output .= '</attributes>';
                            $output .= '</modification>';
                        }
                    }else{
                        $output .= '<modification>';
                        $output .= '<modification-title>' . $c_s .  $product['name'] . $c_e . '</modification-title>';
                        if ($product['weight'] > 0) {
                            $output .= '<weight>'. $c_s . $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id')) . $c_e .'</weight>';
                        }
                        else {
                            $output .= '<weight>'. $c_s . $empty . $c_e .'</weight>';
                        }
                        if($product['length'] == '0.00000000' && $product['height'] == '0.00000000' && $product['width'] == '0.00000000'){
                            $output .= '<length>'. $c_s . $empty . $c_e .'</length>';
                            $output .= '<height>'. $c_s . $empty . $c_e .'</height>';
                            $output .= '<width>'. $c_s . $empty . $c_e .'</width>';
                        }else{
                            $output .= '<length>'. $c_s . $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</length>';
                            $output .= '<height>'. $c_s . $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</height>';
                            $output .= '<width>'. $c_s .  $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id')) / 100 . $c_e .'</width>';
                        }


                        $output .= '<attributes>';
                        $output .= '<barcodes>';
                        $output .= '<barcode>' . $c_s . $product['ean'] . $c_e . '</barcode>';
                        $output .= '</barcodes>';
                        $output .= '<supplier-code>' . $c_s . $product['product_id'] . $c_e . '</supplier-code>';
                        $output .= '<manufacturer-code>' . $c_s . $c_e . '</manufacturer-code>';
                        $output .= '</attributes>';
                        $output .= '</modification>';
                    }

                    $output .= '</modifications>';
                    $output .= '</colour>';
            }


            $output .= '</colours>';
            $output .= '</product>';

        }
        $output .= '</products>';
        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);

    }
    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);
        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }
            $path = $this->getPath($category_info['parent_id'], $new_path);
            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }
}
