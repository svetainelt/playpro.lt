<?php
class ControllerFeedNewExport extends Controller {
    public function index() {

        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<root>';
        $output .= '<products>';
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $products = $this->model_catalog_product->getProducts();


        $domain =  $_SERVER['HTTP_HOST'];
        $full_domain = "http://$domain/image/";


        foreach ($products as $product) {
            if(empty($product['image'])){
                continue;
            }
            if($product['image'] == 'no_image.jpg'){
                continue;
            }


            $c_s = '<![' . 'CDATA[';
            $c_e = ']' . ']>';
            $manufacturer = "manufacturer";
            $empty = "";
            $category_indicator = " > ";
            $output .= '<product>';
            $output .= '<id>' . $c_s . $product['product_id'] . $c_e . '</id>';
            $output .= '<quantity>' . $c_s . ($product['quantity'] + $product['reserve']) . $c_e . '</quantity>';
            $output .= '<price>' . $c_s . $product['price'] . $c_e . '</price>';
            $output .= '<special>' . $c_s . $product['special'] . $c_e . '</special>';
            $output .= '<tag>' . $c_s . $product['tag'] . $c_e . '</tag>';
            $output .= '<stock_status>' . $c_s . $product['stock_status'] . $c_e . '</stock_status>';

            $categories = $this->model_catalog_product->getCategories($product['product_id']);
            $output .= '<categories>';
            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $output .= '<id>' . $c_s . $category['category_id'] . $c_e . '</id>';
                }
            }
            $output .= '</categories>';
            unset($categories);
            $output .= '<title>' . $c_s . $product['name'] . $c_e . '</title>';

            if(empty($product['description'])){
                $output .= '<description>' . $c_s . htmlspecialchars_decode($product['name']) . $c_e . '</description>';
            }else{
                $output .= '<description>' . $c_s . htmlspecialchars_decode($product['description']) . $c_e . '</description>';
            }

            $output .= '<manufacturer>' . $c_s . $product['manufacturer'] . $c_e . '</manufacturer>';

            $product_images = $this->model_catalog_product->getProductImages($product['product_id']);

            $output .= '<images>';

            if ($product_images) {
                foreach ($product_images as $product_image) {
                    $output .= '<image>';
                    $output .= '<url>' . $c_s . $full_domain . $product_image['image'] . $c_e . '</url>';
                    $output .= '</image>';
                }
            } else {
                $output .= '<image>';
                $output .= '<url>' . $c_s . $full_domain . $product['image'] . $c_e . '</url>';
                $output .= '</image>';
            }
            $output .= '</images>';

                $output .= '<ean>' . $c_s . $product['ean'] . $c_e . '</ean>';
                $output .= '<model>' . $c_s . $product['model'] . $c_e . '</model>';

            $output .= '</product>';

        }
        $output .= '</products>';

        $output .= '<categories>';

        $categories = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.status=1")->rows;

        foreach ($categories as $category) {
            $output .= '<category>';
            $output .= '<id>' . $category['category_id'] . '</id>';
            $output .= '<parent>' . $category['parent_id'] . '</parent>';
            $output .= '<name>' . $category['name'] . '</name>';
            $output .= '<sort_order>' . $category['sort_order'] . '</sort_order>';
            $output .= '<description>' . $category['description'] . '</description>';
            $output .= '<meta_title>' . $category['meta_title'] . '</meta_title>';
            $output .= '</category>';
        }


        $output .= '</categories>';

        $output .= '</root>';
        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);

    }
    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);
        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }
            $path = $this->getPath($category_info['parent_id'], $new_path);
            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }
}
