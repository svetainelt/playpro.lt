<?php

class ControllerFeedFacebookFeedProducts extends Controller
{
    protected $registry;
    private $dom = null;
    private $channel = null;
    CONST FILE_NAME = 'facebook_feed_products.xml';

    private function mb_ucwords($str)
    {
        $exceptions = [
            'Hp' => 'HP',
            'Ibm' => 'IBM',
            'Gb' => 'GB',
            'Mb' => 'MB',
            'Cd' => 'CD',
            'Dvd' => 'DVD',
            'Usb' => 'USB',
            'Mm' => 'mm',
            'Cm' => 'cm',
            'A' => 'a',
            'An' => 'an',
            'The' => 'the',
            'And' => 'and',
            'But' => 'but',
            'For' => 'for',
            'Or' => 'or',
            'Nor' => 'nor',
            'To' => 'to',
            'As' => 'as',
            // etc.
        ];

        $separator = [' ', '-', '+'];

        $str = mb_strtolower(trim($str));
        foreach ($separator as $s) {
            $word = explode($s, $str);

            $return = '';
            foreach ($word as $val) {
                $return .= $s . mb_strtoupper(mb_substr($val, 0, 1)) . mb_substr($val, 1, mb_strlen($val) - 1);
            }
            $str = mb_substr($return, 1);
        }

        foreach ($exceptions as $find => $replace) {
            $return = preg_replace('/(?<![A-Za-z])' . $find . '(?![A-Za-z])/', $replace, $return);
        }
        $return = mb_strtoupper(mb_substr($return, 1, 1)) . mb_substr($return, 2);
        $return = preg_replace_callback('/(?<=\\.)\s*\\w/', function ($m) {
            return strtoupper($m[0]);
        }, $return);
        $return = preg_replace_callback('/(?<=")\\w/', function ($m) {
            return strtoupper($m[0]);
        }, $return);
        return $return;
    }

    public function __construct($registry)
    {
        $this->registry = $registry;

        if ($this->config->get('facebookfeedproducts_status')) {
            if (!empty($this->request->get['lang'])) {
                $feed_facebookfeedproducts_language = $this->request->get['lang'];
            } else {
                $feed_facebookfeedproducts_language = $this->config->get('facebookfeedproducts_language');
            }
            if (!empty($feed_facebookfeedproducts_language) && ($feed_facebookfeedproducts_language != $this->config->get('config_language_id'))) {
                $this->config->set('config_language_id', $feed_facebookfeedproducts_language);
            }

            $this->dom = new DomDocument('1.0');
            $rss = $this->dom->appendChild($this->dom->createElement('rss'));
            $rss->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:g', 'http://base.google.com/ns/1.0');
            $this->channel = $rss->appendChild($this->dom->createElement('channel'));

            $title = $this->channel->appendChild($this->dom->createElement('title'));
            $title->appendChild($this->dom->createTextNode($this->config->get('config_name')));
            $link = $this->channel->appendChild($this->dom->createElement('link'));
            $link->appendChild($this->dom->createTextNode($this->getLink()));

            $description = $this->channel->appendChild($this->dom->createElement('description'));
            if (is_array($this->config->get('config_meta_description'))) {
                $description->appendChild($this->dom->createTextNode(array_shift($this->config->get('config_meta_description'))));
            } else {
                $description->appendChild($this->dom->createTextNode($this->config->get('config_meta_description')));
            }
        } else {
            echo 'Module is disabled';
            exit(204);
        }
    }

    public function getFile()
    {
        $this->loadModels();
        $data = array();
        if ($this->config->get('facebookfeedproducts_google_status')) {
            $active_categories = $this->model_feed_facebookfeedproducts->getActiveCategoriesNotGoogle();
            $active_categories_facebook = array();
        } else {
            $active_categories = $this->model_feed_facebookfeedproducts->getActiveCategories();
            $active_categories_facebook = $this->model_feed_facebookfeedproducts->getActiveFacebookCategories();
        }

        $products = [];
        if (!empty(array_merge($active_categories, $active_categories_facebook))) {
            $products = $this->model_feed_facebookfeedproducts->getProductsByCategories(array_merge($active_categories, $active_categories_facebook));
        }

        $prodSettings = [];
        $results = $this->model_feed_facebookfeedproducts->getProdSettings();
        foreach ($results as $result) {
            $prodSettings[$result['key']] = $result['value'];
        }

        // products filter
        if (isset($prodSettings['feed_facebookfeedproducts_productslist_state']) and $prodSettings['feed_facebookfeedproducts_productslist_state'] == '1') {
            if (isset($prodSettings['feed_facebookfeedproducts_producmargestate'])) {
                $productMargeState = $prodSettings['feed_facebookfeedproducts_producmargestate'];

                switch ($productMargeState) {
                    case '0':
                        // Only selected
                        $products = [];
                        $result = $this->model_feed_facebookfeedproducts->getCustomProducts();
                        if (!empty($result)) {
                            foreach ($result as $row) {
                                $products[$row['product_id']] = ['product_id' => $row['product_id']];
                            }
                        }
                        break;
                    case '1':
                        // Everyone except selected
                        $except = $this->model_feed_facebookfeedproducts->getCustomProducts();

                        $products = $this->model_feed_facebookfeedproducts->getProductsList($except);

                        break;
                    case '2':
                        // Add selected to list
                        $tempResult = $this->model_feed_facebookfeedproducts->getCustomProducts();
                        $except = [];
                        if (!empty($tempResult)) {
                            foreach ($tempResult as $row) {
                                $products[$row['product_id']] = ['product_id' => $row['product_id']];
                            }
                        }
                        break;
                    case '3':
                        // Except selected from list
                        $tempResult = $this->model_feed_facebookfeedproducts->getCustomProducts();
                        $result = [];
                        if (!empty($tempResult)) {
                            foreach ($tempResult as $row) {
                                $result[] = $row['product_id'];
                            }
                        }
                        if (!empty($products)) {
                            foreach ($products as $key => $product) {
                                if (in_array($product['product_id'], $result)) {
                                    unset($products[$key]);
                                }
                            }
                        }
                        break;
                }
            }
        }

        if (!empty($products)) {
            $products_options = $this->model_feed_facebookfeedproducts->getAllProductsOptions();
            if (!empty($products)) {
                foreach ($products as $key => $product) {
                    if ($product) {
                        if (!empty($products_options[$product['product_id']])) {
                            $this->createItem($product, $products_options[$product['product_id']]);
                        } else {
                            $this->createItem($product);
                        }
                    }
                }

                $this->dom->formatOutput = true;

                return $this->dom->saveXML();
            } else {
                return "Product list is empty";
            }
        } else {
            return "Category list is empty";
        }
    }

    public function saveFile()
    {
        return file_put_contents('facebook_feed_products.xml', $this->getFile());
    }

    public function linkDownload()
    {
        $this->response->addHeader('Content-type: text/xml; charset=utf-8');
        $this->response->addHeader('Content-Disposition: attachment; filename="' . ControllerFeedFacebookFeedProducts::FILE_NAME . '"');
        $this->response->setOutput($this->getFile());
    }

    public function linkGeneration()
    {
        $this->response->addHeader('Content-type: text/xml; charset=utf-8');
        $this->response->setOutput($this->getFile());
    }

    private function isXml($value)
    {
        $prev = libxml_use_internal_errors(true);

        $doc = simplexml_load_string($value);
        $errors = libxml_get_errors();

        libxml_clear_errors();
        libxml_use_internal_errors($prev);

        return false !== $doc && empty($errors);
    }

    public function checkfeedstatus()
    {
        $file = $this->getFile();

        if ($this->isXml($file)) {
            $json = [
                'success' => true,
            ];
        } else {
            $json = [
                'success' => false,
                'error' => $file,
            ];
        }
        $this->response->addHeader('Content-type: application/json; charset=utf-8');
        $this->response->setOutput(json_encode($json));
    }

    private function createItem($productId, $products_options = null)
    {
        $product = $this->model_feed_facebookfeedproducts->getProduct($productId['product_id']);

        if (!empty($product)) {
            $item = $this->channel->appendChild($this->dom->createElement('item'));

            // Id product
            $this->addProductId($item, $product);

            // Name product
            $this->addProductName($item, $product);

            // Description product
            $this->addProductDescription($item, $product);

            // Link product
            $this->addLinkProduct($item, $product);

            // Brand product
            $this->addProductBrand($item, $product);

            // MPN
            $this->addProductMpn($item, $product);

            // Condition product
            $this->addProductCondition($item);

            // Weight product
            $this->addProductWeight($item, $product);

            // Availability product
            $this->addProductAvailability($item, $product);

            // Price product
            $this->addProductPrice($item, $product);

            // Product main image and additional images
            $this->addProductImages($item, $product);
            if (!$this->config->get('facebookfeedproducts_google_status')) {
                // Google categories
                $this->addGoogleCategories($item, $product);

                $this->addFacebookCategories($item, $product);
            }
            // Product options
            $this->addProductOptions($item, $product, $products_options);
        }
    }

    private function addGoogleCategories($item, $product)
    {
        $data = $this->model_feed_facebookfeedproducts->getGoogleCategoryForProduct($product['product_id']);
        if ($data['google_category_id'] == "0") {
            $data['google_category_id'] = null;
        }
        if ($data) {
            if (!is_null($data['google_category_id']) || !is_null($data['google_main_category_id'])) {
                $googleCategory = !is_null($data['google_category_id']) ? $data['google_category_id'] : $data['google_main_category_id'];
                $googleProductCategory = $item->appendChild($this->dom->createElement('g:google_product_category'));

                $googleProductCategory->appendChild($this->dom->createTextNode($googleCategory));
            }
        }
    }

    private function addFacebookCategories($item, $product)
    {
        $data = $this->model_feed_facebookfeedproducts->getFacebookCategoryForProduct($product['product_id']);
        if ($data['facebook_category_id'] == "0") {
            $data['facebook_category_id'] = null;
        }
        if ($data) {
            if (!is_null($data['facebook_category_id']) || !is_null($data['facebook_main_category_id'])) {
                $facebookCategory = !is_null($data['facebook_category_id']) ? $data['facebook_category_id'] : $data['facebook_main_category_id'];
                $facebookProductCategory = $item->appendChild($this->dom->createElement('g:fb_product_category'));

                $facebookProductCategory->appendChild($this->dom->createTextNode($facebookCategory));
            }
        }
    }

    private function addProductAvailability($item, $product)
    {
        $availability = $item->appendChild($this->dom->createElement('g:availability'));
        $availability->appendChild($this->dom->createTextNode($product['quantity'] ? 'in stock' : 'out of stock'));
    }

    private function addProductCondition($item)
    {
        $condition = $item->appendChild($this->dom->createElement('g:condition'));
        $condition->appendChild($this->dom->createTextNode('new'));
    }

    private function addProductBrand($item, $product)
    {
        if (trim($product['manufacturer'])) {
            $brand = $item->appendChild($this->dom->createElement('g:brand'));
            $brand->appendChild($this->dom->createTextNode(trim($product['manufacturer'])));
        }
    }

    private function addProductMpn($item, $product)
    {
        if (trim($product['mpn'])) {
            $brand = $item->appendChild($this->dom->createElement('g:mpn'));
            $brand->appendChild($this->dom->createTextNode(trim($product['mpn'])));
        }
    }

    private function addProductWeight($item, $product)
    {
        if ((float)$product['weight'] > 0) {
            $weight = $item->appendChild($this->dom->createElement('g:shipping_weight'));
            $weight->appendChild($this->dom->createTextNode((float)$product['weight'] . ' ' . $product['weight_class']));
        }
    }

    private function addProductDescription($item, $product)
    {
        $description = $item->appendChild($this->dom->createElement('g:description'));
        $whitespaceBefore = ["&nbsp;", "<br>", "\r\n"];
        $whitespaceAfter = [" ", " ", " "];
        $prodDescription = trim(strip_tags(html_entity_decode(str_replace($whitespaceBefore, $whitespaceAfter, $product['description']))));
        if (empty($prodDescription)) {
            $prodDescription = trim(strip_tags(html_entity_decode($product['name'])));
        }
        if (mb_strtoupper($prodDescription) == $prodDescription) {
            $prodDescription = $this->mb_ucwords($prodDescription);
        }
        $description->appendChild($this->dom->createTextNode(html_entity_decode($prodDescription)));
    }

    private function addProductName($item, $product)
    {
        $title = $item->appendChild($this->dom->createElement('g:title'));
        //$title->appendChild($this->dom->createTextNode(html_entity_decode(ucfirst(strtolower($product['name'])))));
        $product_name = trim(strip_tags(html_entity_decode($product['name'])));
        if (!preg_match('/[[:lower:]]/', $product_name)) {
            $product_name = ucwords(strtolower($product_name));
        }
        $title->appendChild($this->dom->createTextNode(trim($product_name)));
    }

    private function addProductId($item, $product)
    {
        $id = $item->appendChild($this->dom->createElement('g:id'));
        $id->appendChild($this->dom->createTextNode($product['product_id']));
    }

    private function addProductPrice($item, $product)
    {
        if (isset($this->request->get['cur']) && !empty($this->request->get['cur'])) {
            $feed_facebookfeedproducts_currency = $this->request->get['cur'];
        } else {
            $feed_facebookfeedproducts_currency = $this->config->get('facebookfeedproducts_currency');
        }
        $customer_special_id = $this->config->get('feed_facebookfeedproducts_customer_grup');
        $product_special = $this->model_feed_facebookfeedproducts->getProductSpecials($product['product_id']);
        $current_priority = (string)PHP_INT_MAX;

        foreach ($product_special as $key_s => $value_s) {
            if ($value_s['customer_group_id'] == $customer_special_id && (float)$current_priority > (float)$value_s['priority']) {
                $product['special'] = $value_s['price'];
                $current_priority = $value_s['priority'];
            }
        }

        $cost =
            $this->currency->format(
                $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')),
                $feed_facebookfeedproducts_currency,
                '',
                false
            )
            . ' '
            . $feed_facebookfeedproducts_currency;

        $price = $item->appendChild($this->dom->createElement('g:price'));
        $price->appendChild($this->dom->createTextNode($cost));

        if ($product['special']) {
            $costSpecial =
                $this->currency->format(
                    $this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')),
                    $feed_facebookfeedproducts_currency,
                    '',
                    false
                )
                . ' '
                . $feed_facebookfeedproducts_currency;

            $priceSpecial = $item->appendChild($this->dom->createElement('g:sale_price'));
            $priceSpecial->appendChild($this->dom->createTextNode($costSpecial));
        }
    }

    private function addProductOptions($item, $product, $options)
    {
        if (!empty($options)) {
            $color = array();
            $size = array();
            $pattern = array();
            $material = array();

            foreach ($options as $option) {
                if (is_array($this->config->get('facebookfeedproducts_color'))) {
                    if (@in_array($option['option_name'], $this->config->get('facebookfeedproducts_color'))) {
                        $color[] = $option['option_value'];
                    }
                } else {
                    if ($option['option_name'] == $this->config->get('facebookfeedproducts_color')) {
                        $color[] = $option['option_value'];
                    }
                }

                if (is_array($this->config->get('facebookfeedproducts_size'))) {
                    if (@in_array($option['option_name'], $this->config->get('facebookfeedproducts_size'))) {
                        $size[] = $option['option_value'];
                    }
                } else {
                    if ($option['option_name'] == $this->config->get('facebookfeedproducts_size')) {
                        $size[] = $option['option_value'];
                    }
                }

                if (is_array($this->config->get('facebookfeedproducts_pattern'))) {
                    if (@in_array($option['option_name'], $this->config->get('facebookfeedproducts_pattern'))) {
                        $pattern[] = $option['option_value'];
                    }
                } else {
                    if ($option['option_name'] == $this->config->get('facebookfeedproducts_pattern')) {
                        $pattern[] = $option['option_value'];
                    }
                }

                if (is_array($this->config->get('facebookfeedproducts_material'))) {
                    if (@in_array($option['option_name'], $this->config->get('facebookfeedproducts_material'))) {
                        $material[] = $option['option_value'];
                    }
                } else {
                    if ($option['option_name'] == $this->config->get('facebookfeedproducts_material')) {
                        $material[] = $option['option_value'];
                    }
                }
            }

            if (!empty($color)) {
                $option = $item->appendChild($this->dom->createElement('g:color'));
                $option->appendChild($this->dom->createTextNode(implode(', ', $color)));
            }
            if (!empty($size)) {
                $option = $item->appendChild($this->dom->createElement('g:size'));
                $option->appendChild($this->dom->createTextNode(implode(', ', $size)));
            }
            if (!empty($pattern)) {
                $option = $item->appendChild($this->dom->createElement('g:pattern'));
                $option->appendChild($this->dom->createTextNode(implode(', ', $pattern)));
            }
            if (!empty($material)) {
                $option = $item->appendChild($this->dom->createElement('g:material'));
                $option->appendChild($this->dom->createTextNode(implode(', ', $material)));
            }
        }
    }

    private function addLinkProduct($item, $product)
    {
        $productLink = $this->url->link('product/product', 'product_id=' . $product['product_id']);
        $link = $item->appendChild($this->dom->createElement('g:link'));
        $link->appendChild($this->dom->createTextNode(htmlspecialchars_decode($productLink)));
    }

    private function addProductImages($item, $product)
    {
        $image = $item->appendChild($this->dom->createElement('g:image_link'));
        $image_size = $this->config->get('facebookfeedproducts_image_size') ? $this->config->get('facebookfeedproducts_image_size') : 600;
        $image_file = '';

        if (!empty($product['image'])) {
            if ($this->config->get('facebookfeedproducts_image_size_off') == 1) {
                $image_file = HTTPS_SERVER . 'image/' . $product['image'];
            } else {
                $image_file = $this->model_tool_image->resize($product['image'], (int)$image_size, (int)$image_size);
            }
        }

        if ($this->config->get('facebookfeedproducts_image_empty') && empty($image_file)) {
            if ($this->config->get('facebookfeedproducts_image_size_off') == 1) {
                $image_file = HTTPS_SERVER . 'image/no_image.png';
            } else {
                $image_file = $this->model_tool_image->resize('no_image.png', (int)$image_size, (int)$image_size);
            }
        }

        $image->appendChild($this->dom->createTextNode(htmlspecialchars_decode($image_file)));

        $additionalImages = $this->model_catalog_product->getProductImages($product['product_id']);
        if ($additionalImages) {
            $additionalImages = array_slice($additionalImages, 0, 2);
            foreach ($additionalImages as $image) {
                $image_adap_file = '';
                $addImage = $item->appendChild($this->dom->createElement('g:additional_image_link'));
                if ($this->config->get('facebookfeedproducts_image_size_off') == 1) {
                    $image_adap_file = HTTPS_SERVER . 'image/' . $image['image'];
                } else {
                    $image_adap_file = $this->model_tool_image->resize($image['image'], (int)$image_size, (int)$image_size);
                }
                $addImage->appendChild($this->dom->createTextNode(htmlspecialchars_decode($image_adap_file)));
            }
        }
    }

    private function getOptionsFeed()
    {
        $options = array();

        $options[] = $this->config->get('facebookfeedproducts_color');
        $options[] = $this->config->get('facebookfeedproducts_size');
        $options[] = $this->config->get('facebookfeedproducts_pattern');
        $options[] = $this->config->get('facebookfeedproducts_material');

        return $options;
    }

    private function getLink()
    {
        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $link = $this->config->get('config_ssl');
        } else {
            $link = $this->config->get('config_url');
        }

        return $link;
    }

    private function loadModels()
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('feed/facebookfeedproducts');
    }
}
