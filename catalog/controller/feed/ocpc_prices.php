<?php

class ControllerFeedOCPCPrices extends Controller
{
    public function index()
    {


        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<products>';
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');


        $products = $this->model_catalog_product->getProductsPigu();


        $domain = $_SERVER['HTTP_HOST'];
        $full_domain = "http://$domain/image/";


        foreach ($products as $product) {
            if (empty($product['image'])) {
                continue;
            }
            if ($product['image'] == 'no_image.jpg') {
                continue;
            }

            $categories = $this->model_catalog_product->getCategories($product['product_id']);

            if ($categories[0]['category_id'] == 68 || $categories[0]['category_id'] == 640){
                continue;
            }

            $custom_options_color = [];

            $categories = $this->model_catalog_product->getCategories($product['product_id']);
            $skip = false;
            foreach($categories as $category_product) {
                $category = $this->model_catalog_category->getCategory($category_product['category_id']);
                if ($category['category_id'] == 671 || $category['parent_id'] == 671){
                    $skip = true;
                }
            }
            if ($skip)
                continue;

            $custom_options = [];
            foreach ($this->model_catalog_product->getProductOptions($product['product_id'], true) as $option) {
                if ($option['name'] == "Dydis" || $option['name'] == "Spalva") {
                    if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
                        foreach ($option['option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if ($option['name'] == "Dydis") {
                                    $custom_options[] = array(
                                        'name' => $option_value['name'],
                                        'quantity' => $option_value['quantity']
                                    );
                                }
                                if ($option['name'] == "Spalva") {
                                    $custom_options_color[] = array(
                                        'name' => $option_value['name']
                                    );
                                }
                            }
                        }

                    } elseif ($option['type'] == 'text' || $option['type'] == 'hide' || $option['type'] == 'textarea' || $option['type'] == 'file' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
                        foreach ($option['option_value'] as $option_name) {
                            if ($option['name'] == "Dydis") {
                                $custom_options[] = array(
                                    'name' => $option_name['name'],
                                    'quantity' => $option_name['quantity']

                                );
                            }
                            if ($option['name'] == "Spalva") {
                                $custom_options_color[] = array(
                                    'name' => $option_name['name']
                                );
                            }
                        }
                    }
                }

            }

            $c_s = '<![' . 'CDATA[';
            $c_e = ']' . ']>';
            $collectionhours = "24";

            $empty = '';
            if (!$product['special']){
                $product['special'] = $product['price'];
            }

            if (!empty($custom_options_color)) {
                /*Jei yra spalvos*/
                $counter_custom_option_color = 0;
                foreach ($custom_options_color as $color) {
                    $counter_custom_option_color = $counter_custom_option_color + 1;


                    if (!empty($custom_options)) {
                        $counter_custom_option_second = 0;
                        foreach ($custom_options as $custom_option) {
                            $counter_custom_option_second = $counter_custom_option_second + 1;
                            $output .= '<product>';
                            $output .= '<sku>' . $c_s . $product['product_id'] . $counter_custom_option_second . $counter_custom_option_color . $c_e . '</sku>';
                            $output .= '<ean>' . $c_s . $product['ean'] . $c_e . '</ean>';
                            $output .= '<price-before-discount>' . $c_s . round($product['price'], 2) . $c_e . '</price-before-discount>';
                            $output .= '<price-after-discount>' . $c_s . round($product['special'], 2) . $c_e . '</price-after-discount>';
                            $output .= '<stock>' . $c_s . $custom_option['quantity'] . $c_e . '</stock>';
                            $output .= '<collectionhours>' . $c_s . $collectionhours . $c_e . '</collectionhours>';
                            $output .= '</product>';
                        }
                    } else {
                        $output .= '<product>';
                        $output .= '<sku>' . $c_s . $product['product_id'] . $c_e . '</sku>';
                        $output .= '<ean>' . $c_s . $product['sku'] . $c_e . '</ean>';
                        $output .= '<price-before-discount>' . $c_s . round($product['price'], 2) . $c_e . '</price-before-discount>';
                        $output .= '<price-after-discount>' . $c_s . round($product['special'], 2) . $c_e . '</price-after-discount>';
                        $output .= '<stock>' . $c_s . ($product['quantity'] + $product['reserve']) . $c_e . '</stock>';
                        $output .= '<collectionhours>' . $c_s . $collectionhours . $c_e . '</collectionhours>';
                        $output .= '</product>';
                    }
                }

            } else {
                if (!empty($custom_options)) {
                    $counter_custom_option_second = 0;
                    foreach ($custom_options as $custom_option) {
                        $counter_custom_option_second = $counter_custom_option_second + 1;
                        $output .= '<product>';
                        $output .= '<sku>' . $c_s . $product['product_id'] . $counter_custom_option_second . $c_e . '</sku>';
                        $output .= '<ean>' . $c_s . $empty . $c_e . '</ean>';
                        $output .= '<price-before-discount>' . $c_s . round($product['price'], 2) . $c_e . '</price-before-discount>';
                        $output .= '<price-after-discount>' . $c_s . round($product['special'], 2) . $c_e . '</price-after-discount>';
                        $output .= '<stock>' . $c_s . $custom_option['quantity'] . $c_e . '</stock>';
                        $output .= '<collectionhours>' . $c_s . $collectionhours . $c_e . '</collectionhours>';
                        $output .= '</product>';
                    }
                } else {
                    $output .= '<product>';
                    $output .= '<sku>' . $c_s . $product['product_id'] . $c_e . '</sku>';
                    $output .= '<ean>' . $c_s . $product['sku'] . $c_e . '</ean>';
                    $output .= '<price-before-discount>' . $c_s . round($product['price'], 2) . $c_e . '</price-before-discount>';
                    $output .= '<price-after-discount>' . $c_s . round($product['special'], 2) . $c_e . '</price-after-discount>';
                    $output .= '<stock>' . $c_s . ($product['quantity'] + $product['reserve']) . $c_e . '</stock>';
                    $output .= '<collectionhours>' . $c_s . $collectionhours . $c_e . '</collectionhours>';
                    $output .= '</product>';
                }

            }


        }
        $output .= '</products>';
        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);

    }

    protected function getPath($parent_id, $current_path = '')
    {
        $category_info = $this->model_catalog_category->getCategory($parent_id);
        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }
            $path = $this->getPath($category_info['parent_id'], $new_path);
            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }
}
