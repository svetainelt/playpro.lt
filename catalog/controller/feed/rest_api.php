<?php
error_reporting(0);
class ControllerFeedRestApi extends Controller {

    private $debugIt = false;

    public function orders() {

        $this->checkPlugin();

        $orderData['orders'] = array();

        $this->load->model('account/order');

        if (isset($this->request->get['offset']) && $this->request->get['offset'] != "" && ctype_digit($this->request->get['offset'])) {
            $offset = $this->request->get['offset'];
        } else {
            $offset = 0;
        }

        if (isset($this->request->get['limit']) && $this->request->get['limit'] != "" && ctype_digit($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 10000;
        }

        if ($this->request->get['date_modified']) {

            $results = $this->model_account_order->getOrderByDate($this->request->get['date_modified']);
        }
        else
        {
            $results = $this->model_account_order->getAllOrders($offset, $limit);
        }

        $orders = array();

        if(count($results)){
            foreach ($results as $result) {

                if ($result['invoice_prefix'] == 'FRE-0000') {

                    $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
                    $voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

                    $tax = $this->model_account_order->getOrderTax($result['order_id']);
                    $shipping = $this->model_account_order->getOrderShipping($result['order_id']);

                    $total = $this->currency->format($result['total']);
                    $tax = $this->currency->format($tax);
                    $shipping = $this->currency->format($this->tax->calculate($shipping, 9, $this->config->get('config_tax')));

                    $total = str_replace('€', '', $total);
                    $tax = str_replace('€', '', $tax);
                    $shipping = str_replace('€', '', $shipping);
                    $total = str_replace(' ', '', $total);

                    $total = (float)$total;
                    $tax = (float)$tax;
                    $shipping = (float)$shipping;

                    if ($result['shipping_company']) {
                        $company_name = $result['shipping_company'];
                        $company_name = str_replace('"', '\"', $company_name);
                        $company_name = str_replace('&quot;', '\"', $company_name);
                    }

                    if ($result['payment_company']) {
                        $company_name = $result['payment_company'];
                        $company_name = str_replace('"', '\"', $company_name);
                        $company_name = str_replace('&quot;', '\"', $company_name);
                    }

                    if ($company_name == '-')
                    {
                        $company_name = null;
                    }

                    if ($company_name == '')
                    {
                        $company_name = null;
                    }

                    if ($result['shipping_custom_field']) {

                        $shipping_custom_field = json_decode($result['shipping_custom_field'], true);

                        $company_code = $shipping_custom_field[1];
                        $company_vat = $shipping_custom_field[2];

                    }

                    if ($result['payment_custom_field']) {

                        $payment_custom_field = json_decode($result['payment_custom_field'], true);

                        $company_code = $payment_custom_field[1];
                        $company_vat = $payment_custom_field[2];

                    }

                    if (strpos($company_code, 'kodas') !== false) {

                        $company_code = '';

                    }

                    if (strpos($company_vat, 'PVM') !== false) {

                        $company_vat = '';

                    }

                    if ($result['shipping_address_1']) {
                        $company_address = $result['shipping_address_1'];
                    }

                    if ($result['payment_address_1']) {
                        $company_address = $result['payment_address_1'];
                    }

                    $invoice = $result['invoice_prefix'] . $result['invoice_no'];

                    $products = $this->model_account_order->getAllOrderProducts($result['order_id']);

                    $products_tree = array();

                    foreach ($products as $product) {

                        $product_price = $this->currency->format($this->tax->calculate($product['price'], 9, $this->config->get('config_tax')));
                        $product_total = $this->currency->format($this->tax->calculate($product['total'], 9, $this->config->get('config_tax')));

                        $product_total = str_replace('€', '', $product_total);
                        $product_price = str_replace('€', '', $product_price);
                        $product_total = str_replace(' ', '', $product_total);
                        $product_price = str_replace(' ', '', $product_price);

                        $product['name'] = str_replace('"', '\"', $product['name']);
                        $product['name'] = str_replace('&quot;', '\"', $product['name']);

                        $products_tree[] = array(
                            'product_id' => (float)$product['product_id'],
                            'name' => $product['name'],
                            'model' => $product['model'],
                            'quantity' => (float)$product['quantity'],
                            'price' => (float)$product_price,
                            'total' => (float)$product_total,
                        );
                    }

                    if($result['payment_code'] == 'cod' && strpos($result['shipping_code'], 'omniva') !== false) {
                        $result['payment_code'] = 'omn';
                    }


                    $orders[] = array(
                        'order_id' => (float)$result['order_id'],
                        'name' => $result['firstname'] . ' ' . $result['lastname'],
                        'date_added' => $result['date_added'],
                        'date_modified' => $result['sf_date'],
                        'invoice_number' => (float)$result['invoice_no'],
                        'invoice' => $invoice,
                        'products' => ($product_total + $voucher_total),
                        'payment_method' => $result['payment_method'],
                        'payment_code' => $result['payment_code'],
                        'shipping_price' => $shipping,
                        'tax' => $tax,
                        'total' => $total,
                        'currency_code' => $result['currency_code'],
                        'currency_value' => $result['currency_value'],
                        'company_name' => $company_name,
                        'company_code' => $company_code,
                        'company_vat' => $company_vat,
                        'company_address' => $company_address,
                        'order_products' => $products_tree
                    );
                }

                $company_name = '';
                $company_code = '';
                $company_vat = '';
                $company_address = '';

            }

            $json['success'] 	= true;
            $json['orders'] 	= $orders;

        }else {

            $json['success'] 	= false;

        }

        if ($this->debugIt) {

            echo '<pre>';

            print_r($json);

            echo '</pre>';


        } else {

            $this->response->setOutput(json_encode($json));

        }
    }


    /*
    * Get orders products
    */

    public function products() {

        $this->checkPlugin();

        $orderData['orders'] = array();

        $this->load->model('account/order');

        if ($this->request->get['order_id']) {

            $results = $this->model_account_order->getAllOrderProducts($this->request->get['order_id']);

        }

        $orders = array();

        if(count($results)) {
            foreach ($results as $result) {

                $price = $this->currency->format($this->tax->calculate($result['price'], 9, $this->config->get('config_tax')));
                $total = $this->currency->format($this->tax->calculate($result['total'], 9, $this->config->get('config_tax')));

                $total = str_replace('€', '', $total);
                $price = str_replace('€', '', $price);

                $orders[] = array(
                    'order_id'		=> $result['order_id'],
                    'product_id'	=> $result['product_id'],
                    'name'	        => $result['name'],
                    'model'	        => $result['model'],
                    'quantity'		=> $result['quantity'],
                    'price'			=> $price,
                    'total'			=> $total,
                );

            }

            $json['success'] 	= true;
            $json['orders'] 	= $orders;

        }else {

            $json['success'] 	= false;

        }

        if ($this->debugIt) {

            echo '<pre>';

            print_r($json);

            echo '</pre>';


        } else {

            $this->response->setOutput(json_encode($json));

        }

    }


    private function checkPlugin() {

        $json = array("success"=>false);

        /*check rest api is enabled*/
        if (!$this->config->get('rest_api_status')) {
            $json["error"] = 'API is disabled. Enable it!';
        }

        /*validate api security key*/
        if ($this->config->get('rest_api_key') && (!isset($this->request->get['key']) || $this->request->get['key'] != $this->config->get('rest_api_key'))) {
            $json["error"] = 'Invalid secret key';
        }

        if(isset($json["error"])){
            $this->response->addHeader('Content-Type: application/json');
            echo(json_encode($json));
            exit;
        }else {
            $this->response->setOutput(json_encode($json));
        }
    }

}