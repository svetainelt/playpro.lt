<?php
class ControllerXmlCustomExport extends Controller {
    private $error = array();

    public function index() {

        error_reporting(0);


        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('xml/kaina24export');


        $filter_categories = $this->config->get('kaina24export_category');
        $filter_manufacturers = $this->config->get('kaina24export_manufacturer');
        $filter_min_price = $this->config->get('kaina24export_min_price');

        $filter_data = array(
            'filter_categories' => $filter_categories,
            'filter_manufacturers' => $filter_manufacturers,
            'filter_min_price' => $filter_min_price

        );

        $results = $this->model_xml_kaina24export->getProducts();

        if (empty($results)) {
            return;
        }

        $shipping_rates = explode(',', $this->config->get('kaina24export_delivery_price'));

        $data = array();

        foreach ($results as $result) {

            $categories = array();
            $product_categories = $this->model_xml_kaina24export->getProductCategories($result['product_id'], $filter_categories);
            if ($product_categories) {
                foreach ($product_categories as $key => $product_category) {
                    $categories_info = $this->model_catalog_category->getCategory($product_category['category_id']);
                    $categories[$key]['category_name'] = $categories_info['name'];
                    $categories[$key]['category_id'] = $categories_info['category_id'];
                }
            }


            $category_name = $categories[1]['category_name'];
            $category_id = $categories[1]['category_id'];



            $price = $result['price']/1.05;
            $special = $result['special']/1.05;


            if ($price != '' AND $price > 0) {
                $price = round($this->tax->calculate($price, $this->config->get('kaina24export_tax_class_id'), $this->config->get('config_tax')),2);
                $price = number_format($price, 2, '.', '');
            }

            if ($special != '' AND $special > 0) {
                $special = round($this->tax->calculate($special, $this->config->get('kaina24export_tax_class_id'), $this->config->get('config_tax')),2);
                $special = number_format($special, 2, '.', '');
            }

            //Calculate shipping price by weight
            $shipping_price = '';
            $weight = $result['weight'];



            foreach ($shipping_rates as $rate) {
                $rate_data = explode(':', $rate);

                if ($rate_data[0] >= $weight) {
                    if (isset($rate_data[1])) {
                        $shipping_price = $rate_data[1];
                    }

                    break;
                }
            }

            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'image' => $result['image'],
                'images' => $this->model_catalog_product->getProductImages($result['product_id']),
                'attributes' =>  $this->model_catalog_product->getProductAttributes($result['product_id']),
                'category_id' => $category_id,
                'category_name' => $category_name,
                'quantity' => $result['quantity'],
                'manufacturer' => $result['manufacturer'],
                'description' => html_entity_decode($result['description']),
                'meta_description' => html_entity_decode($result['meta_description']),
                'name' => $result['name'],
                'price' => $price,
                'special' => $special,
                'model' => $result['model'],
                'href'  => $this->url->link('product/product', 'product_id=' . (int)$result['product_id']),
                'tags' => $result['tag'],
                'mpn'	=> $result['mpn'],
                'ean'	=> $result['ean'],
                'condition' => $this->config->get('kaina24export_condition'),
                'category_link' => $this->url->link('product/category', 'path=' . (int)$category_id),
                'delivery_price' => round($this->tax->calculate($shipping_price, $this->config->get('kaina24export_tax_class_id'), $this->config->get('config_tax')),2),
                'delivery_time' => $this->config->get('kaina24export_delivery_time'),
            );


        }


        $this->response->addHeader('Content-Type: application/xml');

        if (VERSION < 2.1) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/xml/kaina24export.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/xml/kaina24export.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/xml/kaina24export.tpl', $data));
            }

        } else {
            //$this->response->setOutput($this->load->view('xml/kaina24export', $data));
            $this->response->setOutput($this->load->view('default/template/xml/kaina24export.tpl', $data));
        }

    }
}

?>