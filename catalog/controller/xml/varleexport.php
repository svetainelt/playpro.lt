<?php 
class ControllerXmlVarleexport extends Controller {
	private $error = array();

	public function index() {
				
		if ($this->config->get('varleexport_status') == 0 OR !$this->config->get('varleexport_stores')) {
			return;
		}
	
		$this->load->model('catalog/product');
		$this->load->model('xml/varleexport');
		
		$categories = $this->model_xml_varleexport->getCategories();

		
		$filter_categories = ($this->config->get('varleexport_category') ? $this->config->get('varleexport_category') : array());
		$filter_manufacturers = ($this->config->get('varleexport_manufacturer') ? $this->config->get('varleexport_manufacturer') : array());
		$filter_min_price = ($this->config->get('varleexport_min_price') ? $this->config->get('varleexport_min_price') : 0);
			
		$filter_data = array(
			'filter_categories' => $filter_categories,
			'filter_manufacturers' => $filter_manufacturers,
			'filter_min_price' => $filter_min_price
				
		);	

		$results = $this->model_xml_varleexport->getProducts($filter_data);
		
		if (empty($results)) {
			return;
		}
		

		$data = array();
		
		$data['categories'] = $categories;
		
		foreach ($results as $result) {
			
			if (isset($filter_min_price) AND (isset($result['special']) AND $result['special'] < $filter_min_price OR $result['price'] < $filter_min_price)) {
				continue;
			}
			
			$categories = $this->model_xml_varleexport->getProductCategories($result['product_id'], $filter_categories);
			
			$price = $result['price'];
			$special = $result['special'];
			
			$prime_costs = !empty($result['special']) ? $special : $price;
			
			if ($this->config->get('varleexport_operator2') != '') {
				$operator2 = $this->config->get('varleexport_operator2');
				$operator_value2 = $this->config->get('varleexport_operator_value2');
			
				if ($prime_costs != '' AND $prime_costs > 0) {
					$price_string2 = $prime_costs.$operator2.$operator_value2;
					eval( '$prime_costs = (' . $price_string2 . ');' );
				}
			}
			
			if ($this->config->get('varleexport_operator') != '') {
				$operator = $this->config->get('varleexport_operator');
				$operator_value = $this->config->get('varleexport_operator_value');
				
				if ($price != '' AND $price > 0) {
					$price_string = $price.$operator.$operator_value;
					eval( '$price = (' . $price_string . ');' );
				}
				if ($special != '' AND $special > 0) {
					$special_string = $special.$operator.$operator_value;
					eval( '$special = (' . $special_string . ');' );
				}
			}

			
			if ($price != '' AND $price > 0) {
				$price = round($this->tax->calculate($price, $this->config->get('varleexport_tax_class_id'), $this->config->get('config_tax')),2);
				$price = number_format($price, 2, '.', '');
			}
				
			if ($special != '' AND $special > 0) {
				$special = round($this->tax->calculate($special, $this->config->get('varleexport_tax_class_id'), $this->config->get('config_tax')),2);
				$special = number_format($special, 2, '.', '');
			}

            if ($prime_costs != '' AND $prime_costs > 0) {
                $prime_costs = number_format($prime_costs, 2, '.', '');
            }
			
			$data['products'][] = array(
				'product_id' => $result['product_id'],
				'image' => $result['image'],
				'images' => $this->model_catalog_product->getProductImages($result['product_id']),
				'attributes' =>  $this->model_catalog_product->getProductAttributes($result['product_id']),
				'options' => $this->model_catalog_product->getProductOptions($result['product_id']),
				'categories' => $categories,
				'quantity' => $result['quantity'],
				'manufacturer' => $result['manufacturer'],
				'description' => html_entity_decode($result['description']),
				'name' => $result['name'],
				'price' => $price,
				'prime_costs' => $prime_costs,
				'special' => $special,
				'model' => $result['model'],
				'ean' => $result['ean'],
				'weight' => $result['weight']
			);
			

		}
		

		$this->response->addHeader('Content-Type: application/xml');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/xml/varleexport.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/xml/varleexport.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/xml/varleexport.tpl', $data));
			}
		
	}
}

?>