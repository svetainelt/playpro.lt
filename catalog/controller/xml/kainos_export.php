<?php 
class ControllerXmlKainosExport extends Controller {
	private $error = array();

	public function index() {

	    error_reporting(0);
		

		if ($this->config->get('kainos_export_status') == 0 OR !($this->config->get('kainos_export_stores')) OR !($this->config->get('kainos_export_category')) OR !($this->config->get('kainos_export_manufacturer'))) {
			return;
		}
		
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('xml/kainos_export');
		
		//$categories = $this->model_xml_kainos_export->getCategories();

		
		$filter_categories = $this->config->get('kainos_export_category');
		$filter_manufacturers = $this->config->get('kainos_export_manufacturer');
		$filter_min_price = $this->config->get('kainos_export_min_price');
			
		$filter_data = array(
			'filter_categories' => $filter_categories,
			'filter_manufacturers' => $filter_manufacturers,
			'filter_min_price' => $filter_min_price
				
		);	

		$results = $this->model_xml_kainos_export->getProducts($filter_data);
		
		if (empty($results)) {
			return;
		}
		
		$shipping_rates = explode(',', $this->config->get('kainos_export_delivery_price'));
		
		$data = array();
		
		foreach ($results as $result) {
			;
			if (isset($filter_min_price) AND (isset($result['special']) AND $result['special'] < $filter_min_price OR $result['price'] < $filter_min_price)) {
				continue;
			}
			
			$categories = array();
			$product_categories = $this->model_xml_kainos_export->getProductCategories($result['product_id'], $filter_categories);
			if ($product_categories) {

				foreach ($product_categories as $key => $product_category) {	
					$categories[$key] = $this->model_catalog_category->getKainosCategoryPath($product_category['category_id']);

				}
			}
			
			/*if ($this->config->get('kainos_export_category_type') == 'last') {
				$categories = array_reverse($categories);
			}*/
			//print_r($categories);die();
			//$category_name = $categories[1]['category_name'];
			//$category_id = $categories[1]['category_id'];
			//echo $category_id;die();
			
			
			$price = $result['price'];
			$special = $result['special'];
			
			if ($this->config->get('kainos_export_operator') != '') {
				$operator = $this->config->get('kainos_export_operator');
				$operator_value = $this->config->get('kainos_export_operator_value');
				
				if ($price != '' AND $price > 0) {
					$price_string = $result['price'].$operator.$operator_value;
					eval( '$price = (' . $price_string . ');' );
				}
				if ($special != '' AND $special > 0) {
					$special_string = $result['special'].$operator.$operator_value;
					eval( '$special = (' . $special_string . ');' );
				}
			}
			
			if ($price != '' AND $price > 0) {
				$price = round($this->tax->calculate($price, $this->config->get('kainos_export_tax_class_id'), $this->config->get('config_tax')),2);
				$price = number_format($price, 2, '.', '');
			}
			
			if ($special != '' AND $special > 0) {
				$special = round($this->tax->calculate($special, $this->config->get('kainos_export_tax_class_id'), $this->config->get('config_tax')),2);
				$special = number_format($special, 2, '.', '');
			}
			
			//Calculate shipping price by weight
			$shipping_price = '';
			$weight = $result['weight'];
			
			
			
			foreach ($shipping_rates as $rate) {
				$rate_data = explode(':', $rate);
			
				if ($rate_data[0] >= $weight) {
					if (isset($rate_data[1])) {
						$shipping_price = $rate_data[1];
					}
			
					break;
				}
			}

            if ($result['quantity'] > 0) {
                $delivery_text = "0-2 d.d";
            }
            elseif ($result['reserve'] > 0) {
                $delivery_text = "1-3 d.d";
            }
            else {
                $delivery_text = "Teirautis";
            }

			$data['products'][] = array(
				'product_id' => $result['product_id'],
				'image' => $result['image'],
				'images' => $this->model_catalog_product->getProductImages($result['product_id']),
				'attributes' =>  $this->model_catalog_product->getProductAttributes($result['product_id']),
				'categories' => $categories,
				'quantity' => $result['quantity'] + $result['reserve'],
				'manufacturer' => $result['manufacturer'],
				'description' => html_entity_decode($result['description']),
				'meta_description' => html_entity_decode($result['meta_description']),
				'name' => $result['name'],
				'price' => $price,
				'special' => $special, 
				'model' => $result['model'],
				'href'  => $this->url->link('product/product', 'product_id=' . (int)$result['product_id']),
				'mpn'	=> $result['model'],
				'ean'	=> $result['ean'],
				'delivery_text' => $delivery_text,
				'delivery_time' => $this->config->get('kainos_export_delivery_time'),
			); 
			

		}

		$data['name_array'] = array(' ', 'ą', 'č', 'ę', 'ė', 'į', 'š', 'ų', 'ū', 'ž');
        $data['value_array'] = array('_', 'a', 'c', 'e', 'e', 'i', 's', 'u', 'u', 'z');

		$this->response->addHeader('Content-Type: application/xml');

		$temp = $this->load->view('default/template/xml/kainos_export.tpl', $data);

		if (VERSION < 2.1) {	
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/xml/kainos_export.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/xml/kainos_export.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/xml/kainos_export.tpl', $data));
			}
		
		} else {
			//$this->response->setOutput($this->load->view('xml/kainos_export', $data));
            //$this->response->setOutput($temp);
            
            // I legitimately have no idea why, I've checked for a good 40 minutes,
            // but specifically spaces before an attribute are replaced with a line
            // break if I don't do this instead of the usual setOutput. This is
            // working, so I'm not touching it any more than I have to from here
            // on out.
            
            header('Content-Type: application/xml');
            echo $temp;
		}
		
	}
}

?>