<?php
class ControllerShippingVenipakShipping extends Controller {

	public function pickups() {
		$this->load->model('localisation/country');
		$country_info = $this->model_localisation_country->getCountry($this->session->data['shipping_address']['country_id']);

		$is_locker_disabled = $this->config->get('shipping_venipak_shipping_disable_locker');
		$is_pickup_disabled = $this->config->get('shipping_venipak_shipping_disable_pickup');
		$pick_up_type = 0;

		if (empty($is_locker_disabled) && $is_pickup_disabled) {
			$pick_up_type = 3;
		}

		if (empty($is_pickup_disabled) && $is_locker_disabled) {
			$pick_up_type = 1;
		}

		$pickups_collection = file_get_contents("https://go.venipak.lt/ws/get_pickup_points?country=" . $country_info['iso_code_2'] . "&pick_up_type=" . $pick_up_type);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput($pickups_collection);
    }
    
    public function settings() {
        $settings = array(
        	"is_map_enabled" => $this->config->get('shipping_venipak_shipping_is_map_enabled'),
        	"googlemap_api_key" => $this->config->get('shipping_venipak_shipping_google_api_key'),
					"is_clusters_enabled" => $this->config->get('shipping_venipak_shipping_is_clusters_enabled'),
					"is_pickup_type_locker_enabled" => empty($is_locker_disabled),
					"is_pickup_type_pickup_enabled" => empty($is_pickup_disabled),
        	"locker_marker" => '/image/catalog/venipak_shipping/locker-marker.svg',
        	"pickup_marker" => '/image/catalog/venipak_shipping/pickup-marker.svg',
        );

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($settings));
	}
}
