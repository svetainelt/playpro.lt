<?php  

function pre($data = array()) {
    echo '<pre>'; print_r($data); echo '<pre>';
}

/*
 * 
 * IMPORT VERSION 1.0
 * OC VERSION 1.5.6.x --> 2.0 
 * 
 */
 
 // istrinti naujam orders: payment_company_id,payment_tax_id

class ControllerCronImport extends Controller {

        private $debug = true;
        private $compare_tables = 0;
        private $compare_columns = 0;
        
        private $tables = array(
//            // BUTINI - gali tureti daug irasu
//            'address', // BUTINAS - viskas atitinka - gali tureti daug irasu
           // 'customer', // BUTINAS - sename nera salt laukelio, del to reik kad visi userei pasikeistu password'us
           // 'product', // BUTINAS - viskas atitinka (naujame OC keli laukai dateti nauji, bet jie nenaudojami) - gali turetu daug irasu
           // 'product_image', // BUTINAS - viskas atitinka - gali turetu daug irasu
           // 'product_option', // BUTINAS - viskas atitinka - gali turetu daug irasu
           // 'product_option_value', // BUTINAS - viskas atitinka - gali turetu daug irasu
           // 'product_to_category', // BUTINAS - viskas atitinka - gali turetu daug irasu
          // 'product_description', // BUTINAS - viskas atitinka - gali turetu daug irasu - PAPILDOMAS product_tag - dazniausiai nenaudojamas
           //'product_related', // BUTINAS - viskas atitinka
           //'product_reward', // BUTINAS - viskas atitinka - nezinau ar reikia
          // 'product_special', // BUTINAS - viskas atitinka
          //'product_to_store', // BUTINAS - viskas atitinka
           // 'information', // BUTINAS - viskas atitinka
           // 'information_description', // BUTINAS - viskas atitinka
           // 'information_to_store', // BUTINAS - viskas atitinka
           // 'manufacturer', // BUTINAS - viskas atitinka
           // 'manufacturer_to_store', // BUTINAS - viskas atitinka
//            
//            // BUTINI - turi labai daug irasu
			// 'order_option', // BUTINAS - viskas atitinka - gali turetu daug irasu (1.4.9): naikinam 'prefix' ir 'price' stulp.
            // 'order', // BUTINAS - viskas atitinka - gali turetu daug irasu
            // 'order_product', // BUTINAS - viskas atitinka - gali turetu daug irasu
            // 'order_total', // BUTINAS - viskas atitinka - gali turetu daug irasu
           // 'category_description', // BUTINAS - viskas atitinka
           // 'category_to_store', // BUTINAS - viskas atitinka
           // 'category', // BUTINAS - viskas atitinka, PAPILDOMAS category_path
          // 'coupon', // BUTINAS - viskas atitinka
          // 'coupon_history', // BUTINAS - viskas atitinka // ner tokio
          // 'coupon_product', // BUTINAS - viskas atitinka
          // 'review', // BUTINAS - viskas atitinka
            
//            // BUTINI
//           'currency', // BUTINAS - viskas atitinka
//           'customer_group', // BUTINAS - viskas atitinka
//           'customer_group_description', // BUTINAS - viskas atitinka
//           'customer_ip', // BUTINAS - viskas atitinka

         // 'language', // BUTINAS - viskas atitinka
//           'length_class', // BUTINAS - viskas atitinka
//           'length_class_description', // BUTINAS - viskas atitinka
//           'option', // BUTINAS - viskas atitinka
//           'option_description', // BUTINAS - viskas atitinka
//           'option_value', // BUTINAS - viskas atitinka
//           'option_value_description', // BUTINAS - viskas atitinka
//           'order_status', // BUTINAS - viskas atitinka
//           'order_voucher', // BUTINAS - viskas atitinka
 //          'product_attribute', // BUTINAS - nenaudojamas, palikti del to, kad butu padaryta TRUNCATE !!! Dazniausiai tuscias
//           'product_discount', // BUTINAS - viskas atitinka
// 			'return',  // BUTINAS - viskas atitinka
// 			'return_action', // BUTINAS - viskas atitinka
//			'return_history', // BUTINAS - viskas atitinka
// 			'return_reason', // BUTINAS - viskas atitinka
// 			'return_status', // BUTINAS - viskas atitinka
//           'stock_status', // BUTINAS - viskas atitinka
//           'tax_class',  // BUTINAS - viskas atitinka
//           'tax_rate', // BUTINAS - viskas atitinka
//           'tax_rule', // BUTINAS - viskas atitinka
//           'url_alias', // BUTINAS - viskas atitinka
//           'voucher', // BUTINAS - viskas atitinka
//           'voucher_history', // BUTINAS - viskas atitinka
//           'voucher_theme', // BUTINAS - viskas atitinka
//           'voucher_theme_description', // BUTINAS - viskas atitinka
//           'weight_class', // BUTINAS - viskas atitinka
//           'weight_class_description', // BUTINAS - viskas atitinka        
//            
//            // BUTINI - CUSTOM
//           'banners', // BUTINAS: TABLES - banner, banner_image, banner_image_description
//           'customer_ban_ip', // BUTINAS - sename kitokie lenteles ir laukeliu pavadinimai
//            
//            
//            // NEREIKALINGI, nes nera naudojami
//            'affiliate', // NEREIK - nenaudojamas
//            'affiliate_transaction', // NEREIK - nenaudojamas
//            'attribute', // NEREIK - nenaudojamas
//            'attribute_description', // NEREIK - nenaudojamas
//            'attribute_group', // NEREIK - nenaudojamas
//            'attribute_group_description', // NEREIK - nenaudojamas
//            'download_description', // NEREIK - nenaudojamas
//            'filter', // NEREIK - nenaudojamas
//            'filter_description', // NEREIK - nenaudojamas
//            'filter_group', // NEREIK - nenaudojamas
//            'filter_group_description', // NEREIK - nenaudojamas
//            'customer_reward', // NEREIK - nenaudojamas
//            'customer_transaction', // NEREIK - nenaudojamas
//            'download', // NEREIK - nenaudojamas
//            'order_download', // NEREIK - nenaudojamas
//            'order_fraud', // NEREIK - nenaudojamas
//            'product_to_download', // NEREIK - nenaudojamas
//            'product_to_layout', // NEREIK - nenaudojamas
//            'store', // NEREIK - nenaudojamas
//
//            // NEREIKALINGI - YRA TIK NAUJAME OC
           // 'category_filter', // NEREIK - sename OC nera
//            'category_to_layout', // NEREIK - sename OC nera
//            'coupon_category', // NEREIK - sename OC nera
//            'custom_field', // NEREIK - sename OC nera
//            'custom_field_description', // NEREIK - sename OC nera
//            'custom_field_to_customer_group', // NEREIK - sename OC nera
//            'custom_field_value', // NEREIK - sename OC nera
//            'custom_field_value_description', // NEREIK - sename OC nera
//            'customer_field', // NEREIK - sename OC nera
//            'customer_history', // NEREIK - sename OC nera
//            'customer_online', // NEREIK - sename OC nera
//            'information_to_layout', // NEREIK - sename OC nera
//            'openbay_faq', // NEREIK - sename OC nera
//            'order_field', // NEREIK - sename OC nera
//            'order_recurring', // NEREIK - sename OC nera
//            'order_recurring_transaction', // NEREIK - sename OC nera
//            'product_filter', // NEREIK - sename OC nera
//            'product_profile', // NEREIK - sename OC nera
//            'product_recurring', // NEREIK - sename OC nera
//            'profile', // NEREIK - sename OC nera
//            'profile_description', // NEREIK - sename OC nera
//            'tax_rate_to_customer_group', // NEREIK - sename OC nera
//
//            // NEREIKALINGI
//            'layout', // NEREIK - nebent buvo naudota kazkas CUSTOM
//            'layout_route', // NEREIK - nebent buvo naudota kazkas CUSTOM
//            'user_group', // NEREIK - turetu visur buti default, del visa ko issaugoti per naujo paciam
//            'user', // NEREIK - viskas atitinka - sename nera salt - reik pasikeist password
//
//                        
//            // KA DARYT???
//            'extension', // ? irasysim modulius patys
//            'setting', // ? supildysim nustatymus patys
//            'country', // ?
//            'geo_zone', //  ?
//            'zone', // ?
//            'zone_to_geo_zone', // ?
        );
        
	public function index() {            
            $this->load->model('cron/import');

            // palyginame kurie table skiriasi
            if($this->compare_tables) {
                $this->diffTables();
            }
            
            foreach($this->tables as $table) {
                if($this->debug) $this->log->write('');

                if($table == 'banners') {
                    $this->importBanners();
                } elseif($table == 'customer_ban_ip') {
                    $this->importCustomerBanIp();
                } elseif($table == 'coupon') {
                    $this->importCoupons();
                } else {
                    $table_data = $this->importTables($table);
                    
                    if($table == 'category') {
                        $this->importCategoryPath($table_data);
                    }
                }
            }

            die();
  	}
        
        private function importTables($table) {
            // ========== patikriname kurie column skiriasi ==========   
            if($this->compare_columns) $this->diffColumns($table);
                   
                    
            // ========== paimame seno OC lenteles duomenis ========== 
            $data = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . $table);
			
			foreach ($data as $key => $value ) {
				if ( $table=='product_option' ) {
					$oldfield='option_value';
					$newfield='value';
					unset($data[$key][$oldfield]);
					$data[$key][$newfield]=$value[$oldfield];
				}
				if ( $table=='order' ) { // only unset
					unset($data[$key]['payment_tax_id']);
					unset($data[$key]['payment_company_id']);
				}
				if ( $table=='language' ) { // only unset
					unset($data[$key]['filename']);
				}
				if ( $table=='order_total' ) {
					$oldfield='text';
					$newfield='title';
					unset($data[$key][$oldfield]);
					$data[$key][$newfield]=$value[$oldfield];
				}
			}
			
			// echo "<pre>"; print_r( $data ); die();
			
            if($this->debug) $this->log->write($table . ' ROWS: ' . count($data));

            
            // ========== triname senus duomenis ========== 
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . $table);
            if($this->debug) $this->log->write($table . ' DELETED');


            // ========== insertiname naujus duomenis ========== 
            if(empty($data)) {
                if($this->debug) $this->log->write($table . ' EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . $table, $data);

                if($this->debug) {
                    $this->countInsertedRows($table, $data);
                } 
            }
            
            return $data;
        }
        
        private function importOption() { //Nebaigtas pilnai
        //// Pritaikyta tik 1 varaicijai (pvz tik "Spalvai")
        //
        //// ========= Sukeliama variaciju informacija =======
//            $ovs = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . 'product_option_value_description');
//            
//            $options=array();
//            foreach($ovs as $ov) {
//                $options[] = trim(strtolower($ov['name']));
//            }
//            
//            $options = array_unique($options, SORT_REGULAR);
//            
//            if (isset($options)) {
//                    foreach ($options as $op) {
//                            $option_id = 5; // Keiciam i option id (pvz. "Spalvos" id)
//                            $language_id = 1;
//                            $name = ucfirst($op);
////                            echo $name."<br>";
//                            $option_value_id = $this->importGetOptionValue($name);
//                            if ($option_value_id <= 0) {
//                                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode('', ENT_QUOTES, 'UTF-8')) . "', sort_order = '0'");
//
//                                $option_value_id = $this->db->getLastId();
//                                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($name) . "'");
//                            }
//                            
//                    }
//            }
////            ============
        
            $ovs2 = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . 'product_option_value_description');
            
            $myOptions=array();
            foreach($ovs2 as $ov) {
                $name = trim(strtolower($ov['name']));
                $myOptions[] = array(
                    'product_id' => $ov['product_id'],
                    'product_option_value_id' => $ov['product_option_value_id'],
                    'option_value_id' => $this->importGetOptionValue($name)
                );
            }
            
            foreach ($myOptions as $opt) {
                $this->db->query("UPDATE `product_option_value` SET option_value_id = '". $opt['option_value_id'] ."' WHERE product_id = '". $opt['product_id'] ."' AND product_option_value_id = '". $opt['product_option_value_id'] ."'");
                echo "UPDATE `product_option_value` SET option_value_id = '". $opt['option_value_id'] ."' WHERE product_id = '". $opt['product_id'] ."' AND product_option_value_id = '". $opt['product_option_value_id'] ."'<br>";
            }
            
//            echo "<pre>"; print_r($myOptions);
        }
        private function importGetOptionValue($name) { //Nebaigtas pilnai 
            $name = trim($name);
            $query = $this->db->query("SELECT option_value_id FROM `" . DB_PREFIX . "option_value_description` WHERE name LIKE '". ucfirst($name) ."' AND language_id=1");
//            echo "SELECT option_value_id FROM `" . DB_PREFIX . "option_value_description` WHERE name LIKE '". ucfirst($name) ."' AND language_id=1"; die();
            
            if($query->num_rows > 0) {
                return $query->row['option_value_id'];
            } else {
                return 0;
            }
        }
        
        private function importBanners() {
            // ========== patikriname kurie column skiriasi ========== 
            
            if($this->compare_columns) {
                $this->diffColumns('banner');
                $this->diffColumns('banner_image');
                $this->diffColumns('banner_image_description');
            }
            
            // ========== paimame seno OC lenteles duomenis ========== 
            
            $banner = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . 'banner');
            if($this->debug) $this->log->write('banner ROWS: ' . count($banner));
            
            $banner_image = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . 'banner_image');
            if($this->debug) $this->log->write('banner_image ROWS: ' . count($banner_image));
            
            $banner_image_description = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . 'banner_image_description');
            if($this->debug) $this->log->write('banner_image_description ROWS: ' . count($banner_image_description));


            // ========== triname senus duomenis ========== 
            
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . 'banner');
            if($this->debug) $this->log->write('banner DELETED');
            
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . 'banner_image');
            if($this->debug) $this->log->write('banner_image DELETED');
            
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . 'banner_image_description');
            if($this->debug) $this->log->write('banner_image_description DELETED');


            // ========== susidedame naujus duomenis ========== 
            
            $banner_image_data = array();
            $banner_image_description_data = array();
            
            foreach($banner_image as $banner_i) {
                $banner_image_data[] = array(
                    'banner_image_id' => $banner_i['banner_image_id'],
                    'banner_id' => $banner_i['banner_id'],
                    'sort_order' => 0
                );
                
                foreach($banner_image_description as $banner_d) {
                    if($banner_i['banner_image_id'] == $banner_d['banner_image_id']) {
                        $banner_image_description_data[] = array(
                            'banner_image_id' => $banner_d['banner_image_id'],
                            'language_id' => $banner_d['language_id'],
                            'banner_id' => $banner_d['banner_id'],
                            'title' => $banner_d['title'],
                           // 'link' => $banner_i['link'],
                           // 'image' => $banner_i['image'],
                        );
                    }
                }
            }

            // ========== insertiname naujus duomenis ========== 
                
            if(empty($banner)) {
                if($this->debug) $this->log->write('banner EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . 'banner', $banner);
                if($this->debug) $this->countInsertedRows('banner', $banner);
            }
            
            if(empty($banner_image_data)) {
                if($this->debug) $this->log->write('banner_image EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . 'banner_image', $banner_image_data);
                if($this->debug) $this->countInsertedRows('banner_image', $banner_image_data);
            }
            
            if(empty($banner_image_description_data)) {
                if($this->debug) $this->log->write('banner_image_description EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . 'banner_image_description', $banner_image_description_data);
                if($this->debug) $this->countInsertedRows('banner_image_description', $banner_image_description_data);
            }
        }
        
        private function importCategoryPath($categories = array()) {
            $table = 'category_path';

            // ========== triname senus duomenis ========== 
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . $table);
            if($this->debug) $this->log->write($table . ' DELETED');
            
            
            // ========== susidedame naujus duomenis ==========
            $category_path = array();
            foreach($categories as $category) {
                $category_path[] = array(
                    'category_id' => $category['category_id'],
                    'path_id' => $category['category_id'],
                    'level' => 0
                );
            }
            
            // ========== insertiname naujus duomenis ==========
            if(empty($category_path)) {
                if($this->debug) $this->log->write($table . ' EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . $table, $category_path);
                if($this->debug) $this->countInsertedRows($table, $category_path);
            }
        }
        
        private function importCoupons() { /* Raimis */
            $table = 'coupon';

            // ========== paimame seno OC lenteles duomenis ========== 
            
            $old_coupons = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . $table);
            if($this->debug) $this->log->write('coupon ROWS: ' . count($old_coupons));
            
            // ========== triname senus duomenis ========== 
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . $table);
            if($this->debug) $this->log->write($table . ' DELETED');
            
            
            // ========== susidedame naujus duomenis ==========
            $coupons_data = array();
            $i=0;
            foreach($old_coupons as $coupon) {
                foreach($coupon as $coupon_key => $coupon_value) {
                    $coupons_data[$i][$coupon_key] = $coupon_value;
                }
                $i++;
            }
            
            //var_dump($coupons_data); die();
            
            // ========== insertiname naujus duomenis ==========
            if(empty($coupons_data)) {
                if($this->debug) $this->log->write($table . ' EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . $table, $coupons_data);
                if($this->debug) $this->countInsertedRows($table, $coupons_data);
            }
        }
        
        private function importCustomerBanIp() {
            $new_table = 'customer_ban_ip';
            $old_table = 'customer_ip_blacklist';

            // ========== patikriname kurie column skiriasi ========== 
            if($this->compare_columns) $this->diffColumns($new_table, $old_table);
            
            
            // ========== paimame seno OC lenteles duomenis ========== 
            $customer_blacklist_ips = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . $old_table);
            if($this->debug) $this->log->write($old_table . ' ROWS: ' . count($customer_blacklist_ips));

            
            // ========== triname senus duomenis ========== 
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . $new_table);
            if($this->debug) $this->log->write($new_table . ' DELETED');
            
            
            // ========== susidedame naujus duomenis ==========
            $customer_ban_ips = array();
            foreach($customer_blacklist_ips as $customer) {
                $customer_ban_ips[] = array(
                    'customer_ban_ip_id' => $customer['customer_ip_blacklist_id'],
                    'ip' => $customer['ip']
                );
            }
            
            // ========== insertiname naujus duomenis ==========
            if(empty($customer_ban_ips)) {
                if($this->debug) $this->log->write($old_table . ' EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . $new_table, $customer_ban_ips);
                if($this->debug) $this->countInsertedRows($new_table, $customer_ban_ips);
            }
        }
        
        private function importProductDescription() {
            $desc_table = 'product_description';
            
            // ========== paimame seno OC lenteles duomenis ========== 
            $product_description = $this->model_cron_import->getTableData($this->old_db, OLD_DB_PREFIX . $desc_table);
            if($this->debug) $this->log->write($desc_table . ' ROWS: ' . count($product_description));

            // ========== triname senus duomenis ========== 
            $this->model_cron_import->truncateTable($this->db, DB_PREFIX . $desc_table);
            if($this->debug) $this->log->write($desc_table . ' DELETED');
            
            // ========== insertiname naujus duomenis ==========
            if(empty($product_description)) {
                if($this->debug) $this->log->write($product_description . ' EMPTY');
            } else {
                $this->model_cron_import->insert_batch($this->db, DB_PREFIX . $desc_table, $product_description);
                if($this->debug) $this->countInsertedRows($desc_table, $product_description);
            }
        }
        
        private function diffTables() {
            $this->log->write('');

            $tables = $this->model_cron_import->getTableNames($this->db, DB_DATABASE);
            $old_tables = $this->model_cron_import->getTableNames($this->old_db, OLD_DB_DATABASE);

            $current_t = $this->model_cron_import->diffrentArrays($tables, $old_tables);
            $old_t = $this->model_cron_import->diffrentArrays($old_tables, $tables);

            if($current_t)
                $this->log->write('DIFFRENT TABLES IN NEW DB: ' . implode(',', $current_t));
            if($old_t)
                $this->log->write('DIFFRENT TABLES IN OLD DB: ' . implode(',', $old_t));

            die('LENTELIU PALYGINIMAS DONE');
        }
        
        private function diffColumns($table = '', $old_table = '') {
            if(!$old_table)
                $old_table = $table;
            
            $columns = $this->model_cron_import->getTableColumnNames($this->db, DB_PREFIX . $table);
            $old_columns = $this->model_cron_import->getTableColumnNames($this->old_db, OLD_DB_PREFIX . $old_table);
            
//            $this->log->write($table . ' COLUMNS: ' . implode(',', $columns));
                      
            $current_c = $this->model_cron_import->diffrentArrays($columns, $old_columns);
            $old_c = $this->model_cron_import->diffrentArrays($old_columns, $columns);

            if($current_c)
                $this->log->write($table . ' DIFFRENT COLUMNS IN NEW DB: ' . implode(',', $current_c));
            if($old_c)
                $this->log->write($old_table . ' DIFFRENT COLUMNS IN OLD DB: ' . implode(',', $old_c));

            die('STULPELIU PALYGINIMAS DONE');
        }
        
        private function countInsertedRows($table = '', $data = array()) {
            $reset_data = reset($data);
            $first_key = key($reset_data);
            $rows_num = $this->model_cron_import->countTableRows($this->db, DB_PREFIX . $table, $first_key);
            $this->log->write($table . ' INSERTED ROWS COUNT: ' . $rows_num);
        }
}