<?php
class ModelModuleGiftTeaser extends Model {
	private $moduleName;
	private $moduleNameSmall;
	private $modulePath;

	public function __construct($registry) {
		parent::__construct($registry);

        // Config Loader
		$this->config->load('isenselabs/giftteaser');

        // Module Constants
		$this->moduleName           = $this->config->get('giftteaser_name');
		$this->moduleNameSmall      = $this->config->get('giftteaser_name_small');
		$this->modulePath           = $this->config->get('giftteaser_path');

        // Load Language
		$this->load->language($this->modulePath);
	}

	public function getCurrentGifts() {
		$now = time();
		$gifts = $this->db->query('SELECT *, gt.description AS description ' . 
			'FROM `'. DB_PREFIX . 'gift_teaser` gt ' .
			'JOIN `' . DB_PREFIX . 'product` AS p ON gt.item_id=p.product_id ' .
			'JOIN `' . DB_PREFIX . 'product_description` AS pd ON p.product_id=pd.product_id ' .
			'WHERE start_date<' . $now . 
			' AND end_date>' . $now .
			' AND store_id=' . $this->config->get('config_store_id') .
			' AND language_id=' . $this->config->get('config_language_id') .
			' ORDER BY gt.sort_order ASC'
		)->rows;
		$this->load->model('tool/image');
		$setting = $this->getGiftsSettings();
		if (isset($setting['giftteaser'])) {
			$setting = $setting['giftteaser'];
		} else {
			return array();
		}
		foreach ($gifts as &$gift) {
			$gift['description'] = $this->buildLocalizedDescription($gift);
			$gift['condition_properties'] = unserialize($gift['condition_properties']);
			$gift['url'] = $this->url->link("product/product&product_id=".$gift['product_id']);
			$gift['image'] = $this->model_tool_image->resize($gift['image'], $setting['giftImageWidth'], $setting['giftImageHeight']);
		}
		return $gifts;
	}

	public function buildLocalizedDescription($gift) {
		$properties = unserialize($gift['condition_properties']);
		$descriptions = unserialize(base64_decode($gift['description']));
		$description = html_entity_decode($descriptions['desc_' . $this->config->get('config_language')]);

		if ($description) {
			$search = array();
			$replace = array();
			$currency_value = (float)$this->currency->getValue($this->session->data['currency']);

			switch ($gift['condition_type']) {
				case '1': // Cart total
					$total_min = $this->currency->format((float)$properties['total'] * $currency_value, $this->session->data['currency']);
					$total_max = $this->currency->format((float)$properties['total_max'] * $currency_value, $this->session->data['currency']);

					$search = array('{total_min}', '{total_max}');
					$replace = array($total_min, $total_max);
					break;

				case '2': // All products
					$results = $this->db->query(
						"SELECT * FROM `" . DB_PREFIX . "product_description` 
						WHERE product_id IN (" . implode(',', $properties['certain']) . ") 
							AND language_id=" . $this->config->get('config_language_id')
					);

					$lists = array();
					foreach ($results->rows as $result) {
						$lists[] = '<a href="' . $this->url->link('product/product', 'product_id=' . $result['product_id'], 'SSL') . '">' . $result['name'] . '</a>';
					}

					$product_qty = isset($properties['certain_product_quantity']) ? $properties['certain_product_quantity'] : 1;

					$search = array('{products}', '{product_qty}');
					$replace = array(implode(', ', $lists), $product_qty);
					break;

				case '3': // Some products
					$results = $this->db->query(
						"SELECT * FROM `" . DB_PREFIX . "product_description` 
						WHERE product_id IN (" . implode(',', $properties['some']) . ") 
							AND language_id=" . $this->config->get('config_language_id')
					);

					$lists = array();
					foreach ($results->rows as $result) {
						$lists[] = '<a href="' . $this->url->link('product/product', 'product_id=' . $result['product_id'], 'SSL') . '">' . $result['name'] . '</a>';
					}

					$total_min = $this->currency->format((float)$properties['some_product_min_total_price'] * $currency_value, $this->session->data['currency']);
					$product_qty = isset($properties['some_product_quantity']) ? $properties['some_product_quantity'] : 1;

					$search = array('{min_total_price}', '{products}', '{product_qty}');
					$replace = array($total_min, implode(', ', $lists), $product_qty);
					break;

				case '4': // Categories
					$results = $this->db->query(
						"SELECT * FROM `" . DB_PREFIX . "category_description` 
						WHERE category_id IN (" . implode(',', $properties['categories']) . ") 
							AND language_id=" . $this->config->get('config_language_id')
					);

					$lists = array();
					foreach ($results->rows as $result) {
						$lists[] = '<a href="' . $this->url->link('product/category', 'category_id=' . $result['category_id'], 'SSL') . '">' . $result['name'] . '</a>';
					}

					$total_min = $this->currency->format((float)$properties['category_min_total_price'] * $currency_value, $this->session->data['currency']);
					$product_qty = isset($properties['category_quantity']) ? $properties['category_quantity'] : 1;

					$search = array('{min_total_price}', '{categories}', '{product_qty}');
					$replace = array($total_min, implode(', ', $lists), $product_qty);
					break;

				case '5': // Manufacturers
					$results = $this->db->query(
						"SELECT * FROM `" . DB_PREFIX . "manufacturer` 
						WHERE manufacturer_id IN (" . implode(',', $properties['manufacturer']) . ")"
					);

					$lists = array();
					foreach ($results->rows as $result) {
						$lists[] = '<a href="' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'], 'SSL') . '">' . $result['name'] . '</a>';
					}

					$total_min = $this->currency->format((float)$properties['manufacturer_min_total_price'] * $currency_value, $this->session->data['currency']);
					$product_qty = isset($properties['manufacturer_quantity']) ? $properties['manufacturer_quantity'] : 1;

					$search = array('{min_total_price}', '{manufacturers}', '{product_qty}');
					$replace = array($total_min, implode(', ', $lists), $product_qty);
					break;
			}

			$description = str_replace($search, $replace, $description);
		} else {
			$description = $this->language->get('no_information');
		}

		return $description;
	}

	public function fetchStoredCartGifts() {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();

		$gifts = $this->db->query('SELECT cg.*, gt.condition_type, gt.condition_properties, gt.item_id as product_id FROM ' . DB_PREFIX . 'cart_gift cg ' .
			'LEFT JOIN `' . DB_PREFIX . 'gift_teaser` AS gt ON (cg.gift_id = gt.gift_id) ' .
			'WHERE api_id = "' . $apiId . '" ' .
			'AND customer_id = "' . $customerId . '" ' .
			'AND session_id = "' . $sessId . '" ' .
			'ORDER BY gift_id ASC'
		)->rows;
		foreach ($gifts as &$gift) {
			if ($gift['option'] != '[]') {
				$gift['option'] = json_decode($gift['option'], true);
			}
			$gift['condition_type'] = (int)$gift['condition_type'];
			$gift['condition_properties'] = unserialize($gift['condition_properties']);
		}
		return $gifts;
	}

	public function deleteStoredCartGifts() {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();

		$this->db->query('DELETE FROM `' . DB_PREFIX . 'cart_gift` ' .
			'WHERE api_id = "' . $apiId . '" ' .
			'AND customer_id = "' . $customerId . '" ' .
			'AND session_id = "' . $sessId . '"'
		);
	}

	public function deleteStoredGift($gift_id) {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();

		$this->db->query('DELETE FROM `' . DB_PREFIX . 'cart_gift` ' .
			'WHERE api_id = "' . $apiId . '" ' .
			'AND customer_id = "' . $customerId . '" ' .
			'AND session_id = "' . $sessId . '" ' .
			'AND gift_id = ' . $gift_id
		);
	}

	public function storeCartGifts($cartGifts) {
		$giftCount = count($cartGifts);
		if (count($cartGifts) == 0) return;
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();
		$query = 'INSERT INTO `' . DB_PREFIX . 'cart_gift` (`api_id`, `customer_id`, `session_id`, `gift_id`, `requires_options`, `option`, `quantity`, `date_added`) VALUES ';
		for ($i = 0; $i < $giftCount; ++$i) {
			$cg = &$cartGifts[$i];
			$query .= '("' . $apiId . '", ' .
				$customerId . ', "' .
				$sessId . '", ' .
				$cg['gift_id'] . ', ' .
				($cg['requires_options'] ? 1 : 0) . ', "' .
				$this->db->escape($cg['option']) . '", ' .
				$cg['quantity'] . ', ' .
				'NOW()' .
			')';
			if ($i != ($giftCount-1)) {
				$query .= ',';
			}
		}
		$this->db->query($query);
	}

	public function updateCartGiftQuantity($cartGiftId, $newQuantity) {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();
		$this->db->query('UPDATE `' . DB_PREFIX . 'cart_gift` SET `quantity` = ' . (int)$newQuantity . ' WHERE ' .
			'`gift_id` = ' . $cartGiftId . ' AND ' .
			'`api_id` = "' . $apiId . '" ' .
			'AND `customer_id` = "' . $customerId . '" ' .
			'AND `session_id` = "' . $sessId . '"'
		);
	}

	public function updateCartGiftOptions($cartGiftId, $options) {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();
		$this->db->query('UPDATE `' . DB_PREFIX . 'cart_gift` SET `option` = "' . $this->db->escape(json_encode($options)) . '" WHERE ' .
			'`gift_id` = ' . $cartGiftId . ' AND ' .
			'`api_id` = "' . $apiId . '" ' .
			'AND `customer_id` = "' . $customerId . '" ' .
			'AND `session_id` = "' . $sessId . '"'
		);
	}

	public function getGift($giftId) {
		return $this->db->query('SELECT * FROM `' . DB_PREFIX . 'gift_teaser` WHERE gift_id = ' . $this->db->escape($giftId))->row;
	}

	// retrieves gifts from the user's cart that require options but have not had them setup
	public function getGiftsThatStillRequireOptions() {
		$sessId = $this->db->escape($this->session->getId());
		$apiId = (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0);
		$customerId = (int)$this->customer->getId();
		return $this->db->query('SELECT cg.*, gt.item_id as product_id FROM `' . DB_PREFIX . 'cart_gift` cg ' .
			'LEFT JOIN `' . DB_PREFIX . 'gift_teaser` AS gt ON (cg.gift_id = gt.gift_id) WHERE ' .
			'`api_id` = "' . $apiId . '" ' .
			'AND `customer_id` = "' . $customerId . '" ' .
			'AND `session_id` = "' . $sessId . '"' . 
			'AND `requires_options` = 1 ' .
			'AND `option` = "[]"'
		)->rows;
	}

	public function getGiftsSettings() {
		$this->load->model('setting/setting');
		$settings = $this->model_setting_setting->getSetting($this->moduleName, $this->config->get('config_store_id'));
		return $settings;
	}

	public function cartProductsInGiftCategories($product_ids, $category_ids, $scope = 'include') {
		if (empty($category_ids) || empty($product_ids)) {
			return false;
		}

		$scope = $scope == 'include'? ' IN ' : ' NOT IN ';
		$results = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_to_category` WHERE category_id " . $scope . " (" . implode(',', $category_ids) . ") AND product_id IN (" . implode(',', $product_ids) . ")");

		$products = array();
		foreach ($results->rows as $result) {
			$products[] = $result['product_id'];
		}

		return $products;
	}

	public function categoriesHaveProducts($category_ids, $product_ids) {
		if (empty($category_ids) || empty($product_ids)) {
            return false;
        }

		$sql = "SELECT COUNT(*) as count FROM `" . DB_PREFIX . "product_to_category` WHERE category_id IN (" . implode(',', $category_ids) . ") AND product_id IN (" . implode(',', $product_ids) . ")";

		$result = $this->db->query($sql);

		return (int)$result->row['count'] > 0;
	}

	public function cartProductsInGiftManufacturers($product_ids, $manufacturer_ids, $scope = 'include') {
		if (empty($manufacturer_ids) || empty($product_ids)) {
			return false;
		}

		$scope = $scope == 'include'? ' IN ' : ' NOT IN ';
		$results = $this->db->query("SELECT product_id FROM `" . DB_PREFIX . "product` WHERE manufacturer_id " . $scope . " (" . implode(',', $manufacturer_ids) . ") AND product_id IN (" . implode(',', $product_ids) . ")");

		$products = array();
		foreach ($results->rows as $result) {
			$products[] = $result['product_id'];
		}

		return $products;
	}

	public function manufacturersHaveProducts($manufacturer_ids, $product_ids) {
		if (empty($manufacturer_ids) || empty($product_ids)) {
			return false;
		}
		$sql = "SELECT COUNT(*) as count FROM `" . DB_PREFIX . "product` WHERE manufacturer_id IN (" . implode(',', $manufacturer_ids) . ") AND product_id IN (" . implode(',', $product_ids) . ")";

		$result = $this->db->query($sql);
		return (int)$result->row['count'] > 0;
	}

	public function checkGift($product_id) {
		$active_gift = $this->db->query("SELECT *
			FROM `". DB_PREFIX . "gift_teaser`
			WHERE item_id = '".(int)$product_id."'
			AND start_date<" . time(). "
			AND end_date>" . time() ."
			AND store_id='" . $this->config->get('config_store_id') . "'");

		return $active_gift->row;
	}

}
