<?php
class ModelModuleNotifyWhenAvailable extends Model {

	public function add_notify_request($data = array()) {

		$check_exist = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "notify_when_available WHERE name = '".$this->db->escape($data['name'])."' AND email = '".$this->db->escape($data['email'])."' AND product_id = '".$this->db->escape($data['product_id'])."' AND customer_id = '".$this->db->escape($data['customer_id'])."'");

		if(!$check_exist->row['total']){
			$this->db->query("INSERT INTO " . DB_PREFIX . "notify_when_available SET name = '".$this->db->escape($data['name'])."', email = '".$this->db->escape($data['email'])."', product_id = '".$this->db->escape($data['product_id'])."', message = '".$this->db->escape($data['message'])."', customer_id = '".$this->db->escape($data['customer_id'])."', language_id = '" . (int)$this->config->get('config_language_id')."', store_id = '" . (int)$this->config->get('config_store_id'). "', date_added = NOW()");
		}else{
			$this->db->query("UPDATE " . DB_PREFIX . "notify_when_available SET name = '".$this->db->escape($data['name'])."', email = '".$this->db->escape($data['email'])."', message = '".$this->db->escape($data['message'])."', date_added = NOW() WHERE customer_id = '".$this->db->escape($data['customer_id'])."' AND product_id = '".$this->db->escape($data['product_id'])."'");
		}

	}

	public function remove_notify_request($product_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "notify_when_available WHERE product_id = '".(int)$product_id."' AND customer_id = '".(int)$this->customer->getId() ."'");
	}

	public function getNotifyProducts($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "notify_when_available` WHERE customer_id = '" . (int)$this->customer->getId() . "'";

		$sort_data = array(
			'amount',
			'description',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalNotifyProducts() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "notify_when_available` WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}

}