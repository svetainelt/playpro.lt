<?php  

class ModelCronImport extends Model {

        /* ========== Paimame visas lenteles ========== */
    
        public function getTableNames($db, $database) {
            $tables = $db->query("SHOW TABLES FROM " . $database);

            $return = array();
            
            if($tables->num_rows > 0) {
                foreach($tables->rows as $key => $array) {
                    foreach($array as $DB => $table) {
                        $return[] = $table;
                    }
                }
            }

            return $return;
        }
        
        /* ========== Paimame lenteleje esancius stulpelius ========== */
    
        public function getTableColumnNames($db, $table) {
            $columns = $db->query("SHOW COLUMNS FROM `" . $table . "`");
            
            $return = array();
            
            if($columns->num_rows > 0) {
                foreach($columns->rows as $row) {
                    $return[] = $row['Field'];
                }
            }
            
            return $return;
        }
        
        /* ========== Paimame duomenis is table ========== */
        
        public function getTableData($db, $table) {
            $query = $db->query("SELECT * FROM `" . $table . "`");
            
            if($query->num_rows > 0) {
                return $query->rows;
            } else {
                return false;
            }
        }
        
        /* ========== Paimame duomenis is table pagal ID ========== */
                /* Raimis */
        public function getTableDataByID($db, $table, $where, $id) {
            $query = $db->query("SELECT * FROM `" . $table . "` WHERE `" . $where . "` = '" . $id . "'");
            
            if($query->num_rows > 0) {
                return $query->row;
            } else {
                return false;
            }
        }

        /* ========== Lenteles isvalymas ========== */
        
        public function truncateTable($db, $table) {
            $db->query("TRUNCATE TABLE `" . $table . "`");
            $db->query("ALTER TABLE `" . $table . "` AUTO_INCREMENT = 1");
        }
        
        /* ========== Skaiciuojame kiek iterptu irasu po insert ========== */
        
        public function countTableRows($db, $table, $id) {
            $query = $db->query("SELECT COUNT(" . $id . ") as number FROM `" . $table . "` LIMIT 1");
            
            if($query->num_rows > 0)
                return $query->row['number'];
            else
                return 0;
        }
        
        /* ========== Dvieju array palyginimas ========== */
        
        public function diffrentArrays($a = array(), $b = array()) {
            $return = array();
            
            foreach($a as $value) {
                if(!in_array($value, $b)) {
                    $return[] = $value;
                }
            }
            
            return $return;
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /* ========== Settings ========== */
    
	public function getSettingValue($group = '', $key = '') {
		return $this->db->query("SELECT setting_id, value FROM " . DB_PREFIX . "setting WHERE `store_id` = '0' AND `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "'");
	}
    
	public function addSettingValue($group = '', $key = '', $value = '') {
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
        }
        
	public function editSettingValue($group = '', $key = '', $value = '') {
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "' WHERE `store_id` = '0' AND `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "'");
	}
        
        public function deleteSettingValue($group = '', $key = '') {
            $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `store_id` = '0' AND `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "'");
        }
        
        /* ========== Currency ========== */
        
	public function addCurrency($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "currency SET title = '" . $this->db->escape($data['title']) . "', code = '" . $this->db->escape($data['code']) . "', symbol_left = '" . $this->db->escape($data['symbol_left']) . "', symbol_right = '" . $this->db->escape($data['symbol_right']) . "', decimal_place = '" . $this->db->escape($data['decimal_place']) . "', value = '" . $this->db->escape($data['value']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW()");

		$this->cache->delete('currency');
	}
        
	public function updateCurrency($currency_code = '', $value = '') {
                $this->db->query("UPDATE " . DB_PREFIX . "currency SET value = '" . $this->db->escape($value) . "', status = '1' WHERE code = '" . $this->db->escape($currency_code) . "'");
	}

        /* ========== Delete system cache files ========== */
        
        function deleteSystemCache() {
            $src = DIR_SYSTEM . 'cache/';
            
            if(!file_exists($src)){
                return false;
            }
            
            $dir = opendir($src);

            while(false !== ( $file = readdir($dir)) ) {
                if (( $file != '.' ) && ( $file != '..' )) {
                    if (strpos($file, 'cache.currency') !== false)
                        unlink($src . '/' . $file);
                }
            }
            
            closedir($dir);
        }
        
        /* ========== Batch insert script ========== */
        
        function insert_batch($db = '', $table = '', $set = NULL) {
            if ($table === '' || is_null($set) || !is_array($set)) {
                return FALSE;
            }

            for ($i = 0, $total = count($set); $i < $total; $i = $i + 100) {
                $arraySlice = array_slice($set, $i, 100);

                $fields = $values = '';

                //field names
                foreach($arraySlice as $arraySet) {
//                    foreach($arraySet as $key => $val) {
                        foreach($arraySet as $k => $v) {
							if($k!='tax_id' && $k!='company_id'){
								$fields .= '`';
								$fields .= $k;
								$fields .= '`';
								$fields .= ', ';
							}
                            
                        }
                        $fields = substr($fields, 0, -2);
//                        break;
//                    }
                    break;
                }

                //field values
                foreach($arraySlice as $arraySet) {
//                    foreach($arraySet as $key => $val) {
                        $values .= ' ( ';
                        foreach($arraySet as $k => $v) {
							if($k!='tax_id' && $k!='company_id'){
								if(is_numeric($v)) {
                                $values .= $v;
                            } else {
                                $values .= '"';
                                $values .= addslashes($v);
                                $values .= '"';
                            }
                            $values .= ', ';
							}
                            
                        }
                        $values = substr($values, 0, -2);
                        $values .= ' ), ';
//                    }
                }

                $values = substr($values, 0, -2); //remove the comma of the last case

                $sql = 'INSERT INTO `';
                $sql .= $table;
                $sql .= '` ( ';
                $sql .= $fields;
                $sql .= ' ) VALUES';
                $sql .= $values;

                $db->query($sql);
            }
        }
        
        /* ========== Batch update script ========== */

        function update_batch($db = '', $table = '', $set = NULL, $index = NULL, $additional_set = array(), $second_index = NULL) {
            if ($table === '' || is_null($set) || is_null($index) || !is_array($set)) {
                return FALSE;
            }

            for ($i = 0, $total = count($set); $i < $total; $i = $i + 100) {
                $arraySlice = array_slice($set, $i, 100);
                $ids = $when = array();
                $cases = '';

                $sql = 'UPDATE `';
                $sql .= $table;
                $sql .= '` SET ';

                //generate the WHEN statements from the set array
                foreach ($arraySlice as $key => $val) {
                    $ids_string = "'";
                    $ids_string .= $val[$index];
                    $ids_string .= "'";

                    $ids[] = $ids_string;

                    if(!is_null($second_index)) {
                        $ids_second_string = "'";
                        $ids_second_string .= $val[$second_index];
                        $ids_second_string .= "'";

                        $ids_second[] = $ids_second_string;
                    }

                    foreach (array_keys($val) as $field) {
                        if ($field != $index && $field != $second_index) {
                            $field_string = 'WHEN ';
                            $field_string .= $index;
                            $field_string .= ' = "';
                            $field_string .= $val[$index];

                            if(!is_null($second_index)) {
                                $field_string .= '" AND ';
                                $field_string .= $second_index;
                                $field_string .= ' = "';
                                $field_string .= $val[$second_index];
                            }

                            $field_string .= '" THEN "';
                            $field_string .= $val[$field];
                            $field_string .= '"';

                            $when[$field][] = $field_string;
                        }
                    }
                }

                //generate the case statements with the keys and values from the when array
                foreach ($when as $k => $v) {
                    $cases .= "\n";
                    $cases .= $k;
                    $cases .= ' = CASE ';
                    $cases .= "\n";

                    foreach ($v as $row) {
                        $cases .= $row;
                        $cases .= "\n";
                    }

                    $cases .= 'ELSE ';
                    $cases .= $k;
                    $cases .= ' END, ';
                 }

                 $sql .= substr($cases, 0, -2); //remove the comma of the last case
                 $sql .= "\n"; //remove the comma of the last case

                 //specific values, for all fields the same
                 if(!empty($additional_set)) {
                     foreach($additional_set as $set_key => $set_val) {
                        $sql .= ', ';
                        $sql .= $set_key;
                        $sql .= ' = ';
                        $sql .= $set_val;
                     }
                 }

                 $sql .= ' WHERE ';
                 $sql .= $index;
                 $sql .= ' IN (';
                 $sql .= implode(',', $ids);
                 $sql .= ')';

                 if(!is_null($second_index)) {
                    $sql .= ' AND ';
                    $sql .= $second_index;
                    $sql .= ' IN (';
                    $sql .= implode(',', $ids_second);
                    $sql .= ')';
                 }

                 $db->query($sql);
            }
        }

}
?>