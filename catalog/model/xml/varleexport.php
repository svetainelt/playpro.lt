<?php 
class ModelXmlVarleexport extends Model {
	public function getCategories() {
		$query = $this->db->query("
				
				SELECT * FROM " . DB_PREFIX . "category c 
				LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
				LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
				WHERE cd.language_id = '" . (int)$this->config->get('varleexport_language_id') . "' 
				AND c2s.store_id IN ('" . implode("','", $this->config->get('varleexport_stores')) . "')
				AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
		
		return $query->rows;
	}
	
	
	public function getProductCategories($product_id, $categories) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id IN ('" . implode("','", $categories) . "') ");
	
		return $query->rows;
	}
	
	public function getProducts($data = array()) {
		
		
		$sql = "SELECT p.product_id ";
	
		if (!empty($data['filter_categories'])) {
			$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
	
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
				WHERE p.status = '1' AND p.quantity > 0 AND p.date_available <= NOW() AND p2s.store_id IN ('" . implode("','", $this->config->get('varleexport_stores')) . "')";
	
		
		// Filter by categories
		if (!empty($data['filter_categories'])) {
			$sql .= " AND p2c.category_id IN ('" . implode("','", $data['filter_categories']) . "')";
		}
	
			
		// Filter by manufacturer		
		if (!empty($data['filter_manufacturers'])) {
			$sql .= " AND p.manufacturer_id IN ('" . implode("','", $data['filter_manufacturers']) . "')";
		}
		
		// Filter by price 
		if (!empty($data['filter_min_price']) AND $data['filter_min_price'] > 0) {
			$sql .= " AND (p.price > '" . $data['filter_min_price'] . "') ";
		}
	
		$sql .= " GROUP BY p.product_id";
	
		$sql .= " ORDER BY p.date_added DESC";
	

		$product_data = array();

		$query = $this->db->query($sql);
	
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
		}
	
		return $product_data;
	}
	
	
	
}
?>