<?php 
class ModelXmlKainosExport extends Model {
	public function getCategories() {
		$query = $this->db->query("
				
				SELECT * FROM " . DB_PREFIX . "category c 
				LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
				LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
				WHERE cd.language_id = '" . (int)$this->config->get('kainos_export_language_id') . "' 
				AND c2s.store_id IN ('" . implode("','", $this->config->get('kainos_export_stores')) . "')
				AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
		
		return $query->rows;
	}
	
	
	public function getProductCategories($product_id, $categories) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND category_id IN ('" . implode("','", $categories) . "') ");
	
		return $query->rows;
	}
	
	public function getProductCategory($product_id) {
		$sql = "SELECT ptc.product_id, cd.name, GROUP_CONCAT(DISTINCT cd.name ORDER BY cp.level SEPARATOR '/') AS name
				FROM " . DB_PREFIX . "product_to_category ptc
				LEFT JOIN " . DB_PREFIX . "category_path cp ON (ptc.category_id = cp.path_id)
				LEFT JOIN " . DB_PREFIX . "category_description cd ON (cp.path_id = cd.category_id)
				WHERE ptc.product_id = ".$product_id."
				GROUP BY ptc.product_id
				ORDER BY cp.level DESC
		";

		$query = $this->db->query($sql);
		if (isset($query->row['name'])) {
			return $query->row['name'];
		}
		return FALSE;
	}
	
	public function getProducts($data = array()) {
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";
	
		if (!empty($data['filter_categories'])) {
			$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}
	
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
				LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
				WHERE pd.language_id = '" . (int)$this->config->get('kainos_export_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id IN ('" . implode("','", $this->config->get('kainos_export_stores')) . "')";
	
		
		// Filter by categories
		if (!empty($data['filter_categories'])) {
			$sql .= " AND p2c.category_id IN ('" . implode("','", $data['filter_categories']) . "')";
		}
	
			
		// Filter by manufacturer
		
		if (!empty($data['filter_manufacturers'])) {
			$sql .= " AND p.manufacturer_id IN ('" . implode("','", $data['filter_manufacturers']) . "')";
		}
		
		// Filter by price 
		if (!empty($data['filter_min_price'])) {
			$sql .= " AND (p.price > '" . $data['filter_min_price'] . "') ";
		}
	
		$sql .= " GROUP BY p.product_id";
	
		$sql .= " ORDER BY p.date_added DESC";
	

		$product_data = array();
		$query = $this->db->query($sql);
	
		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
		}
	
		return $product_data;
	}
	
	
	
}
?>