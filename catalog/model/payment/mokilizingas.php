<?php

class ModelPaymentMokilizingas extends ModelExtensionPaymentMokilizingas { }

class ModelExtensionPaymentMokilizingas extends Model {
    public function addQueryRecord($data)
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "mokilizingas_details` SET 
                          `stamp` = '" . $this->db->escape($data['stamp']) . "', 
                          `id_order` = '" . (int)$data['id_order'] . "', 
                          `sessionId` = '" . $this->db->escape($data['sessionId']) . "', 
                          `errorMessage` = '" . $this->db->escape($data['errorMessage']) . "', 
                          `errorCode` = '" . $this->db->escape($data['errorCode']) . "', 
                          `resultCode` = '" . $this->db->escape($data['resultCode']) . "', 
                          `resultInfo` = '" . $this->db->escape($data['resultInfo']) . "',
                          `resultMsg` = '" . $this->db->escape($data['resultMsg']) . "',
                          `advance` = '" . $this->db->escape($data['advance']) . "',
                          `currency` = '" . $this->db->escape($data['currency']) . "',
                          `contractNo` = '" . $this->db->escape($data['contractNo']) . "',
                          `date_add` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "',
                          `date_upd` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'
                          ");
    }
    public function getOrdersRecords($id_order)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "mokilizingas_details` WHERE `id_order` = '" . (int)$id_order . "' LIMIT 1");
        return $query->row;
    }
    public function executeQueryRecord($data)
    {
        $this->addQueryRecord($data);
    }
    public function updateQueryRecord($data)
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "mokilizingas_details`
                           SET   `sessionId` = '" . (int)$data['sessionId'] . "', `errorMessage` = '" . (int)$data['errorMessage'] . "',   `errorCode` = '" . (int)$data['errorCode'] . "',   `date_upd` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'
                           WHERE `id_order` = '" . $this->db->escape($data['id_order']) . "'");
    }
    
    public function getMokiLizingasData($price, $special)
    {
        if($_SERVER["REMOTE_ADDR"] == '82.135.211.134'){
            return 'yyy';
        }
        
        $data = array();
        
        if ($special) {
            $price = $this->priceToFloat($special);
        } else {
            $price = $this->priceToFloat($price);
        }
        
        $data['price'] = (float)$price;
        $data['calculator'] = $this->getCalculatorKeyById((int)$this->config->get('mokilizingas_calculator_style'));
        $data['term'] = $this->config->get('mokilizingas_term');
        $data['min_price'] = (float)$this->config->get('mokilizingas_minimal_price');
        $data['max_price'] = (float)$this->config->get('mokilizingas_max_price');
        $data['apiKey'] = $this->config->get('mokilizingas_key');
        $data['calculator_color'] = (int)$this->config->get('mokilizingas_calculator_color');
        $data['calculator_show'] = (int)$this->config->get('mokilizingas_calculator');
        $data['show'] = true;
        
        if ($data['price'] < $data['min_price']) {
            $data['show'] = false;
        }
        if ($data['price'] > $data['max_price']) {
            $data['show'] = false;
        }
        if (!$data['apiKey'] || empty($data['apiKey'])) {
            $data['show'] = false;
        }
        
        return $data;
    }
    
    public function priceToFloat($s)
    {
        $s = str_replace(',', '.', $s);
        
        $s = preg_replace("/[^0-9\.]/", "", $s);
        
        $s = str_replace('.', '',substr($s, 0, -3)) . substr($s, -3);
        
        return (float) $s;
    }
    
    public function getCalculatorKeyById($id)
    {
        switch ($id) {
            case 1:
                return 'ib-001';
            case 2:
                return 'ib-002';
            case 3:
                return 'ib-003';
            case 4:
                return 'ib-004';
            default:
                return 'ib-003';
        }
    }
    
    public function getLeasingSessionId($args, $id_order)
    {
        $base = 'https://api.mokilizingas.lt/api2/eshop/auth';
        
        $res = $this->curl($base, $args);
        $res = json_decode($res, true);
        $cont_payment = false;
        $sessionId = '';

        $this->log->write($res);
        
        $queryData = array(
            'stamp' => microtime(true),
            'id_order' => $id_order,
            'sessionId' => $res['sessionId'],
            'errorMessage' => $res['errorMessage'],
            'errorCode' => $res['errorCode'],
            'resultCode' => '',
            'resultInfo' => '',
            'resultMsg' => '',
            'advance' => '',
            'currency' => '',
            'contractNo' => '',
        );
        
        $this->addQueryRecord($queryData);
        
        if (!empty($res['sessionId']) && empty($res['errorMessage'])) {
            $sessionId = $queryData['sessionId'];
            $cont_payment = true;
        }
        
        $leasingInfo = array(
            'cont_payment' => $cont_payment,
            'args' => $args,
            'sessionId' => $sessionId,
            'form_action' => 'https://api.mokilizingas.lt/api2/eshop/post',
        );
        
        return $leasingInfo;
    }
    
    public function getLeasingResponse($url , $args)
    {
        // Prepare fields
        $res = $this->curl($url, $args);
        
        if ($res !== false)
        {
            $res = json_decode($res, true);
            
            return $res;
        }
        
        return false;
    }
    
    public function curl($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
        
        if (0 !== $errno) {
            return false;
        }
        
        return $response;
    }
    
    public function getMethod($address, $total) {
        
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
        } else {
            $this->load->language('payment/mokilizingas');
        }
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('mokilizingas_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
        if ($this->config->get('mokilizingas_minimal_price') > 0 && $this->config->get('mokilizingas_minimal_price') > $total) {
            $status = false;
        } elseif ($this->config->get('mokilizingas_max_price') > 0 && $this->config->get('mokilizingas_max_price') < $total) {
            $status = false;
        } elseif (!$this->config->get('mokilizingas_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }
        if (!in_array($this->session->data['currency'], $this->getSupportedCurrencies())) {
            $status = false;
        }
        
        $method_data = array();
        
        $payment_name = $this->config->get('payment_mokilizingas_payment_name');
        if (empty($payment_name)) {
            $payment_name = $this->language->get('text_title');
        }
        
        if ($status) {
            $method_data = array(
                'code'		 => 'mokilizingas',
                'title'		 => $payment_name,
                'terms'		 => '',
                'sort_order' => $this->config->get('mokilizingas_sort_order')
            );
        }
        
        return $method_data;
    }
    
    public function getSupportedCurrencies() {
        return array(
            'EUR'
        );
    }
}