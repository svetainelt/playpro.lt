<?php
class ModelFeedFacebookFeedProducts extends Model
{
    public function getGoogleCategorysByCategoryId($category_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "google_category` WHERE `category_id` = " . (int) $category_id);

        return $query->row;
    }

    public function getProductSpecials($product_id)
    {
        $query = $this->db->query(
            "SELECT * FROM `" . DB_PREFIX . "product_special` WHERE `product_id` = " . (int) $product_id . " ORDER BY `priority`, `price`"
        );

        return $query->rows;
    }

    public function getGoogleCategories()
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "google_category`");

        return $query->rows;
    }

    public function getProducts()
    {
        return $this->db->query(
            "SELECT `p`.`product_id` "
            ."FROM `" . DB_PREFIX . "product` AS `p` "
            ."LEFT JOIN `" . DB_PREFIX . "product_description` AS `pd` ON (`p`.`product_id` = `pd`.`product_id`) "
            ."LEFT JOIN `" . DB_PREFIX . "product_to_store` AS `p2s` ON (`p`.`product_id` = `p2s`.`product_id`) "
            ."WHERE `pd`.`language_id` = " . (int) $this->config->get('config_language_id') . " "
            ."AND `p`.`status` = 1 "
            ."AND `p`.`date_available` <= NOW() "
            ."AND `p2s`.`store_id` = " . (int) $this->config->get('config_store_id') . " "
            ."GROUP BY `p`.`product_id` "
            ."ORDER BY `p`.`sort_order` ASC, LCASE(`pd`.`name`) ASC"
        )->rows;
    }

    public function getActiveCategories()
    {
        return $this->db->query("SELECT `category_id` FROM `" . DB_PREFIX . "google_category` WHERE `google_category_off` = 0")->rows;
    }

    public function getActiveFacebookCategories()
    {
        return $this->db->query("SELECT `category_id` FROM `" . DB_PREFIX . "facebook_category` WHERE `facebook_category_off` = 0")->rows;
    }

    public function getActiveCategoriesNotGoogle()
    {
        return $this->db->query("SELECT `category_id` FROM `" . DB_PREFIX . "google_category` WHERE `google_category_off` = 1")->rows;
    }

    public function getProductsByCategories($categories)
    {
        $category = [];
        foreach ($categories as $cat) {
            $category[] = (int) $cat['category_id'];
        }
        $sql = "SELECT product_id FROM `" . DB_PREFIX . "product_to_category` WHERE `category_id` IN (" . implode(',', $category) . ")";

        $this->load->model('catalog/product');
        $query = $this->db->query($sql);
        $result = [];
        foreach ($query->rows as $prod) {
            $result[$prod['product_id']] = $this->model_catalog_product->getProduct($prod['product_id']);
        }
        return $result ;
    }

    public function getProduct($product_id)
    {
        if ($this->customer->isLogged()) {
            $customer_group_id = $this->customer->getCustomerGroupId();
        } else {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        $query = $this->db->query(
            "SELECT DISTINCT "
                ."`pd`.`name` AS `name`, "
                ."`pd`.`description` AS `description`, "
                ."`p`.`sku`, "
                ."`p`.`upc`, "
                ."`p`.`ean`, "
                ."`p`.`jan`, "
                ."`p`.`isbn`, "
                ."`p`.`mpn`, "
                ."`p`.`image`, "
                ."`p`.`quantity`, "
                ."`p`.`tax_class_id`, "
                ."`p`.`price`, "
                ."`p`.`product_id`, "
                ."`p`.`weight`, "
                ."("
                    ."SELECT `wcd`.`unit` "
                    ."FROM `" . DB_PREFIX . "weight_class_description` AS `wcd` "
                    ."WHERE `p`.`weight_class_id` = `wcd`.`weight_class_id` "
                    ."AND `wcd`.`language_id` = " . (int) $this->config->get('config_language_id')
                .") AS `weight_class`, "
                ."`m`.`name` AS `manufacturer`, "
                ."("
                    ."SELECT `price` FROM `" . DB_PREFIX . "product_special` AS `ps` "
                    ."WHERE `ps`.`product_id` = `p`.`product_id` "
                    ."AND `ps`.`customer_group_id` = " . (int) $customer_group_id . " "
                    ."AND ("
                        ."(`ps`.`date_start` = '0000-00-00' OR `ps`.`date_start` < NOW()) "
                        ."AND (`ps`.`date_end` = '0000-00-00' OR `ps`.`date_end` > NOW())"
                    .") "
                    ."ORDER BY `ps`.`priority` ASC, `ps`.`price` ASC "
                    ."LIMIT 1"
                .") AS `special` "
                ."FROM `" . DB_PREFIX . "product` AS `p` "
                ."LEFT JOIN `" . DB_PREFIX . "product_description` AS `pd` ON (`p`.`product_id` = `pd`.`product_id`) "
                ."LEFT JOIN `" . DB_PREFIX . "product_to_store` AS `p2s` ON (`p`.`product_id` = `p2s`.`product_id`) "
                ."LEFT JOIN `" . DB_PREFIX . "manufacturer` AS `m` ON (`p`.`manufacturer_id` = `m`.`manufacturer_id`) "
                ."WHERE `p`.`product_id` = " . (int) $product_id . " "
                ."AND `pd`.`language_id` = " . (int) $this->config->get('config_language_id') . " "
                ."AND `p`.`status` = 1 "
                ."AND `p`.`date_available` <= NOW() "
                ."AND `p2s`.`store_id` = " . (int) $this->config->get('config_store_id')
        );

        if ($query->num_rows) {
            return array(
                'product_id'       => $query->row['product_id'],
                'name'             => $query->row['name'],
                'price'            => $query->row['price'],
                'description'      => $query->row['description'],
                'quantity'         => $query->row['quantity'],
                'weight'           => $query->row['weight'],
                'weight_class'     => $query->row['weight_class'],
                'image'            => $query->row['image'],
                'sku'              => $query->row['sku'],
                'upc'              => $query->row['upc'],
                'ean'              => $query->row['ean'],
                'jan'              => $query->row['jan'],
                'isbn'             => $query->row['isbn'],
                'mpn'              => $query->row['mpn'],
                'manufacturer'     => $query->row['manufacturer'],
                'special'          => $query->row['special'],
                'tax_class_id'     => $query->row['tax_class_id'],
            );
        } else {
            return false;
        }
    }

    public function getGoogleCategoryForProduct($product_id, $google_category_off = 0)
    {
        $query = $this->db->query(
            "SELECT * "
            ."FROM `" . DB_PREFIX . "product_to_category` AS `c` "
            ."LEFT JOIN `" . DB_PREFIX . "google_category` AS `gc` ON (`c`.`category_id` = `gc`.`category_id`) "
            ."WHERE `product_id` = " . (int) $product_id . " "
            ."AND `google_category_off` = ".(int) $google_category_off
        );

        if ($query->num_rows) {
            return $query->row;
        } else {
            return false;
        }
    }

    public function getFacebookCategoryForProduct($product_id, $facebook_category_off = 0)
    {
        $query = $this->db->query(
            "SELECT * "
            ."FROM `" . DB_PREFIX . "product_to_category` AS `c` "
            ."LEFT JOIN `" . DB_PREFIX . "facebook_category` AS `gc` ON (`c`.`category_id` = `gc`.`category_id`) "
            ."WHERE `product_id` = " . (int) $product_id . " "
            ."AND `facebook_category_off` = ".(int) $facebook_category_off
        );

        if ($query->num_rows) {
            return $query->row;
        } else {
            return false;
        }
    }

    public function getProdSettings()
    {
        $sqlSelect = "SELECT * FROM " . DB_PREFIX . "facebookfeedproducts_prodsettings;";
        return $this->db->query($sqlSelect)->rows;
    }

    public function getCustomProducts()
    {
        $sqlSelect = "SELECT * FROM " . DB_PREFIX . "facebookfeedproducts_prodlist;";
        $result = $this->db->query($sqlSelect);
        return $result->rows;
    }

    public function getProductsList($excepts)
    {
        $sqlSelect = "SELECT product_id FROM " . DB_PREFIX . "product WHERE 1 ";
        foreach ($excepts as $except) {
            $sqlSelect .= "AND `product_id` != " . $except['product_id'] . " ";
        }
        return $this->db->query($sqlSelect)->rows;
    }

    public function getAllProductsOptions()
    {
        $product_option_data = array();

        $product_option_query = $this->db->query(
            "SELECT `pov`.`product_id`, `pov`.`option_id`, `od`.`name` AS `option_name`, `ovd`.`name`, `od`.`language_id` "
            ."FROM `" . DB_PREFIX . "product_option_value` AS `pov` "
            ."LEFT JOIN `" . DB_PREFIX . "option_value_description` AS `ovd` ON (`pov`.`option_value_id` = `ovd`.`option_value_id`) "
            ."LEFT JOIN `" . DB_PREFIX . "option_description` AS `od` ON (`pov`.`option_id` = `od`.`option_id`) "
            ."WHERE `od`.`language_id` = " . (int) $this->config->get('config_language_id') . " "
            ."AND `ovd`.`language_id` = " . (int) $this->config->get('config_language_id')
        );

        foreach ($product_option_query->rows as $product_option) {
            if (!array_key_exists($product_option['product_id'], $product_option_data)) {
                $product_option_data[$product_option['product_id']] = array();
                $product_option_data[$product_option['product_id']][] = array(
                    'option_id' => $product_option['option_id'],
                    'option_name' => $product_option['option_name'],
                    'option_value' => $product_option['name']
                );
            } else {
                $product_option_data[$product_option['product_id']][] = array(
                    'option_id' => $product_option['option_id'],
                    'option_name' => $product_option['option_name'],
                    'option_value' => $product_option['name']
                );
            }
        }

        return $product_option_data;
    }
}
