<?php
class ModelTotalGiftTeaser extends Model {

	private $moduleName;
	private $moduleTotal2;
	private $modulePath;
	private $moduleVersion;
	private $extensionsLink;
	private $error = array();
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);

		// Config Loader
		$this->config->load('isenselabs/giftteaser');

		// Module Constants
		$this->moduleName        = $this->config->get('giftteaser_name');
		$this->moduleTotal2      = $this->config->get('giftteaser_total2');
		$this->moduleName        = $this->config->get('giftteaser_name');
		$this->modulePath        = $this->config->get('giftteaser_path');
		$this->totalPath         = $this->config->get('giftteaser_total_path2');
		$this->moduleVersion     = $this->config->get('giftteaser_version');
		$this->moduleData_module = $this->config->get('giftteaser_module_data');

		// Load Language
		$this->load->language($this->totalPath);

		// Global Variables
		$this->data['moduleName']        = $this->moduleName;
		$this->data['modulePath']        = $this->modulePath;
		$this->data['feedPath']          = $this->feedPath;
		$this->data['moduleData_module'] = $this->moduleData_module;
	}

	public function getTotal($totals) {
		$total = &$totals['total'];
		$taxes = &$totals['taxes'];
		$total_data = &$totals['totals'];

		$this->load->model('setting/setting');

		$setting = $this->model_setting_setting->getSetting($this->moduleName, $this->config->get('config_store_id'));
		$showGiftPriceReduceInTotal = !empty($setting[$this->moduleName]['gift_price_in_cart']) ? true : false;

		if ($showGiftPriceReduceInTotal && $setting[$this->moduleName]['Enabled'] && $setting[$this->moduleName]['Enabled'] == 'yes' ) {
			$show = false;
			$amount = 0;

			$title = $this->language->get('text_gift_teaser');
			$cart_products = $this->cart->getProducts(); 

			foreach ($cart_products as $key => $cart_product) {
				if (!empty($cart_product['gift_teaser'])) {
					$show = true;
					
			        $amount -= $cart_product['quantity'] * (float)$cart_product['real_price'];
			        $tax_rates = $this->tax->getRates((float)$cart_product['real_price'], $cart_product['real_tax_class_id']);

			        foreach ($tax_rates as $tax_rate) {
			            $taxes[$tax_rate['tax_rate_id']] -= $cart_product['quantity'] * $tax_rate['amount'];
			        }
				}
			}

			if ($show) {
		        $total += $amount;

				$total_data[] = array(
					'code'       => $this->moduleTotal2,
					'title'      => $title,
					'text'       => '' . $this->currency->format($amount, $this->config->get('config_currency')),
					'value'      => $amount,
					'sort_order' => $this->config->get($this->moduleTotal2.'_sort_order')
				);
			}
		}
	}
}
