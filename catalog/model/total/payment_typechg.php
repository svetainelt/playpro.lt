<?php

class ModelTotalPaymentTypeChg extends Model {
    public function getTotal(&$total_data, &$total, &$taxes) {

        if ($this->config->get('payment_typechg_status') && $this->cart->getSubTotal() && isset($this->session->data['payment_method']['code'])) {

            $payments_methods = $this->config->get('payment_typechg_method');
            $shipping_methods = $this->config->get('payment_typechg_shipping');
            if ($this->session->data['payment_method']['code'] == $payments_methods
                && $this->config->get('payment_typechg_charge')
                && $this->config->get('payment_typechg_description_' . $this->config->get('config_language_id'))
                && in_array($this->session->data['shipping_method']['code'], $shipping_methods)) {

                $payments_charges = $this->config->get('payment_typechg_charge');
                $fix_description = $this->config->get('payment_typechg_description_' . $this->config->get('config_language_id'));

                $payment_charge = $payments_charges;

                $payment_charge += ($total*0.005 + 0.95);

                $total_data[] = array(
                    'code'       => 'payment_typechg',
                    'title'      => $fix_description,
                    'value'      => $payment_charge,
                    'sort_order' => $this->config->get('payment_typechg_sort_order')
                );

                if ($this->session->data['shipping_method']['tax_class_id']) {
                    $tax_rates = $this->tax->getRates($payment_charge, $this->session->data['shipping_method']['tax_class_id']);

                    foreach ($tax_rates as $tax_rate) {
                        if (!isset($taxes[$tax_rate['tax_rate_id']])) {
                            $taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
                        } else {
                            $taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
                        }
                    }
                }

                $total += $payment_charge;
            }
        }
    }
}
?>