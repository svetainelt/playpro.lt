<?php
class ModelShippingUniversalWeight extends Model {
	public function getQuote($address) {
		$this->load->language('shipping/universal_weight');

		$quote_data = array();

		$cart_weight = $this->cart->getWeight();
		$cart_subtotal = $this->cart->getSubTotal();
		$cart_taxes = array_sum($this->cart->getTaxes());
		$cart_grandtotal = $cart_subtotal + $cart_taxes;		

		$address_geo_zones = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone z2gz LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (z2gz.geo_zone_id = gz.geo_zone_id) WHERE country_id = '" . (int) $address['country_id'] . "' AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')");
		foreach ($query->rows as $row) {
			$address_geo_zones[] = $row['geo_zone_id'];
		}

		$shipping_rules = $this->config->get('universal_weight_rules');

		$i = 0;

		if ($shipping_rules) {                    
                        // Sort
			$sort_order = array();
			foreach ($shipping_rules as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
                        array_multisort($sort_order, SORT_ASC, $shipping_rules);

                        // Get current language ID
                        $this->load->model('localisation/language');
                        $languages = $this->model_localisation_language->getLanguages();
                        $language_code = $this->session->data['language'];
                        $language_id = isset($languages[$language_code]) ? $languages[$language_code]['language_id'] : 0;
                        
			foreach ($shipping_rules as $rule) {
				$rule['max_price'] = str_replace('*', PHP_INT_MAX, $rule['max_price']);
				$rule['max_weight'] = str_replace('*', PHP_INT_MAX, $rule['max_weight']);
				
				$i ++;
				if (
					(
						$rule['geo_zone'] == '*'
						|| in_array($rule['geo_zone'], $address_geo_zones) 
					)
					&& $cart_grandtotal >= $rule['min_price'] 
					&& $cart_grandtotal <= $rule['max_price']
					&& $cart_weight >= $rule['min_weight']
					&& $cart_weight <= $rule['max_weight']
					&& $rule['status'] == '1' ) 
				{
					$quote_data['universal_weight_' . $i] = array(
						'code' => 'universal_weight.universal_weight_' . $i,
						'title' => isset($rule['title'][$language_id]) ? $rule['title'][$language_id] : '',
						'cost' => $rule['cost'],
						'tax_class_id' => $this->config->get('universal_weight_tax_class_id'),
						'text' => $this->currency->format($this->tax->calculate($rule['cost'], $this->config->get('universal_weight_tax_class_id'), $this->config->get('config_tax')))
					);
				}
			}
		}

		$method_data = array();

		if ($quote_data) {
			$method_data = array(
				'code' => 'universal_weight',
				'title' => $this->language->get('text_title'),
				'quote' => $quote_data,
				'sort_order' => $this->config->get('universal_weight_sort_order'),
				'error' => false
			);
		}

		return $method_data;
	}

}