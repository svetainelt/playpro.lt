<?php
/* LPexpress shipping  for OpenCart v.2.x.x and 2.2.x
 *
 * @version 2.0.2
 * @date 01/02/2018
 * @author Kestutis Banisauskas
 * @Smartechas
 */
class ModelShippingLpexpress extends Model {
    function getQuote($address) {
        $this->load->language('shipping/lpexpress');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('lpexpress_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
        if (!$this->config->get('lpexpress_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $quote_data = array();

        $cost = '';
        $is_restricted = false;


        $weight = null;
        $cart_volume = null;

        foreach ($this->cart->getProducts() as  $product) {
            if ($product['shipping']) {
                $length = $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $width = $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $height = $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $weight = $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
                $quantity = $product['quantity'];

                $cart = array ($length, $width, $height);

                $cart_mins = array (min($cart));
                foreach ($cart_mins as $key=>$val){
                    $cart_min[] =($val);
                }
                $cart_maxs = array (max($cart));
                foreach ($cart_maxs as $key=>$val){
                    $cart_max[] =($val);
                }
                $cart_h = max($cart_min);
                $cart_l = max($cart_max);

                $cart_volume_items = array ($length * $width * $height);
                foreach ($cart_volume_items as $key=>$val){
                    $cart_volume_item[] = ($val);
                }
                $cart_volume_item_max = max($cart_volume_item);

                if ($quantity == '1') {
                    $cart_volume += $length * $width * $height * $quantity;
                } else if ($quantity >= '2') {
                    $cart_volume += $length * $width * $height * $quantity * 1.25;
                } else if ($quantity >= '4') {
                    $cart_volume += $length * $width * $height * $quantity * 1.2;
                } else if ($quantity >= '6') {
                    $cart_volume += $length * $width * $height * $quantity * 1.05;
                }


                $volume_cart = array_product($cart);
                $x = min($cart);
                $y = max($cart);
                if ($x != '0' && $volume_cart != '0'){
                    $cart_volume_item_w = array((array_product($cart))/$x/$y);
                } else {
                    $cart_volume_item_w = array('0');
                }
                foreach ($cart_volume_item_w as $key=>$val){
                    $cart_w_max[] =($val);
                }
                $cart_w = max($cart_w_max);

            }

        }

        $containers = array();
        $containers= array (
            'lpexpress_length_s',
            'lpexpress_width_s',
            'lpexpress_height_s',
            'lpexpress_length_m',
            'lpexpress_width_m',
            'lpexpress_height_m',
            'lpexpress_length_l',
            'lpexpress_width_l',
            'lpexpress_height_l',
            'lpexpress_length_xl',
            'lpexpress_width_xl',
            'lpexpress_height_xl'
        );
        foreach ($containers as $container) {
            $data[$container] = $this->length->convert($this->config->get($container), $this->config->get('lpexpress_length_class_id'), $this->config->get('config_length_class_id'));
        }
        $s_c_length = $data['lpexpress_length_s'];
        $s_c_width = $data['lpexpress_width_s'];
        $s_c_height = $data['lpexpress_height_s'];

        $s_c_weight = $this->weight->convert($this->config->get('lpexpress_weight_s'), $this->config->get('lpexpress_weight_class_id'), $this->config->get('config_weight_class_id'));

        $s_container_volume = $s_c_length * $s_c_width * $s_c_height;
        $s_container = array($s_c_length, $s_c_width, $s_c_height);
        $s_container_l = max($s_container);
        $s_container_h = min($s_container);
        $s_container_w = $s_container_volume / $s_container_l / $s_container_h;



        $m_c_length = $data['lpexpress_length_m'];
        $m_c_width = $data['lpexpress_width_m'];
        $m_c_height = $data['lpexpress_height_m'];

        $m_c_weight = $this->weight->convert($this->config->get('lpexpress_weight_m'), $this->config->get('lpexpress_weight_class_id'), $this->config->get('config_weight_class_id'));

        $m_container_volume = $m_c_length * $m_c_width * $m_c_height;
        $m_container = array($m_c_length, $m_c_width, $m_c_height);
        $m_container_l = max($m_container);
        $m_container_h = min($m_container);
        $m_container_w = $m_container_volume / $m_container_l / $m_container_h;


        $l_c_length = $data['lpexpress_length_l'];
        $l_c_width = $data['lpexpress_width_l'];
        $l_c_height = $data['lpexpress_height_l'];

        $l_c_weight = $this->weight->convert($this->config->get('lpexpress_weight_l'), $this->config->get('lpexpress_weight_class_id'), $this->config->get('config_weight_class_id'));

        $l_container_volume = $l_c_length * $l_c_width * $l_c_height;
        $l_container = array($l_c_length, $l_c_width, $l_c_height);
        $l_container_l = max($l_container);
        $l_container_h = min($l_container);
        $l_container_w = $l_container_volume / $l_container_l / $l_container_h;



        $xl_c_length = $data['lpexpress_length_xl'];
        $xl_c_width = $data['lpexpress_width_xl'];
        $xl_c_height = $data['lpexpress_height_xl'];

        $xl_c_weight = $this->weight->convert($this->config->get('lpexpress_weight_xl'), $this->config->get('lpexpress_weight_class_id'), $this->config->get('config_weight_class_id'));

        $xl_container_volume = $xl_c_length * $xl_c_width * $xl_c_height;
        $xl_container = array($xl_c_length, $xl_c_width, $xl_c_height);
        $xl_container_l = max($xl_container);
        $xl_container_h = min($xl_container);
        $xl_container_w = $xl_container_volume / $xl_container_l / $xl_container_h;


        if (($cart_l <= $s_container_l) && ($cart_h <= $s_container_h) && ($cart_w <= $s_container_w) && ($cart_volume <= $s_container_volume) && ($weight <= $s_c_weight) && ($cart_volume <= $s_container_volume))
        {	$is_restricted = true;
            $cost = $this->config->get('lpexpress_s_rate');
        } else if ((($cart_l >= $s_container_l) || ($cart_h >= $s_container_h) || ($cart_w >= $s_container_w) || ($cart_volume >= $s_container_volume) || ($weight >= $s_c_weight)) && (($cart_l <= $m_container_l) && ($cart_h <= $m_container_h) && ($cart_w <= $m_container_w) && ($cart_volume <= $m_container_volume) && ($weight <= $m_c_weight)))
        {	$is_restricted = true;
            $cost = $this->config->get('lpexpress_m_rate');
        } else if ((($cart_l >= $m_container_l) || ($cart_h >= $m_container_h) || ($cart_w >= $m_container_w) || ($cart_volume >= $m_container_volume) || ($weight >= $m_c_weight)) && (($cart_l <= $l_container_l) && ($cart_h <= $l_container_h) && ($cart_w <= $l_container_w) && ($cart_volume <= $l_container_volume) && ($weight <= $l_c_weight)))
        {	$is_restricted = true;
            $cost = $this->config->get('lpexpress_l_rate');
        } else if ((($cart_l >= $l_container_l) || ($cart_h >= $l_container_h) || ($cart_w >= $l_container_w) || ($cart_volume >= $l_container_volume) || ($weight >= $l_c_weight)) && (($cart_l <= $xl_container_l) && ($cart_h <= $xl_container_h) && ($cart_w <= $xl_container_w) && ($cart_volume <= $xl_container_volume) && ($weight <= $xl_c_weight)))
        {	$is_restricted = true;
            $cost = $this->config->get('lpexpress_xl_rate');
        }

        /*terminals select*/

        $terminals_data = $this->cache->get('terminals.' . (int)$this->config->get('config_language_id'));
        if (!$terminals_data) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lp_terminals");
            $terminals_data = $query->rows;
            $this->cache->set('terminals.' . (int)$this->config->get('config_language_id'), $terminals_data);
        }
        $terminals = $terminals_data;

        if ($is_restricted && $status &&  $terminals && (string)$cost != ''  ) {
            $first = '';

            $j_script = '<script>
 $( "input[name=shipping_method]" ).focus(function() { $( this ).blur(); });
  $.each( $("input[name=shipping_method][value*=\'lpexpress.lpexpress_\']"), function( index, value ) { 
       if ( !index ) return;
       $(value).parent().parent().hide();
  });
 
  function lpexpressOnFocus() {
    if ($(\'#accordion\').length > 0) { $(\'#lpexpress\').parent().find(\'input\').eq(0).prop(\'checked\',true); }  /*default / journal*/  
    if ($(\'.journal-checkout\').length > 0) { $(\'#lpexpress\').parent().parent().find(\'input\').eq(0).prop(\'checked\',true); } /*quickcheckout*/
    if ($(\'.quickcheckout-checkout\').length > 0 || $(\'.simplecheckout\').length > 0) { $(\'#lpexpress\').parent().parent().parent().find(\'input\').eq(0).prop(\'checked\',true); }  /*quickcheckout/simplecheckout */
	if ($(\'#d_quickcheckout\').length > 0) { $(\'#lpexpress\').parent().parent().find(\'input\').eq(0).prop(\'checked\',true); }
  }
                       
  function lpexpressOnChange(el) { 
    if ($(\'#accordion\').length > 0) { $(\'#lpexpress\').parent().find(\'input\').eq(0).val($(el).val()).prop(\'checked\',true).trigger(\'change\'); }  /*default oc checkout or journal default checkout*/ 
    if ($(\'.journal-checkout\').length > 0) { $(\'#lpexpress\').parent().parent().find(\'input\').eq(0).val($(el).val()).prop(\'checked\',true).trigger(\'change\'); }  /*journalcheckout*/
    if ($(\'.quickcheckout-checkout\').length > 0 || $(\'.simplecheckout\').length > 0) { $(\'#lpexpress\').parent().parent().parent().find(\'input\').eq(0).val($(el).val()).prop(\'checked\',true).trigger(\'change\'); }  /*quickcheckout/simplecheckout*/
	if ($(\'#d_quickcheckout\').length > 0) { $(\'#lpexpress\').parent().parent().find(\'input\').eq(0).val($(el).val()); $(\'#lpexpress\').parent().find(\'input\').eq(0).prop(\'checked\',true); }
  }
  $( document ).ready(function() { 
     if ( $(\'.simplecheckout\').length > 0 && $("input[name=shipping_method_current]").length && $("input[name=shipping_method_current]").val().substr(0,9) == "lpexpress" ) { 
        $( "input[name=shipping_method][value*=\'lpexpress.lpexpress_\']" ).eq(0).prop("checked",true).val($("input[name=shipping_method_current]").val()); 
        $( "#lpexpress" ).val($("input[name=shipping_method_current]").val()); 
     }
	if ($(\'#d_quickcheckout\').length > 0) {
     if ( qc.shippingMethod.attributes.shipping_method.code && qc.shippingMethod.attributes.shipping_method.code.substr(0,9) == "lpexpress") { 
        $( "input[name=shipping_method][value*=\'lpexpress.lpexpress_\']" ).eq(0).prop("checked",true).val(qc.shippingMethod.attributes.shipping_method.code); 
        $( "#lpexpress" ).val(qc.shippingMethod.attributes.shipping_method.code); 
                 }
	}
	
     if ( $(\'.input-radio\').length > 0 && $("input[name=shipping_method_current]").length && $("input[name=shipping_method_current]").val().substr(0,9) == "lpexpress" ) { 
        $( "input[name=shipping_method][value*=\'lpexpress.lpexpress_\']" ).eq(0).prop("checked",true).val($("input[name=shipping_method_current]").val()); 
        $( "#lpexpress" ).val($("input[name=shipping_method_current]").val()); 
     
	}
	
		$( "span.price" ).css("text-align", "right");
  });
</script>';
            $terminal_select = '<select class="form-control" name="lpexpress" id="lpexpress" onchange="lpexpressOnChange(this);" onfocus="lpexpressOnFocus();">';
            foreach ($terminals as $terminal) {
                if (!$first) $first = $terminal['code'];
                $terminal_select .= '<option style="max-width: 250px; display: block;" class="form-control" value="lpexpress.lpexpress_'.$terminal['code'].'">'.$terminal['city'].': '.$terminal['address'].' '.$terminal['place'].'</option>'."\n";
                $sub_quote['lpexpress_' .$terminal['code']] = array(
                    'code'         => 'lpexpress.lpexpress_' . $terminal['code'],
                    'title'        => $this->language->get('text_description').': '.$terminal['city'].' - '.$terminal['address'].' '.$terminal['place'],
                    'cost'         => $cost,
                    'tax_class_id' => $this->config->get('lpexpress_tax_class_id'),
                    'text'         => $terminal['city'].': '.$terminal['address'].' '.$terminal['place'] . ' - ' . $this->currency->format($this->tax->calculate($cost, $this->config->get('lpexpress_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
                );

                                    $sub_quote['lpexpress_fake' .$terminal['code']] = array(
                                        'code'         => 'lpexpress.lpexpress_fake' . $terminal['code'],
                                        'title'        => '<div id="lpexpress_fake' . $terminal['code'].'"><script>$(\'#lpexpress_fake'.$terminal['code'].'\').parent().parent().hide().prev().hide();</script></div>',
                                        'cost'         => $cost,
                                        'tax_class_id' => $this->config->get('lpexpress_tax_class_id'),
                                        'text'         => 'fake'
                                    );
            }

            $terminal_select .= '</select>' . $j_script;


            $quote_data['lpexpress'] = array(
                'code'         => 'lpexpress.lpexpress_' . ( $first ? $first : $this->config->get('lpexpress_geo_zone_id')),
                'title'        => $this->language->get('text_description'),
                'cost'         => $cost,
                'tax_class_id' => $this->config->get('lpexpress_tax_class_id'),
                'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('lpexpress_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']) . '  ' . $terminal_select            );

        }


        $method_data = array();

        if (isset($this->request->get['route']) && strpos($this->request->get['route'],'api') === 0) {
            $quote_data = array_filter($sub_quote,function($a){ return strpos($a,false) === 0 ? 0: 1;},ARRAY_FILTER_USE_KEY);
            array_walk($quote_data,function(&$a,$b){ $a['text'] = preg_replace('/.*\s([^s]+)$/','$1',$a['text']);});

            $sub_quote = array();        }

        if ($quote_data) {
            $method_data = array(
                'code'       => 'lpexpress',
                'title'        => $this->language->get('text_title'),
                'quote'      => array_merge($quote_data,$sub_quote),
                'sort_order' => $this->config->get('lpexpress_sort_order'),
                'error'      => false
            );
        }

        return $method_data;
    }


    /*
public function cart_array_replace($find, $replace, $cart){
   if (!is_array($replace)) {
    return str_replace($find, $replace, $cart);
   }
            $newCart = array();
    foreach ($cart as $key => $value) {
    $newCart[$key] = cart_array_replace($find, $replace, $value);
    }
    return $newCart;
}



function GetTerminals()
{
    $terminals_data = $this->cache->get('terminals.' . (int)$this->config->get('config_language_id'));
    if (!$terminals_data) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lp_terminals");
        $terminals_data = $query->rows;
        $this->cache->set('terminals.' . (int)$this->config->get('config_language_id'), $terminals_data);
    }
    //return $terminals_data;
    @$temp.= "<select name='lp_terminals' class='form-control' style='max-width: 250px'>";
    foreach($terminals_data as $data)
    {
        @$temp.= "<option value='".$data['city'].", ".$data['address'].", ".$data['place']."'>".$data['city'].", ".$data['address'].", ".$data['place']."</option>";
    }
    @$temp.= "</select>";
    return @$temp;
}
*/
}
