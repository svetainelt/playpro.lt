<?php
class ModelShippingVenipakShipping extends Model {
	function getQuote($address) {

		$this->load->language('shipping/venipak_shipping');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('venipak_shipping_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('venipak_shipping_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$html_container = '
			<div id="venipak_shipping_pickup_options" style="display: none; width: 500px;">
				<select style="width: 500px;" class="venipak_shipping_pickup_points" name="venipak_shipping_pickup_point"><option value="">Pasirinkite paštomatą</option></select>
				<div style="clear: both; padding-top: 15px; text-align: right;" id="selected-pickup-info"></div>
			</div>
			<div id="map" style="margin: 15px 0; display: none; width: 100%; height: 300px;"></div>
			<script>
				window.venipakShipping.init();
			</script>';

			$is_courier_disabled = $this->config->get('venipak_shipping_disable_courier');
			$venipak_shipping_disable_locker = $this->config->get('venipak_shipping_disable_locker');
			$venipak_shipping_disable_pickup = $this->config->get('venipak_shipping_disable_pickup');

            $pickup_status = true;
            $cartProducts = $this->cart->getProducts();

            $max_weight        = 30;
            $max_length        = 60;
            $max_width         = 38;
            $max_height        = 38;

            $dimensions_omniva = [$max_length, $max_width, $max_height];
            sort($dimensions_omniva);
            foreach ($cartProducts as $product) {
                $dimensions = [];
                $weight     = $this->weight->convert(($product['weight'] / $product['quantity']), $product['weight_class_id'], $this->config->get('config_weight_class_id'));
                $length     = $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $width      = $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $height     = $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id'));

                $dimensions = [$length, $width, $height];

                sort($dimensions);

                if (($weight <= $max_weight) && ($dimensions[0] <= $dimensions_omniva[0]) && ($dimensions[1] <= $dimensions_omniva[1]) && ($dimensions[2] <= $dimensions_omniva[2])) {
                    $pickup_status = true;
                } else {
                    $pickup_status = false;
                    break;
                }
            }


            if((empty($venipak_shipping_disable_locker) || empty($venipak_shipping_disable_pickup)) && $pickup_status) {
                $quote_data['venipak_shipping_pickup'] = array(
                    'code'         => 'venipak_shipping.venipak_shipping_pickup',
                    'title'        => $this->config->get('venipak_shipping_method_title_pickup'),
                    'cost'         => $this->config->get('venipak_shipping_cost_pickup'),
                    'tax_class_id' => $this->config->get('venipak_shipping_tax_class_id'),
                    'text'         => $this->currency->format($this->tax->calculate($this->config->get('venipak_shipping_cost_pickup'), $this->config->get('venipak_shipping_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']) . $html_container
                );
            }
			if(empty($is_courier_disabled)) {
					$quote_data['venipak_shipping_courier'] = array(
							'code'         => 'venipak_shipping.venipak_shipping_courier',
							'title'        => $this->config->get('venipak_shipping_method_title_courier'),
							'cost'         => $this->config->get('venipak_shipping_cost_courier'),
							'tax_class_id' => $this->config->get('venipak_shipping_tax_class_id'),
							'text'         => $this->currency->format($this->tax->calculate($this->config->get('venipak_shipping_cost_courier'), $this->config->get('venipak_shipping_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
					);
			}


			$method_data = array(
				'code'       => 'venipak_shipping',
				'title'      => '<img src="/image/catalog/venipak_shipping/venipak-logo.png" width="100" /> ' . $this->config->get('venipak_shipping_method_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_flat_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}