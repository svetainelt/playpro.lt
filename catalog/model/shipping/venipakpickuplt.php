<?php
class ModelShippingVenipakpickuplt extends Model {
    function getQuote($address) {

//        $this->log->write('getQuote triggered');
        $this->load->language('shipping/venipakpickuplt');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('shipping_venipakpickuplt_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if (!$this->config->get('shipping_venipakpickuplt_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = array();

        $cart_weight = $this->cart->getWeight();

        $shippings_weight_data = $this->config->get('shipping_venipakpickuplt_weight_data');

        foreach($shippings_weight_data as $k => $s)
        {
            if ($s['weight_from'] != 0 && $cart_weight <= $s['weight_from'])
                unset($shippings_weight_data[$k]);
            elseif ($s['weight_to'] != 0 && $s['weight_to'] < $cart_weight)
                unset($shippings_weight_data[$k]);
        }

        $checkbox = 'venipakpickuplt.venipakpickuplt';

        $checkbox_js = '';
        if (isset($this->session->data['shipping_method']) && strpos($this->session->data['shipping_method']['code'], 'venipakpickuplt') !== false) {
            $checkbox = $this->session->data['shipping_method']['code'];
            $checkbox_js = $this->session->data['shipping_method']['code'];
        }

        $dropSelect_js = "
		<script>
		var shp_methods = document.getElementsByName('shipping_method');
		for(var i = 0; i < shp_methods.length; i++){
			if(shp_methods[i].value == '".$checkbox_js."'){
				shp_methods[i].checked = true;
			}
		}
		</script>
		";
        if ($status) {

            $cartProducts = $this->cart->getProducts();

            $max_weight        = 30;
            $max_length        = 60;
            $max_width         = 38;
            $max_height        = 38;


            $dimensions_omniva = [$max_length, $max_width, $max_height];
            sort($dimensions_omniva);
            foreach ($cartProducts as $product) {
                $dimensions = [];
                $weight     = $this->weight->convert(($product['weight'] / $product['quantity']), $product['weight_class_id'], $this->config->get('config_weight_class_id'));
                $length     = $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $width      = $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                $height     = $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id'));

                $dimensions = [$length, $width, $height];

                sort($dimensions);

                if (($weight <= $max_weight) && ($dimensions[0] <= $dimensions_omniva[0]) && ($dimensions[1] <= $dimensions_omniva[1]) && ($dimensions[2] <= $dimensions_omniva[2])) {

                } else {
                    $status = false;
                    break;
                }
            }

        }

        if ($status && count($shippings_weight_data)) {


            $shipping_weight_data = reset($shippings_weight_data);
            $venipakpickuplt_cost = $shipping_weight_data['cost'] + $shipping_weight_data['each_next']*($this->cart->countProducts()-1);

            if ( $this->config->get('shipping_venipakpickuplt_freelimit') > 0 && $this->cart->getSubtotal() > $this->config->get('shipping_venipakpickuplt_freelimit') ) {
                $venipakpickuplt_cost = 0.00;
            }

            if ( $this->config->get('shipping_venipakpickuplt_show_all') == "N") {
                $sql_status = " AND STATUS = 'Y'";
            } else {
                $sql_status = "";
            }


            $quote_data = array();

            $dropSelect = $this->language->get('text_name');
            $dropSelect .= '</label>';
            $dropSelect .= '<select name="venipakpickuplt_sel" id="venipakpickuplt_sel" style="width: 80%; display: inline;" class="form-control" ';
            $dropSelect .= 'onfocus="$(\'#venipakpickuplt_sel\').parent().find(\'input\').eq(0).val($(this).val()); $(\'#venipakpickuplt_sel\').parent().find(\'input\').eq(0).prop(\'checked\',true).trigger(\'change\');" ';
            $dropSelect .= 'onchange="$(\'#venipakpickuplt_sel\').parent().find(\'input\').eq(0).val($(this).val()); $(\'#venipakpickuplt_sel\').parent().find(\'input\').eq(0).prop(\'checked\',true).trigger(\'change\');">';


            $sql = "SELECT ID, REGION, NAME FROM ".DB_PREFIX."xx_venipakpickup_offices WHERE TYPE = 57 ".$sql_status." ORDER BY REGION, CITY, NAME, SORT" ;
            $query = $this->db->query($sql);

            $reqion_old = '';
            foreach($query->rows as $result){
                $reqion_new = $result['REGION'];
                $terminal_name = $result['NAME'];
                if($reqion_new != $reqion_old && $reqion_old != '') {
                    $dropSelect .= "</optgroup>\r\n";
                }
                if($reqion_new != $reqion_old) {
                    $dropSelect .= "<optgroup label='". $reqion_new ."'>\r\n";
                }
                if (isset($this->session->data['shipping_method']) && $this->session->data['shipping_method']['code'] == 'venipakpickuplt.venipakpickuplt'.$result['ID']) {
                    $dropSelect .= "<option value='".'venipakpickuplt.venipakpickuplt'.$result['ID']."' selected='selected'>".$terminal_name."</option>\r\n";
                } else {
                    $dropSelect .= "<option value='".'venipakpickuplt.venipakpickuplt'.$result['ID']."'>".$terminal_name."</option>\r\n";
                }
                $reqion_old = $reqion_new;
            }
            if ($reqion_old != '') {
                $dropSelect .= "</optgroup>\r\n";
            }


            $dropSelect .= "</select>";


            $quote_data['venipakpickuplt.venipakpickuplt_select'] = array(
                'code'         => $checkbox.'" onclick="$(this).val($(\'#venipakpickuplt_sel\').val());',
                'title'        => $dropSelect.'<!--',
                'cost'         => $venipakpickuplt_cost,
                'tax_class_id' => $this->config->get('shipping_venipakpickuplt_tax_class_id'),
                'text'         => $this->currency->format($this->tax->calculate($this->currency->convert($venipakpickuplt_cost, $this->config->get('config_currency'), $this->session->data['currency']), $this->config->get('shipping_venipakpickuplt_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], 1.0000000)
            );


            //if (!isset($_POST['country_id'])) {
            //die('COUNTRY_EXISTS');
            //}
            //if (!isset($this->request->get['country_id'])) {
            // Start Build Selection
            $quote_data['venipakpickuplt.venipakpickuplt_begin'] = array(
                'code'         => 'venipakpickuplt.venipakpickuplt_begin',
                'title'        => '',
                'cost'         => $venipakpickuplt_cost,
                'tax_class_id' => $this->config->get('shipping_venipakpickuplt_tax_class_id'),
                'text'         => $this->currency->format($this->tax->calculate($this->currency->convert($venipakpickuplt_cost, $this->config->get('config_currency'), $this->session->data['currency']), $this->config->get('shipping_venipakpickuplt_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], 1.0000000)
            );

            $sql = "SELECT ID, REGION, NAME FROM ".DB_PREFIX."xx_venipakpickup_offices WHERE TYPE = 57 ".$sql_status." ORDER BY REGION, CITY, NAME, SORT" ;
            $query = $this->db->query($sql);
            foreach($query->rows as $result){
                $venipakpickuplt[] = $result;
                $quote_data['venipakpickuplt'.$result['ID']] = array(
                    'code'         => 'venipakpickuplt.venipakpickuplt'.$result['ID'],
                    'title'        => $this->language->get('text_title').' - '.$result['NAME'],
                    'cost'         => $venipakpickuplt_cost,
                    'tax_class_id' => $this->config->get('shipping_venipakpickuplt_tax_class_id'),
                    'text'         => $this->currency->format($this->tax->calculate($this->currency->convert($venipakpickuplt_cost, $this->config->get('config_currency'), $this->session->data['currency']), $this->config->get('shipping_venipakpickuplt_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], 1.0000000)
                );
            }


            $quote_data['venipakpickuplt.venipakpickuplt_end'] = array(
                'code'         => 'venipakpickuplt.venipakpickuplt_end',
                'title'        => '-->'.$dropSelect_js,
                'cost'         => $venipakpickuplt_cost,
                'tax_class_id' => $this->config->get('shipping_venipakpickuplt_tax_class_id'),
                'text'         => $this->currency->format($this->tax->calculate($this->currency->convert($venipakpickuplt_cost, $this->config->get('config_currency'), $this->session->data['currency']), $this->config->get('shipping_venipakpickuplt_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], 1.0000000)
            );
            // End Build Selection
            //}

            if (isset($this->session->data['api_id'])) {
                //print_r($this->session);
                $quote_data = array();

                $sql = "SELECT ID, REGION, NAME FROM ".DB_PREFIX."xx_venipakpickup_offices WHERE TYPE = 57 ".$sql_status." ORDER BY REGION, CITY, NAME, SORT" ;
                $query = $this->db->query($sql);
                foreach($query->rows as $result){
                    $quote_data['venipakpickuplt'.$result['ID']] = array(
                        'code'         => 'venipakpickuplt.venipakpickuplt'.$result['ID'],
                        'title'        => $this->language->get('text_title').' - '.$result['NAME'],
                        'cost'         => $venipakpickuplt_cost,
                        'tax_class_id' => $this->config->get('shipping_venipakpickuplt_tax_class_id'),
                        'text'         => $this->currency->format($this->tax->calculate($this->currency->convert($venipakpickuplt_cost, $this->config->get('config_currency'), $this->session->data['currency']), $this->config->get('shipping_venipakpickuplt_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], 1.0000000)
                    );
                }
            }

            $method_data = array(
                'code'       => 'venipakpickuplt',
                'title'      => $this->language->get('text_title'),
                'quote'      => $quote_data,
                'sort_order' => $this->config->get('shipping_venipakpickuplt_sort_order'),
                'error'      => false
            );

        }

        if ($status) {
            return $method_data;
        } else {
            return [];
        }
    }
}
?>