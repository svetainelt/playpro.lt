<?php

use unisend_shipping\cons\UnisendShippingConst;
use unisend_shipping\context\UnisendShippingContextHolder;
use unisend_shipping\services\UnisendShippingCarrierService;
use unisend_shipping\services\UnisendShippingConfigService;

require_once(DIR_SYSTEM . 'library/unisend_shipping/vendor/autoload.php');

class ModelShippingUnisendShipping extends Model
{
    public function index() {

            UnisendShippingContextHolder::load($this);

    }

    function getQuote($address)
    {

        $this->session->data['unisend_shipping']['shipping_address'] = $this->session->data['shipping_address'] ?? $address;
        $checkoutPage = $_REQUEST['route'] == 'checkout/shipping_method';
        UnisendShippingContextHolder::load($this);


        $this->load->language('shipping/unisend_shipping');

        $products = $this->cart->getProducts();
        $availableCarriers = UnisendShippingCarrierService::getAvailableCarriers($products, $this->toOrderInfo(), $this->cart->getTotal());

        foreach ($availableCarriers as $carrier) {
            $quote_data[$carrier['code']] = array(
                'code' => 'unisend_shipping.' . $carrier['code'],
                'title' => $carrier['title'],
                'cost' => $carrier['price'],
                'tax_class_id' => $this->config->get(UnisendShippingConst::SETTING_KEY_TAX_CLASS_ID),
                'text' => $this->currency->format($this->tax->calculate($carrier['price'], $this->config->get(UnisendShippingConst::SETTING_KEY_TAX_CLASS_ID), $this->config->get('config_tax')), $this->session->data['currency'])
            );
        }

        if (empty($availableCarriers)) {
            return false;
        }
        $method_data = array(
            'code' => 'unisend_shipping',
            'title' => $checkoutPage ? '<img src="' . $this->config->get('config_url') . 'image/catalog/unisend_shipping/unisend_shipping_logo_small.png" /> ' : null . 'Unisend shipping',
            'quote' => $quote_data,
            'sort_order' => $this->config->get(UnisendShippingConst::SETTING_KEY_SHIPPING_METHOD_SORT),
            'error' => false
        );
        return $method_data;
    }


    private function toOrderInfo()
    {
        $orderInfo = [];
        $shippingAddress = $this->session->data['shipping_address'];
        foreach ($shippingAddress as $key => $value) {
            $orderInfo['shipping_' . $key] = $value;
        }
        $orderInfo['telephone'] = $shippingAddress['phone'] ?? $this->customer->getTelephone();
        $orderInfo['email'] = $shippingAddress['email'] ?? $this->customer->getEmail();
        $weight = $this->cart->getWeight();
        $weight = $this->weight->convert($weight, $this->config->get('config_weight_class_id'), (UnisendShippingConfigService::get(UnisendShippingConst::SETTING_KEY_DEFAULT_WEIGHT_CLASS_ID) ?: 2));
        $orderInfo['weight'] = max($weight, 1);
        return $orderInfo;
    }
}