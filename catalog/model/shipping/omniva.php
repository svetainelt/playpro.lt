<?php

class ModelShippingOmniva extends Model {

    public function getQuote($address) {
        $this->load->language('shipping/omniva');

        $quote_data = array();
        $method_data = array();

        $this->load->model('setting/setting');

        $omniva_addresses = $this->model_setting_setting->getSetting('omniva_addresses');

        $tab_zones = array('lv', 'lt', 'ee');
        $current_country = '';

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");

        foreach ($query->rows as $result) {
            foreach ($tab_zones as $tab_zone) {
                if (($this->config->get('omniva_' . $tab_zone . '_status')) && ($this->config->get('omniva_' . $tab_zone . '_geo_zone_id') == $result['geo_zone_id'])) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int) $result['geo_zone_id'] . "' AND country_id = '" . (int) $address['country_id'] . "' AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')");

                    if ($query->num_rows) {
                        $status = true;
                        $current_country = $tab_zone;
                    } else {
                        $status = false;
                    }
                } else {
                    $status = false;
                }

                if ($status) {
                    $cost = $this->config->get('omniva_' . $tab_zone . '_cost');
                    $max_weight = $this->config->get('omniva_weight');
                    $max_length = $this->config->get('omniva_length');
                    $max_width = $this->config->get('omniva_width');
                    $max_height = $this->config->get('omniva_height');
                    $dimensions_omniva = array($max_length, $max_width, $max_height);
                    sort($dimensions_omniva);

                    if ($this->config->get('omniva_' . $tab_zone . '_cost_option') == 'each_product')
                        $total_cost = 0;
                    else
                        $total_cost = $cost;
                    
                    $cartProducts = $this->cart->getProducts();

                    foreach ($cartProducts as $product) {
                        $dimensions = array();
                        $weight = $this->weight->convert(($product['weight'] / $product['quantity']), $product['weight_class_id'], $this->config->get('config_weight_class_id'));
                        $length = $this->length->convert($product['length'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                        $width = $this->length->convert($product['width'], $product['length_class_id'], $this->config->get('config_length_class_id'));
                        $height = $this->length->convert($product['height'], $product['length_class_id'], $this->config->get('config_length_class_id'));

                        $dimensions = array($length, $width, $height);
                        sort($dimensions);

                        if (($weight <= $max_weight) && ($dimensions[0] <= $dimensions_omniva[0]) && ($dimensions[1] <= $dimensions_omniva[1]) && ($dimensions[2] <= $dimensions_omniva[2])) {
                            if ($this->config->get('omniva_' . $tab_zone . '_cost_option') == 'each_product')
                                $total_cost += $cost * $product['quantity'];
                        } else {
                            $status = false;
                            break;
                        }
                    }

                    if ($status) {
                        if (isset($omniva_addresses['omniva_addresses_' . $tab_zone . '_addresses'])) {
                            $new_array = [];

                            sort($omniva_addresses['omniva_addresses_' . $tab_zone . '_addresses']);
                            foreach ($omniva_addresses['omniva_addresses_' . $tab_zone . '_addresses'] as $cabin => $value) {
                                $group = explode(',',explode(' - ', $value)[1])[0];
                                $new_array[$group][$cabin] = $value;

                            }

                            $first = '';

                            $cabine_select = '<script>$( "input[name=shipping_method]" ).focus(function() { $( this ).blur(); });</script>
                               <select name="omniva" id="omniva" style="width:245px;"
                                onchange="$(this).parent().find(\'input\').eq(0).val($(this).val()); $(this).parent().find(\'input\').eq(0).prop(\'checked\',true).trigger(\'change\');" 
                                onfocus="$(this).parent().find(\'input\').eq(0).prop(\'checked\',true);">';

                            foreach($new_array as $group_name => $group) {
                                $cabine_select                 .= '<optgroup label="'.$group_name.'">';
                                foreach ($group as $cabin => $value) {
                                    if (!$first)
                                        $first = $cabin;

                                    $cabine_select                 .= '<option value="omniva.omniva_' . $cabin . '">' . $value . '</option>' . "\n";
                                    $sub_quote['omniva_' . $cabin] = [
                                        'code'         => 'omniva.omniva_' . $cabin,
                                        'title'        => $this->language->get('text_description') . ': ' . $value,
                                        'cost'         => $total_cost,
                                        'tax_class_id' => $this->config->get('omniva_' . $tab_zone . '_tax_class_id'),
                                        'text'         => $value . ' ' . $this->currency->format($this->tax->calculate($total_cost, $this->config->get('omniva_' . $tab_zone . '_tax_class_id'), $this->config->get('config_tax')))
                                    ];

                                    $sub_quote['omniva_fake' . $cabin] = [
                                        'code'         => 'omniva_fake' . $cabin,
                                        'title'        => '<div id="omniva_fake' . $cabin . '"><script>$(\'#omniva_fake' . $cabin . '\').parent().parent().hide().prev().hide();</script></div>',
                                        'cost'         => $total_cost,
                                        'tax_class_id' => $this->config->get('omniva_' . $tab_zone . '_tax_class_id'),
                                        'text'         => 'fake'
                                    ];
                                }
                                $cabine_select                 .= '</optgroup>';
                            }
                            $cabine_select .= '</select>
';

                            $quote_data['omniva'] = array(
                                'code' => 'omniva.omniva_' . $first,
                                'title' => $this->language->get('text_title') . ' ' . $cabine_select . ' ',
                                'cost' => $total_cost,
                                'tax_class_id' => $this->config->get('omniva_' . $tab_zone . '_tax_class_id'),
                                'text' => $this->currency->format($this->tax->calculate($total_cost, $this->config->get('omniva_' . $tab_zone . '_tax_class_id'), $this->config->get('config_tax')))
                            );
                        }
                    }
                    
                    if ($quote_data) {
                        $method_data = array(
                            'code' => 'omniva',
                            'title' => $this->language->get('text_title'),
                            'quote' => array_merge($quote_data, $sub_quote),
                            'sort_order' => $this->config->get('omniva_sort_order'),
                            'cost' => ($current_country) ? $this->currency->format($this->tax->calculate($total_cost, $this->config->get('omniva_' . $current_country . '_tax_class_id'), $this->config->get('config_tax'))) : '',
                            'error' => false
                        );
                    }
                }
            }
        }
        
        if (isset($method_data))
            return $method_data;
    }

}