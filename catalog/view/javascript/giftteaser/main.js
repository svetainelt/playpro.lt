(function(){
	// Polyfills for String.prototype.startsWith and endsWith on Internet Explorer
	if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position) {
			position = position || 0;
			return this.indexOf(searchString, position) === position;
		};
	}

	if (!String.prototype.endsWith) {
		String.prototype.endsWith = function(search, this_len) {
			if (this_len === undefined || this_len > this.length) {
				this_len = this.length;
			}
			return this.substring(this_len - search.length, this_len) === search;
		};
	}

	var giftTeaserConfig = {
		// don't put the same things in the cart update and check option urls, nor any ajax urls that get triggered by abortPropagationSelector clicks
		cartUpdateUrls: [
			// vanilla open cart
			'cart/add',
			'cart/remove',
			// journal
			'journal2/checkout/cart_update',
			'journal2/checkout/cart_delete',
			'journal3/checkout/cart_update',
			'journal3/checkout/cart_delete'
		],
		// used when trying to detect a product id on the page
		productIdInputs: ['product_id'],
		productIdQueryParams: ['product_id'],
		// elements after which the "this product is available as a gift" message is inserted
		insertGiftNoticeAfter: ['.breadcrumb'],
		// urls for pages on which we want to perform a check for gift options and display the dialog
		checkOptionGiftsURLs: [
			'index.php?route=checkout/cart',
			'index.php?route=checkout/checkout'
		],
		// selectors for elements whose click handler will be ignored if the customer still needs to enter options for a gift
		abortPropagationSelectors: [
			// pairs of selectors - we register the callback as a delegate on the first one, listening for clicks on the second one (see injectClickHandlers)
			// done because vanilla opencart delegates click handlers to the document
			// when trying to stop a delegated event, the first selector specified here needs to be between the original one holding the delegated handler and the clicked element in the dom
			// ['body', '#button-account'] // here for example, we're stopping an event registered as $(document).on('click', '#button-account'), by stopping propagation in the body events (before we bubble up to the document events)
			// for an element whose handler is registered directly ($('#myel').click(function{})), you can use any parent dom element as the first selector, including document (no quotes)
			['body', '#button-payment-method'],
			['body', '#button-confirm'],
			['body', '#journal-checkout-confirm-button']
		]
	};

	$(document).ajaxComplete(function(event, request, settings) {
		// listen for ajax requests that modify the cart and display the options dialog if necessary
		if (isACartUpdateRequest(settings.url)) {
			checkForOptionGifts(true, receiveOptionsResponse);
		}
	});

	$(document).ready(function() {
		// check if the current page is a product page for something that's offered as a gift
		let productId = detectProductId();
		if (productId != null) {
			checkIfProductIsGift(productId);
		}
		// check if there are any gifts without options (only on the specified pages so we don't bother the customer while they are browsing)
		let len = giftTeaserConfig.checkOptionGiftsURLs.length;
		let urlPath = getUrlPath();
		for (let i=0; i<len; ++i) {
			if (urlPath.endsWith(giftTeaserConfig.checkOptionGiftsURLs[i])) {
				checkForOptionGifts(true, receiveOptionsResponse);
				break;
			}
		}
		injectClickHandlers();
	});

	function injectClickHandlers() {
		// registers the stopForOptions handler according to giftTeaserConfig.abortPropagationSelectors
		let len = giftTeaserConfig.abortPropagationSelectors.length;
		for (let i=0; i<len; ++i) {
			$(giftTeaserConfig.abortPropagationSelectors[i][0]).on('click', giftTeaserConfig.abortPropagationSelectors[i][1], stopForOptions);
		}
	}

	function stopForOptions(event) {
		// event handler for elements before whose click we want to check for gift options
		// stops propagation and shows the options dialog when necessary
		json = checkForOptionGifts(false);
		if (json.length > 0) {
			event.stopPropagation();
			showOptionsDialog();
		}
	}

	function isACartUpdateRequest(url) {
		// checks if the provided url matches one of the configured cart modifying requests
		let len = giftTeaserConfig.cartUpdateUrls.length;
		for (let i=0; i<len; ++i) {
			if (url.indexOf(giftTeaserConfig.cartUpdateUrls[i]) != -1) return true;
		}
		return false;
	}

	function checkForOptionGifts(async, then) {
		// performs the check whether the customer has gifts requiring options in their cart
		let retValue = null;
		$.ajax({
			url: 'index.php?route=module/giftteaser/optionGifts',
			async: async,
			type: 'POST',
			dataType: 'json',
			success: function(json) {
				if (json.receivedGift) {
					showReceivedGiftNotification(json.notificationText);
				}
				if (!async) {
					retValue = json.withOptions;
				} else {
					then(json.withOptions);
				}
			}
		});
		return retValue;
	}

	function receiveOptionsResponse(json) {
		// success handler for checking if the customer has any gifts in his cart that require options
		if (json.length > 0) {
			showOptionsDialog();
		}
	}

	function showReceivedGiftNotification(text) {
		if ($('.notification-cart').length ) { // Journal 3
			$('.notification-cart .notification-text').append('<div style="background: #a7ffb9;padding:5px 8px;line-height: 17px;font-size: 14px;margin-top: 6px;">' + text + '</div>');
		} else if ($('.alert .ui-pnotify-text .notification-buttons').length) { // Journal 2
			$('.alert .ui-pnotify-text .notification-buttons').before('<div style="background: #a7ffb9;padding:5px 8px;line-height: 17px;font-size: 14px;margin-top: 6px;">' + text + '</div>');
		} else {
			$('#content').parent().before(
				'<div class="alert alert-success gift-success success"><i class="fa fa-check-circle"></i> ' + text + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>'
			);
		}
	}

	function showOptionsDialog() {
		// fetches and displays the dialog for selecting gift options
		$.ajax({
			url: 'index.php?route=module/giftteaser/giftTeaserOptionsDialog',
			type: 'GET',
			dataType: 'html',
			success: function(html) {
				$.fancybox.open({
					content: html,
					width:'40%',
					height:'50%',
					openEffect : 'elastic',
					openSpeed  : 150,
					fitToView   : true,
					closeBtn  : true,
				});
			}
		})
	}

	function checkIfProductIsGift(product_id) {
		// checks if the product on this page is available as a gift and inserts the little notification the customer can get it for free
		$.ajax({
			url: 'index.php?route=module/giftteaser/showGiftBox',
			type: 'post',
			data: {product_id:product_id},
			dataType: 'json',
			success: function(json) {
				if (json['ActiveGift']) {
					$('<style type="text/css">'+json['custom_css']+'</style>').appendTo('head');

					let teaserClass = '';
					if ($('html').data('jv')) {
						teaserClass = 'container';
					}

					var gift_box_container = '<div class="freeGiftsMessageWrapper ' + teaserClass + '"><div class="giftBoxHeading">'+json['GiftNote']+'</div><div class="giftBoxDescription">'+json['GiftNoteDescription']+'</div></div>';
					var product_cont = null;
					let len = giftTeaserConfig.insertGiftNoticeAfter.length;
					for (let i=0; i<len; ++i) {
						product_cont = $(giftTeaserConfig.insertGiftNoticeAfter[i]);
						if (product_cont.length > 0) break;
					}
					if (product_cont.length > 0) {
						product_cont.after(gift_box_container);

						var styles = {
							'background-color' : json['NoteBackgroundColor'],
							'color': json['NoteFontColor'],
							'border-color': json['NoteBorderColor'],
						};

						$('.freeGiftsMessageWrapper').css(styles);
					}
				}
			}
		});
	}

	function detectProductId() {
		// look for a hidden product_id input field
		let len = giftTeaserConfig.productIdInputs.length;
		for (let i=0; i<len; ++i) {
			let pidInput = $('input[name="' + giftTeaserConfig.productIdInputs[i] + '"]');
			if (pidInput.length == 1 && (pidInput.attr('type') == 'hidden' || pidInput.is(':hidden'))) {
				return pidInput.val();
			}
		}

		// Journal 3
		let journalPidInput = $('input[name="' + giftTeaserConfig.productIdInputs + '"][id="product-id"]').val();
		if (journalPidInput) {
			return journalPidInput;
		}

		// check if the page url contains a product_id query param
		len = giftTeaserConfig.productIdQueryParams.length;
		for (let i=0; i<len; ++i) {
			let pid = getParameterByName(giftTeaserConfig.productIdQueryParams[i]);
			if (pid != '') return pid;
		}

		return null;
	}

	function getParameterByName(name) {
		// extracts query parameters from the page url
		url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	function getUrlPath() {
		// returns only the request uri part of the page url (no leading slash)
		url = window.location.href;
		origin = window.location.origin;
		path = url.replace(origin, '');
		if (path.startsWith('/')) path = path.substr(1);
		return path;
	}
})();
