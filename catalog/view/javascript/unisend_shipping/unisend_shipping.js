const terminalMatcher = (params, data) => {
  const originalMatcher = $.fn.select2.defaults.defaults.matcher;
  const result = originalMatcher(params, data);
  if (
      result &&
      data.children &&
      result.children &&
      data.children.length
  ) {
    if (
        data.children.length !== result.children.length &&
        data.text.toLowerCase().includes(params.term.toLowerCase())
    ) {
      result.children = data.children;
    }
    return result;
  }
  return null;
}

$.fn.bindFirst = function (name, fn) {
  this.on(name, fn);
  this.each(function () {
    var handler, handlers;
    handlers = $._data(this, 'events')[name.split('.')[0]];
    handler = handlers.pop();
    handlers.splice(0, 0, handler);
  });
};

(function( $ ) {
	'use strict';

  window.unisendShipping = {};
  $(document).delegate('#button-loggedorder, #qc_confirm_order, #quick-checkout-button-confirm', 'click', function (e) {
    return processValidation(this, e);
  });


  $(document).ready(function () {

    console.log('asdasd');

    setTimeout(() => {
      bindFirstButtonConfirm();
    }, 1000);

    const container = $('.one-page-checkout .checkout-shipping-methods, #checkout-checkout, .content-delivery-method, .quick-checkout-wrapper, #d_quickcheckout, #collapse-shipping-method, #content .panel-group');
    if (container && container.length > 0) {
      setTimeout(() => {
        processSelectedShippingMethod();
      }, 300);

      $(container).on('change', function () {
        if (this.id === 'd_quickcheckout') {
          setTimeout(() => {
            processSelectedShippingMethod();
          }, 2000);
        } else {
          console.log('asdasd1');
          processSelectedShippingMethod();
        }
      });
      $(container).on('click', '#button-shipping-method', function (e) {
        return processValidation(this, e);
      });
    }

  });

  function stopEvent(event) {
    event.stopImmediatePropagation();
    event.preventDefault();
    journalPreloaderStop();
    if (typeof preloaderStop === "function") {
      setTimeout(() => {
        preloaderStop();
      }, 1000)
    }
  }

  function journalPreloaderStop() {
    if ($(".journal-loading-overlay").length) {
      $(".journal-loading-overlay").hide();
      $("#quick-checkout-button-confirm").button("reset");
      $([document.documentElement, document.body]).animate({
        scrollTop: $("input[type=radio][name=shipping_method]:checked").offset().top - 200
      }, 1000);
    }
  }

  function processSelectedShippingMethod() {
    const shippingMethodElement = $('input[type=radio][name=shipping_method]:checked');
    if (shippingMethodElement.length > 0) {
      const shippingMethod = shippingMethodElement.val();
      const terminalOptionsElm = $('#unisend_shipping_terminals_options');
      if (window.unisendShipping.shippingMethod === shippingMethod && terminalOptionsElm.length > 0) {
        return;
      }
      window.unisendShipping.shippingMethod = shippingMethod;
      terminalOptionsElm.remove();
      createUnisendTerminalsOptions();
    }
  }

  function bindFirstButtonConfirm() {
    $('#button-confirm').bindFirst('click', function (e) {
      return processValidation(this, e);
    });
  }

  function displayApiError() {
    $('input[type=radio][name=shipping_method]:checked').parent().append("<div class='alert alert-danger alert-dismissible'>" + window.unisendShipping.error + "<button type='button' class='close' style='padding-right: 0.4em;' data-dismiss='alert'>&times;</button></div>");
  }

  function resetApiError() {
    delete window.unisendShipping.error;
  }

  function apiErrorExists() {
    return window.unisendShipping.error != null;
  }

  function isJournalCheckout() {
    return $('#quick-checkout-button-confirm').length > 0;
  }

  function isJournalElement(element) {
    return $(element).attr('id') === 'quick-checkout-button-confirm';
  }

  function doProcessApiValidation(element) {
    if (apiErrorExists()) {
      return false;
    }
    if (isJournalCheckout() && !isJournalElement(element)) {
      return false;
    }
    return true;
  }

  function processValidation(element, event) {
    if (doProcessApiValidation(element)) {
      apiValidate();
    }
    if (apiErrorExists()) {
      if (!isJournalElement(element)) {
        displayApiError();
        resetApiError();
      } else {
        bindFirstButtonConfirm();
      }
      stopEvent(event);
      return false;
    }
    return true;
  }

  function createUnisendTerminalsOptions() {
    const target = $('input[type=radio][name=shipping_method]:checked');
    const targetValue = target.val().split(':')[0];
    if (targetValue === 'unisend_shipping.unisend_shipping_terminal') {
      if ($('#unisend_shipping_terminals_options optgroup').length === 0) {
        $(target).parent().append("<div id='unisend_shipping_terminals_options' style='display: none;'><select class='unisend_shipping_terminals_options' name='unisend_shipping_terminals_options'><option></option></select><div style='clear: both; padding-top: 15px; text-align: right;'></div></div><div style='margin: 15px 0; display: none; height: 300px;'></div>");
        showOptions();
      }
      $('#unisend_shipping_terminals_options').show();
    }
  }

  async function showOptions() {
    if (!window.unisendShipping.terminals) {
      await loadTerminals();
    }
    if (window.unisendShipping.terminals) {
      createTerminalsOptions(window.unisendShipping.terminals);
    }
  }

  function createTerminalsOptions(cityTerminals) {
    const data = cityTerminals.map(value => ({
      text: `${value.name}`,
      children: value.terminals.map(terminal => ({
        id: `${terminal.id}`,
        text: `${terminal.name}, ${terminal.address}`,
        selected: window.unisendShipping.selectedTerminalId && terminal.id === window.unisendShipping.selectedTerminalId
      })),
    }));
    $('.unisend_shipping_terminals_options').select2({
      placeholder: window.unisendShipping.translations['text_shipping_unisend_shipping_checkout_select_parcel_locker_placeholder'],
      width: 'resolve',
      matcher(params, data) {
        return terminalMatcher(params, data);
      },
      data: data,
      });
    $(".unisend_shipping_terminals_options").on("change", function () {
      const selectedOption = $(this).find("option:selected");
      const terminalId = selectedOption.attr('value');
      const terminalName = selectedOption.text();
      if (!isNaN(terminalId)) {
        saveSelectedTerminal(terminalId, terminalName);
      }
    });
    }

  function loadTerminals() {
    return new Promise((resolve, reject) => {
      $.get('index.php?route=shipping/unisend_shipping/terminals', function (data) {
        window.unisendShipping.translations = data['translations'];
        window.unisendShipping.terminals = data['terminals'];
        return resolve(window.unisendShipping.terminals);
      }, 'json');
    });
  }

  function appendInputElementData(formData, elementName) {
    $("input[name^='" + elementName + ".']").each(function (i, element) {
      const value = $(element).val();
      if (value && typeof value === 'string') {
        const name = $(element).attr('name').replace(elementName + '.', '');
        formData.append(elementName + '[' + name + ']', $(element).val());
      }
    });
  }

  function appendSelectOptionElementData(formData, elementName) {
    $("select[name^='" + elementName + ".']").each(function (i, element) {
      const optionElement = $(this).children('option:selected');
      const value = $(optionElement).val();
      if (value && typeof value === 'string') {
        const name = $(element).attr('name').replace(elementName + '.', '');
        formData.append(elementName + '[' + name + ']', $(element).val());
      }
    });
  }

  function apiValidate() {
    var formData = new FormData;
    appendInputElementData(formData, "payment_address");
    appendInputElementData(formData, "shipping_address");
    appendSelectOptionElementData(formData, "payment_address");
    appendSelectOptionElementData(formData, "shipping_address");
    formData.append('shipping_method', window.unisendShipping.shippingMethod);
    if (window.unisendShipping.selectedTerminalId) {
      formData.append('terminalId', window.unisendShipping.selectedTerminalId);
      formData.append('terminalName', window.unisendShipping.selectedTerminalName);
    }

    $.ajax({
      url: 'index.php?route=shipping/unisend_shipping/validate',
      type: "POST",
      async: false,
      processData: false,
      contentType: false,
      data: formData,
      success: function (data) {
        if (data['error']) {
          window.unisendShipping.error = data['error'];
        }
      }
    });
  }

  function saveSelectedTerminal(terminalId, terminalName) {
    window.unisendShipping.selectedTerminalId = terminalId;
    window.unisendShipping.selectedTerminalName = terminalName;
    $.post("index.php?route=shipping/unisend_shipping/save_selected_terminal",
        {
          name: terminalName,
          id: terminalId
        }
    )
  }
})( jQuery );
