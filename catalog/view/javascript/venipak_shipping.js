(function(send) {
  XMLHttpRequest.prototype.send = function(body) {
    if (body && body.indexOf("shipping_method=") === 0) {
      const value = $('select.venipak_shipping_pickup_points').val();
      body = `${body}&venipak_shipping_pickup_point=${value}`;
    }
    send.call(this, body);
  };
})(XMLHttpRequest.prototype.send);

(function( $ ) {
	'use strict';

	window.venipakShipping = {
    markers: new Map(),
    init,
  };

  fetchCheckoutSettings();

  async function init() {
    $(window).on('load', function(){
      if ($('input[type=radio][name=shipping_method]:checked').val() === 'venipak_shipping.venipak_shipping_pickup') {
        showOptions();
      }
    });
  	$('input[type=radio][name=shipping_method]').change((e) => {
			if (e.target.value === 'venipak_shipping.venipak_shipping_pickup') {
				showOptions();
			} else {
        $('#venipak_shipping_pickup_options').hide();
        $('#map').hide();
			}
		});
  }

  async function showOptions() {
  	const pickupPointsCollection = await fetchPickupPoints();
    if (window.venipakShipping.settings.is_map_enabled) {
      await initMap(pickupPointsCollection);
    }
    initPickupSelect(pickupPointsCollection);
    if (window.venipakShipping.settings.is_map_enabled) {
      drawPickupMarkers(pickupPointsCollection);
      $('#map').show();
      showAllMarkers();
    }


    $.ajax({
      url: 'index.php?route=journal2/checkout/get_venipak',
      type: 'get',
      dataType: 'json',
      success: function(json) {
        if (json !== null) {
          $('.venipak_shipping_pickup_points').val(json);
          $('.venipak_shipping_pickup_points').change();
          $('#venipak_shipping_pickup_options').show();
        }
        else {
          $('#venipak_shipping_pickup_options').show();
        }
        $('select[name=venipak_shipping_pickup_point]').on('change', function (e) {
          $.ajax({
            url: 'index.php?route=journal2/checkout/save_venipak',
            type: 'post',
            data: {
              venipak_value: $('select[name=venipak_shipping_pickup_point] option:selected').val(),
            },
            dataType: 'json',
            success: function() {

            }
          });
        });
      }
    });
  }

  function showAllMarkers() {
    window.venipakShipping.map.setCenter(window.venipakShipping.mapBounds.getCenter());
    window.venipakShipping.map.fitBounds(window.venipakShipping.mapBounds);
  }


  function drawPickupMarkers(pickupPointsCollection) {
    const map = window.venipakShipping.map;
    const pickup_image = {
      url: window.venipakShipping.settings.pickup_marker,
      scaledSize: new google.maps.Size(30, 43),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 22),
    };
    const locker_image = {
      url: window.venipakShipping.settings.locker_marker,
      scaledSize: new google.maps.Size(30, 43),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 22),
    };
    const markerCollection = [];
    window.venipakShipping.mapBounds = new google.maps.LatLngBounds();
    for (let i = 0; i < pickupPointsCollection.length; i++) {
      const marker = pickupPointsCollection[i];
      const markerEntity = new google.maps.Marker({
        position: { lat: parseFloat(marker.lat), lng: parseFloat(marker.lng) },
        map,
        title: marker.name,
        icon: marker.type === 1 ? pickup_image : locker_image,
      });
      window.venipakShipping.markers.set(marker.id, markerEntity);
      markerCollection.push(markerEntity);
      markerEntity.addListener("click", () => {
        $('.venipak_pickup_point').val(JSON.stringify(marker)).trigger('change');
        window.venipakShipping.activeMarker = markerEntity;
        setSelectedPickupInfo(marker);
      });
      const infoWindow = getInfoWindow(marker);
      markerEntity.addListener('mouseover', function() {
        infoWindow.open(map, this);
      });

      // assuming you also want to hide the infowindow when user mouses-out
      markerEntity.addListener('mouseout', function() {
        infoWindow.close();
      });
      window.venipakShipping.mapBounds.extend(markerEntity.getPosition());
    }
    if (!!window.venipakShipping.settings.is_clusters_enabled) {
      new MarkerClusterer(map, markerCollection, {
        imagePath:
          "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
      });
    }
  }

  function getInfoWindow(marker) {
    const { name, address, city, zip, working_hours } = marker;
    let contentString = `<span>${name}<br /><small>${address}, ${city}, ${zip}</small></span>`;
    if (working_hours) {
      contentString += getWorkingHours(working_hours);
    }

    return new google.maps.InfoWindow({
      content: contentString,
    });
  }

  function getWorkingHours(data) {
    let result = '';
    const hoursCollection = JSON.parse(data);  
    let dayFrom = 0;
    let dayTo = 0;
    let i = 0;
    while (dayTo < hoursCollection.length - 1) {
      if (i === 0) {
        dayFrom = i;
        dayTo = i;
        i++;
        continue
      }
      if (i < hoursCollection.length - 1 && JSON.stringify(hoursCollection[i]) === JSON.stringify(hoursCollection[dayFrom])) {
        dayTo = i;
        i++;
        continue;
      } else {
        if (i === hoursCollection.length - 1) {
          dayTo++;
        }
        result += `<div>${getDaysString(dayFrom, dayTo)}: ${hoursCollection[dayFrom].from_h.padStart(2, "0")}:${hoursCollection[dayFrom].from_m.padStart(2, "0")} - ${hoursCollection[dayTo].to_h.padStart(2, "0")}:${hoursCollection[dayTo].to_m.padStart(2, "0")}</div>`;
        dayFrom = dayTo + 1;
        dayTo = dayFrom;
        i++;
      }
    }
    return result;
  }

  function getDaysString(dayFrom, dayTo) {
    if (dayFrom === dayTo) {
      return getName(dayFrom);
    }
    return `${getName(dayFrom)}-${getName(dayTo)}`;

    function getName(day) {
      switch (day) {
        case 0:
          return 'I';
          break;
        case 1: 
          return 'II';
          break;
        case 2:
          return 'III';
          break;
        case 3:
          return 'IV';
          break;
        case 4:
          return 'V';
          break;
        case 5:
          return 'VI';
          break;
        case 6:
          return 'VII';
          break;
      }
    }
  }

  function getUserLocation(address, city, postcode) {
    return new Promise(async (resolve, reject) => {
      if (!address) {
        const position = await getLocationByIp();
        resolve(position);
      } else  {
        $.get(
          "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "+" + city + "+" + postcode + "&key=" + window.venipakShipping.settings.googlemap_api_key,
          function(data) {
            if (data.results.length === 0) {
              resolve(null);
            }
            if (data.results[0] && data.results[0].geometry) {
              return resolve(data.results[0].geometry.location);
            }
            return resolve();
          },'json');
        }
    });
  }

  function getLocationByIp() {
    return new Promise ((resolve) => {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          return resolve({ lat: position.coords.latitude, lng: position.coords.longitude });
        }
      );
    });
  }

  function initMap() {
    return new Promise((resolve, reject) => {
      if (window.venipakShipping.map) {
        window.initMap();
        return resolve(window.venipakShipping.map);
      }
      const script = document.createElement('script');
      script.src = "https://maps.googleapis.com/maps/api/js?key=" + window.venipakShipping.settings.googlemap_api_key + "&callback=initMap";
      script.defer = true;
      window.initMap = function() {
        window.venipakShipping.map = new google.maps.Map(document.getElementById("map"));
        return resolve(window.venipakShipping.map);
      };
      document.head.appendChild(script);
    });
  }

  function setSelectedPickupInfo(data) {
    const { name, address, city, working_hours } = data;
    let result = `<div><b>${name}</b>, ${address}, ${city}</div>`;
    if (working_hours) {
      result += getWorkingHours(working_hours);
    }
    $('#selected-pickup-info').html(result);

  }

  async function findAddressByInputText() {
    const map = window.venipakShipping.map;
    if (!map) return;
    const text = $('.select2-search__field').val();
    const position = await getCustomLocation(text);
    if (position && position.lat && position.lng) {
      map.setCenter(position);
      map.setZoom(15);
    }
  }

  function getCustomLocation(keyword) {
    return new Promise((resolve, reject) => {
      $.get(
        "https://maps.googleapis.com/maps/api/geocode/json?address=" + keyword + "&key=" + window.venipakShipping.settings.googlemap_api_key,
        (data) => {
          if (data.results.length === 0) {
            resolve(null);
          }
          if (data.results[0] && data.results[0].geometry.location) {
            return resolve(data.results[0].geometry.location);
          }
          return data.results;
        },
        'json',
      );
    });
  }

  function initPickupSelect(collection) {
    const map = window.venipakShipping.map;
    $('.venipak_shipping_pickup_points').select2({
      data: collection.map(value => ({
        id: JSON.stringify(value),
        text: `${value.name}|${value.address}|${value.city}|${value.zip}`,
      })),
      // matcher: matchCustom,
      templateResult,
      templateSelection,
    });
    if (map) {
      $('.venipak_shipping_pickup_points').on('select2:select', function (e) {
        const markerData = JSON.parse(e.params.data.id);
        window.venipakShipping.activeMarker = window.venipakShipping.markers.get(markerData.id);
        map.setCenter(window.venipakShipping.activeMarker.getPosition());
        map.setZoom(15);
        map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
        map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        setSelectedPickupInfo(markerData);
      });

      $('body').on('DOMSubtreeModified', '.select2-results', debounce(function () {
        const markers = [];
        const filteredRows = $('.select2-results__options li');
        if (filteredRows.length === 1 && filteredRows[0].attributes.role.value === 'alert') {
          findAddressByInputText();
          return;
        }
        if (filteredRows.length - 1 === window.venipakShipping.markers.size) {
          return;
        }
        filteredRows.map(function() {
          if (!this.id) return;
          const marker = this.id.slice(53);
          const markerData = JSON.parse(marker);
          const markerEntity = window.venipakShipping.markers.get(markerData.id);
          markers.push(markerEntity);
        });
        if (markers.length > 0) {
          const bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
          }
          window.venipakShipping.map.setCenter(bounds.getCenter());
          window.venipakShipping.map.fitBounds(bounds);
        }
      }, 1000));
    }

    function templateResult (item) {
      if (!item.id) return item.text;
      const [text, address, city, zip] = item.text.split('|');
      return $(`<span>${text}<br /><small>${address}, ${city}, ${zip}</small></span>`);
    }
    function templateSelection (item) {
      if (!item.id) return item.text;
      const [text] = item.text.split('|');
      return text;
    }

    function matchCustom(params, data) {
      // If there are no search terms, return all of the data
      if ($.trim(params.term) === '') {
        return data;
      }

      // Do not display the item if there is no 'text' property
      if (typeof data.text === 'undefined') {
        return null;
      }

      if (data.text.toLowerCase().includes(params.term.toLowerCase())) {
        return data;
      }

      // Return `null` if the term should not be displayed
      return null;
    }
  }

  function fetchPickupPoints() {
    return new Promise((resolve, reject) => {
      $.get('/index.php?route=shipping/venipak_shipping/pickups', function(data) {
        window.venipakShipping.pickupPoints = data;
        return resolve(window.venipakShipping.pickupPoints);
      }, 'json');
    });
  }

  function fetchCheckoutSettings() {
  	return new Promise((resolve, reject) => {
      $.get('/index.php?route=shipping/venipak_shipping/settings', function(data) {
        window.venipakShipping.settings = data;
        return resolve(window.venipakShipping.settings);
      }, 'json');
    });
  }


function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

})( jQuery );
