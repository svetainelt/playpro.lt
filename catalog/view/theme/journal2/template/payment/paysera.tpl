<style type="text/css">
    #wtp-checkout table tr td {
        border: none;
    }

    .w2p-table img {
        cursor: pointer;
        margin: 0px;
        /* vertical center */
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }

    .w2p-table td.radio {
        width: 30px;
    }

    .paysera-payment-methods {

    }
    .payment-title {
        margin: 5px 0;
    }
    .paysera-payment {

    }
    .payment-box {
        <?php if($this->journal2->settings->get('one_page_status', 'default') === 'one-page'): ?>
            width: 33.33%;
        <?php else: ?>
            width: 25%;
        <?php endif; ?>
        float: left;
    }

    .paysera-select-country .col-2 {
        width: 48%;
    }
            
    @media only screen and (max-width: 760px) {
        .payment-box {
            width: 50%;
        }
    }

    .payment-box .box-image {
        height: 100px;
        margin: 5px;
        border: 1px solid #c9c9c9;

        text-align: center;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }
    .payment-box .box-image:hover {
        border-color: rgb(95, 104, 116);
    }
    .payment-box .box-image img {
        max-width: 150px !important;
        height: auto;
        /*margin: 0 auto;*/
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#w2p-cs').change(function () {
            var value = $('#w2p-cs option:selected').val();
            $('input[name="pmethod"]').each(function () {
                $(this).attr('checked', false);
            });
            $('.w2p-table').each(function () {
                var tid = $(this).attr('rel');
                if (tid == value) {
                    $(this).css({'display' : 'block'});
                } else {
                    $(this).css({'display' : 'none'});
                }
            });
        });
    });

    function submit_form(el) {
        var val = el.parent().find('.radio').val();
        $('#payment_key').val(val);
        if($('#qc_confirm_order').length) {
            $('#qc_confirm_order').click();
        } else if($('#journal-checkout-confirm-button').length) {
            $('#journal-checkout-confirm-button').click();
        } else {
            $('#wtp-checkout').submit();
        }
    }

//    $('.confirm-button').on('click', function () {
//        $('#wtp-checkout').submit();
//    });
    $('#button-confirm').on('click', function () {
        $('#wtp-checkout').submit();
    });
</script>

<div class="content">
    <form action="<?php echo $action; ?>" method="post" id="wtp-checkout">        
        <div class="paysera-select-country">
            <div class="col-2 left">
                <a href="http://www.paysera.com" target="_blank">
                    <img style="float: left; margin: 0px 10px 5px 0px;" alt="Paysera" src="https://www.paysera.com/payment/m/m_images/wfiles/iwsnwf1830.png">
                </a>                
            </div>
            
            <div class="col-2 right">
                <?php if($paysera_display_payments == 1): ?>
                    <?php echo $text_chosen; ?>
                    <br />
                    <?php echo $text_paycountry; ?>
                    
                    <select id="w2p-cs">
                        <?php if($use_filters): ?>
                            <?php foreach($countries as $country): ?>
                                <?php if(in_array($country->getCode(), $allowed_countries)): ?>
                                    <?php if($country->getCode() == $country_iso): ?>
                                        <option value="<?php echo $country->getCode(); ?>" selected="selected"><?php echo $country->getTitle(); ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $country->getCode(); ?>"><?php echo $country->getTitle(); ?></option>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?php foreach($countries as $country ): ?>
                                <?php if($country->getCode() == $country_iso): ?>
                                    <option value="<?php echo $country->getCode(); ?>" selected="selected"><?php echo $country->getTitle(); ?></option>
                                <?php else: ?>
                                    <option value="<?php echo $country->getCode(); ?>"><?php echo $country->getTitle(); ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                <? endif; ?>
            </div>
            
            <div style="clear: both;"></div>
        </div>

        <?php if($paysera_display_payments == 1): ?>
            <div class="paysera-payment-methods">
                <?php if($use_filters): ?>
                    <?php foreach ($countries as $country): ?>
                        <?php if(in_array($country->getCode(), $allowed_countries)): ?>
                            <div rel="<?php echo $country->getCode(); ?>" class="w2p-table" style="display: <?php echo ($country->getCode() == $country_iso) ? 'block' : 'none'; ?>;">
                                <?php foreach ($country->getGroups() as $group): ?>
                                    <div class="payment-title">
                                        <b><?php echo $group->getTitle(); ?></b>
                                    </div>
                                    <div class="paysera-payment">
                                        <?php foreach ($group->getPaymentMethods() as $paymentMethod): ?>
                                            <?php $payment_key = $paymentMethod->getKey() . '_' . $country->getCode(); ?>
                                            <?php if(in_array($payment_key, $allowed_payments)): ?>
                                                <?php if ($paymentMethod->getLogoUrl()): ?>
                                                    <div class="payment-box">
                                                        <div class="box-image">
                                                            <input type="hidden" class="radio" name="payment" value="<?php echo $paymentMethod->getKey(); ?>" />
                                                            <img src="<?php echo $paymentMethod->getLogoUrl(); ?>" 
                                                                title="<?php echo htmlentities($paymentMethod->getTitle()); ?>" 
                                                                alt="<?php echo htmlentities($paymentMethod->getTitle()); ?>" 
                                                                onclick="submit_form($(this));" />
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <div style="clear:both;"></div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php foreach ($countries as $country): ?>
                        <div rel="<?php echo $country->getCode(); ?>" class="w2p-table" style="display: <?php echo ($country->getCode() == $country_iso) ? 'block' : 'none'; ?>;">
                            <?php foreach ($country->getGroups() as $group): ?>
                                <div class="payment-title">
                                    <b><?php echo $group->getTitle(); ?></b>
                                </div>
                                <div class="paysera-payment">
                                    <?php foreach ($group->getPaymentMethods() as $paymentMethod): ?>
                                        <?php if ($paymentMethod->getLogoUrl()): ?>
                                            <div class="payment-box">
                                                <div class="box-image">
                                                    <input type="hidden" class="radio" name="payment" value="<?php echo $paymentMethod->getKey(); ?>" />
                                                    <img src="<?php echo $paymentMethod->getLogoUrl(); ?>" 
                                                        title="<?php echo htmlentities($paymentMethod->getTitle()); ?>" 
                                                        alt="<?php echo htmlentities($paymentMethod->getTitle()); ?>" 
                                                        onclick="submit_form($(this));" />
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <div style="clear:both;"></div>
                                </div>                          
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        
        <div style="clear:both;"></div>
        <input id="payment_key" type="hidden" name="payment" value="" />
    </form>
</div>

<div class="buttons">
    <div class="pull-right">
        <button id="button-confirm" class="btn btn-primary" style="text-indent: 0px;"><?php echo $button_confirm; ?></button>
    </div>
</div>
