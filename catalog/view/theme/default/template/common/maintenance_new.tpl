<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $heading_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/animate.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/fontello.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/owl.carousel.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/jquery.countdown.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo $base; ?>maintenance/css/style.css">
    </head>
    <body>
        <section id="intro" class="fullsection">
            <div class="story">
                <?php if ($logo): ?>
                    <div id="logo" class="wow flipInY" data-wow-delay="0.2s">
                        <a href="<?php echo $base; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    </div>
                <?php endif; ?>
                <div class="welcome-text auto-center">
                    <div class="wow flipInY" data-wow-delay="0.4s">
                        <h4><?php echo $name; ?></h4>
                        <div id="owl-text">
                            <?php echo $message; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="preload">
            <div id="preload-content">
                <div class="loader2"></div>
            </div>
        </div>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $base; ?>maintenance/js/jquery.appear.min.js"></script>
        <script type="text/javascript" src="<?php echo $base; ?>maintenance/js/jquery.wow.min.js"></script>
        <script type="text/javascript" src="<?php echo $base; ?>maintenance/js/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $base; ?>maintenance/js/script-simple.js"></script>
    </body>
</html>