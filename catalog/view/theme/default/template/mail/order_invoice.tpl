<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
    <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>

<style>

    @media (min-width: 1200px) {

    .container {
        width: 1170px;
    }

    }

    @media (min-width: 992px) {

    .container {
        width: 970px;
    }

    }
    @media (min-width: 768px) {

    .container {
        width: 750px;
    }

    }

    .container {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        font-family: dejavu sans, sans-serif !important;
        font-size: 12px;
        color: #666666;
        text-rendering: optimizeLegibility;
    }

    .table-bordered {
        border: 1px solid #ddd;
    }

    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }

    table {
        background-color: transparent;
    }

    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
        border-top: 0;
    }

    .table thead > tr > td, .table tbody > tr > td {
        vertical-align: middle;
    }
    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        border-bottom-width: 2px;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table thead td {
        font-weight: bold;
    }
    td, th {
        padding: 0;
    }

</style>


<div class="container">

    <?php foreach ($orders as $order) { ?>
    <div style="page-break-after: always;">

        <h1>PVM sąskaita faktūra <?php echo $order['invoice_no']; ?></h1>

        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="2"><?php echo $text_order_detail; ?></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="width: 50%;"><address>
                        <strong><?php echo $order['store_name']; ?></strong><br />
                        <?php echo $order['store_address']; ?>
                    </address>
                    <b><?php echo $text_telephone; ?></b> <?php echo $order['store_telephone']; ?><br />
                    <?php if ($order['store_fax']) { ?>
                    <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
                    <?php } ?>
                    <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
                    <b><?php echo $text_website; ?></b> <a href="<?php echo $order['store_url']; ?>"><?php echo $order['store_url']; ?></a></td>
                <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
                    <?php if ($order['invoice_no']) { ?>
                    <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
                    <?php } ?>
                    <b><?php echo $text_order_id; ?></b> <?php echo $order['order_id']; ?><br />
                    <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
                    <?php if ($order['shipping_method']) { ?>
                    <b><?php echo $text_shipping_method; ?></b> <?php echo $order['shipping_method']; ?><br />
                    <?php } ?></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td style="width: 50%;"><b><?php echo $text_payment_address; ?></b></td>
                <td style="width: 50%;"><b><?php echo $text_shipping_address; ?></b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><address>
                        <?php echo $order['payment_address']; ?>
                    </address></td>
                <td><address>
                        <?php echo $order['shipping_address']; ?>
                    </address></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td><b><?php echo $column_product; ?></b></td>
                <td><b><?php echo $column_model; ?></b></td>
                <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
                <td class="text-right"><b><?php echo $column_price; ?></b></td>
                <td class="text-right"><b><?php echo $column_total; ?></b></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($order['product'] as $product) { ?>
            <tr>
                <td><?php echo $product['name']; ?>
                    <?php foreach ($product['option'] as $option) { ?>
                    <br />
                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                    <?php } ?></td>
                <td><?php echo $product['model']; ?></td>
                <td class="text-right"><?php echo $product['quantity']; ?></td>
                <td class="text-right"><?php echo $product['price']; ?></td>
                <td class="text-right"><?php echo $product['total']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($order['voucher'] as $voucher) { ?>
            <tr>
                <td><?php echo $voucher['description']; ?></td>
                <td></td>
                <td class="text-right">1</td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($order['total'] as $total) { ?>
            <tr>
                <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
                <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php if ($order['comment']) { ?>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td><b><?php echo $text_comment; ?></b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo $order['comment']; ?></td>
            </tr>
            </tbody>
        </table>

        <div style="clear: both;">
        <?php } ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td><b><?php echo $text_guarantee; ?></b></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo $guarantee_data; ?></td>
                </tr>
                </tbody>
            </table>
    </div>
    <?php } ?>
</div>
</body>
</html>