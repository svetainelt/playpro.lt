<products>    
<?php foreach ($products as $product) {?>
	<product id="<?php echo $product['product_id'];?>">       
	<title><![CDATA[<?php echo $product['name'];?>]]></title>
	<description><![CDATA[<?php echo $product['meta_description'];?>]]></description>
	 <?php if (isset($product['special']) AND $product['special'] != '') { ?>
		<price><?php echo $product['special'];?></price>
	<?php } else { ?>
		<price><?php echo $product['price'];?></price>
	<?php } ?>
	<condition><![CDATA[<?php echo $product['condition'];?>]]></condition>
	<stock><![CDATA[<?php echo $product['quantity'];?>]]></stock>
	<ean_code><![CDATA[<?php echo $product['ean'];?>]]></ean_code>
	<manufacturer_code><![CDATA[<?php echo $product['mpn'];?>]]></manufacturer_code>
	<manufacturer><![CDATA[<?php echo $product['manufacturer'];?>]]></manufacturer>
	<model><![CDATA[<?php echo $product['model'];?>]]></model>  
	<image_url><![CDATA[<?php echo HTTPS_SERVER.'image/'.$product['image'];?>]]></image_url>
	<product_url><![CDATA[<?php echo html_entity_decode($product['href']);?>]]></product_url>
	<purchase_url><![CDATA[<?php echo html_entity_decode($product['href']);?>]]></purchase_url>
	<category_id><![CDATA[<?php echo $product['category_id'];?>]]></category_id>
	<category_name><![CDATA[<?php echo $product['category_name'];?>]]></category_name>
	<category_link><![CDATA[<?php echo html_entity_decode($product['category_link']);?>]]></category_link>     
	<delivery_price><?php echo $product['delivery_price'];?></delivery_price>
	<delivery_time><![CDATA[<?php echo $product['delivery_time'];?>]]></delivery_time>
	</product> 
<?php } ?>       
</products>