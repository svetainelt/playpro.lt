<root>

<categories>
<?php foreach ($categories as $category) {?>

	<category>
		<id><?php echo $category['category_id'];?></id>
		<parent><?php echo $category['parent_id'];?></parent>
		<name><?php echo $category['name'];?></name>
	</category>
	<?php } ?>
</categories>


<products>
	<?php foreach ($products as $product) {?>
	<product>
		<id><?php echo $product['product_id'];?></id>
		<categories>
			<?php foreach ($product['categories'] as $product_category ) { ?>
			<category><?php echo $product_category['category_id']?></category>
			<?php } ?>
		</categories>
		
		<title><![CDATA[<?php echo $product['name'];?>]]></title>
		<model><![CDATA[<?php echo $product['model'];?>]]></model>
		<barcode><?php echo $product['ean'];?></barcode>
		<description><![CDATA[<?php echo $product['description'];?>]]></description>
		<quantity><?php echo $product['quantity'];?></quantity>
		<warranty></warranty>
		
		<?php if (isset($product['special']) AND $product['special'] != '') { ?>
			<price><?php echo $product['special'];?></price>
			<price_old><?php echo $product['price'];?></price_old>
		<?php } else { ?>
			<price><?php echo $product['price'];?></price>
			<price_old></price_old>
		<?php } ?>
		<prime_costs><?php echo $product['prime_costs'];?></prime_costs>
		<delivery_text></delivery_text>
		<weight><?php echo $product['weight'];?></weight>
		
		<manufacturer><?php echo $product['manufacturer'];?></manufacturer>
		<images>
			<image><![CDATA[<?php echo HTTPS_SERVER.'image/'.$product['image'];?>]]></image>
			<?php if ($product['images']) { ?>
				<?php foreach ($product['images'] as $image) { ?>
					<image><![CDATA[<?php echo HTTPS_SERVER.'image/'.$image['image'];?>]]></image>
				<?php } ?>
			<?php } ?>
		</images>
		
		<variants>
			<?php if (!empty($product['options'])) { ?>
				<?php foreach ($product['options'] as $options_groups) { ?>
					<?php foreach ($options_groups['product_option_value'] as $option) {?>
							<variant title="<?php echo $options_groups['name']?>">
								<title><?php echo $option['name'];?></title>
							</variant>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</variants>
		
		<attributes>
			<?php if ($product['attributes']) { ?>
				<?php foreach ($product['attributes'] as $attributes_groups) { ?>

					<?php foreach ($attributes_groups['attribute'] as $attribute) { ?>
						<attribute title="<?php echo $attribute['name'];?>"><?php echo $attribute['text'];?></attribute>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</attributes>
	</product>
	<?php } ?>
	
</products>

</root>


