<?php echo '<?xml version="1.0" encoding="utf-8"?>' ?>
<products>
<?php foreach ($products as $product) {?>
    <product id="<?php echo $product['product_id'] ?>">
        <title><![CDATA[<?php echo $product['name'];?>]]></title>
        <?php if (isset($product['special']) AND $product['special'] != '') { ?>
            <item_price><![CDATA[<?php echo $product['special'];?>]]></item_price>
        <?php } else { ?>
            <item_price><![CDATA[<?php echo $product['price'];?>]]></item_price>
        <?php } ?>
        <manufacturer><![CDATA[<?php echo $product['manufacturer'];?>]]></manufacturer>
        <image_url><![CDATA[<?php echo HTTPS_SERVER.'image/'.$product['image'];?>]]></image_url>
        <product_url><![CDATA[<?php echo html_entity_decode($product['href']);?>]]></product_url>
        <categories>
            <?php foreach ($product['categories'] as $category) { ?>
            <category><![CDATA[<?php echo $category;?>]]></category>
            <?php } ?>
        </categories>
        <description><![CDATA[<?php echo $product['meta_description'];?>]]></description>
        <stock><![CDATA[<?php echo $product['quantity'];?>]]></stock>
        <ean_code><![CDATA[<?php echo $product['ean'];?>]]></ean_code>
        <manufacturer_code><![CDATA[<?php echo $product['mpn'];?>]]></manufacturer_code>
        <model><![CDATA[<?php echo $product['model'];?>]]></model>
        <?php if (!empty($product['images'])) { ?>
        <additional_images>
            <?php foreach ($product['images'] as $image) { ?>
                <image><![CDATA[<?php echo HTTPS_SERVER.'image/'.$image['image'];?>]]></image>
            <?php } ?>
        </additional_images>
        <?php } if (!empty($product['attributes'])) { ?>
        <specs>
            <?php foreach ($product['attributes'] as $attribute_group) { ?>
                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                <spec name="<?php echo $attribute['name'];?>" value="<?php echo str_replace($name_array, $value_array, strtolower($attribute['name']));?>"><![CDATA[<?php echo $attribute['text'];?>]]></spec>
                <?php } ?>
            <?php } ?>
        </specs>
        <?php } ?>
        <delivery_time><![CDATA[<?php echo $product['delivery_time'];?>]]></delivery_time>
        <delivery_text><![CDATA[<?php echo $product['delivery_text'];?>]]></delivery_text>
	</product>
<?php } ?>
</products>