<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	  <?php if (!$show_data) { ?>
		<h2><?php echo $text_personal_data_deletion; ?></h2>
		<p><?php echo $text_hash_error_delete; ?></p>
		<div class="buttons clearfix">
		  <div class="pull-right">
			<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
		  </div>
		</div>
	  <?php } else { ?>
		<h2><?php echo $text_personal_data_deletion; ?></h2>
		<br />
		<h3><?php echo $text_confirm_the_action; ?></h3>
		<p><?php echo $text_confirm_the_action_helper; ?></p>
		<div class="buttons clearfix">
		  <div class="text-center">
			<a href="<?php echo $accept_deletion; ?>" class="btn btn-lg btn-default"><?php echo $text_button_confirm_deletion; ?></a>
			<br /><br />
			<a href="<?php echo $cancel_deletion; ?>" class="btn btn-lg btn-primary"><?php echo $text_button_cancel_deletion; ?></a>
		  </div>
		</div>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>