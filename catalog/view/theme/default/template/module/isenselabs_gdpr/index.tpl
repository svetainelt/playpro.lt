<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
		<h2><?php echo $text_data_rectification; ?></h2>
		<p><?php echo $text_data_rectification_helper; ?></p>
		<ul class="list-unstyled">
			<li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
			<li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
			<li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
			<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
		</ul>

		<h2><?php echo $text_data_portability; ?></h2>
		<p><?php echo $text_data_portability_helper; ?></p>
		<ul class="list-unstyled">
			<li><a href="<?php echo $download_gdpr_requests; ?>"><?php echo $text_gdpr_requests; ?></a></li>
			<li><a href="<?php echo $download_personal_info; ?>"><?php echo $text_personal_information; ?></a></li>
			<li><a href="<?php echo $download_addresses; ?>"><?php echo $text_personal_addresses; ?></a></li>
			<li><a href="<?php echo $download_orders; ?>"><?php echo $text_orders; ?></a></li>
		</ul>
     	
     	<h2><?php echo $text_access_to_personal_data; ?></h2>
		<p><?php echo $text_access_to_personal_data_helper; ?></p>
		<ul class="list-unstyled">
			<li><a href="<?php echo $personal_data_request; ?>"><?php echo $text_request_a_report; ?></a></li>
		</ul>
    
    	<h2><?php echo $text_right_to_be_forgotten; ?></h2>
		<p><?php echo $text_right_to_be_forgotten_helper; ?></p>
		<ul class="list-unstyled">
			<li><a href="<?php echo $deletion_request; ?>"><?php echo $text_request_personal_data_deletion; ?></a></li>
		</ul>
     
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>