<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	  <?php if (!empty($success)) { ?>
		<h2><?php echo $text_successful_request; ?></h2>
		<p><?php echo $text_successful_request_helper; ?></p>
		<div class="buttons clearfix">
		  <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
		  <div class="pull-right">
			<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
		  </div>
		</div>
	  <?php } else { ?>
		<h2><?php echo $heading_title; ?></h2>
		<p><?php echo $heading_title_helper; ?></p>
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
			<fieldset>
			  <legend><?php echo $text_your_email; ?></legend>
			  <div class="form-group required">
				<label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?>:</label>
				<div class="col-sm-10">
				  <input type="email" required="required" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
				  <br />
				  <p><strong><?php echo $how_this_works; ?></strong></p>
				  <p><?php echo $how_this_works_helper; ?></p>
				</div>
			  </div>
			</fieldset>
			<div class="buttons clearfix">
			  <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
			  <div class="pull-right">
				<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
			  </div>
			</div>
		</form>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>