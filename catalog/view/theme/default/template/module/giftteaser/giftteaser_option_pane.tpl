<div class="gift_option">
	<h2><?php echo $text_option_heading; ?></h2>
	<?php if ($gift) { ?>
	<form method="post" class="GiftTeaserOptionsForm">
		<?php if ($gift['thumb']) { ?>
			<div class="options_image"><a href="<?php echo $gift['href']; ?>"><img src="<?php echo $gift['thumb']; ?>" alt="<?php echo $gift['name']; ?>" /></a></div>
		<?php } ?>
		<div class="gift_options_product_field"><a href="<?php echo $gift['href']; ?>"><?php echo $gift['name']; ?></a>
			<?php if ($gift['options']) { ?>
				<div class="gift_options">
					<?php foreach ($gift['options'] as $option) { ?>
						<?php if ($option['type'] == 'select') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
								<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<select class="form-control" name="option[<?php echo $option['product_option_id']; ?>]">
								<option value=""><?php echo $text_select; ?></option>
								<?php foreach ($option['option_value'] as $option_value) { ?>
									<option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'radio') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<?php foreach ($option['option_value'] as $option_value) { ?>
							<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
							<label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
							</label>
							<br />
							<?php } ?>
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'checkbox') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<?php foreach ($option['option_value'] as $option_value) { ?>
							<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
							<label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
							</label>
							<br />
							<?php } ?>
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'image') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<table class="option-image">
								<?php foreach ($option['option_value'] as $option_value) { ?>
								<tr>
									<td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
									<td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
									<td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
									</label></td>
								</tr>
								<?php } ?>
							</table>
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'text') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'textarea') { ?>
						<div id="option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="16" rows="5"><?php echo $option['option_value']; ?></textarea>
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'file') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
							<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'date') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'datetime') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
						</div>
						<?php } ?>
						<?php if ($option['type'] == 'time') { ?>
						<div id="gift_option-<?php echo $option['product_option_id']; ?>" class="option">
							<?php if ($option['required']) { ?>
							<span class="required">*</span>
							<?php } ?>
							<b><?php echo $option['name']; ?>:</b><br />
							<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
						</div>
						<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<input type="hidden" name="gift_id" value="<?php echo $gift['gift_id']; ?>" />
		<div style="clear:both;"></div>
	</form>
	<br />
	<div class="gift_options_footer"><div class="gift_options_continue">
		<a class="add_gift_with_option btn btn-primary"><?php echo $Continue; ?></a></div>
	</div>
<?php } ?>
</div>
