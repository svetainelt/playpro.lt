<?php foreach ($renders as $giftId => $render) { ?>
<div id="gift-panel-<?php echo $giftId; ?>" class="gift-panel" data-panelId="<?php echo $giftId; ?>">
	<?php echo $render; ?>
</div>
<?php } ?>

<script>
	totalPanel = <?php echo count($renders); ?>;
	$('.add_gift_with_option').bind('click', function() {
		let optionData = $(this).closest('.gift-panel').find('.gift_option input[type=\'text\'], .gift_option input[type=\'hidden\'], .gift_option input[type=\'radio\']:checked, .gift_option input[type=\'checkbox\']:checked, .gift_option select, .gift_option textarea');
        let giftId = $(this).closest('.gift-panel').find('.gift_option input[name=\'gift_id\']').val();
		$.ajax({
			url: 'index.php?route=module/giftteaser/updateGiftOptions',
			type: 'POST',
			data: optionData,
			dataType: 'json',
			success: function(json) {
				$('.fancybox-inner').find('.success, .warning, .attention, information, .error, .gift-success').remove();
				if (json['error']) {
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#gift_option-' + i).after('<span class="text-danger error">' + json['error']['option'][i] + '</span>');
						}
					}
				}
				if (json['success']) {
					if (totalPanel > 1) {
                        $('#gift-panel-' + giftId).hide();
                        totalPanel--;
					} else {
						endGiftOptionsSelector();
					}
				}
			}
		});
	});

	function endGiftOptionsSelector() {
		$.fancybox.close();
		$('html, body').animate({ scrollTop: 0 }, 'slow');
		$('#cart > ul').load('index.php?route=common/cart/info ul li');
	}

	$('.date').datetimepicker({
		pickTime: false
	});

	$('.datetime').datetimepicker({
		pickDate: true,
		pickTime: true
	});

	$('.time').datetimepicker({
		pickDate: false
	});

	$('button[id^=\'button-upload\']').on('click', function() {
		var node = this;
		$('#form-upload').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
		$('#form-upload input[name=\'file\']').trigger('click');
		timer = setInterval(function() {
			if ($('#form-upload input[name=\'file\']').val() != '') {
				clearInterval(timer);
				$.ajax({
					url: 'index.php?route=tool/upload',
					type: 'post',
					dataType: 'json',
					data: new FormData($('#form-upload')[0]),
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$(node).button('loading');
					},
					complete: function() {
						$(node).button('reset');
					},
					success: function(json) {
						$('.text-danger').remove();
						if (json['error']) {
							$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
						}
						if (json['success']) {
							alert(json['success']);
							$(node).parent().find('input').attr('value', json['code']);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
		}, 500);
	});
</script>
