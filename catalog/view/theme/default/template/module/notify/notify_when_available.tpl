<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_message; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-left"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($notify_products) { ?>
            <?php foreach ($notify_products  as $result) { ?>
            <tr id="product_<?php echo $result['product_id']; ?>">
              <td class="text-right"><a href="<?php echo $result['product_href']; ?>" target="_blank"><?php echo $result['product_name']; ?></a></td>
              <td class="text-left"><?php echo $result['message']; ?></td>
              <td class="text-left"><?php echo $result['date_added']; ?></td>
              <td>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" onclick="notify_remove(<?php echo $result['product_id'] ?>)" class="btn btn-danger"><i class="fa fa-times-circle"></i></button></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="4"><?php echo $text_empty; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript">
  function notify_remove(product_id){
    $.ajax({
                    url: 'index.php?route=module/notify_when_available/remove_notify_request',
                    type: 'post',
                    dataType: 'json',
                    data: 'product_id='+product_id,
                    success: function(json) {
                      $('table #product_'+product_id).remove();
                        location.reload();
                    }
                });
  }
</script>
<?php echo $footer; ?>