<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?php echo $notify_heading ?></h4>
                    </div>
                    <div class="modal-body">
                        <form id="notify_when_available" onclick="return false;">
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" id="product_id">
                        <div class="form-group">
                            <label><?php echo $entry_name ?></label>
                            <input type="text" name="name" id="name" class="form-control" style="width: 90%;" value="<?php echo $name ?>">
                        </div>
                        <div class="form-group">
                            <label><?php echo $entry_email ?></label>
                            <input type="text" name="email" id="email" class="form-control" style="width: 90%;" value="<?php echo $email ?>">
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label>--><?php //echo $entry_message ?><!--</label>-->
<!--                            <textarea name="message" id="message" style="width: 90%;" class="form-control"></textarea>-->
<!--                        </div>-->
<!--                        --><?php //echo $captcha; ?>
                        <div class="form-group"><br>
                        
                            <button type="button" id="submit"  data-loading-text="<?php echo $text_loading ?>" class="btn btn-primary"><?php echo $button_notify_send ?></button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
<script type="text/javascript">
    $(document).ready(function(e){
            $('#notify_when_available #submit').click(function(e){
                $.ajax({
                    url: 'index.php?route=module/notify_when_available/add_notify_request',
                    type: 'post',
                    dataType: 'json',
                    data: $("#notify_when_available form").serialize(),
                    beforeSend: function() {
                        $('#notify_when_available #submit').button('loading');
                    },
                    complete: function() {
                        $('#notify_when_available #submit').button('reset');
                    },
                    success: function(json) {
                        $('.alert-dismissible').remove();

                        if (json['error']) {
                            $('#notify_when_available form').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            $('#notify_when_available form').prepend('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                            $('#notify_when_available input').val('');
                            $('#notify_when_available textarea').val('');
                            setTimeout(function(e){
                                $('#notify_when_available').modal('hide');
                            },2000);
                        }
                    }
                });
            });
        });
</script>