<div class="content mokilizingas-checkout-block">
    <?php if (($min_price > $total) OR ($total > $max_price)) { ?>
    <?php if ($min_price > $total) { ?>
    <p style="color: red;">Minimali užsakymo suma, kuriai suteikiamas lizingas: <?php echo $min_price; ?> € </p>
    <?php } elseif ($total > $max_price) { ?>
    <p style="color: red;">Maksimali užsakymo suma, kuriai suteikiamas lizingas: <?php echo $max_price; ?> € </p>
    <?php }  ?>
    <?php } else { ?>
    <form action="<?php echo $action; ?>" method="post">
        <input type="hidden" name="order_id" value="<?php echo $order_id; ?>"/>
        <div class="buttons">
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        <input type="submit" value="Patvirtinti" class="button btn btn-primary btn-mokilizingas-confirm">
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <?php }  ?>
</div>
