<?php if (empty($leasing['sessionId'])) { ?>
<p class="alert alert-warning">
    Deja, ištiko klaidų su Jūsų mokėjimu. Prašome susiekti parduotuvėje nurodytais kontaktais.
</p>
<?php } else { ?>
<form id="mokilizingas-redirect-form" class="form-horizontal" method="post" action="<?php echo $leasing_url; ?>">
    <div class="box" style="text-align: center;">

        <h3 class="page-subheading seb-subheading" style="padding: 30px 40px;">
            <i class="mokilizingas_logo"></i>
            <span> Pirkimas išsimokėtinai su Inbank | mokilizingas </span>
        </h3>

        <p>- Ačiū! Patvirtinome Jūsų užsakymą. Paspaudus "tęsti ir pildyti paraišką" pateksite į Inbank | mokilizingas aplinką.</p>
        <p>- Baigus pildyti paraišką, grįšite į el.parduotuvę. Apie tolimesnę eigą informuosime el.paštu.</p>

        <input type="hidden" name="session_id" value="<?php echo $leasing['sessionId']; ?>">

        <div class="form-group pull-right">
            <div class="col-xs-12">
                <button type="submit" id="mokilizingas-submit" class="btn btn-primary center-block" style="color: #FFF; background: #2b0055; margin-top: 30px; padding: .5rem 1rem; font-size: 1rem; line-height: 1.5; border-radius: 1.5rem;">
                    <span>Patvirtinti užsakymą</span>
                </button>
            </div>
        </div>
    </div>
</form>
<?php }  ?>

<script type="text/javascript">
    window.onload=function() {
        document.getElementById("mokilizingas-submit").click();
    };
</script>