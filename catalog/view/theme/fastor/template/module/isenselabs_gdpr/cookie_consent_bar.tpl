<?php if (!empty($enabled) && $enabled == true) { ?>
	<!--Cookie Consent Bar by iSenseLabs GDPR Extension-->
	<script>
		var analytics_cookies_disable = '<?php echo $analytics_cookies_disable; ?>';
		var marketing_cookies_disable = '<?php echo $marketing_cookies_disable; ?>';
		var disabled_cookie_sets = '<?php echo $disabled_cookie_sets; ?>';
		var url_variations = JSON.parse('<?php echo $url_variations; ?>');
		var disable_marketing = false;
		var disable_analytics = false;

		var set_gdpr_handlers = function(){
			<?php if (!empty($track_pp_clicks) && $track_pp_clicks == 1) { ?>
			$('body').delegate('a.cc-btn.cc-allow', 'click', function(e) {
				$.get('<?php echo $action; ?>');
			});
			<?php } ?> 
			$('body').delegate('a.cc-btn.cc-dismiss', 'click', function(e) {
				<?php if ($close_action=='nothing') { ?>
					// return false;
				<?php } else if ($close_action=='analytics_marketing') { ?>
					var cookie_text = 'analytics,marketing';
					Cookies.set('cookieconsent_preferences_disabled', cookie_text, { expires: 365 });			
				<?php } else if ($close_action=='analytics') { ?>
					var cookie_text = 'analytics';
					Cookies.set('cookieconsent_preferences_disabled', cookie_text, { expires: 365 });			
				<?php } else if ($close_action=='marketing') { ?>
					var cookie_text = 'marketing';
					Cookies.set('cookieconsent_preferences_disabled', cookie_text, { expires: 365 });			
				<?php } ?>
			});
			
			$('body').delegate('a.cc-btn-close-settings', 'click', function(e) {
				$('.cc-settings-view').fadeOut(100);
			});
			$('body').delegate('a.cc-btn-save-settings', 'click', function(e) {
				var cookie_text = '';
				if ($('input[name="cookie_isl_analytics"]:checked').length == 0) {
					cookie_text += 'analytics,';
				}
				if ($('input[name="cookie_isl_marketing"]:checked').length == 0) {
					cookie_text += 'marketing,';
				}
				Cookies.set('cookieconsent_preferences_disabled', cookie_text, { expires: 365 });
				$('.cc-settings-view').fadeToggle(100);
			});
		};
		
		function maintainCookies() {
			if (disabled_cookie_sets.indexOf('marketing') >= 0) disable_marketing = true;
			if (disabled_cookie_sets.indexOf('analytics') >= 0) disable_analytics = true;
			$.each(Cookies.get(), function(i, item) {
				if (disable_analytics && (analytics_cookies_disable.indexOf(i) >= 0)) {
					$.each(url_variations, function(ai, asite) {
						clearCookie(i,asite,'/');
					});
				}
				if (disable_marketing && (marketing_cookies_disable.indexOf(i) >= 0)) {
					$.each(url_variations, function(mi, msite) {
						clearCookie(i,msite,'/');
					});
				}
			});
			return true;
		}

		var cc_popup;
		window.addEventListener("load", function(){
			cookieconsent.initialise({
			  <?php if ($position != 'default' && $position != 'top-pushdown') { ?>
			  "position": "<?php echo $position; ?>",
			  <?php } ?>
			  <?php if ($position == 'top-pushdown') { ?>
			  "position": "top",
			  "static": true,
			  <?php } ?>
			  "palette": {
				"popup": {
				  "background": "<?php echo $banner_bg; ?>",
				  "text": "<?php echo $banner_text; ?>"
				},
				"button": {
				  "background": "<?php echo $button_bg; ?>",
				  "text": "<?php echo $button_text; ?>"
				}
			  },
			  "type": "opt-in",
              "revokeBtn": "<div class='cc-revoke {{classes}}'><?php echo $as_text; ?></div>",
			  <?php if (!empty($always_show) && $always_show == 1 && !empty($as_text)) { ?>
		      "alwaysShow": true,
			  <?php } else { ?>
			  "alwaysShow": false,  
			  <?php } ?>
			  <?php if (empty($show_pp_link) || empty($pp_text) || $show_pp_link == 0) { ?>
			  "showLink": false,
			  <?php } ?>
			  "content": {
				"message": "<?php echo $message; ?>",
				"allow": "<?php echo $accept_text; ?>",
				"dismiss": "<?php echo $dismiss_text; ?>",
				"settings": "<?php echo $cookie_settings_text; ?>",
				<?php if (!empty($show_pp_link) && $show_pp_link == 1 && !empty($pp_text)) { ?>
				"link": "<?php echo $pp_text; ?>",
				"href": "<?php echo $pp_link; ?>"
				<?php } ?>
			  }
			}, function (popup) {
				cc_popup = popup;
			}, function (err) {
				console.log(err);
			});
			
			var html_cookie_settings = '<div id="cookieconsent:settings" class="cc-settings-view"><div class="cc-settings-dialog"><span class="cookie_settings_header"><?php echo $text_cookie_settings_header; ?></span><br /><br /><label><input type="checkbox" checked="checked" disabled="disabled" value="functional" /> <?php echo $text_general_cookies; ?></label><br /><?php echo $text_general_cookies_helper; ?><br /><br /><label><input type="checkbox" <?php echo $analytics_cookies_check; ?> name="cookie_isl_analytics" value="analytics" /> <?php echo $text_general_analytics; ?></label><br /><?php echo $text_general_analytics_helper; ?><br /><br /><label><input type="checkbox" <?php echo $marketing_cookies_check; ?> name="cookie_isl_marketing" value="marketing" /> <?php echo $text_general_marketing; ?></label><br /><?php echo $text_general_marketing_helper; ?><br /><br /><div class="cc-compliance cc-highlight"><a class="cc-btn cc-btn-close-settings"><?php echo $text_btn_close; ?></a>&nbsp;&nbsp;<a class="cc-btn cc-btn-save-settings"><?php echo $text_btn_save; ?></a></div></div></div>';
			$('body').append(html_cookie_settings);
			set_gdpr_handlers();
			maintainCookies();
		});

	</script>
	
	<?php if (!empty($custom_css)) { ?><style><?php echo $custom_css; ?></style><?php } ?>
<?php } ?>