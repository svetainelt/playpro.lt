  <div class="breadcrumb full-width">
    <div class="background-breadcrumb"></div>
      <div class="background">
        <div class="shadow"></div>
          <div class="pattern">
            <div class="container">
             <div class="clearfix">
               <h1 id="title-page"><?php echo $text_gdpr ; ?></h1>
               <ul>
               <?php foreach ($breadcrumbs as $breadcrumb) { ?>
               <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
               <?php } ?>
               </ul>
             </div>
           </div>
         </div>
       </div>
  </div>

