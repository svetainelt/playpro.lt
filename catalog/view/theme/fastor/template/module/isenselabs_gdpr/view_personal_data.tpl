<?php echo $header; ?>
<div class="container">

  <?php include ("fastor_breadcrumb.tpl") ; ?>

  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	  <?php if (!$show_data) { ?>
		<h2><?php echo $text_view_personal_data; ?></h2>
		<p><?php echo $text_hash_error; ?></p>
		<div class="buttons clearfix">
		  <div class="pull-right">
			<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
		  </div>
		</div>
	  <?php } else { ?>
		<h2><?php echo $text_view_personal_data; ?></h2>
		<br />
		<h3><?php echo $text_customer_information; ?></h3>
		<?php if (!empty($customer_info)) { ?> 
		<table class="table table-hover">
			<tr>
				<td><?php echo $text_firstname; ?></td>
				<td><?php echo $customer_info['firstname']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_lastname; ?></td>
				<td><?php echo $customer_info['lastname']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_email; ?></td>
				<td><?php echo $customer_info['email']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_telephone; ?></td>
				<td><?php echo $customer_info['telephone']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_fax; ?></td>
				<td><?php echo $customer_info['fax']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_newsletter_subscription; ?></td>
				<td><?php echo $customer_info['newsletter_subscription']; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_ip; ?></td>
				<td><?php echo $customer_info['ip']; ?></td>
			</tr>
		</table>
		<?php } else { ?>
			<p><?php echo $text_no_data; ?></p>
		<?php } ?>

		<h3><?php echo $text_saved_addresses; ?></h3>
		<?php if (!empty($customer_addresses)) { ?>
			<?php foreach ($customer_addresses as $index => $customer_address) { ?> 
			<h4><?php echo $text_address . ' ' . ($index+1); ?></h4>
			<table class="table table-hover">
				<tr>
					<td><?php echo $text_firstname; ?></td>
					<td><?php echo $customer_address['firstname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_lastname; ?></td>
					<td><?php echo $customer_address['lastname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_company; ?></td>
					<td><?php echo $customer_address['company']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_1; ?></td>
					<td><?php echo $customer_address['address_1']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_2; ?></td>
					<td><?php echo $customer_address['address_2']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_city; ?></td>
					<td><?php echo $customer_address['city']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_zone; ?></td>
					<td><?php echo $customer_address['zone']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_postcode; ?></td>
					<td><?php echo $customer_address['postcode']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_country; ?></td>
					<td><?php echo $customer_address['country']; ?></td>
				</tr>
			</table>
			<br />
			<?php } ?>
		<?php } else { ?>
			<p><?php echo $text_no_data; ?></p>
		<?php } ?>
		
		<?php if (!empty($guest_orders)) { ?>
		
		<h3><?php echo $text_personal_data; ?></h3>
		<p><?php echo $text_personal_data_helper; ?></p>
			<?php foreach ($guest_orders as $index => $order) { ?> 
			<h4><?php echo $text_order . ' ' . ($order['order_id']); ?></h4>
			<table class="table table-hover">
				<tr>
					<td colspan="2"><strong><?php echo $text_order_view_data; ?></strong></td>
				</tr>
				<tr>
					<td><?php echo $text_firstname; ?></td>
					<td><?php echo $order['firstname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_lastname; ?></td>
					<td><?php echo $order['lastname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_email; ?></td>
					<td><?php echo $order['email']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_telephone; ?></td>
					<td><?php echo $order['telephone']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_fax; ?></td>
					<td><?php echo $order['fax']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_ip; ?></td>
					<td><?php echo $order['ip']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_user_agent; ?></td>
					<td><?php echo $order['user_agent']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_accept_language; ?></td>
					<td><?php echo $order['accept_language']; ?></td>
				</tr>
				<tr>
					<td colspan="2"><strong><?php echo $text_payment_view_data; ?></strong></td>
				</tr>
				<tr>
					<td><?php echo $text_firstname; ?></td>
					<td><?php echo $order['payment_firstname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_lastname; ?></td>
					<td><?php echo $order['payment_lastname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_company; ?></td>
					<td><?php echo $order['payment_company']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_1; ?></td>
					<td><?php echo $order['payment_address_1']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_2; ?></td>
					<td><?php echo $order['payment_address_2']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_city; ?></td>
					<td><?php echo $order['payment_city']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_zone; ?></td>
					<td><?php echo $order['payment_zone']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_postcode; ?></td>
					<td><?php echo $order['payment_postcode']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_country; ?></td>
					<td><?php echo $order['payment_country']; ?></td>
				</tr>
				<tr>
					<td colspan="2"><strong><?php echo $text_shipping_view_data; ?></strong></td>
				</tr>
				<tr>
					<td><?php echo $text_firstname; ?></td>
					<td><?php echo $order['shipping_firstname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_lastname; ?></td>
					<td><?php echo $order['shipping_lastname']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_company; ?></td>
					<td><?php echo $order['shipping_company']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_1; ?></td>
					<td><?php echo $order['shipping_address_1']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_address_2; ?></td>
					<td><?php echo $order['shipping_address_2']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_city; ?></td>
					<td><?php echo $order['shipping_city']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_zone; ?></td>
					<td><?php echo $order['shipping_zone']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_postcode; ?></td>
					<td><?php echo $order['shipping_postcode']; ?></td>
				</tr>
				<tr>
					<td><?php echo $text_country; ?></td>
					<td><?php echo $order['shipping_country']; ?></td>
				</tr>
			</table>
			<br />
			<?php } ?>
		<?php } ?>
		
		<h3><?php echo $text_data_third_party; ?></h3>
		<p><?php echo $text_data_third_party_helper; ?></p>
		<table class="table table-hover">
			<tr>
				<td><?php echo $text_third_party_services; ?></td>
				<td><?php echo $third_party_services; ?></td>
			</tr>
			<tr>
				<td><?php echo $text_other; ?></td>
				<td><?php echo $other_services; ?></td>
			</tr>
		</table>
		<br />

		<div class="buttons clearfix">
		  <div class="pull-right">
			<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
		  </div>
		</div>
      <?php } ?>
      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
<?php echo $footer; ?>
