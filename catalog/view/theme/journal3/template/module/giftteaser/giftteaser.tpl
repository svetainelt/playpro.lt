<?php if(isset($data['Enabled']) && $data['Enabled'] == 'yes' && !empty($gifts)) { 
$widget = isset($data['widget']) && $data['widget'] == 'yes' ? true : false;
$customDesign = isset($data['customDesign']) && $data['customDesign'] == 'custom' ? true : false;
?>

<div class="<?php echo $widget ? '' : 'gt-nopanel' ; ?> giftTeaserWidget <?php echo $widget && $customDesign ? 'gt-custom' : ''; ?> ">
	<?php if ($widget) { ?>
		<div class="module-title">
    		<h3><?php echo html_entity_decode($data['headtitle_' . $language]);?></h3>
    		<div class="title-divider"></div>
			<div class="subtitle gt-description"><?php echo html_entity_decode($data['notification_' . $language]);?></div>
    	</div>
	<?php } else { ?>
		<h3 class="gt-nopanel-heading"><?php echo html_entity_decode($data['headtitle_' . $language]);?></h3>
	<?php } ?>

    <div class="<?php echo $widget ? "" : "gt-nopanel-content"; ?>"> 
		<?php if (!$widget) { ?>
			<div class="gt-description"><?php echo html_entity_decode($data['notification_' . $language]);?></div>
		<?php } ?>
		<div class="box-product">
			<?php foreach($gifts as $key => $gift) {?>
			<div class="gift">
				<div class="image">
					<a href="<?php echo $gift['url']; ?>"><img src="<?php echo $gift['image']; ?>" alt="<?php echo $gift['name']; ?>" /></a>
				</div>
				<div class="gt-info" style="margin-left: <?php echo $data['giftImageWidth']; ?>px">
					<div class="name"><a href="<?php echo $gift['url']; ?>"><?php echo $gift['name']; ?></a></div>
					<div class="gt-description"><?php echo $gift['description']; ?></div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>

<style type="text/css">
<?php if ($widget && $customDesign) { ?>
	.giftTeaserWidget.gt-custom {
		background: <?php if(isset($data['BackgroundColor'])){ echo $data['BackgroundColor'];}?>;
		border: 1px solid <?php if(isset($data['BorderColor'])){ echo $data['BorderColor'];}?>;
		color: <?php if(isset($data['FontColor'])){ echo $data['FontColor'];}?>;
		border-radius: 5px;
	}
	.giftTeaserWidget.gt-custom .panel-heading {
		background-color: <?php if(isset($data['headingBackground'])){ echo $data['headingBackground'];  }?>;
		color: <?php if(isset($data['FontColor'])){ echo $data['FontColor'];}?>;
		border:  0;
		margin:  -1px;
	}
	<?php }
	if(isset($data['customCss'])) {
		echo $data['customCss'];
	}
	?>
</style>
</div>
<?php } ?>
