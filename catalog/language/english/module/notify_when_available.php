<?php
// Heading
$_['out_of_stock']  = "Out of Stock";
$_['error_name']     = 'Name must be between 1 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_warning'] 				= "Please referesh the page and try again.";
$_['text_success']       			= 'Success: Your request has been successfully added.';
$_['text_notify_when_available']  	= "In Stock Requests";
$_['text_account']  				= "Account";
$_['column_action']  				= "Action";

// Column
$_['column_date_added']  	= 'Date Added';
$_['column_product'] 		= 'Product';
$_['column_message']      	= 'Message';

$_['entry_name'] 	= "Name";
$_['entry_email'] 	= "Email";
$_['entry_message'] = "Message";
$_['button_notify_send'] = "Send";

$_['notify_heading'] 		= "Notify when available";
$_['text_notify_button'] 	= "Notify";
$_['text_empty']  			= "No Record";
