<?php
// Text
$_['text_title']						= 'Išsimokėtinai su Inbank | mokilizingas';
$_['text_title']						= 'Išsimokėtinai su Inbank | mokilizingas';
$_['text_payment']						= 'Inbank | mokilizingas mokėjimas';
$_['text_payment_failed']			    = 'Pastebėjome klaidų užsakyme. Susisiekite parduotuvėje nurodytais kontaktais.';
$_['text_payment_success']			    = 'Inbank | mokilizingas patvirtino mokėjimą.';
$_['confirmation_message_1']			= '- Ačiū! Patvirtinome Jūsų užsakymą. Paspaudus "tęsti ir pildyti paraišką" pateksite į Inbank | mokilizingas aplinką.';
$_['confirmation_message_2']			= '- Baigus pildyti paraišką, grįšite į el.parduotuvę. Apie tolimesnę eigą informuosime el.paštu.';
// Error
$_['error_confirmation']				= 'Pastebėjome klaidų užsakyme. Susisiekite parduotuvėje nurodytais kontaktais..';
// Button
$_['button_confirm']					= 'Patvirtinti užsakymą';
?>
