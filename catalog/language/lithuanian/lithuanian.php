<?php

// Code
$_['code']                              = 'lt';

// Direction
$_['direction']                         = 'ltr';

// Date
$_['date_format_short']                 = 'Y.m.d';
$_['date_format_long']                  = 'l Y F dS';

// Time
$_['time_format']                       = 'H:i:s ';

// Datetime
$_['datetime_format']                   = 'Y.m.d H:i:s';

// Decimal
$_['decimal_point']                     = '.';

// Thousand
$_['thousand_point']                    = ' ';

// Text
$_['text_home']                         = '<i class="fa fa-home"></i>';
$_['text_yes']                          = 'Taip';
$_['text_no']                           = 'Ne';
$_['text_none']                         = ' --------- ';
$_['text_select']                       = ' --- Pasirinkite --- ';
$_['text_all_zones']                    = 'Visos zonos';
$_['text_pagination']                   = 'Rodoma nuo %d iki %d iš %d (%d puslapių)';
$_['text_loading']                      = 'Įkeliama...';

// Button
$_['button_address_add']                = 'Pridėti adresą';
$_['button_back']                       = 'Atgal';
$_['button_continue']                   = 'Tęsti';
$_['button_cart']                       = 'Į krepšelį';
$_['button_cancel']                     = 'Atšaukti';
$_['button_compare']                    = 'Palyginti';
$_['button_wishlist']                   = 'Pageidauti';
$_['button_checkout']                   = 'Užsakyti';
$_['button_confirm']                    = 'Patvirtinti užsakymą';
$_['button_coupon']                     = 'Taikyti';
$_['button_delete']                     = 'Pašalinti';
$_['button_download']                   = 'Parsisiųsti';
$_['button_edit']                       = 'Koreguoti';
$_['button_filter']                     = 'Patikslinkite paiešką';
$_['button_new_address']                = 'Naujas adresas';
$_['button_change_address']             = 'Pakeisti adresą';
$_['button_reviews']                    = 'Atsiliepimai';
$_['button_write']                      = 'Parašyti įvertinimą';
$_['button_login']                      = 'Prisijungti';
$_['button_update']                     = 'Atnaujinti';
$_['button_remove']                     = 'Pašalinti';
$_['button_reorder']                    = 'Pertvarkyti';
$_['button_return']                     = 'Grįžti';
$_['button_shopping']                   = 'Tęsti apsipirkimą';
$_['button_search']                     = 'Paieška';
$_['button_shipping']                   = 'Taikyti pristatymą';
$_['button_submit']                     = 'Pateikti';
$_['button_guest']                      = 'Leisti svečiui apmokėti užsakymą';
$_['button_view']                       = 'Peržiūra';
$_['button_voucher']                    = 'Taikyti';
$_['button_upload']                     = 'Įkelti failą';
$_['button_reward']                     = 'Taikyti';
$_['button_quote']                      = 'Skaičiuoti';
$_['button_list']                       = 'Sąrašas';
$_['button_grid']                       = 'Tinklelis';
$_['button_map']                        = 'Žiūrėti Google žemėlapį';

// Error
$_['error_exception']                   = 'Klaidos kodas (%s): %s linijoje %s';
$_['error_upload_1']                    = 'Įspėjimas: įkelto failo dydis viršija parametro upload_max_filesize reikšmę, esančią php.ini!';
$_['error_upload_2']                    = 'Įspėjimas: įkelto failo dydis viršija parametro MAX_FILE_SIZE reikšmę, nurodytą HTML formoje!';
$_['error_upload_3']                    = 'Įspėjimas: failas įkeltas tik dalinai!';
$_['error_upload_4']                    = 'Įspėjimas: failas neįkeltas!';
$_['error_upload_6']                    = 'Įspėjimas: nerastas laikinasis katalogas!';
$_['error_upload_7']                    = 'Įspėjimas: nepavyko įrašyti failo į kaupiklį!';
$_['error_upload_8']                    = 'Įspėjimas: failo įkėlimas sustabdytas pagal plėtinį!';
$_['error_upload_999']                  = 'Įspėjimas: nenustatytas klaidos kodas!';
