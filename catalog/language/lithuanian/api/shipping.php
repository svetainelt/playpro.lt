<?php

// Text
$_['text_address']                      = 'Sėkmingai nustatytas pristatymo adresas!';
$_['text_method']                       = 'Sėkmingai nystatytas pristatymo metodas!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių pasiekti API!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 3 iki 128 simbolių!';
$_['error_postcode']                    = 'Pašto kodo ilgis turi būti nuo 2 iki 10 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_custom_field']                = '%s reikalingas!';
$_['error_address']                     = 'Įspėjimas: pristatymo adresas reikalingas!';
$_['error_method']                      = 'Įspėjimas: pristatymo metodas reikalingas!';
$_['error_no_shipping']                 = 'Įspėjimas: pristatymo būdų nėra!';
