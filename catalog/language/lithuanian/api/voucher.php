<?php

// Text
$_['text_success']                      = 'Sėkmingai pritaikėte dovanų čekio nuolaidą!';
$_['text_cart']                         = 'Sėkmingai modifikuotas krepšelis!';
$_['text_for']                          = '%s dovanų čekis %s';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių pasiekti API!';
$_['error_voucher']                     = 'Įspėjimas: dovanų čekis netinkamas arba balansas jau išnaudotas!';
$_['error_to_name']                     = 'Gavėjo vardo ilgis turi būti nuo 1 ir 64 simbolių!';
$_['error_from_name']                   = 'Jūsų vardo ilgis turi būti nuo 1 ir 64 simbolių!';
$_['error_email']                       = 'El. pašto adresas įvestas klaidingai.';
$_['error_theme']                       = 'Privalote pasirinkti temą!';
$_['error_amount']                      = 'Kiekis privalo būti tarp %s ir %s!';
