<?php

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas krepšelis!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių pasiekti API!';
$_['error_stock']                       = 'Prekių, pažymėtų su ***, nėra norimo kiekio arba jų nėra likę sandėlyje!';
$_['error_minimum']                     = 'Minimalus užsakymo %s kiekis yra %s!';
$_['error_store']                       = 'Prekės negali būti perkamos iš pasirinktos parduotuvės!';
$_['error_required']                    = '%s reikalingas!';
