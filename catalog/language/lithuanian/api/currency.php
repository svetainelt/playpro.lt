<?php

// Text
$_['text_success']                      = 'Sėkmingai pakeistos valiutos!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių pasiekti API!';
$_['error_currency']                    = 'Įspėjimas: valiutos kodas neteisingas!';
