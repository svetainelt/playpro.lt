<?php

// Text
$_['text_success']                      = 'Sėkmingai pritaikyta kupono nuolaida!';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių pasiekti API!';
$_['error_coupon']                      = 'Įspėjimas: kuponas arba negaliojantis, pasibaigęs arba panaudotas!';
