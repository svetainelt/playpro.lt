<?php

// Heading
$_['heading_title']                     = 'Partnerystės stebėjimas';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_description']                  = 'Tam, kad Jūsų sąskaita būtų pildoma premijomis, mes turime sekti pirkėjus, ateinančius per Jūsų partnerytės nuorodą, kurioje turi būti įrašytas stebėjimo kodas. Galite pasinaudoti žemiau esančiu įrankiu %s Partnerystės nuorodoms generuoti.';

// Entry
$_['entry_code']                        = 'Jūsų stebėjimo kodas';
$_['entry_generator']                   = 'Stebėjimo nuorodos generatorius';
$_['entry_link']                        = 'Stebėjimo nuoroda';

// Help
$_['help_generator']                    = 'Įveskite produkto pavadinimą, kurį norite susieti su';
