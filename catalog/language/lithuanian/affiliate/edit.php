<?php

// Heading
$_['heading_title']                     = 'Mano paskyros informacija';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_edit']                         = 'Koreguoti informaciją';
$_['text_your_details']                 = 'Jūsų asmeninė informacija';
$_['text_your_address']                 = 'Jūsų adresas';
$_['text_success']                      = 'Sėkmingai atnaujinta paskyros informacija.';

// Entry
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_company']                     = 'Įmonė';
$_['entry_website']                     = 'Tinklalapis';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_postcode']                    = '<a href="https://postit.lt/" title="Rask savo pašto kodą" target="_blank">Pašto kodas</a>';
$_['entry_city']                        = 'Miestas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';

// Error
$_['error_exists']                      = 'Įspėjimas: nurodytas el. pašto adresas jau užregistruotas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas įvestas klaidingai.';
$_['error_telephone']                   = 'Telefono numeris turi būti nuo 3 iki 32 simbolių!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_postcode']                    = 'Pašto kodas turi būti nuo 2 iki 10 simbolių!';
