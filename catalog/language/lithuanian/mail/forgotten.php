<?php

// Text
$_['text_subject']                      = '%s - naujas slaptažodis';
$_['text_greeting']                     = 'Gavome prašymą, atsiųsti naują slaptažodį, prisijungimui prie paskyros %s el. parduotuvėje.';
$_['text_password']                     = 'Jūsų naujas, prisijungimo prie el. parduotuvės, slaptažodis yra:';
