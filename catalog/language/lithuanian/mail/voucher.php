<?php

// Text
$_['text_subject']                      = 'Jums atsiųstas dovanų čekis iš %s';
$_['text_greeting']                     = 'Sveikiname, Jūs gavote dovanų čekį, kurio vertė %s';
$_['text_from']                         = 'Šis dovanų čekis buvo atsiųstas Jums nuo %s';
$_['text_message']                      = 'Prie čekio palikta žinutę: ';
$_['text_redeem']                       = 'Tam, kad galėtumėte pasinaudoti šiuo dovanų čekiu, turite paspausti žemiau esančią nuorodą, išsirinkti norimas prekes, įsidėti jas į krepšelį ir, prieš tvirtinant užsakymą, įvesti dovanų čekio kodą <b>%s</b>.';
$_['text_footer']                       = 'Prašome atsakyti į šį laišką, jeigu turite kokių nors klausimų.';
