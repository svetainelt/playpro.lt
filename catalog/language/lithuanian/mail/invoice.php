<?php

// Heading
$_['heading_title']                     = 'Užsakymai';

// Text
$_['text_list']                         = 'Užsakymų sąrašas';
$_['text_auto_comment']                         = '
Sveiki,

Dar kartą dėkojame už Jūsų užsakymą.

SĄSKAITĄ FAKTŪRĄ - GARANTINĮ DOKUMENTĄ RASITE PRISEGTUKE.
Prietaiso garantinio laikotarpio metu būtina išsaugoti pirkimo dokumentą.

Lauksime sugrįžtant!

P.S. Jei iškiltų kokių nors klausimų, visada prašome kreiptis.

Turite nusiskundimų, pastabų, patarimų mums? Parašykite, mums labai
svarbi Jūsų nuomonė.

Patiko mūsų aptarnavimas? Skleiskite apie mus žinią, būsime labai dėkingi!
';
$_['text_add']                          = 'Pridėti užsakymą';
$_['text_guarantee']                         = 'Garantija';
$_['text_guarantee_client']                     = 'Prekių garantija: 24 mėn. Garantinis aptarnavimas: www.playpro.lt';
$_['text_guarantee_company']                     = 'Prekių garantija: 12 mėn. Garantinis aptarnavimas: www.playpro.lt';
$_['text_edit']                         = 'Koreguoti užsakymą';
$_['text_order_detail']                 = 'Užsakymo detalės';
$_['text_customer_detail']              = 'Kliento detalės';
$_['text_option']                       = 'Pasirinkimai';
$_['text_store']                        = 'Parduotuvė';
$_['text_date_added']                   = 'Data';
$_['text_payment_method']               = 'Apmokėjimo metodas';
$_['text_shipping_method']              = 'Siuntimo būdas';
$_['text_customer']                     = 'Klientas';
$_['text_customer_group']               = 'Klientų grupė';
$_['text_email']                        = 'El. paštas';
$_['text_telephone']                    = 'Telefono nr.';
$_['text_invoice']                      = 'Sąskaita';
$_['text_reward']                       = 'Lojalumo taškai';
$_['text_affiliate']                    = 'Partneris';
$_['text_order']                        = 'Užsakymas (#%s)';
$_['text_payment_address']              = 'Mokėjimo adresas';
$_['text_shipping_address']             = 'Pristatymo adresas';
$_['text_comment']                      = 'Užsakymo komentarai';
$_['text_history']                      = 'Užsakymo istorija';
$_['text_history_add']                  = 'Pridėti užsakymo istoriją';
$_['text_account_custom_field']         = 'Paskyros skirtingi laukai';
$_['text_payment_custom_field']         = 'Mokėjimo adreso skirtingi laukai';
$_['text_shipping_custom_field']        = 'Pristatymo adreso skirtingi laukai';
$_['text_browser']                      = 'Naršyklė';
$_['text_ip']                           = 'IP adresas';
$_['text_forwarded_ip']                 = 'Peradresuotas IP';
$_['text_user_agent']                   = 'Naršyklė';
$_['text_accept_language']              = 'Palaikomos kalbos';
$_['text_order_id']                     = 'Užsakymo nr.:';
$_['text_fax']                          = 'Faksas:';
$_['text_website']                      = 'Tinklalapis:';
$_['text_invoice_no']                   = 'Sąskaitos numeris:';
$_['text_invoice_date']                 = 'Sąskaitos data:';
$_['text_sku']                          = 'SKU:';
$_['text_upc']                          = 'UPC:';
$_['text_ean']                          = 'EAN:';
$_['text_jan']                          = 'JAN:';
$_['text_isbn']                         = 'ISBN:';
$_['text_mpn']                          = 'MPN:';
$_['text_missing']                      = 'Trūkstami užsakymai';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_product']                      = 'Pridėti prekę';
$_['text_voucher']                      = 'Pridėti dovanų čekį (-ius)';
$_['text_shipping']                     = 'Pristatymas';
$_['text_contact']                      = 'Kontaktas';
$_['text_reward_added']                 = 'Sėkmingai pridėti lojalumo taškai!';
$_['text_reward_removed']               = 'Sėkmingai pašalinti lojalumo taškai!';
$_['text_commission_added']             = 'Sėkmingai pridėti komisiniai!';
$_['text_commission_removed']           = 'Sėkmingai pašalinti komisiniai!';
$_['text_restock']                      = 'Sėkmingai papildytos produktų atsargos!';
$_['text_upload']                       = 'Jūsų failas sėkmingai įkeltas!';
$_['text_picklist']                     = 'Išsiųsti pranešimą';

// Column
$_['column_order_id']                   = 'Užsakymo nr.';
$_['column_customer']                   = 'Klientas';
$_['column_status']                     = 'Būsena';
$_['column_date_added']                 = 'Data';
$_['column_date_modified']              = 'Modifikavimo data';
$_['column_total']                      = 'Iš viso';
$_['column_product']                    = 'Prekė';
$_['column_model']                      = 'Prekės kodas';
$_['column_quantity']                   = 'Kiekis';
$_['column_price']                      = 'Kaina (vnt.)';
$_['column_comment']                    = 'Komentaras';
$_['column_notify']                     = 'Klientas informuotas';
$_['column_location']                   = 'Vieta';
$_['column_reference']                  = 'Nuoroda';
$_['column_action']                     = 'Veiksmas';
$_['column_weight']                     = 'Produkto svoris';

// Entry
$_['entry_store']                       = 'Parduotuvė';
$_['entry_customer']                    = 'Klientas';
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_address']                     = 'Pasirinkite adresą';
$_['entry_company']                     = 'Įmonė';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_city']                        = 'Miestas';
$_['entry_postcode']                    = 'Pašto kodas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_zone_code']                   = 'Rajono kodas';
$_['entry_product']                     = 'Pasirinkite prekę';
$_['entry_option']                      = 'Pasirinkite variantus';
$_['entry_quantity']                    = 'Kiekis';
$_['entry_to_name']                     = 'Gavėjo vardas';
$_['entry_to_email']                    = 'Gavėjo el. paštas';
$_['entry_from_name']                   = 'Siuntėjo vardas';
$_['entry_from_email']                  = 'Siuntėjo el. paštas';
$_['entry_theme']                       = 'Dovanų čekio tema';
$_['entry_message']                     = 'Žinutė';
$_['entry_amount']                      = 'Kiekis';
$_['entry_affiliate']                   = 'Partneris';
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_notify']                      = 'Informuoti klientą';
$_['entry_override']                    = 'Keisti';
$_['entry_comment']                     = 'Komentaras';
$_['entry_currency']                    = 'Valiuta';
$_['entry_shipping_method']             = 'Siuntimo būdas';
$_['entry_payment_method']              = 'Apmokėjimo metodas';
$_['entry_coupon']                      = 'Nuolaidų kuponas';
$_['entry_voucher']                     = 'Dovanų čekis';
$_['entry_reward']                      = 'Lojalumo taškai';
$_['entry_order_id']                    = 'Užsakymo nr.';
$_['entry_total']                       = 'Iš viso';
$_['entry_date_added']                  = 'Data';
$_['entry_date_modified']               = 'Modifikavimo data';

// Help
$_['help_override']                     = 'Jei kliento užsakymas yra blokuojamas nuo užsakymo būsenos pakeitimo dėl anti-fraud plėtinio įgalinkite "keitimą".';

// Error
$_['error_warning']                     = 'Įspėjimas: Atidžiai patikrinkite formos duomenis dėl klaidų!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti užsakymų!';
$_['error_action']                      = 'Įspėjimas: Nepavyko atlikti šio veiksmo!';
$_['error_filetype']                    = 'Netinkamas failo tipas!';