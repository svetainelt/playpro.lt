<?php

// Text
$_['text_subject']                      = '%s - Prekės atsiliepimas';
$_['text_waiting']                      = 'Jūs turite naują produkto atsiliepimą.';
$_['text_product']                      = 'Prekė: %s';
$_['text_reviewer']                     = 'Atsiliepimas: %s';
$_['text_rating']                       = 'Įvertinimas: %s';
$_['text_review']                       = 'Atsiliepimo tekstas:';
