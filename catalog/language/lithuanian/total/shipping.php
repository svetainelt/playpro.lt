<?php

// Heading
$_['heading_title']                     = 'Paskaičiuoti pristatymo kainą';

// Text
$_['text_success']                      = 'Sėkmingai pritaikyta apskaičiuoto pristatymo suma!';
$_['text_shipping']                     = 'Pasirinkite pristatymo šalį.';
$_['text_shipping_method']              = 'Prašome pasirinkti pageidaujamą pristatymo metodą šiam užsakymui.';

// Entry
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_postcode']                    = '<a href="https://postit.lt/" title="Rask savo pašto kodą" target="_blank">Pašto kodas</a>';

// Error
$_['error_postcode']                    = 'Pašto kodas turi būti nuo 2 iki 10 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_shipping']                    = 'Įspėjimas: pristatymo metodas reikalingas!';
$_['error_no_shipping']                 = 'Įspėjimas: pristatymo būdų nėra. Prašome <a href="%s">susisiekti su mumis</a> jei reikia pagalbos!';
