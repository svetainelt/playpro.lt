<?php

// Heading
$_['heading_title']                     = 'Naudoti nuolaidos kodą';

// Text
$_['text_coupon']                       = 'Kuponas (%s)';
$_['text_success']                      = 'Sėkmingai pritaikyta kupono nuolaida!';

// Entry
$_['entry_coupon']                      = 'Įveskite nuolaidos kodą';

// Error
$_['error_coupon']                      = 'Įspėjimas: kuponas negalioja, pasibaigęs arba pasiekė savo naudojimo ribą!';
$_['error_empty']                       = 'Įspėjimas: prašome įvesti kupono kodą!';
