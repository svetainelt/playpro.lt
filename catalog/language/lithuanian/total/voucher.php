<?php

// Heading
$_['heading_title']                     = 'Naudoti dovanų čekį';

// Text
$_['text_voucher']                      = 'Dovanų čekis (%s)';
$_['text_success']                      = 'Sėkmingai pritaikėte dovanų čekio nuolaidą!';

// Entry
$_['entry_voucher']                     = 'Įveskite čekio kodą';

// Error
$_['error_voucher']                     = 'Įspėjimas: dovanų čekis netinkamas arba balansas jau išnaudotas!';
$_['error_empty']                       = 'Įspėjimas: prašome įvesti dovanų čekio kodą!';
