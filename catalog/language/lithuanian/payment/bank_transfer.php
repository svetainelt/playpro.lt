<?php

// Text
$_['text_title']                        = 'Sumokėti bankiniu pavedimu vėliau';
$_['text_instruction']                  = 'Instrukcija bankiniam pavedimui';
$_['text_description']                  = 'Perveskite nurodytą užsakymo sumą į žemiau nurodytą sąskaitą';
$_['text_payment']                      = 'Atlikdami pavedimą, nepamirškite įrašyti užsakymo numerį mokėjimo paskirties laukelyje. Prekės bus išsiųstos, gavus apmokėjimą.';
