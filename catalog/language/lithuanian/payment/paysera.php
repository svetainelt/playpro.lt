<?php
// Heading
$_['heading_title']         = 'Paysera.lt';
$_['text_title']            = 'Apmokėti iškart, internetine bankininkyste Swedbank, SEB, DNB, Danske ir kt., Paysera.lt';

// Text 
$_['text_development']      = '<span style="color: red;">In Development</span>';
$_['text_successful']       = 'On - Always Successful';
$_['text_declined']         = 'On - Always Declined';
$_['text_off']              = 'Off';
$_['text_response']         = '';
$_['text_failure']          = '... Jūsų mokėjimas atmestas!';
$_['text_failure_wait']     = '<b><span style="color: #FF0000">Prašome palaukti...</span></b><br>'
        . 'Jeigu Jūs nebūsite nukreipti į kitą puslapį per 10 sekundžių, paspauskite <a href="%s">čia</a>.';										  

// Pay Method Select
$_['text_paycountry']       = 'Mokėjimo šalis: ';
$_['text_chosen']           = 'Pasirinkote atsiskaitymą per paysera.lt sistemą<br />Paspaudę ant norimo banko logotipo, iškart būsite nukreipti į jūsų pasirinktą banką.';