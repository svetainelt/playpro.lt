<?php

// Text
$_['text_title']                        = 'Kreditinė kortelė / Debetinė kortelė (Authorize.Net)';
$_['text_credit_card']                  = 'Kretitinės kortelės duomenys';

// Entry
$_['entry_cc_owner']                    = 'Kortelės savininkas';
$_['entry_cc_number']                   = 'Kortelės numeris';
$_['entry_cc_expire_date']              = 'Kortelės galiojimo pabaigos data';
$_['entry_cc_cvv2']                     = 'Kortelės saugos kodas (CVV2)';
