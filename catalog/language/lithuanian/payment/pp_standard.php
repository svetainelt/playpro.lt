<?php

// Text
$_['text_title']                        = 'PayPal';
$_['text_testmode']                     = 'Įspėjimas: PayPal mokėjimai yra testiniame režime. Atlikus užsakymą, jūsų pinigai nebus nuskaičiuoti nuo jūsų sąskaitos.';
$_['text_total']                        = 'Pristatymas, Administravimas, Nuolaidos, Mokesčiai';
