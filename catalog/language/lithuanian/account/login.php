<?php

// Heading
$_['heading_title']                     = 'Prisijungti prie pirkėjo paskyros';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_login']                        = 'Prisijungti';
$_['text_new_customer']                 = 'Pirmą kartą mūsų el. parduotuvėje?';
$_['text_register']                     = 'Užsiregistruokite!';
$_['text_register_account']             = 'Susikurdami paskyrą galėsite apsipirkti greičiau, būsite informuotas apie užsakymo būsenos pakeitimus ir galėsite sekti visus savo užsakymus.';
$_['text_returning_customer']           = 'Esamiems parduotuvės klientams';
$_['text_i_am_returning_customer']      = 'Jau turite pirkėjo paskyrą? Prisijunkite!';
$_['text_forgotten']                    = 'Pamiršote slaptažodį?';

// Entry
$_['entry_email']                       = 'El. paštas';
$_['entry_password']                    = 'Slaptažodis';

// Error
$_['error_login']                       = 'Įspėjimas: neteisingas el. pašto adresas ir/arba slaptažodis.';
$_['error_attempts']                    = 'Įspėjimas: Jūsų paskyra viršijo leistiną skaičių prisijungimo bandymų. Bandykite dar kartą po 1 valandos.';
$_['error_approved']                    = 'Įspėjimas: negalėsite prisijungti, kol Jūsų paskyros nepatvirtins el. parduotuvės administracija.';
