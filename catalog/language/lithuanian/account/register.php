<?php

// Heading
$_['heading_title']                     = 'Registruoti vartotoją';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_register']                     = 'Registruotis';
$_['text_account_already']              = 'Jei jau turite paskyrą, prašome <a href="%s">prisijungti</a>.';
$_['text_your_details']                 = 'Jūsų asmeninė informacija';
$_['text_your_address']                 = 'Jūsų adresas';
$_['text_newsletter']                   = 'Naujienų prenumerata';
$_['text_your_password']                = 'Jūsų slaptažodis';
$_['text_agree']                        = 'Susipažinau ir sutinku su <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_company']                     = 'Įmonė';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_postcode']                    = '<a href="https://postit.lt/" title="Rask savo pašto kodą" target="_blank">Pašto kodas</a>';
$_['entry_city']                        = 'Miestas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_newsletter']                  = 'Prenumeruoti';
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Patvirtinkite slaptažodį';

// Error
$_['error_exists']                      = 'Įspėjimas: el. pašto adresas jau užregistruotas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas įvestas klaidingai.';
$_['error_telephone']                   = 'Telefono numeris turi būti 8 simbolių ir prasidėti 6!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_postcode']                    = 'Pašto kodas turi būti nuo 2 iki 10 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_custom_field']                = '%s reikalingas!';
$_['error_password']                    = 'Slaptažodis turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_confirm']                     = 'Slaptažodžio patvirtinimas nesutampa su įvestu slaptažodžiu!';
$_['error_agree']                       = 'Įspėjimas: Jūs turite sutikti su %s!';
