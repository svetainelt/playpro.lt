<?php

// Heading
$_['heading_title']                     = 'Naujienų prenumerata';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_newsletter']                   = 'Naujienų prenumerata';
$_['text_success']                      = 'Sėkmingai atnaujinta Jūsų naujienlaiškio prenumerata!';

// Entry
$_['entry_newsletter']                  = 'Prenumeruoti';
