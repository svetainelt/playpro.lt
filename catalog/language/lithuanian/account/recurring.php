<?php

// Heading
$_['heading_title']                     = 'Periodiniai atsiskaitymai';

// Button
$_['button_continue']                   = 'Tęsti';
$_['button_view']                       = 'Peržiūra';

// Text
$_['text_empty']                        = 'Periodiniai atsiskaitymai nerasti';
$_['text_product']                      = 'Prekė:';
$_['text_order']                        = 'Užsakymas:';
$_['text_quantity']                     = 'Kiekis:';
$_['text_account']                      = 'Paskyra';
$_['text_action']                       = 'Veiksmas';
$_['text_recurring']                    = 'Periodinis atsiskaitymas';
$_['text_transactions']                 = 'Sandoriai';

// Button
$_['button_return']                     = 'Grįžti';

// Text
$_['text_empty_transactions']           = 'Šiam periodiniui atsiskaitymui sandorių nėra';

// Column
$_['column_date_added']                 = 'Sukurta';
$_['column_type']                       = 'Tipas';
$_['column_amount']                     = 'Kiekis';
$_['column_status']                     = 'Būsena';
$_['column_product']                    = 'Prekė';
$_['column_action']                     = 'Veiksmas';
$_['column_recurring_id']               = 'Profilio ID';

// Text
$_['text_recurring_detail']             = 'Periodinio atsiskaitymo detalės';
$_['text_recurring_id']                 = 'Profilio ID:';
$_['text_payment_method']               = 'Apmokėjimo metodas:';
$_['text_date_added']                   = 'Sukurta:';
$_['text_recurring_description']        = 'Aprašymas:';
$_['text_status']                       = 'Būsena:';
$_['text_ref']                          = 'Nuoroda:';
$_['text_status_active']                = 'Aktyvus';
$_['text_status_inactive']              = 'Neaktyvus';
$_['text_status_cancelled']             = 'Atšauktas';
$_['text_status_suspended']             = 'Atidėtas';
$_['text_status_expired']               = 'Pasibaigęs';
$_['text_status_pending']               = 'Vykdomas';
$_['text_transaction_date_added']       = 'Sukurta';
$_['text_transaction_payment']          = 'Apmokėjimas';
$_['text_transaction_outstanding_payment'] = 'Neapmokėtas mokėjimas';
$_['text_transaction_skipped']          = 'Mokėjimas praleistas';
$_['text_transaction_failed']           = 'Mokėjimas nepavyko';
$_['text_transaction_cancelled']        = 'Atšauktas';
$_['text_transaction_suspended']        = 'Atidėtas';
$_['text_transaction_suspended_failed'] = 'Atidėtas iš nepavykusio apmokėjimo';
$_['text_transaction_outstanding_failed'] = 'Neapmokėtas mokėjimas nepavyko';
$_['text_transaction_expired']          = 'Pasibaigęs';

// Error
$_['error_not_cancelled']               = 'Klaida: %s';
$_['error_not_found']                   = 'Nepavyko atšaukti periodinio atsiskaitymo';

// Text
$_['text_cancelled']                    = 'Periodinis atsiskaitymas buvo atšauktas';
