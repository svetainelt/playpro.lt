<?php

// Heading
$_['heading_title']                     = 'Adresų knyga';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_address_book']                 = 'Adresų knygos įrašai';
$_['text_edit_address']                 = 'Keisti adresą';
$_['text_add']                          = 'Jūsų adresas buvo sėkmingai įdėtas';
$_['text_edit']                         = 'Jūsų adresas buvo sėkmingai atnaujintas';
$_['text_delete']                       = 'Jūsų adresas buvo sėkmingai pašalintas';
$_['text_empty']                        = 'Jūs neturite adresų savo paskyroje.';

// Entry
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_company']                     = 'Įmonė';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_postcode']                    = '<a href="https://postit.lt/" title="Rask savo pašto kodą" target="_blank">Pašto kodas</a>';
$_['entry_city']                        = 'Miestas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_default']                     = 'Ar tai Jūsų numatytasis adresas?';

// Error
$_['error_delete']                      = 'Įspėjimas: Jūs turite tūrėti mažiausiai vieną adresą!';
$_['error_default']                     = 'Įspėjimas: Jūs negalite ištrinti savo pagrindinio adreso!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_vat']                         = 'Nurodytas neteisingas PVM mokėtojo kodas!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_postcode']                    = 'Pašto kodas turi būti nuo 2 iki 10 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_custom_field']                = '%s reikalingas!';
