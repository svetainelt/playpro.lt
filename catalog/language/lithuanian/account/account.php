<?php

// Heading
$_['heading_title']                     = 'Mano paskyra';

// Text
$_['text_account']                      = 'Paskyra';
$_['text_my_account']                   = 'Mano paskyra';
$_['text_my_orders']                    = 'Mano užsakymai';
$_['text_my_newsletter']                = 'Naujienų prenumerata';
$_['text_edit']                         = 'Koreguoti paskyros informaciją';
$_['text_password']                     = 'Keisti slaptažodį';
$_['text_address']                      = 'Keisti adresų knygos įrašus';
$_['text_wishlist']                     = 'Keisti pageidavimų sąrašą';
$_['text_order']                        = 'Peržiūrėti užsakymų istoriją';
$_['text_download']                     = 'Parsisiuntimai';
$_['text_reward']                       = 'Jūsų lojalumo taškai';
$_['text_return']                       = 'Peržiūrėti grąžinimo prašymus';
$_['text_transaction']                  = 'Sandoriai';
$_['text_newsletter']                   = 'Prenumeruoti/atsisakyti naujienlaiškių';
$_['text_recurring']                    = 'Periodiniai atsiskaitymai';
$_['text_transactions']                 = 'Sandoriai';
