<?php
// Heading
$_['out_of_stock']  = "Nėra prekės";
$_['error_name']     = 'Vardas turi būti tarp 1 ir 32 simbolių!';
$_['error_email']    = 'El. pašto adresas klaidingas!';
$_['error_warning'] 				= "Prašome atnaujinti puslapį ir bandyti dar kartą.";
$_['text_success']       			= 'Jūsų prašymas buvo sėkmingai pridėtas.';
$_['text_notify_when_available']  	= "Esantys prašomi produktai";
$_['text_account']  				= "Paskyra";
$_['column_action']  				= "Veiksmas";

// Column
$_['column_date_added']  	= 'Pridėjimo data';
$_['column_product'] 		= 'Produktas';
$_['column_message']      	= 'Pranešimas';

$_['entry_name'] 	= "Vardas";
$_['entry_email'] 	= "El. paštas";
$_['entry_message'] = "Pranešimas";
$_['button_notify_send'] = "Siųsti";

$_['notify_heading'] 		= "Pranešti, kai turėsime";
$_['text_notify_button'] 	= "Pranešti";
$_['text_empty']  			= "Įrašų nėra";
