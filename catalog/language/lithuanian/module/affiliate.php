<?php

// Heading
$_['heading_title']                     = 'Partneris';

// Text
$_['text_register']                     = 'Registruotis';
$_['text_login']                        = 'Prisijungti';
$_['text_logout']                       = 'Atsijungti';
$_['text_forgotten']                    = 'Pamiršote slaptažodį?';
$_['text_account']                      = 'Mano paskyra';
$_['text_edit']                         = 'Redaguoti paskyrą';
$_['text_password']                     = 'Slaptažodis';
$_['text_payment']                      = 'Apmokėjimo parametrai';
$_['text_tracking']                     = 'Partnerystės stebėjimas';
$_['text_transaction']                  = 'Sandoriai';
