<?php

// Text
$_['text_information']                  = 'Informacija';
$_['text_service']                      = 'Klientų aptarnavimas';
$_['text_extra']                        = 'Kita informacija';
$_['text_contact']                      = 'Susisiekite su mumis';
$_['text_return']                       = 'Grąžinimo forma';
$_['text_sitemap']                      = 'Parduotuvės žemėlapis';
$_['text_manufacturer']                 = 'Prekiniai ženklai';
$_['text_voucher']                      = 'Dovanų čekiai';
$_['text_affiliate']                    = 'Partnerystės programa';
$_['text_special']                      = 'Akcijos';
$_['text_account']                      = 'Mano paskyra';
$_['text_order']                        = 'Užsakymų istorija';
$_['text_wishlist']                     = 'Pageidavimų sąrašas';
$_['text_newsletter']                   = 'Naujienų prenumerata';
$_['text_powered']                      = '%s &copy; %s';
