<?php

// Heading
$_['heading_title']                     = 'Tvarkymo darbai';

// Text
$_['text_maintenance']                  = 'Tvarkymo darbai';
$_['text_message']                      = '<h1 style="text-align: center">Vyksta parduotuvės atnaujinimo darbai. Prašome apsilankyti šiek tiek vėliau.</h1>';
