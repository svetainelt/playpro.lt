<?php

// Text
$_['text_items']                        = '%s prekė(s) - %s';
$_['text_empty']                        = 'Jūsų krepšelis tuščias!';
$_['text_cart']                         = 'Peržiūrėti krepšelį';
$_['text_checkout']                     = 'Užsakyti';
$_['text_recurring']                    = 'Apmokėjimo profilis';
