<?php

// Heading
$_['heading_title']                     = 'Ooops! Puslapis nerastas.';

// Text
$_['text_error']                        = 'Jūsų ieškomas puslapis buvo perkeltas, ištrintas arba neegzistuoja.<br /><br />Prašome grįžti į <a href="' . (($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http") . '://' . $_SERVER['SERVER_NAME'] . '" title="Pagrindinis puslapis">pagrindinį</a> puslapį.';
