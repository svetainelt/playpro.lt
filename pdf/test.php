<?php

require_once("dompdf_config.inc.php");

// We check wether the user is accessing the demo locally
$local = array("::1", "127.0.0.1");
$is_local = in_array($_SERVER['REMOTE_ADDR'], $local);


  if ( get_magic_quotes_gpc() )
    $_POST["html"] = stripslashes($_POST["html"]);

  $dompdf = new DOMPDF();
  $dompdf->load_html('<html>
<head>
<style>

/* Type some style rules here */

</style>
</head>

<body>

<!-- Type some HTML here -->
<div style="page-break-after: always;width:889px">
        <table style="border: 0 none; display: inline-table; left: 0; position: relative; top: 0; height: 135px; width: 192mm; border-collapse: collapse; border-spacing: 0;">
            <tbody><tr>
                <td style="border: 0 none; height: 70px; width: 130mm;height: 61px;">
                    <img src="logo.jpg" alt="$store_name">
                </td>
                <td rowspan="2" style="border: 0 none; font-size: 1em; padding-left: 40px;height: 133px; width: 50mm;">
                    46 Passaic Avenue<br>
Fairfield, NJ 07004<br>
USA<br>
973-625-3888<br>
                    Telephone: 973-625-3888<br>
                    Fax: 973-226-4249<br>
                    todd.ryan@jrcweb.com<br>
                    http://beta2.instockconvertingmachinery.com                </td>
            </tr>
            <tr>
                <td style="border: 0 none; height: 61px; width: 130mm;">
                    <table style="border-collapse: collapse; border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;bottom: 0;position: relative;height: 56px;width: 140mm;border-spacing: 0;">
                        <tbody><tr>
                            <th style="border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;background-color: #EFEFEF;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                            	Type                            </th>
                            <th style="border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;background-color: #EFEFEF;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                            	Quotation N.:                            </th>
                            <th style="border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;background-color: #EFEFEF;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                            	Date:                            </th>
                            <th style="border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;background-color: #EFEFEF;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                            	Expiry Date                            </th>
                            <th style="border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;background-color: #EFEFEF;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;width: 12mm;">
                            	Page                            </th>
                        </tr>
                        <tr>
                            <td style="padding: 7px;text-align: center;width: 30%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;border-left: 1px solid #AAAAAA;font-size: 12px;">
                            	<strong><b>QUOTATION</b></strong>
                            </td>
                            <td style="padding: 7px;text-align: center;width: 15%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;">
                            	<strong><b>884999004</b></strong>
                            </td>
                            <td style="padding: 7px;text-align: center;width: 15%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;">
                            	<strong><b>11/14/2015</b></strong>
                            </td>
                            <td style="padding: 7px;text-align: center;width: 15%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;">
                            	<strong><b>12/14/2015</b></strong>
                            </td>
                            <td style="padding: 7px;text-align: center;width: 15%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;">
                            	<strong><b>1/1</b></strong>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody></table>
        <p style="line-height: 10px;margin: 0;padding: 0;">&nbsp;</p>
                    <table style="border-collapse: collapse;display: inline-table;left: 0;position: relative;top: 0;border-spacing: 0;width: 750px;">
                    <tbody><tr>
                        <th style="background-color: #EFEFEF;border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                            To                        </th>
                        <th style="background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                            Ship To (if different address)                        </th>
                        <th style="background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                            Payment Method:                        </th>
                    </tr>
                    <tr>
                        <td style="width: 35%;border-bottom: 1px solid #AAAAAA;border-left: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;" rowspan="3">
                            saqib ashraf<br>testing<br>arct, Louisiana<br>United States                        </td>
                        <td style="width: 35%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;" rowspan="3">
                            saqib ashraf<br>testing<br>arct, Louisiana<br>United States                        </td>
                        <td style="width: 30%;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;">
                            Bank Transfer                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                            Shipping Method:                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;">
                            Pickup From Store                        </td>
                    </tr>
                </tbody></table>
                <p style="line-height: 10px;margin: 0;padding: 0;">&nbsp;</p>
        <table style="border-collapse: collapse;border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;left: 0;position: relative;top: 0; width: 750px;">
            <tbody><tr>
                <th style="width: 12%;height: 15px;background-color: #EFEFEF;border-left: 1px solid #AAAAAA;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                	<b>Model</b>
                </th>
                <th style="width: 44%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: left;">
                	<b>Product</b>
                </th>
                <th style="width: 5%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                	<b>Quantity</b>
                </th>
                <th style="width: 12%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                	<b>Unit Price</b>
                </th>
                <th style="width: 12%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                	<b>Total</b>
                </th>
                <th style="width: 5%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                	<b>Sc %</b>
                </th>
                <th style="width: 10%;height: 15px;background-color: #EFEFEF;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;color: #222222;font-size: 12px;font-weight: bold;padding: 5px;text-align: center;">
                	<b>VAT</b>
                </th>
            </tr>
                            	<tr>
                    	<td style="width: 12%;border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;">
                        	500374                        </td>
                        <td style="width: 44%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;">
                        	test 10162015                            <small></small>                        </td>
                        <td style="width: 5%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;">
                        	1                        </td>
                        <td style="width: 12%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;">
                        	$10.00                        </td>
                        <td style="width: 12%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;">
                        	$10.00                        </td>
                        <td style="width: 5%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;">
                        	0                        </td>
                       	<td style="width: 10%;border-top: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;">
                        	$0.00                        </td>
                    </tr>
            	                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                            	<tr>
                    	<td style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                        <td style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: left;height: 15px;text-align: right;"></td>
                    </tr>
                                                    	<tr>
                	<td align="right" colspan="5" style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;">
                    	<b>Sub-Total:</b>
                    </td>
                    <td align="right" style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;" colspan="2">
                    	<strong><b>$10.00</b></strong>
                    </td>
                </tr>
                        	<tr>
                	<td align="right" colspan="5" style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;">
                    	<b>Pickup From Store:</b>
                    </td>
                    <td align="right" style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;" colspan="2">
                    	<strong><b>$0.00</b></strong>
                    </td>
                </tr>
                        	<tr>
                	<td align="right" colspan="5" style="border-left: 1px solid #AAAAAA;border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;">
                    	<b>Total:</b>
                    </td>
                    <td align="right" style="border-bottom: 1px solid #AAAAAA;border-right: 1px solid #AAAAAA;font-size: 12px;padding: 5px;text-align: right;height: 15px;" colspan="2">
                    	<strong><b>$10.00</b></strong>
                    </td>
                </tr>
                    </tbody></table>
    </div>

</body>
</html>
');
  $dompdf->set_paper('letter','portrait');
  $dompdf->render();

  $dompdf->stream("dompdf_out.pdf", array("Attachment" => true));

  exit(0);
  ?>
