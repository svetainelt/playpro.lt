<?php
// HTTP
define('HTTP_SERVER', 'https://playpro.lt/');

// HTTPS
define('HTTPS_SERVER', 'https://playpro.lt/');

// DIR
define('DIR_APPLICATION', '/home/playpro/domains/playpro.lt/public_html/catalog/');
define('DIR_SYSTEM', '/home/playpro/domains/playpro.lt/public_html/system/');
define('DIR_LANGUAGE', '/home/playpro/domains/playpro.lt/public_html/catalog/language/');
define('DIR_TEMPLATE', '/home/playpro/domains/playpro.lt/public_html/catalog/view/theme/');
define('DIR_CONFIG', '/home/playpro/domains/playpro.lt/public_html/system/config/');
define('DIR_IMAGE', '/home/playpro/domains/playpro.lt/public_html/image/');
define('DIR_CACHE', '/home/playpro/domains/playpro.lt/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/playpro/domains/playpro.lt/public_html/system/storage/download/');
define('DIR_LOGS', '/home/playpro/domains/playpro.lt/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/playpro/domains/playpro.lt/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/playpro/domains/playpro.lt/public_html/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'playpro_shop');
define('DB_PASSWORD', 'KlauptinisUzraugenti');
define('DB_DATABASE', 'playpro_shop');
define('DB_PORT', '3306');
define('DB_PREFIX', '');

// OLD DB
// define('OLD_DB_DRIVER', 'mysqli');
// define('OLD_DB_HOSTNAME', 'localhost');
// define('OLD_DB_USERNAME', 'playpro_old');
// define('OLD_DB_PASSWORD', 'PrimonesKolegPrimones');
// define('OLD_DB_DATABASE', 'playpro_old');
// define('OLD_DB_PORT', '3306');
// define('OLD_DB_PREFIX', '');
