<?php

class ControllerPaymentMokilizingas extends ControllerExtensionPaymentMokilizingas { }

class ControllerExtensionPaymentMokilizingas extends Controller {
    
    const API_LIVE_URL = [
        'lt_auth_leasing' => 'https://api.mokilizingas.lt/api2/eshop/auth',
        'lt_post_leasing' => 'https://api.mokilizingas.lt/api2/eshop/post',
        'lt_check_leasing' => 'https://api.mokilizingas.lt/api2/eshop/check',
        'lt_brand_leasing' => 'https://api.mokilizingas.lt/api2/eshop/defaults?brand=ML',
    ];
    
    private $error = array();
    
    private $moduleVersion = '2.0.0';
    
    public $_settings = [
        'errors' => ['payment_name', 'warning', 'user', 'password', 'key', 'minimal_price', 'max_price', 'phone', 'email', 'term', 'logo_url'],
        'inputs' => ['payment_name', 'status', 'mode', 'user', 'password', 'key', 'minimal_price',  'max_price', 'phone', 'email', 'calculator', 'calculator_style', 'calculator_color', 'term', 'waiting_order_status_id', 'canceled_order_status_id', 'accepted_order_status_id', 'logo_url', 'geo_zone_id', 'sort_order'],
    ];
    
    public function index() {
        // For OpenCart older versions;
        $route = $this->request->get['route'];
        if (stripos($route, 'install') !== false) {
            $this->install();
            return;
        }
        
        // Load language;
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
        } else {
            $this->load->language('payment/mokilizingas');
        }
        
        // Set additional style
        $this->document->addStyle('view/stylesheet/mokilizingas.css');
        
        // Set title;
        $this->document->setTitle($this->language->get('heading_title'));
        
        // Load models;
        $this->load->model('setting/setting');
        $this->load->model('localisation/order_status');
        $this->load->model('localisation/geo_zone');
        $this->load->model('payment/mokilizingas');
        
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $this->model_setting_setting->editSetting('mokilizingas', $this->request->post);
            
            $this->session->data['success'] = $this->language->get('text_success');
            
            $redirectParams = 'token='.$this->session->data['token'];
            if (version_compare(VERSION, '2.3', '>=')) {
                $this->response->redirect($this->url->link('extension/extension', $redirectParams.'&type=payment', true));
            } elseif (version_compare(VERSION, '2', '>=')) {
                $this->response->redirect($this->url->link('extension/payment', $redirectParams, true));
            } else {
                $this->redirect($this->url->link('extension/payment', $redirectParams, true));
            }
        }
        
        // Breadcrumb
        $data['breadcrumbs'] = array();
        if (version_compare(VERSION, '2.3', '>=')) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_extension'),
                'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=payment', true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('extension/payment/mokilizingas', 'token=' . $this->session->data['token'], true)
            );
        } else {
            $data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => false
            );
            $data['breadcrumbs'][] = array(
                'text'      => $this->language->get('text_payment'),
                'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            );
            $data['breadcrumbs'][] = array(
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('payment/mokilizingas', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            );
        }
        
        // Actions;
        $data['action'] = $this->url->link('payment/mokilizingas', 'token='.$this->session->data['token'], true);
        if (version_compare(VERSION, '2.3', '>=')) {
            $data['cancel'] = $this->url->link('extension/extension', 'token='.$this->session->data['token'].'&type=payment', true);
        } else {
            $data['cancel'] = $this->url->link('extension/payment', 'token='.$this->session->data['token'], true);
        }
        
        // Set var;
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_save']   = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        
        // Set / get input values
        foreach ($this->_settings['inputs'] as $input) {
            if (isset($this->request->post['mokilizingas_'.$input])) {
                $data['mokilizingas_'.$input] = $this->request->post['mokilizingas_'.$input];
            } else {
                $data['mokilizingas_'.$input] = $this->config->get('mokilizingas_'.$input);
            }
        }
        
        // Check settings
        foreach ($this->_settings['errors'] as $errorType) {
            if (isset($this->error[$errorType])) {
                $data['error_'.$errorType] = $this->error[$errorType];
            } else {
                $data['error_'.$errorType] = '';
            }
        }
        
        // Check updates;
        $data['module_status'] = $this->check('opencart-mokilizingas', $this->moduleVersion, VERSION);
        
        // Set template and output page;
        $this->template = 'payment/mokilizingas.tpl';
        
        if (version_compare(VERSION, '2', '<')) {
            $this->data = array_merge($this->data, $data);
            
            $this->children = array(
                'common/header',
                'common/footer'
            );
            
            $this->response->setOutput($this->render(true), $this->config->get('config_compression'));
        } else {
            $data['header']      = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer']      = $this->load->controller('common/footer');
            
            $this->response->setOutput($this->load->view($this->template, $data));
        }
    }
    
    public function install()
    {
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->model('user/user_group');
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'payment/mokilizingas');
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'payment/mokilizingas');
        }
        
        $this->load->model('payment/mokilizingas');
        $this->model_payment_mokilizingas->install();
    }
    
    protected function validate() {
        $this->load->model('payment/mokilizingas');
        
        if (!$this->request->post['mokilizingas_payment_name']) {
            $this->error['payment_name'] = $this->language->get('error_payment_name');
        }
        
        if (!$this->request->post['mokilizingas_user']) {
            $this->error['user'] = $this->language->get('error_user');
        }
        if (!$this->request->post['mokilizingas_password']) {
            $this->error['password'] = $this->language->get('error_password');
        }
        if (!$this->request->post['mokilizingas_key']) {
            $this->error['key'] = $this->language->get('error_key');
        }
        if (!$this->request->post['mokilizingas_minimal_price']) {
            $this->error['minimal_price'] = $this->language->get('error_minimal_price');
        }
        if (!$this->request->post['mokilizingas_max_price']) {
            $this->error['max_price'] = $this->language->get('error_max_price');
        }
        if (!$this->request->post['mokilizingas_phone']) {
            $this->error['phone'] = $this->language->get('error_phone');
        }
        if (!$this->request->post['mokilizingas_email']) {
            $this->error['email'] = $this->language->get('error_email');
        }
        if (!$this->request->post['mokilizingas_term']) {
            $this->error['term'] = $this->language->get('error_term');
        }
        if (!$this->request->post['mokilizingas_logo_url']) {
            $this->error['logo_url'] = $this->language->get('error_logo_url');
        }
        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }
        
        // Check API connection;
        if (!$this->model_payment_mokilizingas->checkApiSettings($this->request->post['mokilizingas_user'], $this->request->post['mokilizingas_password'])) {
            $this->error['user'] = 'Neteisingas vartotojas arba slaptažodis.';
            $this->error['password'] = 'Neteisingas vartotojas arba slaptažodis.';
        }
        
        // Check brand update;
        $this->updateBrand();
        
        return !$this->error;
    }
    
    public function order() {
        // Load language;
        if (version_compare(VERSION, '2.3', '>=')) {
            $this->load->language('extension/payment/mokilizingas');
        } else {
            $this->load->language('payment/mokilizingas');
        }
        
        $this->load->model('payment/mokilizingas');
        
        $data['text_leasing'] = $this->language->get('text_leasing');
        $data['text_admin_info'] = $this->language->get('text_admin_info');
        $data['text_check_response'] = $this->language->get('text_check_response');
        $data['text_column_id'] = $this->language->get('text_column_id');
        $data['text_column_id_order'] = $this->language->get('text_column_id_order');
        $data['text_column_sessionId'] = $this->language->get('text_column_sessionId');
        $data['text_column_errorMessage'] = $this->language->get('text_column_errorMessage');
        $data['text_column_errorCode'] = $this->language->get('text_column_errorCode');
        $data['text_column_resultCode'] = $this->language->get('text_column_resultCode');
        $data['text_column_resultInfo'] = $this->language->get('text_column_resultInfo');
        $data['text_column_resultMsg'] = $this->language->get('text_column_resultMsg');
        $data['text_column_advance'] = $this->language->get('text_column_advance');
        $data['text_column_currency'] = $this->language->get('text_column_currency');
        $data['text_column_contractNo'] = $this->language->get('text_column_contractNo');
        $data['text_column_date_add'] = $this->language->get('text_column_date_add');
        
        $data['logs'] = $this->model_payment_mokilizingas->getOrdersRecords($this->request->get['order_id']);
        $data['user_token'] = $this->session->data['token'];
        $data['order_id'] = $this->request->get['order_id'];
        
        if (version_compare(VERSION, '2', '<')) {
            $this->data = array_merge($this->data, $data);
            
            $this->response->setOutput($this->render(true), $this->config->get('config_compression'));
        } else {
            return $this->load->view('payment/mokilizingas_order.tpl', $data);
        }
        
    }
    
    public function status() {
        $id_session = 0;
        
        $this->load->model('payment/mokilizingas');
        
        if (isset($this->request->post['order_id'])) {
            $order_id = $this->request->post['order_id'];
        } else {
            $order_id = 0;
        }
        $session = $this->model_payment_mokilizingas->getSessionIdByOrderId($order_id);
        
        $id_session = $session['sessionId'];
        if ($order_id > 0 && $id_session) {
            $queryData = array(
                'stamp' => microtime(true),
                'id_order' => $order_id,
                'sessionId' => $id_session,
                'errorMessage' => '',
                'errorCode' => '',
                'resultCode' => '',
                'resultInfo' => 'Checking status from order admin',
                'resultMsg' => '',
                'advance' => '',
                'currency' => '',
                'contractNo' => '',
            );
            
            $base = 'https://api.mokilizingas.lt/api2/eshop/check';
            
            $args = array(
                'session_id' => $id_session,
                'order_id' => $order_id,
                'user_name' => $this->config->get('mokilizingas_user'),
                'user_psw' => $this->config->get('mokilizingas_password'),
            );
            $leasingResponse = $this->model_payment_mokilizingas->getLeasingResponse($base, $args);
            // Update log array;
            $queryData['resultCode'] = $leasingResponse['result'];
            $queryData['resultInfo'] = $leasingResponse['resultInfo'];
            $queryData['advance'] = $leasingResponse['advance'];
            $queryData['currency'] = $leasingResponse['currency'];
            $queryData['contractNo'] = $leasingResponse['contractNo'];
            
            if ($leasingResponse['result'] == 'SIGNED_DEAL') {
                $queryData['resultMsg'] = 'Signed: '.$leasingResponse['resultMsg'];
                $this->model_payment_mokilizingas->updateOrderStatus($this->config->get('mokilizingas_accepted_order_status_id'), $order_id);
            } elseif ($leasingResponse['result'] == 'NO_LOAN_POSSIBLE' || $leasingResponse['result'] == 'CLIENT_CANCELED') {
                $queryData['resultMsg'] = 'Cancelled: '.$leasingResponse['resultMsg'];
                $this->model_payment_mokilizingas->addOrderHistory($order_id, $this->config->get('mokilizingas_canceled_order_status_id'));
            } elseif ($leasingResponse['result'] == 'WAITING_FOR_PERMISSION') {
                $queryData['resultMsg'] = 'Waiting response: '.$leasingResponse['resultMsg'];
            } else {
                $queryData['resultMsg'] = 'Custom response: '.$leasingResponse['resultMsg'];
            }
            
            $this->model_payment_mokilizingas->addQueryRecord($queryData);
            $json['status'] = true;
            $json['msg'] = '';
        } else {
            $json['status'] = false;
            $json['msg'] = 'Could not get order id';
        }
        $this->response->setOutput(json_encode($json));
    }
    
    private function updateBrand()
    {
        $defaultTheme = 'default';
        if (version_compare(VERSION, '2.3', '>=')) {
            $defaultTheme = $this->config->get('theme_default_directory');
        }
        
        $leasing_logo = DIR_CATALOG.'view/theme/'.$defaultTheme.'/image/mokilizingas.svg';
        
        if (file_exists($leasing_logo)) {
            if (strtotime(date("Y-m-d H:i:s", filemtime($leasing_logo))) < (date("U") - 86400)) {
                try {
                    $this->getBrandData($leasing_logo);
                } catch (Exception $e) {
                    return 'Caught exception: '.  $e->getMessage();
                }
            }
        } else {
            $this->getBrandData($leasing_logo);
        }
        
        return true;
    }
    
    private function getBrandData($path) {
        $json_brand_data = file_get_contents(self::API_LIVE_URL['lt_brand_leasing']);
        
        // Get brand JSON data;
        if (isset($json_brand_data) && !empty($json_brand_data)) {
            $brand_data = json_decode($json_brand_data);
            
            // Check if logo exist and size > 0 kb;
            if (isset($brand_data->logo6) && !empty($brand_data->logo6) && $this->checkRemoteFile($brand_data->logo6) > 0) {
                
                $ctx = stream_context_create(array('http' => array('timeout' => 5)));
                
                $inbank_logo = file_get_contents($brand_data->logo6, false, $ctx);
                
                if (!empty($inbank_logo)) {
                    file_put_contents($path, $inbank_logo);
                }
            }
        }
        
    }
    
    public function check($module_name, $module_version, $system_version)
    {
        $content = $this->getNewestVersion($module_name, $module_version, $system_version);
        $new_version = $this->getConfig(strtoupper($module_name.'_last_update_check_version'), $module_version);
        if (version_compare($new_version, $module_version, '>'))
        {
            $response_json = $this->getConfig(strtoupper($module_name.'_last_update_check_response'), '');
            if (!empty($response_json))
            {
                $response = json_decode($response_json, true);
                $docs_url = $response['docs_url'];
                $update_url = $response['update_url'];
                $additional_text = base64_decode($response['additional_text_base64']);
                $linkToDownload = $readDocumentation = '';
                if (isset($update_url) && !empty($update_url)) {
                    $linkToDownload = $this->l('Updated module can be downloaded from').' - <a target="_blank" href="'.$update_url.'">'.$update_url.'</a>.';
                }
                if (isset($docs_url) && !empty($docs_url)) {
                    $readDocumentation = $this->l('More about update').' <a target="_blank" href="'.$docs_url.'">'.$this->l('read here').'</a>.';
                }
                $content .=  $this->displayWarning($this->l('New module update was found.').' '.$linkToDownload.' '.$readDocumentation. ' '.$additional_text);
            }
        }
        return $content;
    }
    
    public function getNewestVersion($module_name, $module_version, $system_version)
    {
        try
        {
            if (1 == 1 || $this->getConfig(strtoupper($module_name . '_last_update_check_date'), 0) < (date("U") - 86400))
            {
                $this->updateConfig(strtoupper($module_name . '_last_update_check_date'), date("U"));
                if (ini_get("allow_url_fopen"))
                {
                    if (function_exists("file_get_contents"))
                    {
                        $ctx = stream_context_create(array(
                            'http' => array(
                                'timeout' => 10,  // timeout 10 sec
                            )
                        ));
                        $response_json = @file_get_contents('http://update.elpresta.eu/get.php?m=' . $module_name . "&v=" . $this->encrypt($module_version) . "&pv=" . $this->encrypt($system_version) . "&h=" . $this->encrypt($this->getUrl()), false, $ctx);
                        if (!empty($response_json))
                        {
                            $response = json_decode($response_json, true);
                            if (isset($response['new_version']) && !empty($response['new_version']))
                            {
                                $this->updateConfig(strtoupper($module_name . '_last_update_check_version'), $response['new_version']);
                                $this->updateConfig(strtoupper($module_name . '_last_update_check_response'), $response_json);
                                if (version_compare($module_version, $response['new_version'], '='))
                                    return $this->displayConfirmation($this->l('Successfully checked for updates. No new updates were found. You are using the latest module version.'));
                            }
                        }
                    }
                    else
                        return $this->displayWarning($this->l('Unable to check for updates. Funcion `file_get_contents` is disabled.'));
                }
                else
                    return $this->displayWarning($this->l('Unable to check for updates. Setting `allow_url_fopen` is disabled.'));
            }
            return '';
        }
        catch (Exception $ex)
        {
            $this->updateConfig(strtoupper($module_name . '_last_update_check_date'), date("U"));
            return $this->displayWarning($this->l('Failed to check for updates. Error: ').$ex->getMessage());
        }
    }
    
    public function encrypt($input)
    {
        return strtr(base64_encode($input), '+/=', '._-');
    }
    
    public function displayConfirmation($text)
    {
        return "<div class=\"mokilizingas__updater-msg mokilizingas__updater-msg--success\">
                <p><strong>{$text}</strong></p>
            </div>";
    }
    
    public function displayWarning($text)
    {
        return "<div class=\"mokilizingas__updater-msg mokilizingas__updater-msg--warning\">
                <p><strong>{$text}</strong></p>
            </div>";
    }
    
    public function l($text)
    {
        return $text;
    }
    
    public function getConfig($key, $default_value)
    {
        $options = $this->config->get($key);
        if (empty($options))
            return $default_value;
        else
            return $options;
    }
    
    public function updateConfig($key, $value)
    {
        $settings[$key] = $value;
        $this->model_setting_setting->editSetting($key, $settings);
    }
    
    public function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        
        $result = curl_exec($ch);
        curl_close($ch);
        if($result !== FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    public function getUrl()
    {
        return $_SERVER['HTTP_HOST'];
    }
}
?>