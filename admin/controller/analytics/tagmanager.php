<?php
/******************************************************
 * @package Google Tag Manager for OC2.1.0.1, 2.1.0.2
 * @version 9.3
 * @author Muhammad Akram
 * @link https://aits.xyz
 * @copyright Copyright (C)2021 aits.xyz All rights reserved.
 * @email:info@aits.pk. 
 * $date: 17 DEC 2021
*******************************************************/

class ControllerAnalyticsTagManager extends Controller {

const MODULE = '30750'; const PREFIX = 'analytics_'; const TEMPLATE = 'tagmanager'; const TMV = '8.4'; const TMC = 'GTM-TWB4J2D'; private $token; private $catalog_url; private $error = array();

public function __construct($registry) { parent::__construct($registry); $this->catalog = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG; $this->token = isset($this->session->data['user_token']) ? 'user_token='.$this->session->data['user_token'] : 'token='.$this->session->data['token']; }

public function index() { 
    $this->load->language('analytics/tagmanager');
    $this->load->controller('extension/analytics/tagmanager');
}

public function clear() {

$file = DIR_LOGS . 'tagmanager.log';

$ver = substr(VERSION,0,1); $store_id = 0; $sub_ver = substr(VERSION,0,3);

$handle = fopen($file, 'w+');

fclose($handle);

$this->session->data['success'] = 'Log cleared';

if ($ver == '3') { $this->response->redirect($this->url->link('extension/analytics/tagmanager', $this->token .  '&store_id=' . $store_id , true)); } else { if ($sub_ver == '2.1' || $sub_ver == '2.2') { $this->response->redirect($this->url->link('analytics/tagmanager', $this->token .  '&store_id=' . $store_id , 'SSL')); } else { $this->response->redirect($this->url->link('extension/analytics/tagmanager', $this->token .  '&store_id=' . $store_id , 'SSL')); } } }

protected function validate() {

$ver = substr(VERSION,0,1); $PREFIX = ''; $store_id = 0; $sub_ver = substr(VERSION,0,3);


if(substr(VERSION,0,1)=='3' ) { $PREFIX = 'analytics_'; $ver = '3'; } else { $ver = '2'; $PREFIX = ''; }

if (!isset($this->request->get['store_id'])) { $store_id = 0; } else { $store_id = $this->request->get['store_id']; }

if (isset($this->request->post['order_id']) || isset($this->request->post['datas'])) { return false; }

if ($sub_ver == '2.1' || $sub_ver == '2.2') { if (!$this->user->hasPermission('modify', 'analytics/tagmanager')) { $this->error['warning'] = $this->language->get('error_permission'); } } else { if (!$this->user->hasPermission('modify', 'extension/analytics/tagmanager')) { $this->error['warning'] = $this->language->get('error_permission'); } }

if (isset($this->request->post[$PREFIX . 'tagmanager_primary']) && empty($this->request->post[$PREFIX . 'tagmanager_primary'])) { $this->error['primary'] = $this->language->get('error_primary'); }

if (isset($this->request->post[$PREFIX . 'tagmanager_ua_status']) && $this->request->post[$PREFIX . 'tagmanager_ua_status'] ==  '1') { if (!$this->request->post[$PREFIX . 'tagmanager_gid']) { $this->error['analytics'] = $this->language->get('error_analytics'); } }

if (isset($this->request->post[$PREFIX . 'tagmanager_ga4_status']) && $this->request->post[$PREFIX . 'tagmanager_ga4_status'] ==  '1') { if (!$this->request->post[$PREFIX . 'tagmanager_ga4_mid']) { $this->error['ga4'] = $this->language->get('error_ga4'); } }

if (isset($this->request->post[$PREFIX . 'tagmanager_conversion_value2']) && empty($this->request->post[$PREFIX . 'tagmanager_conversion_value2'])) { $this->request->post[$PREFIX . 'tagmanager_conversion_value2'] = 1; }

return !$this->error; }

private function getlang() {

$languageVariables = array( 'heading_title', 'primary', 'entry_server', 'entry_server_url', 'text_edit', 'text_enabled', 'text_disabled', 'text_signup', 'text_about', 'text_about_cookie', 'text_version', 'heading_container', 'text_container', 'text_order', 'entry_primary', 'entry_status', 'entry_ampcode', 'entry_ampstatus', 'entry_admin', 'entry_cache', 'entry_debug', 'entry_gid', 'entry_ua_status', 'entry_ga4_status', 'entry_ga4_mid', 'entry_ga4_api', 'entry_mp', 'entry_custom_dimension1', 'entry_custom_dimension2', 'entry_custom_dimension3', 'entry_custom_dimension4', 'entry_custom_dimension5', 'entry_custom_dimension6', 'entry_custom_dimension7', 'entry_custom_dimension8', 'entry_custom_dimension9', 'entry_custom_dimension', 'entry_google_optimize', 'entry_google_optimize_status', 'entry_greview', 'entry_greview_badge', 'entry_merchant_id', 'entry_custom', 'entry_remarketing', 'entry_userid_status', 'entry_product', 'entry_ptitle', 'entry_id_prefix', 'entry_id_suffix', 'entry_customcode', 'entry_adword', 'entry_adword2', 'entry_conversion_id', 'entry_conversion_label', 'entry_adword_ec', 'entry_conversion_id2', 'entry_conversion_label2', 'entry_conversion_route2', 'entry_conversion_value2', 'entry_aw_optional', 'entry_aw_merchant_id', 'entry_aw_feed_country', 'entry_aw_feed_language', 'entry_dynx_itemid', 'entry_dynx_itemid2', 'entry_dynx_pagetype', 'entry_dynx_totalvalue', 'entry_ecomm_pagetype', 'entry_ecomm_prodid', 'entry_ecomm_totalvalue', 'entry_pixel', 'entry_pixelcode', 'entry_fb_api', 'entry_fb_token', 'entry_alt_currency', 'entry_alt_currency_status', 'entry_alt_currency_val', 'entry_fb_catalog_id', 'entry_twitter_status', 'entry_twitter_tag', 'entry_pinterest_status', 'entry_pinterest_tag', 'entry_glami_status', 'entry_glami_code', 'entry_hotjar_status', 'entry_hotjar_siteid', 'entry_luckyorange_status', 'entry_luckyorange_siteid', 'entry_tiktok_status', 'entry_tiktok_code', 'entry_clarity_status', 'entry_clarity_siteid', 'entry_bing_status', 'entry_bing_uetid', 'entry_skroutz_status', 'entry_skroutz_siteid', 'entry_skroutz_manual_tax', 'entry_skroutz_manual_tax_value', 'entry_skroutz_payment_fee', 'entry_yandex_status', 'entry_yandex_code', 'entry_admitad_status', 'entry_admitad_code', 'entry_admitad_category', 'entry_admitad_additional_type', 'entry_admitad_invoice_broker', 'entry_admitad_invoice_category', 'entry_admitad_retag_status', 'entry_admitad_retag_code', 'entry_admitad_retag_code1', 'entry_admitad_retag_code2', 'entry_admitad_retag_code3', 'entry_admitad_retag_code4', 'entry_admitad_retag_code5', 'entry_freshchat_status', 'entry_freshchat_code', 'entry_freshchat_host', 'entry_snap_pixel_status', 'entry_snap_pixel_id', 'entry_yandex_code', 'entry_performant_status', 'entry_performant_code', 'entry_performant_confirm', 'entry_affgateway_status', 'entry_affgateway_code', 'entry_sendinblue_status', 'entry_sendinblue_code', 'entry_eu_cookie', 'entry_eu_cookie_enforce', 'entry_cookie_position', 'entry_cookie_title', 'entry_cookie_text', 'entry_cookie_text2', 'entry_cookie_link', 'entry_cookie_button1', 'entry_cookie_button2', 'entry_cookie_button3', 'entry_cookie_bg_popup', 'entry_cookie_text_popup', 'entry_cookie_bg_button', 'entry_cookie_text_button', 'entry_cookie_heading_color', 'entry_cookie_badge', 'entry_cookie_badge_position', 'entry_cookie_badge_color', 'entry_zopim_status', 'entry_zopim_code', 'entry_zenchat_status', 'entry_zenchat_code', 'entry_zopimchat_status', 'entry_zopimchat_code', 'entry_hubspot_status', 'entry_hubspot_code', 'entry_smartsupp_status', 'entry_smartsupp_code', 'entry_paypal_status', 'entry_paypal_code', 'entry_route_checkout', 'entry_route_success', 'column_oid', 'column_status', 'column_action', 'help_ua', 'help_server', 'help_server_url', 'help_ga4', 'help_ga4_api', 'help_gid', 'help_secondary', 'help_admin', 'help_conversion_id', 'help_userid', 'help_conversion_label', 'help_conversion_value2', 'help_remarketing', 'help_product', 'help_ptitle', 'help_mp', 'help_cache', 'help_ac', 'help_ac_value', 'help_custom', 'help_route', 'help_route_checkout', 'help_route_success', 'help_id_prefix', 'help_id_suffix', 'help_customcode', 'help_custom_dim', 'help_aw', 'help_aw_ec', 'help_aw_secondary', 'help_aw_optional', 'help_aw_merchant', 'help_aw_country', 'help_aw_language', 'help_aw_route', 'help_cenforce', 'help_ctitle', 'help_ctext', 'help_clink', 'help_debug', 'tab_tab1', 'tab_tab2', 'tab_tab3', 'tab_tab4', 'tab_tab5', 'tab_tab6', 'tab_tab7', 'tab_tab8', 'button_save', 'button_cancel', 'button_send', 'button_refund', 'error_permission', 'error_primary', 'error_secondary', 'error_analytics', 'error_warrning', 'error_ga4', 'entry_debug_api', 'help_debug_api', 'entry_performant_tax', 'entry_performant_tax_value', 'entry_performant_currency', ); foreach ($languageVariables as $languageVariable) { $data[$languageVariable] = $this->language->get($languageVariable); } return $data; }

public function install(){ $this->updateDatabase();

}

public function uninstall() {


}

private function updateDatabase() {

$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "analytics_tracking` ( `id` int(11) NOT NULL AUTO_INCREMENT, `order_id` int(11) DEFAULT NULL, `cid` varchar(128) DEFAULT NULL, `uid` varchar(64) DEFAULT NULL, `ip` varchar(64) DEFAULT NULL, `geoid` varchar(64) DEFAULT NULL, `sr` varchar(64) DEFAULT NULL, `vp` varchar(64) DEFAULT NULL, `ul` varchar(64) DEFAULT NULL, `dr` varchar(250) DEFAULT NULL, `hit` tinyint(1) NOT NULL DEFAULT '0', `tid` varchar(24) DEFAULT NULL, `user_agent` varchar(250) DEFAULT NULL, `currency_code` varchar(11) DEFAULT NULL, `currency_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

}

private function columnExistsInTable($table, $column) { $query = $this->db->query("DESC `" . DB_PREFIX . $table . "`;"); foreach($query->rows as $row) { if($row['Field'] == $column) { return true; } } return false; }

private function getTagmanger() {

$tagmanager = array(); $PREFIX = '';

$cid = (isset($_COOKIE['_ga']) ? $_COOKIE['_ga'] : '');

if(substr(VERSION,0,1)=='3' ) { $PREFIX = 'analytics_'; }

$tagmanager = array ( 'primary' 				=> $this->config->get($PREFIX . 'tagmanager_primary'), 'status'				=> $this->config->get($PREFIX . 'tagmanager_status'), 'ampstatus'				=> $this->config->get($PREFIX . 'tagmanager_ampstatus'), 'admin'					=> $this->config->get($PREFIX . 'tagmanager_admin'), 'language'				=> (isset($_COOKIE['language']) ? $_COOKIE['language'] : ''), 'vs'					=> base64_encode($this->config->get($PREFIX . 'tagmanager_primary').$this->request->server['SERVER_NAME']), 'host'					=> $this->request->server['SERVER_NAME'], 'currency'				=> (isset($this->session->data['currency']) ? $this->session->data['currency'] : $this->config->get('config_currency')), 'alt_currency'			=> $this->config->get($PREFIX . 'tagmanager_alt_currency'), 'alt_currency_status'	=> $this->config->get($PREFIX . 'tagmanager_alt_currency_status') ); if (empty($tagmanager['alt_currency']) || $tagmanager['alt_currency_status'] != '1' ) { $tagmanager['alt_currency'] = (isset($this->session->data['currency']) ? $this->session->data['currency'] : $this->config->get('config_currency')); }

return $tagmanager;

}

private function getTransactions($store_id) {

$sql  = "SELECT o.id, o.order_id, o.cid, o.uid, o.ip, o.geoid, o.hit, o.ul,o.tid,o.currency_code,o.currency_id, os.order_status_id FROM `" . DB_PREFIX . "analytics_tracking` AS o LEFT JOIN `" . DB_PREFIX ."order` AS os ON o.order_id = os.order_id"; $sql .= " WHERE os.order_status_id > 0 AND os.store_id = '" . $store_id . "'"; $sql .= " ORDER BY id DESC"; $sql .= " LIMIT 20"; $query = $this->db->query($sql);

return $query->rows; }

private function getSettingValue($key, $store_id = 0) { $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

if ($query->num_rows) { return $query->row['value']; } else { return null; } }

private function getNewURL() { $url = false; $temp = $this->request->server['SERVER_NAME']; $explode = explode(".", $temp); $counter = $this->check_array($explode); if ($counter) { $i = count($explode); if ($i == 2) { $url = $explode[0] . '.' . $explode[1]; } elseif ($i == 3) { if (strtolower($explode[0]) != 'www' ) { $url = $explode[0] . '.' . $explode[1] . '.' . $explode[2]; } else { $url = $explode[1] . '.' . $explode[2]; } } elseif ($i == 4) { $url = $explode[1] . '.' . $explode[2] . '.' . $explode[3]; } } return $url; }

private function check_array($var) { return is_array($var) || $var instanceof \Countable || $var instanceof \SimpleXMLElement || $var instanceof \ResourceBundle; } }
?>