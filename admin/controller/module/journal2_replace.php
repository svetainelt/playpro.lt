<?php
class ControllerModuleJournal2Replace extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/journal2_replace');
        
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/journal2_replace');
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_journal2_replace->editColorsAndFonts($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');
            
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_from'] = $this->language->get('text_from');
        $data['text_to'] = $this->language->get('text_to');
        $data['text_choose_font'] = $this->language->get('text_choose_font');
        $data['text_swap'] = $this->language->get('text_swap');
        $data['entry_color'] = $this->language->get('entry_color');
        $data['entry_google_font'] = $this->language->get('entry_google_font');
        $data['entry_system_font'] = $this->language->get('entry_system_font');
        $data['entry_color_placeholder'] = $this->language->get('entry_color_placeholder');
        
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $this->load->model('journal2/fonts');
        $fonts = $this->model_journal2_fonts->get();
        
        $data['google_fonts'] = $fonts['google_fonts'];
        $data['system_fonts'] = $fonts['system_fonts'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/journal2_replace', 'token=' . $this->session->data['token'], 'SSL')
		);

        $data['action'] = $this->url->link('module/journal2_replace', 'token=' . $this->session->data['token'], 'SSL');
        
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        
        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['journal2_color_from'])) {
            $data['journal2_color_from'] = $this->request->post['journal2_color_from'];
        } else {
            $data['journal2_color_from'] = '';
        }
        
        if (isset($this->request->post['journal2_color_to'])) {
            $data['journal2_color_to'] = $this->request->post['journal2_color_to'];
        } else {
            $data['journal2_color_to'] = '';
        }
        
        if (isset($this->request->post['journal2_google_font_from'])) {
            $data['journal2_google_font_from'] = $this->request->post['journal2_google_font_from'];
        } else {
            $data['journal2_google_font_from'] = '';
        }
        
        if (isset($this->request->post['journal2_google_font_to'])) {
            $data['journal2_google_font_to'] = $this->request->post['journal2_google_font_to'];
        } else {
            $data['journal2_google_font_to'] = '';
        }
        
        if (isset($this->request->post['journal2_system_font_from'])) {
            $data['journal2_system_font_from'] = $this->request->post['journal2_system_font_from'];
        } else {
            $data['journal2_system_font_from'] = '';
        }
        
        if (isset($this->request->post['journal2_system_font_to'])) {
            $data['journal2_system_font_to'] = $this->request->post['journal2_system_font_to'];
        } else {
            $data['journal2_system_font_to'] = '';
        }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/journal2_replace.tpl', $data));
    }

    private function validate() {
//		if (!$this->user->hasPermission('modify', 'module/journal2_replace')) {
//			$this->error['warning'] = $this->language->get('error_permission');
//		}

        $colorFrom = $this->request->post['journal2_color_from'];
        $colorTo = $this->request->post['journal2_color_to'];

        $google_font_from = $this->request->post['journal2_google_font_from'];
        $google_font_to = $this->request->post['journal2_google_font_to'];

        $system_font_from = $this->request->post['journal2_system_font_from'];
        $system_font_to = $this->request->post['journal2_system_font_to'];

        if ((!empty($colorFrom) && !preg_match('/rgb *\( *(\d+), *(\d+), *(\d+) *\)/', $colorFrom)) || (!empty($colorTo) && !preg_match('/rgb *\( *(\d+), *(\d+), *(\d+) *\)/', $colorTo))) {
            $this->error['warning'] = $this->language->get('error_rgb_code');
        }

        if (
            (!empty($colorFrom) && empty($colorTo) ) || ( empty($colorFrom) && !empty($colorTo) ) ||
            (!empty($google_font_from) && empty($google_font_to) ) || ( empty($google_font_from) && !empty($google_font_to) ) ||
            (!empty($system_font_from) && empty($system_font_to) ) || ( empty($system_font_from) && !empty($system_font_to) )
        ) {
            $this->error['warning'] = $this->language->get('error_one_empty');
        }

        return !$this->error;
    }
}