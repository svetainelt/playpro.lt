<?php
class ControllerModuleNotifyWhenAvailable extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('module/notify_when_available');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		$this->load->model('module/notify_when_available');

		$data['software_version']	= 1.0;
		$data['version_histories'] 	= [];
		$latest_version 			= $data['software_version'];
		$module_url 				= "http://notifywhenavailable.webtuningtechnology.com/";
		$ch = curl_init($module_url.'version.php');
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$version_result = curl_exec($ch); // This is the result from the API
		curl_close($ch);
		$version_result = json_decode($version_result,true);

		if($version_result){
			$latest_version = $version_result['version'];
			if($latest_version > $data['software_version']){
				$data['new_version_notification'] = "New Version ".$latest_version." is available.<a href='".$module_url."' style='color:#FFF;' target='_blank'> Click here for more detail.</a>";
			}
			$data['version_histories'] = $version_result['version_histories'];
		}

		//languages
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$subject 	= [];
		$body 		= [];
		foreach ($data['languages'] as $language) {
			$subject[$language['language_id']]['subject'] 	= $this->language->get('email_subject');
			$body[$language['language_id']]['body'] 		= $this->language->get('email_body');
		}

		
		$this->model_module_notify_when_available->add_tables();

		$data['token'] = $this->session->data['token'];

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('module_notify_when_available', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'] . '&type=module', true));

		}

		$data['heading_title'] 			= $this->language->get('heading_title');
		$data['entry_status'] 			= $this->language->get('entry_status');
		$data['entry_out_of_stock'] 	= $this->language->get('entry_out_of_stock');
		$data['entry_notify_button'] 	= $this->language->get('entry_notify_button');
		$data['tab_notify_product'] 	= $this->language->get('tab_notify_product');
		$data['tab_email'] 				= $this->language->get('tab_email');
		$data['tab_general'] 			= $this->language->get('tab_general');
		$data['button_product'] 		= $this->language->get('button_product');
		$data['entry_email'] 			= $this->language->get('entry_email');
		$data['entry_email_subject'] 	= $this->language->get('entry_email_subject');
		$data['entry_email_subject'] 	= $this->language->get('entry_email_subject');
		$data['entry_email_body'] 		= $this->language->get('entry_email_body');

		$data['text_edit'] 					= $this->language->get('text_edit');
		$data['text_disabled'] 				= $this->language->get('text_disabled');
		$data['text_enabled'] 				= $this->language->get('text_enabled');
		$data['text_no_results'] 			= $this->language->get('text_no_results');

		$data['button_save'] 				= $this->language->get('button_save');
		$data['button_cancel'] 				= $this->language->get('button_cancel');

		

		//email customer credit
		if (isset($this->error['module_notify_when_available_email_subject'])) {
			$data['error_module_notify_when_available_email_subject'] = $this->error['module_notify_when_available_email_subject'];
		} else {
			$data['error_module_notify_when_available_email_subject'] = '';
		}

		if (isset($this->error['module_notify_when_available_email_body'])) {
			$data['error_module_notify_when_available_email_body'] = $this->error['module_notify_when_available_email_body'];
		} else {
			$data['error_module_notify_when_available_email_body'] = '';
		}



		//help
		$data['help_email_subject'] 	= $this->language->get('help_email_subject');
		$data['help_email_body'] 		= $this->language->get('help_email_body');
		
		// titles
		$data['heading_email_to_customer'] 	= $this->language->get('heading_email_to_customer');
		$data['heading_email_to_admin'] 	= $this->language->get('heading_email_to_admin');
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/notify_when_available', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('module/notify_when_available', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['module_notify_when_available_status'])) {
			$data['module_notify_when_available_status'] = $this->request->post['module_notify_when_available_status'];
		} else {
			$data['module_notify_when_available_status'] = $this->config->get('module_notify_when_available_status');
		}

		if (isset($this->request->post['module_notify_when_available_stock'])) {
			$data['module_notify_when_available_stock'] = $this->request->post['module_notify_when_available_stock'];
		} else {
			$data['module_notify_when_available_stock'] = $this->config->get('module_notify_when_available_stock');
		}

		if (isset($this->request->post['module_notify_when_available_notify'])) {
			$data['module_notify_when_available_notify'] = $this->request->post['module_notify_when_available_notify'];
		} else {
			$data['module_notify_when_available_notify'] = $this->config->get('module_notify_when_available_notify');
		}


		//debit email
		if (isset($this->request->post['module_notify_when_available_email_subject'])) {
			$data['module_notify_when_available_email_subject'] = $this->request->post['module_notify_when_available_email_subject'];
		} else if($this->config->get('module_notify_when_available_email_subject')){
			$data['module_notify_when_available_email_subject'] = $this->config->get('module_notify_when_available_email_subject');
		}else {
			$data['module_notify_when_available_email_subject'] = $subject;
		}

		if (isset($this->request->post['module_notify_when_available_email_body'])) {
			$data['module_notify_when_available_email_body'] = $this->request->post['module_notify_when_available_email_body'];
		} else if($this->config->get('module_notify_when_available_email_body')){
			$data['module_notify_when_available_email_body'] = $this->config->get('module_notify_when_available_email_body');
		}else {
			$data['module_notify_when_available_email_body'] = $body;
		}




		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/notify/notify_when_available.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/notify_when_available')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(count($this->request->post['module_notify_when_available_email_subject'])==0) {
				$this->error['warning'] = $this->language->get('error_wallet_email_subject');
			}else{
				foreach ($this->request->post['module_notify_when_available_email_subject'] as $language_id => $value) {
					if(utf8_strlen($value['subject']) < 3 || utf8_strlen($value['subject']) > 120){
					$this->error['module_notify_when_available_email_subject'][$language_id] = $this->language->get('error_wallet_email_subject');
					}
				}
			}

			if(count($this->request->post['module_notify_when_available_email_body'])==0) {
				$this->error['warning'] = $this->language->get('error_wallet_email_body');
			}else{
				foreach ($this->request->post['module_notify_when_available_email_body'] as $language_id => $value) {
					if(utf8_strlen(strip_tags(html_entity_decode($value['body'], ENT_QUOTES, 'UTF-8')))  < 10){
						$this->error['module_notify_when_available_email_body'][$language_id] = $this->language->get('error_wallet_email_body');
					}
				}
			}

		return !$this->error;
	}

	public function products() {

		$this->load->language('module/notify_when_available');
		$this->load->model('module/notify_when_available');
		$this->load->model('customer/customer');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('setting/setting');
		$this->load->model('localisation/language');
		
		$data['text_no_results'] 			= $this->language->get('text_no_results');
		$data['text_register'] 	 	= $this->language->get('text_register');
		$data['text_unregister'] 	= $this->language->get('text_unregister');
		$data['text_show_customer'] = $this->language->get('text_show_customer');
		$data['column_product'] 	= $this->language->get('column_product');
		$data['column_customer'] 	= $this->language->get('column_customer');
		$data['column_message'] 	= $this->language->get('column_message');
		$data['column_date_added'] 	= $this->language->get('column_date_added');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['products'] = array();
		$results = $this->model_module_notify_when_available->get_notify_products(($page - 1) * 10, 10);

		foreach ($results as $result) {
			$product_info = $this->model_catalog_product->getProduct($result['product_id']);
			$product_edit = $this->url->link('catalog/product/edit', 'product_id='.$result['product_id'].'&token=' . $this->session->data['token'], true);
			$customer_edit = $this->url->link('customer/customer/edit', 'customer_id='.$result['customer_id'].'&token=' . $this->session->data['token'], true);
			
			if (is_file(DIR_IMAGE . $product_info['image'])) {
				$thumb = $this->model_tool_image->resize($product_info['image'], 40, 40);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			if($result['store_id']>0){
				$store_info = $this->model_setting_setting->getSetting('config', $result['store_id']);
				$store_name = $store_info['config_name'];
			}else{
				$store_name = $this->language->get('text_default');
			}	

			$language 	= $this->model_localisation_language->getLanguage($result['language_id']);

			$data['products'][] = array(
				'product_id'		=> $result['product_id'],
				'product_name'		=> ($product_info)?$product_info['name']:'',
				'product_thumb'		=> $thumb,
				'product_edit'		=> $product_edit,
				'customer_id'		=> $result['customer_id'],
				'customer_name'		=> $result['name'],
				'customer_email'	=> $result['email'],
				'message'			=> $result['message'],
				'date_added'  		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'customer_edit'		=> $customer_edit,
				'store_name' 		=> sprintf($this->language->get('text_store_name'),$store_name),
				'language' 			=> "<img src='view/image/flags/".$language['code'].".png' title='". $language['name'] ."' /> ".$language['name'],
			);
		}

		$notify_product_total = $this->model_module_notify_when_available->get_notify_total();

		$pagination = new Pagination();
		$pagination->total = $notify_product_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('module/notify_when_available/get_notify_products', 'token=' . $this->session->data['token'] . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($notify_product_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($notify_product_total - 10)) ? $notify_product_total : ((($page - 1) * 10) + 10), $notify_product_total, ceil($notify_product_total / 10));

		$this->response->setOutput($this->load->view('module/notify/products.tpl', $data));
	}
}