<?php
class ControllerModuleHideModules extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/hide_modules');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('hide_modules', $this->request->post);

            $this->css($this->request->post);
            
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_check_to_hide'] = $this->language->get('text_check_to_hide');
        $data['text_check_to_show'] = $this->language->get('text_check_to_show');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_module'] = $this->language->get('tab_module');
        $data['tab_payment'] = $this->language->get('tab_payment');
        $data['tab_shipping'] = $this->language->get('tab_shipping');
        $data['tab_total'] = $this->language->get('tab_total');
        $data['tab_menu'] = $this->language->get('tab_menu');

        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_style_status'] = $this->language->get('entry_style_status');
        
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/hide_modules', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/hide_modules', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        
        $data['modules'] = $this->getModules('module');
        $data['payments'] = $this->getModules('payment');
        $data['shippings'] = $this->getModules('shipping');
        $data['totals'] = $this->getModules('total');
        
		if (isset($this->request->post['hide_modules'])) {
			$data['hide_modules'] = $this->request->post['hide_modules'];
		} else {
			$data['hide_modules'] = $this->config->get('hide_modules');
		}

		if (isset($this->request->post['hide_modules_status'])) {
			$data['hide_modules_status'] = $this->request->post['hide_modules_status'];
		} else {
			$data['hide_modules_status'] = $this->config->get('hide_modules_status');
		}
        
		if (isset($this->request->post['hide_modules_style_status'])) {
			$data['hide_modules_style_status'] = $this->request->post['hide_modules_style_status'];
		} else {
			$data['hide_modules_style_status'] = $this->config->get('hide_modules_style_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/hide_modules.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/hide_modules')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
    
    private function getModules($type) {
        $types = array(
            'module', 'shipping', 'payment', 'total'
        );
        
        if(!in_array($type, $types))
            return false;
        
		$extensions = array();

		$files = glob(DIR_APPLICATION . 'controller/' . $type . '/*.php');

		if ($files) {
			foreach ($files as $file) {
				$extension = basename($file, '.php');

				$this->load->language($type . '/' . $extension);

				$extensions[] = array(
					'name'       => $this->language->get('heading_title'),
                    'extension'  => $extension
				);
			}
		}
        
        return $extensions;
    }    
    
    public function css($post) {
        $data = array();
        
        $module_data = empty($post['hide_modules']) ? $this->config->get('hide_modules') : $post['hide_modules'];
        
        if (!empty($module_data) && isset($module_data['menu'])) {
            $data['css'] = $this->css_rules($module_data['menu']);

            if(empty($post)) {
                $this->response->addHeader('Content-type: text/css');

                $this->response->setOutput($this->load->view('module/hide_modules_css.tpl', $data));
            } else {
                $filename = 'hide_modules.css';
                $filepath = DIR_TEMPLATE . '../stylesheet/' . $filename;

                file_put_contents($filepath, $data['css'], LOCK_EX);
            }
        }
    }
    
    private function css_rules($menius) {
        $string = '#menu li {display: none;}' . "\n";
        
        $string .= $this->recursive_css_rules($menius);
        
        // remove last ,
        $string = substr($string, 0, -2);
        
        $string .= '{ display: block !important; }';

        return $string;
    }
    
    private function recursive_css_rules($menius, $string = '', $parent_rule = '') {
        if(!empty($menius)) {
            foreach($menius as $key => $menu) {
                $child_rule = '> li:nth-child(' . $key . ')';
                
                if(!empty($menu) && is_array($menu)) {
                    $string .= '#menu ' . $parent_rule . $child_rule . ',' . "\n";
                    $child_rule = $parent_rule . $child_rule . ' > ul ';
                    $string = $this->recursive_css_rules($menu, $string, $child_rule);
                } else {
                    $string .= '#menu ' . $parent_rule . $child_rule . ',' . "\n";
                }
            }
        }
        
        return $string;
    }
}