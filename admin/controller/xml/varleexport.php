<?php 
class ControllerXmlVarleexport extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('xml/varleexport');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
						
			//print_r($this->request->post);die();
			
			$this->model_setting_setting->editSetting('varleexport', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('xml/varleexport', 'token=' . $this->session->data['token'], 'SSL'));
		}
		

		$data['heading_title'] = $this->language->get('heading_title');
		$data['entry_heading'] = $this->language->get('entry_heading');
		
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_categories'] = $this->language->get('entry_categories');
		$data['entry_select_all'] = $this->language->get('entry_select_all');
		$data['entry_unselect_all'] = $this->language->get('entry_unselect_all');
		$data['entry_language_id'] = $this->language->get('entry_language_id');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturers'] = $this->language->get('entry_manufacturers');
		$data['entry_min_price'] = $this->language->get('entry_min_price');
		$data['entry_status_info'] = $this->language->get('entry_status_info');
		$data['entry_language_id_info'] = $this->language->get('entry_language_id_info');
		$data['entry_min_price_info'] = $this->language->get('entry_min_price_info');
		$data['entry_retail_price'] = $this->language->get('entry_retail_price');
		$data['entry_retail_price_info'] = $this->language->get('entry_retail_price_info');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_operator'] = $this->language->get('entry_operator');
		$data['entry_value'] = $this->language->get('entry_value');
		$data['entry_prime_cost'] = $this->language->get('entry_prime_cost');
		$data['entry_prime_cost_info'] = $this->language->get('entry_prime_cost_info');
		
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		
		$data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('xml/varleexport', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['prime_cost'])) {
			$data['error_prime_cost'] = $this->error['prime_cost'];
		} else {
			$data['error_prime_cost'] = '';
		}
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		
		$data['xml_url'] = HTTPS_CATALOG . 'index.php?route=xml/varleexport';
		$data['save'] = $this->url->link('xml/varleexport', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('xml/varleexport', 'token=' . $this->session->data['token'], 'SSL');
		
		
		if (isset($this->request->post['varleexport_status'])) {
			$data['varleexport_status'] = $this->request->post['varleexport_status'];
		} else {
			$data['varleexport_status'] = $this->config->get('varleexport_status');
		}
		
		
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		if (isset($this->request->post['varleexport_language_id'])) {
			$data['varleexport_language_id'] = $this->request->post['varleexport_language_id'];
		} elseif ($this->config->get('varleexport_language_id')) {
			$data['varleexport_language_id'] = $this->config->get('varleexport_language_id');
		} else {
			$data['varleexport_language_id'] = (int)$this->config->get('config_language_id');
		}
		
		
		if (isset($this->request->post['varleexport_min_price'])) {
			$data['varleexport_min_price'] = $this->request->post['varleexport_min_price'];
		} else {
			$data['varleexport_min_price'] = $this->config->get('varleexport_min_price');
		}
		
		
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		
		if (isset($this->request->post['varleexport_stores'])) {
			$data['varleexport_stores'] = $this->request->post['varleexport_stores'];
		} elseif ($this->config->get('varleexport_stores')) {
			$data['varleexport_stores'] = $this->config->get('varleexport_stores');
		}  else {
			$data['varleexport_stores'] = array();
		}
		
		$this->load->model('catalog/category');
		
		$filter_data = array("sort" => "name");
		$data['categories'] = $this->model_catalog_category->getCategories($filter_data);


		if (isset($this->request->post['varleexport_category'])) {
			$data['varleexport_categories'] = $this->request->post['varleexport_category'];
		} elseif ($this->config->get('varleexport_category')) {
			$data['varleexport_categories'] = $this->config->get('varleexport_category');
		} else {
			$data['varleexport_categories'] = array();
		}
		
		
		$this->load->model('localisation/tax_class');
		
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		if (isset($this->request->post['varleexport_tax_class_id'])) {
			$data['varleexport_tax_class_id'] = $this->request->post['varleexport_tax_class_id'];
		} elseif ($this->config->get('varleexport_tax_class_id')) {
			$data['varleexport_tax_class_id'] = $this->config->get('varleexport_tax_class_id');
		} else {
			$data['varleexport_tax_class_id'] = 0;
		}
		
		$this->load->model('catalog/manufacturer');
		$data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();
		
		if (isset($this->request->post['varleexport_manufacturer'])) {
			$data['varleexport_manufacturers'] = $this->request->post['varleexport_manufacturer'];
		} elseif ($this->config->get('varleexport_manufacturer')) {
			$data['varleexport_manufacturers'] = $this->config->get('varleexport_manufacturer');
		} else {
			$data['varleexport_manufacturers'] = array();
		}
		
		
		$data['operators'] = array('*','/','+','-');
		
		if (isset($this->request->post['varleexport_operator'])) {
			$data['varleexport_operator'] = $this->request->post['varleexport_operator'];
		} else {
			$data['varleexport_operator'] = $this->config->get('varleexport_operator');
		}
		
		if (isset($this->request->post['varleexport_operator_value'])) {
			$data['varleexport_operator_value'] = $this->request->post['varleexport_operator_value'];
		} else {
			$data['varleexport_operator_value'] = $this->config->get('varleexport_operator_value');
		}
		
		if (isset($this->request->post['varleexport_operator2'])) {
			$data['varleexport_operator2'] = $this->request->post['varleexport_operator2'];
		} else {
			$data['varleexport_operator2'] = $this->config->get('varleexport_operator2');
		}
		
		if (isset($this->request->post['varleexport_operator_value2'])) {
			$data['varleexport_operator_value2'] = $this->request->post['varleexport_operator_value2'];
		} else {
			$data['varleexport_operator_value2'] = $this->config->get('varleexport_operator_value2');
		}
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['column_left'] = $this->load->controller('common/column_left');
		
		$this->response->setOutput($this->load->view('xml/varleexport.tpl', $data));

	}
	
	
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'xml/varleexport')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->request->post['varleexport_tax_class_id'] == 0 && ($this->request->post['varleexport_operator2'] == '' OR utf8_strlen($this->request->post['varleexport_operator_value2']) < 1)) {
			$this->error['prime_cost'] = $this->language->get('error_prime_cost');
		}
		
	

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>