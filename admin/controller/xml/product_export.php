<?php 
class ControllerXmlProductExport extends Controller {
	public function index() {
        global $config;
        global $log;
        $config = $this->config;
        $log = $this->log;
        set_error_handler('error_handler_for_export', E_ALL);
        register_shutdown_function('fatal_error_shutdown_handler_for_export');
        $database = & $this->db;

        // We use the package from http://pear.php.net/package/Spreadsheet_Excel_Writer/
        chdir(DIR_SYSTEM . 'pear');
        require_once "Spreadsheet/Excel/Writer.php";
        chdir(DIR_APPLICATION);

        // Creating a workbook
        $workbook = new Spreadsheet_Excel_Writer();
        $workbook->setTempDir(DIR_CACHE);
        $workbook->setVersion(8); // Use Excel97/2000 BIFF8 Format

        // sending HTTP headers
        $date = date("Y-m-d");
        $workbook->send($date . '_products.xls');

        // Creating the categories worksheet
        $worksheet = & $workbook->addWorksheet('sheet');
        $worksheet->setInputEncoding('UTF-8');
        $this->populateProductsWorksheet($worksheet, $database);

        // Let's send the file
        $workbook->close();

        // Clear the spreadsheet caches
        $this->clearSpreadsheetCache();
        exit;
	}

    protected function clearSpreadsheetCache() {
        $files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    @unlink($file);
                    clearstatcache();
                }
            }
        }
    }

    private function populateProductsWorksheet(&$worksheet, &$database) {
        // The options headings row
        $i = 0;
        $j = 0;
        $worksheet->writeString($i, $j++, 'Prekės kodas');
        $worksheet->writeString($i, $j++, 'EAN');
        $worksheet->writeString($i, $j++, 'Pavadinimas');
        $worksheet->writeString($i, $j++, 'Kiekis vnt.');
        $worksheet->writeString($i, $j++, 'Rezervas');
        $worksheet->writeString($i, $j++, 'Kaina be PVM ');

        $i += 1;
        $j = 0;

        $query = "SELECT p.* FROM `" . DB_PREFIX . "product` p WHERE p.status = 1 AND p.quantity > 0";
        $result = $database->query($query);

        foreach ($result->rows as $row) {
            $id = $row['product_id'];
            $desc_query = "SELECT pd.name FROM `" . DB_PREFIX . "product_description` pd WHERE pd.product_id = $id";
            $desc = $database->query($desc_query)->row;

            $special_query = "SELECT ps.price FROM `" . DB_PREFIX . "product_special` ps WHERE ps.product_id = $id AND ps.customer_group_id = 1";
            $special = $database->query($special_query)->row;


            $worksheet->writeString($i, $j++, $row['model'] ? $row['model'] : '');
            $worksheet->writeString($i, $j++, $row['ean'] ? $row['ean'] : '');
            $worksheet->writeString($i, $j++, $desc['name'] ? $desc['name'] : '');
            $worksheet->writeString($i, $j++, $row['quantity'] ? $row['quantity'] : '0' );
            $worksheet->writeString($i, $j++, $row['reserve'] ? $row['reserve'] : '0' );
            $worksheet->writeString($i, $j++,  str_replace('.', ',', $special['price'] ? $this->currency->format($special['price'],'','',false) : $this->currency->format($row['price'],'','',false))  );
            $i += 1;
            $j = 0;
        }
    }
}
?>