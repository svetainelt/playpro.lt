<?php 
class ControllerXmlKainosExport extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('xml/kainos_export');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
						
			//print_r($this->request->post);die();
			
			$this->model_setting_setting->editSetting('kainos_export', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('xml/kainos_export', 'token=' . $this->session->data['token'], 'SSL'));
		}
		

		$data['heading_title'] = $this->language->get('heading_title');
		$data['entry_heading'] = $this->language->get('entry_heading');
		
		$data['text_none'] = $this->language->get('text_none');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_categories'] = $this->language->get('entry_categories');
		$data['entry_select_all'] = $this->language->get('entry_select_all');
		$data['entry_unselect_all'] = $this->language->get('entry_unselect_all');
		$data['entry_language_id'] = $this->language->get('entry_language_id');
		$data['entry_category_type'] = $this->language->get('entry_category_type');
		$data['entry_category_type_info'] = $this->language->get('entry_category_type_info');
		$data['entry_category_type_first'] = $this->language->get('entry_category_type_first');
		$data['entry_category_type_last'] = $this->language->get('entry_category_type_last');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_manufacturers'] = $this->language->get('entry_manufacturers');
		$data['entry_delivery_price'] = $this->language->get('entry_delivery_price');
		$data['entry_delivery_price_info'] = $this->language->get('entry_delivery_price_info');
		$data['entry_delivery_time'] = $this->language->get('entry_delivery_time');
		$data['entry_delivery_time_info'] = $this->language->get('entry_delivery_time_info');
		$data['entry_min_price'] = $this->language->get('entry_min_price');
		$data['entry_status_info'] = $this->language->get('entry_status_info');
		$data['entry_language_id_info'] = $this->language->get('entry_language_id_info');
		$data['entry_min_price_info'] = $this->language->get('entry_min_price_info');
		$data['entry_operator'] = $this->language->get('entry_operator');
		$data['entry_operator_info'] = $this->language->get('entry_operator_info');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');	
		$data['entry_condition'] = $this->language->get('entry_condition');
		$data['entry_condition_info'] = $this->language->get('entry_condition_info');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		
		$data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('xml/kainos_export', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
		);
		
		
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['token'] = $this->session->data['token'];
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		
		$data['xml_url'] = HTTPS_CATALOG . 'index.php?route=xml/kainos_export';
		$data['save'] = $this->url->link('xml/kainos_export', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('xml/kainos_export', 'token=' . $this->session->data['token'], 'SSL');
		
		
		if (isset($this->request->post['kainos_export_status'])) {
			$data['kainos_export_status'] = $this->request->post['kainos_export_status'];
		} else {
			$data['kainos_export_status'] = $this->config->get('kainos_export_status');
		}
		
		
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		if (isset($this->request->post['kainos_export_language_id'])) {
			$data['kainos_export_language_id'] = $this->request->post['kainos_export_language_id'];
		} elseif ($this->config->get('kainos_export_language_id')) {
			$data['kainos_export_language_id'] = $this->config->get('kainos_export_language_id');
		} else {
			$data['kainos_export_language_id'] = (int)$this->config->get('config_language_id');
		}
		
		if (isset($this->request->post['kainos_export_category_type'])) {
			$data['kainos_export_category_type'] = $this->request->post['kainos_export_category_type'];
		} elseif ($this->config->get('kainos_export_category_type')) {
			$data['kainos_export_category_type'] = $this->config->get('kainos_export_category_type');
		} else {
			$data['kainos_export_category_type'] = '';
		}
		
		if (isset($this->request->post['kainos_export_condition'])) {
			$data['kainos_export_condition'] = $this->request->post['kainos_export_condition'];
		} elseif ($this->config->get('kainos_export_condition')) {
			$data['kainos_export_condition'] = $this->config->get('kainos_export_condition');
		} else {
			$data['kainos_export_condition'] = '';
		}
		
		if (isset($this->request->post['kainos_export_delivery_price'])) {
			$data['kainos_export_delivery_price'] = $this->request->post['kainos_export_delivery_price'];
		} else {
			$data['kainos_export_delivery_price'] = $this->config->get('kainos_export_delivery_price');
		}
		
		if (isset($this->request->post['kainos_export_delivery_time'])) {
			$data['kainos_export_delivery_time'] = $this->request->post['kainos_export_delivery_time'];
		} else {
			$data['kainos_export_delivery_time'] = $this->config->get('kainos_export_delivery_time');
		}
		
		if (isset($this->request->post['kainos_export_min_price'])) {
			$data['kainos_export_min_price'] = $this->request->post['kainos_export_min_price'];
		} else {
			$data['kainos_export_min_price'] = $this->config->get('kainos_export_min_price');
		}
		
		$this->load->model('localisation/tax_class');
		
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		if (isset($this->request->post['kainos_export_tax_class_id'])) {
			$data['kainos_export_tax_class_id'] = $this->request->post['kainos_export_tax_class_id'];
		} elseif ($this->config->get('kainos_export_tax_class_id')) {
			$data['kainos_export_tax_class_id'] = $this->config->get('kainos_export_tax_class_id');
		} else {
			$data['kainos_export_tax_class_id'] = 0;
		}
		
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		
		if (isset($this->request->post['kainos_export_stores'])) {
			$data['kainos_export_stores'] = $this->request->post['kainos_export_stores'];
		} elseif ($this->config->get('kainos_export_stores')) {
			$data['kainos_export_stores'] = $this->config->get('kainos_export_stores');
		}  else {
			$data['kainos_export_stores'] = array();
		}
		
		$this->load->model('catalog/category');
		
		$filter_data = array("sort" => "name");
		$data['categories'] = $this->model_catalog_category->getCategories($filter_data);


		if (isset($this->request->post['kainos_export_category'])) {
			$data['kainos_export_categories'] = $this->request->post['kainos_export_category'];
		} elseif ($this->config->get('kainos_export_category')) {
			$data['kainos_export_categories'] = $this->config->get('kainos_export_category');
		} else {
			$data['kainos_export_categories'] = array();
		}
		
		
		
		$this->load->model('catalog/manufacturer');
		$data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();
		
		if (isset($this->request->post['kainos_export_manufacturer'])) {
			$data['kainos_export_manufacturers'] = $this->request->post['kainos_export_manufacturer'];
		} elseif ($this->config->get('kainos_export_manufacturer')) {
			$data['kainos_export_manufacturers'] = $this->config->get('kainos_export_manufacturer');
		} else {
			$data['kainos_export_manufacturers'] = array();
		}
		
		
		$data['operators'] = array('*','/','+','-');
		
		if (isset($this->request->post['kainos_export_operator'])) {
			$data['kainos_export_operator'] = $this->request->post['kainos_export_operator'];
		} else {
			$data['kainos_export_operator'] = $this->config->get('kainos_export_operator');
		}
		
		if (isset($this->request->post['kainos_export_operator_value'])) {
			$data['kainos_export_operator_value'] = $this->request->post['kainos_export_operator_value'];
		} else {
			$data['kainos_export_operator_value'] = $this->config->get('kainos_export_operator_value');
		}
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['column_left'] = $this->load->controller('common/column_left');
		
		$this->response->setOutput($this->load->view('xml/kainos_export.tpl', $data));

	}
	
	
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'xml/kainos_export')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>