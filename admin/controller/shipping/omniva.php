<?php

define('SHIPPING_OMNIVA_VERSION', "2.2.0");

class ControllerShippingOmniva extends Controller {
    
	private $error = array(); 
	
	public function index() {   
		$this->load->language('shipping/omniva');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('omniva', $this->request->post);		

			$omniva_addresses_set = $this->model_setting_setting->getSetting('omniva_addresses');
			
			if (!$omniva_addresses_set) $this->update_omniva($this->request->post);
			
                        if ($this->request->post['do_update'] && $this->request->post['do_update'] == '1') {
                            $this->update_omniva($this->request->post);
                        }

			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_module_version'] = $this->language->get('text_module_version');
		$data['text_select_zone'] = $this->language->get('text_select_zone');
		$data['text_each_product'] = $this->language->get('text_each_product');
		$data['text_all_cart'] = $this->language->get('text_all_cart');
		$data['text_edit'] = $this->language->get('text_edit');
		
                $data['entry_weight'] = $this->language->get('entry_weight');
		$data['entry_dimension'] = $this->language->get('entry_dimension');
		$data['entry_cost'] = $this->language->get('entry_cost');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_import_url'] = $this->language->get('entry_import_url');
		$data['entry_addresses'] = $this->language->get('entry_addresses');

		$data['help_dimension'] = $this->language->get('help_dimension');
		$data['help_weight'] = $this->language->get('help_weight');
		
		$data['button_update'] = $this->language->get('button_update');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['omniva_weight'])) {
			$data['error_omniva_weight'] = $this->error['omniva_weight'];
		} else {
			$data['error_omniva_weight'] = '';
		}

		if (isset($this->error['omniva_length'])) {
			$data['error_omniva_length'] = $this->error['omniva_length'];
		} else {
			$data['error_omniva_length'] = '';
		}
		
		if (isset($this->error['omniva_width'])) {
			$data['error_omniva_width'] = $this->error['omniva_width'];
		} else {
			$data['error_omniva_width'] = '';
		}

		if (isset($this->error['omniva_height'])) {
			$data['error_omniva_height'] = $this->error['omniva_height'];
		} else {
			$data['error_omniva_height'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/omniva', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		$data['update'] = $this->url->link('shipping/omniva/update_omniva', 'token=' . $this->session->data['token'], 'SSL');

		$data['action'] = $this->url->link('shipping/omniva', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		$tab_zones[0] = array(
			'code' => 'lv',
			'title' => $this->language->get('tab_latvia')
		);

		$tab_zones[1] = array(
			'code' => 'lt',
			'title' => $this->language->get('tab_lithuania')
		);

		$tab_zones[2] = array(
			'code' => 'ee',
			'title' => $this->language->get('tab_estonia')
		);

		$data['tab_zones'] = $tab_zones;

		$omniva_addresses = $this->model_setting_setting->getSetting('omniva_addresses');

		foreach ($tab_zones as $tab_zone) {
			if (isset($this->request->post['omniva_' . $tab_zone['code'] . '_geo_zone_id'])) {
				$data['omniva_' . $tab_zone['code'] . '_geo_zone_id'] = $this->request->post['omniva_' . $tab_zone['code'] . '_geo_zone_id'];
			} else {
				$data['omniva_' . $tab_zone['code'] . '_geo_zone_id'] = $this->config->get('omniva_' . $tab_zone['code'] . '_geo_zone_id');
			}

			if (isset($this->request->post['omniva_' . $tab_zone['code'] . '_tax_class_id'])) {
				$data['omniva_' . $tab_zone['code'] . '_tax_class_id'] = $this->request->post['omniva_' . $tab_zone['code'] . '_tax_class_id'];
			} else {
				$data['omniva_' . $tab_zone['code'] . '_tax_class_id'] = $this->config->get('omniva_' . $tab_zone['code'] . '_tax_class_id');
			}

			if (isset($this->request->post['omniva_' . $tab_zone['code'] . '_cost'])) {
				$data['omniva_' . $tab_zone['code'] . '_cost'] = $this->request->post['omniva_' . $tab_zone['code'] . '_cost'];
			} else {
				$data['omniva_' . $tab_zone['code'] . '_cost'] = $this->config->get('omniva_' . $tab_zone['code'] . '_cost');
			}

			if (isset($this->request->post['omniva_' . $tab_zone['code'] . '_cost_option'])) {
				$data['omniva_' . $tab_zone['code'] . '_cost_option'] = $this->request->post['omniva_' . $tab_zone['code'] . '_cost_option'];
			} else if ($this->config->get('omniva_' . $tab_zone['code'] . '_cost_option')) {
				$data['omniva_' . $tab_zone['code'] . '_cost_option'] = $this->config->get('omniva_' . $tab_zone['code'] . '_cost_option');
			} else {
				$data['omniva_' . $tab_zone['code'] . '_cost_option'] = 'all_cart';
			}

			if (isset($this->request->post['omniva_' . $tab_zone['code'] . '_status'])) {
				$data['omniva_' . $tab_zone['code'] . '_status'] = $this->request->post['omniva_' . $tab_zone['code'] . '_status'];
			} else {
				$data['omniva_' . $tab_zone['code'] . '_status'] = $this->config->get('omniva_' . $tab_zone['code'] . '_status');
			}

			$data['omniva_' . $tab_zone['code'] . '_addresses'] = '';
			
			if (($omniva_addresses)&&(isset($omniva_addresses['omniva_addresses_' . $tab_zone['code'] . '_addresses']))) {
				foreach ($omniva_addresses['omniva_addresses_' . $tab_zone['code'] . '_addresses'] as $k => $v) {
                                        $data['omniva_' . $tab_zone['code'] . '_addresses'][$k] = $v;
				}		
				
				usort($data['omniva_' . $tab_zone['code'] . '_addresses'], 'strnatcmp');
			}
		}

                if (isset($this->request->post['omniva_weight'])) {
                        $data['omniva_weight'] = $this->request->post['omniva_weight'];
                } else if ($this->config->get('omniva_weight')) {
                        $data['omniva_weight'] = $this->config->get('omniva_weight');
                } else {
                        $data['omniva_weight'] = '30';
                }

                if (isset($this->request->post['omniva_length'])) {
                        $data['omniva_length'] = $this->request->post['omniva_length'];
                } else if ($this->config->get('omniva_length')) {
                        $data['omniva_length'] = $this->config->get('omniva_length');
                } else {
                        $data['omniva_length'] = '38';
                }

                if (isset($this->request->post['omniva_width'])) {
                        $data['omniva_width'] = $this->request->post['omniva_width'];
                } else if ($this->config->get('omniva_width')) {
                        $data['omniva_width'] = $this->config->get('omniva_width');
                } else {
                        $data['omniva_width'] = '41';
                }

                if (isset($this->request->post['omniva_height'])) {
                        $data['omniva_height'] = $this->request->post['omniva_height'];
                } else if ($this->config->get('omniva_height')) {
                        $data['omniva_height'] = $this->config->get('omniva_height');
                } else {
                        $data['omniva_height'] = '64';
                }

                if (isset($this->request->post['omniva_import_url'])) {
                        $data['omniva_import_url'] = $this->request->post['omniva_import_url'];
                } else if ($this->config->get('omniva_import_url')) {
                        $data['omniva_import_url'] = $this->config->get('omniva_import_url');
                } else {
                        $data['omniva_import_url'] = 'http://www.omniva.ee/locations.csv';
                }

		if (isset($this->request->post['omniva_status'])) {
			$data['omniva_status'] = $this->request->post['omniva_status'];
		} else {
			$data['omniva_status'] = $this->config->get('omniva_status');
		}
			
		if (isset($this->request->post['omniva_sort_order'])) {
			$data['omniva_sort_order'] = $this->request->post['omniva_sort_order'];
		} else {
			$data['omniva_sort_order'] = $this->config->get('omniva_sort_order');
		}				

		$this->load->model('localisation/tax_class');
			
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
			
		$this->load->model('localisation/length_class');

		$length_class_data = $this->model_localisation_length_class->getLengthClass($this->config->get('config_length_class_id'));
			
		$data['default_length_class'] = $length_class_data['unit'];

		$this->load->model('localisation/weight_class');

		$weight_class_data = $this->model_localisation_weight_class->getWeightClass($this->config->get('config_weight_class_id'));
			
		$data['default_weight_class'] = $weight_class_data['unit'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		//Send the output.
		$this->response->setOutput($this->load->view('shipping/omniva.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/omniva')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['omniva_weight']) {
			$this->error['omniva_weight'] = $this->language->get('error_omniva_weight');
		}

		if (!$this->request->post['omniva_length']) {
			$this->error['omniva_length'] = $this->language->get('error_omniva_length');
		}
		
		if (!$this->request->post['omniva_width']) {
			$this->error['omniva_width'] = $this->language->get('error_omniva_width');
		}

		if (!$this->request->post['omniva_height']) {
			$this->error['omniva_height'] = $this->language->get('error_omniva_height');
		}

		return (!$this->error);
	}

	private function update_omniva($ndata) {			
		$this->load->model('setting/setting');
		
		$data_addresses = array();

                if ((isset($ndata['omniva_import_url']))&&(!empty($ndata['omniva_import_url']))) {
			$ps_zones = array();
			$ps_zones = $this->runcsv($ndata['omniva_import_url'], 'utf-8');

			$new_ps_addresses = array();
			$i = 1;
                        foreach ($ps_zones as $ps_zone_k => $ps_zone_v) {
				if ($ps_zone_v['omniva_country'] == 'EE' && $ps_zone_v['omniva_type'] == '0') {
                    $new_ps_addresses['omniva_address_'.$ps_zone_v['omniva_index']] = 'Omniva - ' . $ps_zone_v['omniva_address'] . ', ' . $ps_zone_v['omniva_name'] . ', ' . $ps_zone_v['omniva_address2'] . ' ' . $ps_zone_v['omniva_address3'];
					$i++;
				}
                        }
			$data_addresses['omniva_addresses_ee_addresses'] = $new_ps_addresses;
			
			
			$new_ps_addresses = array();
			$i = 1;
                        foreach ($ps_zones as $ps_zone_k => $ps_zone_v) {
				if ($ps_zone_v['omniva_country'] == 'LV') {
                    $new_ps_addresses['omniva_address_'.$ps_zone_v['omniva_index']] = 'Omniva - ' . $ps_zone_v['omniva_city'] . ', ' . $ps_zone_v['omniva_name'] . ', ' . $ps_zone_v['omniva_address'];
					$i++;
				}
                        }
			$data_addresses['omniva_addresses_lv_addresses'] = $new_ps_addresses;

                        
			$new_ps_addresses = array();
			$i = 1;
                        foreach ($ps_zones as $ps_zone_k => $ps_zone_v) {
				if ($ps_zone_v['omniva_country'] == 'LT') {
                    $new_ps_addresses['omniva_address_'.$ps_zone_v['omniva_index']] = 'Omniva - ' . $ps_zone_v['omniva_city'] . ', ' . $ps_zone_v['omniva_name'] . ', ' . $ps_zone_v['omniva_address'];
					$i++;
				}
                        }
			$data_addresses['omniva_addresses_lt_addresses'] = $new_ps_addresses;			
                }
		
                $this->model_setting_setting->editSetting('omniva_addresses', $data_addresses);

//                $this->session->data['success'] = $this->language->get('text_success');
	}
	
        private function runcsv($value, $encoding) {
//                header('Content-Type: text/html; charset=utf-8');
                header('Content-Type: text/html; charset=$encoding');
//                if(!setlocale(LC_ALL, 'ru_RU.utf8')) setlocale(LC_ALL, 'en_US.utf8');
//                if(setlocale(LC_ALL, 0) == 'C') die('Error locale (ru_RU.utf8, en_US.utf8)');

                $row_d = array();
                $i = 0;

                if (($handle = fopen($value, "r")) !== FALSE) {		
                        while (($fdata = fgetcsv($handle, 0, ";")) !== FALSE) {
                                $row_d[$i] = array(
                                        'omniva_name' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[1]), 'utf-8', 'ISO-8859-13') : trim($fdata[1]),
                                        'omniva_type' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[2]), 'utf-8', 'ISO-8859-13') : trim($fdata[2]),
                                        'omniva_city' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[4]), 'utf-8', 'ISO-8859-13') : trim($fdata[4]),
                                        'omniva_address' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[5]), 'utf-8', 'ISO-8859-13') : trim($fdata[5]),
                                        'omniva_address2' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[8]), 'utf-8', 'ISO-8859-13') : trim($fdata[8]),
                                        'omniva_address3' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[10]), 'utf-8', 'ISO-8859-13') : trim($fdata[10]),
                                        'omniva_index' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[0]), 'utf-8', 'ISO-8859-13') : trim($fdata[0]),
                                        'omniva_country' => ($encoding != 'utf-8') ? mb_convert_encoding(trim($fdata[3]), 'utf-8', 'ISO-8859-13') : trim($fdata[3])
                                );
                                $i++;
                        }
                        fclose($handle);
                }       

                unset($row_d[0]);
//                unset($row_d[1]);
//                unset($row_d[2]);
                return $row_d;
        }

	public function uninstall() {
		if (!$this->user->hasPermission('modify', 'extension/shipping')) {
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$dirCache = DIR_CATALOG.'../vqmod/vqcache/';
			foreach(glob($dirCache.'*.*') as $fileCache){ unlink($fileCache); }

			$this->load->model('setting/setting');
				
			$this->model_setting_setting->deleteSetting('omniva_addresses');

			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
        
}