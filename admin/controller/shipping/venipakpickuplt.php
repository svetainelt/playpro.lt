<?php
//admin/controller/shipping 
class ControllerShippingVenipakpickuplt extends Controller {
	private $error = array(); 
	
	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "xx_venipakpickup_offices` (
			  `ID` int(11) NOT NULL,
			  `TYPE` int(11) NOT NULL,
			  `NAME` varchar(250) NOT NULL,
			  `REGION` varchar(250) DEFAULT NULL,
			  `CITY` varchar(100) DEFAULT NULL,
			  `COUNTRY` varchar(10) DEFAULT NULL,
			  `STATUS` varchar(2) DEFAULT NULL,
			  `SORT` int(11) DEFAULT NULL,
			  KEY `ID` (`ID`,`TYPE`,`NAME`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci
		");
		$this->refresh_data();
	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "xx_venipakpickup_offices`");
	}
	

	public function refresh_data() {
		ini_set("allow_url_fopen", 1);
		
		$json = file_get_contents('https://go.venipak.lt/ws/get_pickup_points');
		$obj = json_decode($json);
		//echo $obj->access_token;
		$this->db->query("DELETE FROM `" . DB_PREFIX . "xx_venipakpickup_offices` WHERE TYPE = 57");
		foreach ($obj as $key => $jsons) { // This will search in the 2 jsons
			$sqlString_col = "";
			$sqlString_val = "";
			$sqlString_name = "";
			$sqlString_address = "";
			$sqlString_phone = "";
			$insert_record = 'N';
			$sqlString = "INSERT INTO `" . DB_PREFIX . "xx_venipakpickup_offices`("; //ID,NAME,REGION,CITY,SORT,TYPE) VALUES (
			foreach($jsons as $key => $value) {
				
				if($key == "id") {
					$sqlString_col .= "ID,";
					$sqlString_val .= "'".$value."',";
				}
				if($key == "cod_enabled") {
					$sqlString_col .= "cod_enabled,";
					$sqlString_val .= "'".$value."',";
				}
				if($key == "name") {
					//$sqlString_col = "NAME,";
					$sqlString_name = str_replace("Venipak Pickup, ","",$value);
				}
				if($key == "city") {
					$sqlString_col .= "CITY,REGION,";
					$sqlString_val .= "'".$value."','".$value."',";
					$sqlString_city = ", ".$value."";
				}
				if($key == "country") {
					$sqlString_col .= "TYPE,COUNTRY,";
					if($value == "EE") $sqlString_val .= "'55','".$value."',";
					if($value == "LV") $sqlString_val .= "'56','".$value."',";
					if($value == "LT") $sqlString_val .= "'57','".$value."',";
				}
				if($key == "address") {
					$sqlString_address = "".$value."";
				}
				if($key == "contact_t") {
					$sqlString_phone = ", Phone Number: ".$value."";
				}
				
				if($key == "terminal") {
					$sqlString_col .= "SORT,";
					$sqlString_val .= "'".$value."',";
				}
				
				if($key == "pick_up_enabled") {
					$sqlString_col .= "STATUS,";
					if($value == "1") {
						$sqlString_val .= "'Y',";
					} else {
						$sqlString_val .= "'N',";
					}
					/*if($value == "1") {
						$insert_record = 'Y';
					}*/
				}
				

			}
			
				$sqlString .= $sqlString_col."NAME) VALUES (";
				$sqlString .= $sqlString_val."'".$sqlString_name." (".$sqlString_address.$sqlString_city.$sqlString_phone.")'";
				$sqlString .= ")";
				//if($insert_record == "Y") 
				$this->db->query($sqlString);
		}

	}	

	
	public function populate_data() {
		$this->refresh_data();
        $this->index();
	}
	
	public function index() {   
		$this->language->load('shipping/venipakpickuplt');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('venipakpickuplt', $this->request->post);
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}
				
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_show_all_yes'] = $this->language->get('text_show_all_yes');
		$data['text_show_all_no'] = $this->language->get('text_show_all_no');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');
		
		$data['entry_cost'] = $this->language->get('entry_cost');
		$data['entry_freelimit'] = $this->language->get('entry_freelimit');
		$data['entry_show_all'] = $this->language->get('entry_show_all');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['entry_weight_from'] = $this->language->get('entry_weight_from');
		$data['entry_weight_from_tooltip'] = $this->language->get('entry_weight_from_tooltip');
		$data['entry_weight_to'] = $this->language->get('entry_weight_to');
		$data['entry_weight_to_tooltip'] = $this->language->get('entry_weight_to_tooltip');
		$data['entry_first'] = $this->language->get('entry_first');
		$data['entry_next'] = $this->language->get('entry_next');
		
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_add'] = $this->language->get('button_add');
		
		$data['refresh_data_href'] = $this->url->link('shipping/venipakpickuplt/populate_data', 'token=' . $this->session->data['token'], true);
		
		$data['button_refresh'] = $this->language->get('button_refresh');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token']. '&type=shipping', true)
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/venipakpickuplt', 'token=' . $this->session->data['token'], true)
   		);
		
		$data['action'] = $this->url->link('shipping/venipakpickuplt', 'token=' . $this->session->data['token'], true);
		
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token']. '&type=shipping', true);
		
		if (isset($this->request->post['venipakpickuplt_freelimit'])) {
			$data['venipakpickuplt_freelimit'] = $this->request->post['venipakpickuplt_freelimit'];
		} else {
			$data['venipakpickuplt_freelimit'] = $this->config->get('venipakpickuplt_freelimit');
		}
		
		if (isset($this->request->post['venipakpickuplt_show_all'])) {
			$data['venipakpickuplt_show_all'] = $this->request->post['venipakpickuplt_show_all'];
		} else {
			$data['venipakpickuplt_show_all'] = $this->config->get('venipakpickuplt_show_all');
		}
		
		if (isset($this->request->post['venipakpickuplt_weight_data'])) {
			$data['venipakpickuplt_weight_data'] = $this->request->post['venipakpickuplt_weight_data'];
		} else {
			$data['venipakpickuplt_weight_data'] = $this->config->get('venipakpickuplt_weight_data');
		}

		if (isset($this->request->post['venipakpickuplt_tax_class_id'])) {
			$data['venipakpickuplt_tax_class_id'] = $this->request->post['venipakpickuplt_tax_class_id'];
		} else {
			$data['venipakpickuplt_tax_class_id'] = $this->config->get('venipakpickuplt_tax_class_id');
		}
		
		$this->load->model('localisation/tax_class');
		
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['venipakpickuplt_geo_zone_id'])) {
			$data['venipakpickuplt_geo_zone_id'] = $this->request->post['venipakpickuplt_geo_zone_id'];
		} else {
			$data['venipakpickuplt_geo_zone_id'] = $this->config->get('venipakpickuplt_geo_zone_id');
		}
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['venipakpickuplt_status'])) {
			$data['venipakpickuplt_status'] = $this->request->post['venipakpickuplt_status'];
		} else {
			$data['venipakpickuplt_status'] = $this->config->get('venipakpickuplt_status');
		}
		
		if (isset($this->request->post['venipakpickuplt_sort_order'])) {
			$data['venipakpickuplt_sort_order'] = $this->request->post['venipakpickuplt_sort_order'];
		} else {
			$data['venipakpickuplt_sort_order'] = $this->config->get('venipakpickuplt_sort_order');
		}				

		$sql = 'SELECT ID, REGION, NAME, STATUS FROM '.DB_PREFIX.'xx_venipakpickup_offices WHERE TYPE = 57 ORDER BY REGION, CITY, NAME, SORT' ;
		$query = $this->db->query($sql);
		foreach($query->rows as $result){
			$data['venipakpickuplt_places'][$result['ID']] = array(
				'dickup_place'  => $result['NAME'],
				'dickup_region'   => $result['REGION'],
				'dickup_status'   => $result['STATUS']
			);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
				
		$this->response->setOutput($this->load->view('shipping/venipakpickuplt.tpl', $data));
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/venipakpickuplt')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	

}
?>