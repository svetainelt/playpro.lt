<?php
class ControllerShippingUniversalWeight extends Controller { 
	private $error = array();
	
	public function index() {
		/* Loaders */
		$this->load->model('setting/setting');
		$this->load->language('shipping/universal_weight');

		/* On POST submit */
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('universal_weight', $this->request->post);	
			$this->session->data['success'] = $this->language->get('text_success');				
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
                $this->document->setTitle($this->language->get('heading_title'));
                
		/* Loaders */
		$this->load->model('localisation/geo_zone');
		$this->load->model('localisation/tax_class');
		$this->load->model('localisation/weight_class');
		$this->load->model('localisation/currency');
		$this->load->model('localisation/language');
		
		/* Breadcrumbs */
  		$data['breadcrumbs'] = array();
   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);
   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
   		);
   		$data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/universal_weight', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		/* Error handling */
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		/* Data pieces */
		$data['heading_title'] = $this->language->get('heading_title');
                $data['text_edit'] = $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_rate'] = $this->language->get('entry_rate');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_flat_cost'] = $this->language->get('entry_flat_cost');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_min_price'] = $this->language->get('entry_min_price');
		$data['entry_max_price'] = $this->language->get('entry_max_price');
		$data['entry_min_weight'] = $this->language->get('entry_min_weight');
		$data['entry_max_weight'] = $this->language->get('entry_max_weight');
		$data['entry_action'] = $this->language->get('entry_action');
		$data['entry_rest_of_world_price'] = $this->language->get('entry_rest_of_world_price');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
                $data['button_remove'] = $this->language->get('button_remove');
		$data['button_rule_add'] = $this->language->get('button_rule_add');
                $data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['action'] = $this->url->link('shipping/universal_weight', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 
                
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		$data['weight_class'] = $this->model_localisation_weight_class->getWeightClass($this->config->get('config_weight_class_id'));
		$data['currency'] = $this->model_localisation_currency->getCurrencyByCode($this->config->get('config_currency'));
                $data['languages'] = $this->model_localisation_language->getLanguages();

		$config_settings = array(
			'universal_weight_status' => false,
			'universal_weight_sort_order' => false,
			'universal_weight_tax_class_id' => false,
			'universal_weight_rules' => array(),
		);

		foreach ($config_settings as $setting => $default_value) {
			if (isset($this->request->post[$setting])) {
				$data[$setting] = $this->request->post[$setting];
			} elseif ($this->config->get($setting)) {
				$data[$setting] = $this->config->get($setting);
			} else {
				$data[$setting] = $default_value;
			}
		}

		/* Template & output */
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/universal_weight.tpl', $data));
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/universal_weight')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;	
	}
}