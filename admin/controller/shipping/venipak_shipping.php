<?php
class ControllerShippingVenipakShipping extends Controller {
	private $error = array();

	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "venipak_shipping` (
				`venipak_id` INT NOT NULL AUTO_INCREMENT ,
				`order_id` INT NOT NULL ,
				`venipak_shipping_pickup_point` TEXT NOT NULL ,
				`status` VARCHAR(10) NOT NULL ,
				`tracking` VARCHAR(14) NOT NULL ,
				`manifest` VARCHAR(14) NOT NULL ,
				`packs` TEXT NOT NULL,
				`error_message` TEXT NOT NULL,
				PRIMARY KEY (`venipak_id`),
				UNIQUE (`order_id`)) 
				CHARACTER SET utf8 COLLATE utf8_general_ci;
		");
	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "venipak_shipping`");
	}

	public function index() {
		$this->load->language('shipping/venipak_shipping');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('venipak_shipping', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'] . '&type=shipping', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('shipping/venipak_shipping', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('shipping/venipak_shipping', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'] . '&type=shipping', true);

		$data['venipak_shipping_status'] = $this->getParam('venipak_shipping_status');
		$data['venipak_shipping_sort_order'] = $this->getParam('venipak_shipping_sort_order');
		$data['venipak_shipping_method_title'] = $this->getParam('venipak_shipping_method_title') ?: 'Venipak';
		$data['venipak_shipping_method_title_courier'] = $this->getParam('venipak_shipping_method_title_courier') ?: 'Courier';
		$data['venipak_shipping_method_title_pickup'] = $this->getParam('venipak_shipping_method_title_pickup') ?: 'Pickup';
		$data['venipak_shipping_client_id'] = $this->getParam('venipak_shipping_client_id');
		$data['venipak_shipping_client_username'] = $this->getParam('venipak_shipping_client_username');
		$data['venipak_shipping_client_password'] = $this->getParam('venipak_shipping_client_password');
		$data['venipak_shipping_test'] = $this->getParam('venipak_shipping_test');
		$data['venipak_shipping_cost_courier'] = $this->getParam('venipak_shipping_cost_courier');
		$data['venipak_shipping_cost_pickup'] = $this->getParam('venipak_shipping_cost_pickup');
		$data['venipak_shipping_free'] = $this->getParam('venipak_shipping_free');
		$data['venipak_shipping_geo_zone_id'] = $this->getParam('venipak_shipping_geo_zone_id');

		$this->load->model('localisation/geo_zone');
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		$data['venipak_shipping_sender_name'] = $this->getParam('venipak_shipping_sender_name');
		$data['venipak_shipping_sender_company_code'] = $this->getParam('venipak_shipping_sender_company_code');
		$data['venipak_shipping_sender_country'] = $this->getParam('venipak_shipping_sender_country');
		$data['venipak_shipping_sender_city'] = $this->getParam('venipak_shipping_sender_city');
		$data['venipak_shipping_sender_address'] = $this->getParam('venipak_shipping_sender_address');
		$data['venipak_shipping_sender_postcode'] = $this->getParam('venipak_shipping_sender_postcode');
		$data['venipak_shipping_sender_contact_person'] = $this->getParam('venipak_shipping_sender_contact_person');
		$data['venipak_shipping_sender_contact_tel'] = $this->getParam('venipak_shipping_sender_contact_tel');
		$data['venipak_shipping_sender_contact_email'] = $this->getParam('venipak_shipping_sender_contact_email');

		$data['venipak_shipping_products_per_pack'] = $this->getParam('venipak_shipping_products_per_pack') ?: 1;
		$data['venipak_shipping_initial_tracking_number'] = $this->getParam('venipak_shipping_initial_tracking_number') ?: 1000001;
		$data['venipak_shipping_is_map_enabled'] = $this->getParam('venipak_shipping_is_map_enabled');
		$data['venipak_shipping_google_api_key'] = $this->getParam('venipak_shipping_google_api_key');
		$data['venipak_shipping_is_clusters_enabled'] = $this->getParam('venipak_shipping_is_clusters_enabled');
		$data['venipak_shipping_disable_pickup'] = $this->getParam('venipak_shipping_disable_pickup');
		$data['venipak_shipping_disable_locker'] = $this->getParam('venipak_shipping_disable_locker');
		$data['venipak_shipping_disable_courier'] = $this->getParam('venipak_shipping_disable_courier');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_sender_settings'] = $this->language->get('text_sender_settings');
		$data['text_venipak_connection_settings'] = $this->language->get('text_venipak_connection_settings');
		$data['text_venipak_shipping_client_id'] = $this->language->get('text_venipak_shipping_client_id');
		$data['text_venipak_shipping_client_username'] = $this->language->get('text_venipak_shipping_client_username');
		$data['text_venipak_shipping_client_password'] = $this->language->get('text_venipak_shipping_client_password');
		$data['text_venipak_shipping_test'] = $this->language->get('text_venipak_shipping_test');
		$data['text_shipping_method_settings'] = $this->language->get('text_shipping_method_settings');
		$data['text_venipak_shipping_method_title'] = $this->language->get('text_venipak_shipping_method_title');
		$data['text_venipak_shipping_method_title_courier'] = $this->language->get('text_venipak_shipping_method_title_courier');
		$data['text_venipak_shipping_method_title_pickup'] = $this->language->get('text_venipak_shipping_method_title_pickup');
		$data['text_venipak_shipping_sort_order'] = $this->language->get('text_venipak_shipping_sort_order');
		$data['text_venipak_shipping_cost_courier'] = $this->language->get('text_venipak_shipping_cost_courier');
		$data['text_venipak_shipping_cost_pickup'] = $this->language->get('text_venipak_shipping_cost_pickup');
		$data['text_venipak_shipping_free'] = $this->language->get('text_venipak_shipping_free');
		$data['text_venipak_shipping_products_per_pack'] = $this->language->get('text_venipak_shipping_products_per_pack');
		$data['text_venipak_shipping_initial_tracking_number'] = $this->language->get('text_venipak_shipping_initial_tracking_number');
		$data['text_venipak_shipping_is_map_enabled'] = $this->language->get('text_venipak_shipping_is_map_enabled');
		$data['text_venipak_shipping_google_api_key'] = $this->language->get('text_venipak_shipping_google_api_key');
		$data['text_venipak_shipping_is_clusters_enabled'] = $this->language->get('text_venipak_shipping_is_clusters_enabled');
		$data['text_venipak_shipping_geo_zone_id'] = $this->language->get('text_venipak_shipping_geo_zone_id');
		$data['text_venipak_shipping_disable_courier'] = $this->language->get('text_venipak_shipping_disable_courier');
		$data['text_venipak_shipping_disable_pickup'] = $this->language->get('text_venipak_shipping_disable_pickup');
		$data['text_venipak_shipping_disable_locker'] = $this->language->get('text_venipak_shipping_disable_locker');
		$data['text_venipak_shipping_sender_name'] = $this->language->get('text_venipak_shipping_sender_name');
		$data['text_venipak_shipping_sender_company_code'] = $this->language->get('text_venipak_shipping_sender_company_code');
		$data['text_venipak_shipping_sender_country'] = $this->language->get('text_venipak_shipping_sender_country');
		$data['text_venipak_shipping_sender_city'] = $this->language->get('text_venipak_shipping_sender_city');
		$data['text_venipak_shipping_sender_address'] = $this->language->get('text_venipak_shipping_sender_address');
		$data['text_venipak_shipping_sender_postcode'] = $this->language->get('text_venipak_shipping_sender_postcode');
		$data['text_venipak_shipping_sender_contact_person'] = $this->language->get('text_venipak_shipping_sender_contact_person');
		$data['text_venipak_shipping_sender_contact_tel'] = $this->language->get('text_venipak_shipping_sender_contact_tel');
		$data['text_venipak_shipping_sender_contact_email'] = $this->language->get('text_venipak_shipping_sender_contact_email');
		

		$this->response->setOutput($this->load->view('shipping/venipak_shipping.tpl', $data));
	}

	protected function getParam($key) {
		return isset($this->request->post[$key])
			? $this->request->post[$key]
			: $this->config->get($key);
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/venipak_shipping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}