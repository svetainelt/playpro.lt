<?php

class ControllerFeedFacebookFeedProducts extends Controller
{
    private $error = array();

    public function index()
    {
      $data =  $this->load->language('feed/facebookfeedproducts');

        $this->document->setTitle(strip_tags($this->language->get('heading_title')));
        $this->load->model('setting/setting');
        $this->load->model('catalog/option');
        $this->load->model('feed/facebookfeedproducts');
        $data['success'] = '';

        if (
            ($this->request->server['REQUEST_METHOD'] == 'POST')
            && $this->validate()
            && !isset($this->request->post['form_feed_ud'])
            && !isset($this->request->post['form_ub'])
        ) {
            $this->model_setting_setting->editSetting('facebookfeedproducts', $this->request->post);

            //  $this->session->data['success'] = $this->language->get('text_success');
            $data['success'] = $this->language->get('text_success');

            // $this->response->redirect($this->url->link('extension/feed', 'token=' . $this->session->data['token'].'&type=feed', 'SSL'));
        } elseif (isset($this->request->post['google_product_category_url']) && $this->request->post['google_product_category_url'] != '') {
            $data1 = array();
            $rule = strpos($this->request->post['google_product_category_url'], 'https://www.google.com');
            if ($rule !== false) {
                $file = file($this->request->post['google_product_category_url']);
                $this->model_extension_feed_facebookfeedproducts->clearGoogleCategorys();
                foreach ($file as $f) {
                    if (!strstr($f, '#')) {
                        $buffer = explode('-', $f, 2);
                        if (is_numeric(trim($buffer[0]))) {
                            $google_category_name = trim($buffer[1]);
                            $google_category_id = trim($buffer[0]);
                            $google_category_parent = 0;
                            if (count($buffer1 = explode('>', $buffer[1])) > 1) {
                                //$google_category_name = trim($buffer1[count($buffer1)-1]);
                                $parent = $this->model_extension_feed_facebookfeedproducts->getGoogleCategoryByName(trim($buffer1[0]));
                                $google_category_parent = $parent["google_category_id"];
                            }
                            $data1 = array('google_category_id' => $google_category_id, 'google_category_name' => $google_category_name, 'google_category_parent' => $google_category_parent);
                            $this->model_extension_feed_facebookfeedproducts->updateGoogleCategorys($data1);
                        } else {
                            $this->error['warning'] = 'File not found';
                        }
                    }
                }
            } else {
                $this->error['warning'] = 'File not found';
            }
        } elseif (isset($this->request->post['facebook_product_category_url']) && $this->request->post['facebook_product_category_url'] != '') {
            $rule = strpos($this->request->post['facebook_product_category_url'], 'https://www.facebook.com');
            if ($rule !== false) {
                $this->model_extension_feed_facebookfeedproducts->clearFacebookCategorys();

                $opts = [
                    "http" => [
                        "method" => "GET",
                        'header' => "Accept-language: en\r\n" .
                            "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                            "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad
                    ]
                ];
                $context = stream_context_create($opts);

                if ($file = explode("\n", file_get_contents(trim($this->request->post['facebook_product_category_url']), false, $context))) {
                    $rule = iconv_strlen($file[0]);
                    if ($rule < 30) {
                        foreach ($file as $key => $f) {
                            if (!$key) continue; //skip first
                            list($id, $category) = explode(',', $f, 2);
                            if ((strlen($category) > 1) && ($category[0] == '"') && ($category[strlen($category) - 1] == '"')) {
                                //unescaping
                                $category = preg_replace_callback('/\\\\\\\\|\\\\"/', function ($m) {
                                    return $m[0] == '\\"' ? '"' : '\\';
                                }, substr($category, 1, strlen($category) - 2));
                            }
                            $category = explode('>', $category);
                            if ((count($category)) !== 1) continue; // if not valid root category - continue
                            $name = trim($category[0]);
                            $this->model_extension_feed_facebookfeedproducts->setFacebookParent($id, $name);
                        }
                        foreach ($file as $key => $f) {
                            if (!$key) continue; //skip first
                            list($id, $category) = explode(',', $f, 2);
                            if ((strlen($category) > 1) && ($category[0] == '"') && ($category[strlen($category) - 1] == '"')) {
                                //unescaping
                                $category = preg_replace_callback('/\\\\\\\\|\\\\"/', function ($m) {
                                    return $m[0] == '\\"' ? '"' : '\\';
                                }, substr($category, 1, strlen($category) - 2));
                            }
                            $name = $category;
                            $category = explode('>', $category);
                            if ((count($category)) < 2) continue; // if not valid category - continue
                            $root_name = trim($category[0]);
                            /*
                                                if (isset($category[2])) {
                                                    $name = $root_name . ' > ' . $category[1] . ' > ' . $category[2];
                                                } else {
                                                    $name = $root_name . ' > ' . $category[1];
                                                }
                            */
                            $this->model_extension_feed_facebookfeedproducts->setFacebookCategory($id, $name, $root_name);
                        }
                    }else{
                        $this->error['warning'] = 'File not found';
                    }
                }
            } else {
                $this->error['warning'] = 'File not found';
            }
        }

        $productSettings = $this->model_feed_facebookfeedproducts->getProdSettings();
        foreach ($productSettings as $productSetting) {
            $data[$productSetting['key']] = $productSetting['value'];
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_feed'),
            'href' => $this->url->link('extension/feed', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('feed/facebookfeedproducts', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('feed/facebookfeedproducts', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/feed', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL');
        $data['link'] = str_replace('admin/', '', $this->url->link('feed/facebookfeedproducts/linkGeneration','','SSL'));
        $data['data_feed'] = str_replace('admin/', '', $this->url->link('feed/facebookfeedproducts/linkDownload','','SSL'));
        $data['cron_link_generation'] = str_replace('admin/', '', $this->url->link('feed/facebookfeedproducts/saveFile','','SSL'));
        $data['link_file'] = HTTPS_CATALOG.'facebook_feed_products.xml';

        if (isset($this->request->post['facebookfeedproducts_status'])) {
            $data['facebookfeedproducts_status'] = $this->request->post['facebookfeedproducts_status'] ? $this->request->post['facebookfeedproducts_status'] : '';
            $data['facebookfeedproducts_currency'] = $this->request->post['facebookfeedproducts_currency'] ? $this->request->post['facebookfeedproducts_currency'] : '';
            $data['facebookfeedproducts_language'] = $this->request->post['facebookfeedproducts_language'] ? $this->request->post['facebookfeedproducts_language'] : '';
            $data['facebookfeedproducts_size'] = isset($this->request->post['facebookfeedproducts_size']) ? $this->request->post['facebookfeedproducts_size'] : '';
            $data['facebookfeedproducts_color'] = isset($this->request->post['facebookfeedproducts_color']) ? $this->request->post['facebookfeedproducts_color'] : '';
            $data['facebookfeedproducts_pattern'] = isset($this->request->post['facebookfeedproducts_pattern']) ? $this->request->post['facebookfeedproducts_pattern'] : '';
            $data['facebookfeedproducts_material'] = isset($this->request->post['facebookfeedproducts_material']) ? $this->request->post['facebookfeedproducts_material'] : '';
            $data['feed_facebookfeedproducts_customer_grup'] = $this->request->post['feed_facebookfeedproducts_customer_grup'] ? $this->request->post['feed_facebookfeedproducts_customer_grup'] : '';
        } else {
            $data['facebookfeedproducts_status'] = $this->config->get('facebookfeedproducts_status');
            $data['facebookfeedproducts_currency'] = $this->config->get('facebookfeedproducts_currency');
            $data['facebookfeedproducts_language'] = $this->config->get('facebookfeedproducts_language');
            $data['facebookfeedproducts_size'] = $this->config->get('facebookfeedproducts_size');
            $data['facebookfeedproducts_color'] = $this->config->get('facebookfeedproducts_color');
            $data['facebookfeedproducts_pattern'] = $this->config->get('facebookfeedproducts_pattern');
            $data['facebookfeedproducts_material'] = $this->config->get('facebookfeedproducts_material');
            $data['feed_facebookfeedproducts_customer_grup'] = $this->config->get('feed_facebookfeedproducts_customer_grup');
        }
        if (isset($this->request->post['facebookfeedproducts_image_size'])) {
            $data['facebookfeedproducts_image_size'] = $this->request->post['facebookfeedproducts_image_size'];
        } else {
            $data['facebookfeedproducts_image_size'] = $this->config->get('facebookfeedproducts_image_size') ? $this->config->get('facebookfeedproducts_image_size') : 600;
        }
        if (isset($this->request->post['facebookfeedproducts_google_status'])) {
            $data['facebookfeedproducts_google_status'] = $this->request->post['facebookfeedproducts_google_status'];
        } else {
            $data['facebookfeedproducts_google_status'] = $this->config->get('facebookfeedproducts_google_status') ? $this->config->get('facebookfeedproducts_google_status') : false;
        }
        if (isset($this->request->post['facebookfeedproducts_image_empty'])) {
            $data['facebookfeedproducts_image_empty'] = $this->request->post['facebookfeedproducts_image_empty'];
        } else {
            $data['facebookfeedproducts_image_empty'] = $this->config->get('facebookfeedproducts_image_empty') ? $this->config->get('facebookfeedproducts_image_empty') : 1;
        }
        if (isset($this->request->post['facebookfeedproducts_image_size_off'])) {
            $data['facebookfeedproducts_image_size_off'] = $this->request->post['facebookfeedproducts_image_size_off'];
        } else {
            $data['facebookfeedproducts_image_size_off'] = $this->config->get('facebookfeedproducts_image_size_off') ? $this->config->get('facebookfeedproducts_image_size_off') : 1;
        }
        $data['saveCategory'] = [];
        $saveCategory = $this->model_feed_facebookfeedproducts->getSaveCategory();
        if(!empty($saveCategory)){
            foreach ($saveCategory as $elem){
                $data['saveCategory'][$elem['category_id']] = 1;
            }
        }

        $this->load->model('catalog/category');
        $data1['sort'] = 'name';
        $results = $this->model_feed_facebookfeedproducts->getCategories($data1);

        foreach ($results as $result) {
            $google_category_name = '';
            $google_category_id = '';
            $google_category_id = $this->model_feed_facebookfeedproducts->getGoogleCategorysByCategoryId($result['category_id']);
            if ($google_category_id) {
                $google_category_name = $this->model_feed_facebookfeedproducts->getGoogleCategoryById($google_category_id['google_category_id']);
            }

            $data['categories'][] = array(
                'category_id' => $result['category_id'],
                'google_category_id' => ($google_category_id) ? $google_category_id["google_main_category_id"] : '',
                'google_category_name' => ($google_category_name) ? $google_category_name["google_category_name"] : '',
                'name' => $result['name']
            );

            $facebook_category_name = '';
            $facebook_category_id = '';
            $facebook_category_id = $this->model_feed_facebookfeedproducts->getFacebookCategorysByCategoryId($result['category_id']);
            if ($facebook_category_id) {
                $facebook_category_name = $this->model_feed_facebookfeedproducts->getFacbookCategoryById($facebook_category_id['facebook_category_id']);
            }

            $data['categoriesf'][] = array(
                'category_id' => $result['category_id'],
                'facebook_category_id' => ($facebook_category_id) ? $facebook_category_id["facebook_main_category_id"] : '',
                'facebook_category_name' => ($facebook_category_name) ? $facebook_category_name["facebook_category_name"] : '',
                'name' => $result['name']
            );
        }
        $results = $this->model_feed_facebookfeedproducts->getGoogleMainCategory();

        foreach ($results as $result) {
            $data['googlemaincategorys'][] = array(
                'google_category_id' => $result['google_category_id'],
                'google_category_name' => $result['google_category_name']
            );
        }

        $resultsf = $this->model_feed_facebookfeedproducts->getFacebookMainCategory();

        foreach ($resultsf as $resultf) {
            $data['facebookmaincategorys'][] = array(
                'facebook_category_id' => $resultf['facebook_category_id'],
                'facebook_category_name' => $resultf['facebook_category_name']
            );
        }

        $data_opt = array();
        $opt = $this->model_catalog_option->getOptions($data_opt);

        foreach ($opt as $op) {
            $data['options'][] = array(
                'name' => $op['name']
            );
        }

        $this->load->model('localisation/currency');
        $mydata = array();
        $currencies = $this->model_localisation_currency->getCurrencies($mydata);

        foreach ($currencies as $result) {
            $data['currencies'][] = array(
                'id' => $result['currency_id'],
                'title' => $result['title'],
                'code' => $result['code'],
                'value' => $result['value'],
            );
        }

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages($data);

        foreach ($languages as $result) {
            $data['languages'][] = array(
                'id' => $result['language_id'],
                'name' => $result['name'],
                'code' => $result['code']
            );
        }

        $this->load->model('customer/customer_group');
        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();
        $data['text_edit'] = 'Edit';
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('feed/facebookfeedproducts.tpl', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'feed/facebookfeedproducts')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (isset($this->request->post['facebookfeedproducts_image_size']) && $this->request->post['facebookfeedproducts_image_size'] < 600) {
            $this->error['warning'] = "error size min. 600";
            $this->request->post['facebookfeedproducts_image_size'] = 600;
        }
        return !$this->error;
    }

    public function install()
    {
        $this->load->model('feed/facebookfeedproducts');
        $this->model_feed_facebookfeedproducts->install();
        $file = file('https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt');
        if (empty($file)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            $file = explode(PHP_EOL,$output);
        }
        foreach ($file as $f) {
            if (!strstr($f, '#')) {
                $buffer = explode('-', $f, 2);
                $google_category_name = trim($buffer[1]);
                $google_category_id = trim($buffer[0]);
                $google_category_parent = 0;
                if (count($buffer1 = explode('>', $buffer[1])) > 1) {
                    $parent = $this->model_feed_facebookfeedproducts->getGoogleCategoryByName(trim($buffer1[0]));
                    $google_category_parent = $parent["google_category_id"];
                }

                $data = array('google_category_id' => $google_category_id, 'google_category_name' => $google_category_name, 'google_category_parent' => $google_category_parent);
                $this->model_feed_facebookfeedproducts->updateGoogleCategorys($data);
            }
        }

        $this->model_feed_facebookfeedproducts->clearFacebookCategorys();

        $opts = [
            "http" => [
                "method" => "GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                    "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad
            ]
        ];
        $context = stream_context_create($opts);

        if ($file = explode("\n",file_get_contents('https://www.facebook.com/products/categories/en_US.txt', false, $context))) {
            foreach ($file as $key => $f) {
                if (!$key) continue; //skip first
                list($id, $category) = explode(',', $f, 2);
                if ((strlen($category) > 1) && ($category[0] == '"') && ($category[strlen($category) - 1] == '"')) {
                    //unescaping
                    $category = preg_replace_callback('/\\\\\\\\|\\\\"/', function ($m) {return $m[0] == '\\"' ? '"' : '\\';}, substr($category, 1, strlen($category) - 2));
                }
                $category = explode('>', $category);
                if ((count($category)) !== 1 ) continue; // if not valid root category - continue
                $name = trim($category[0]);
                $this->model_feed_facebookfeedproducts->setFacebookParent($id, $name);
            }
            foreach ($file as $key => $f) {
                if (!$key) continue; //skip first
                list($id, $category) = explode(',', $f, 2);
                if ((strlen($category) > 1) && ($category[0] == '"') && ($category[strlen($category) - 1] == '"')) {
                    //unescaping
                    $category = preg_replace_callback('/\\\\\\\\|\\\\"/', function ($m) {return $m[0] == '\\"' ? '"' : '\\';}, substr($category, 1, strlen($category) - 2));
                }
                $name = $category;
                $category = explode('>', $category);
                if ((count($category)) < 2 ) continue; // if not valid category - continue
                $root_name = trim($category[0]);
/*
                if (isset($category[2])) {
                    $name = $root_name . ' > ' . $category[1] . ' > ' . $category[2];
                } else {
                    $name = $root_name . ' > ' . $category[1];
                }
*/
                $this->model_feed_facebookfeedproducts->setFacebookCategory($id, $name, $root_name);
            }
        }
    }

    public function uninstall()
    {
        $this->load->model('feed/facebookfeedproducts');
        $this->model_feed_facebookfeedproducts->uninstall();
    }

    public function customproductgetlist()
    {
        $this->load->model('feed/facebookfeedproducts');
        $this->load->model('catalog/product');

        $results = $this->model_feed_facebookfeedproducts->getCustomProducts();
        $product_ids = [];

        if (!empty($results)) {
            foreach ($results as $result) {
                $product_ids[] = $result['product_id'];
            }
        }

        $page = $this->request->post['page'];
        $limit = 20;
        $start = $limit * ($page - 1);
        $filters = [
            'start' => (int)$start,
            'limit' => "$limit",
        ];

        $products = $this->model_catalog_product->getProducts($filters);
        $total = $this->model_catalog_product->getTotalProducts();

        $paginationHtml = '';
        $pageCount = (int)($total / $limit);

        $couunter = 0;
        while ($couunter <= $pageCount) {
            $couunter += 1;
            $class = '';
            if ($page == $couunter) {
                $class = 'active';
            }
            $paginationHtml .= "<li class='$class'><a href='#' class='product-list-pagination-href' data-value='$couunter'>$couunter</a></li>";
        }

        $paginationHtml =
            '<div class="form-group" style="border: 0;">
                    <label class="col-sm-3"><ul class="pagination">'.
            $paginationHtml
            .
            '</ul></label>
                    <div class="col-sm-3"></div>
                </div>';
        $products_html = '<br><div class="my-incredible-name"></div>';

        if (!empty($products)) {
            foreach ($products as $product) {
                $checked = '';
                if (in_array((int)$product['product_id'], $product_ids)) {
                    $checked = 'checked="checked"';
                }
                $products_html .=
                    '<div class="form-group" style="border: 0;">
                    <label class="col-sm-3 control-label">' .
                    $product['name'] .
                    ' (model: ' .
                    $product['model'] . ')'.
                    '</label>
                    <div class="col-sm-8">
                        <input
                            class=" list settings-product-list settings-ajax"
                            type="checkbox"
                            name="' . $product['product_id'] . '"
                            value="1"
                            ' . $checked . '
                        >
                        <img src="view/image/facebookfeedproducts/load.gif" class="setting-save-ico setting-save-proccess-ico" style="height: 16px;">
                        <i class="fa fa-check setting-save-ico setting-save-success-ico" style="font-size: 14px;"></i>
                    </div>
                </div>';
            }
        }
        echo json_encode($paginationHtml . $products_html . $paginationHtml);
        die;
    }

    public function updateproductsettings()
    {
        $key = $this->request->post['key'];
        $val = $this->request->post['value'];

        $this->load->model('feed/facebookfeedproducts');
        $result = $this->model_feed_facebookfeedproducts->setProductsSV($key, $val);

        if ($result) {
            echo json_encode(['status' => true]);
        } else {
            echo json_encode(['status' => false]);
        }
    }

    public function savefacebookcat()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'GET') {
            $this->load->model('feed/facebookfeedproducts');
            if (isset($this->request->get["facebook_main_category_id"])) {
                $data = array('category_id' => $this->request->get["category_id"], 'facebook_main_category_id' => $this->request->get['facebook_main_category_id'], 'facebook_category_id' => NULL);
            } else {
                $data = array('category_id' => $this->request->get["category_id"], 'facebook_main_category_id' => NULL, 'facebook_category_id' => $this->request->get['facebook_category_id']);
            }
            $this->model_feed_facebookfeedproducts->saveFacebookCategory($data);
        }
    }

    public function autocompletef()
    {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('feed/facebookfeedproducts');

            $filterName = $this->request->get['filter_name'];
            $data = array(
                'facebook_category_name' => ($filterName != '--select--') ? $filterName : '',
                'facebook_category_id' => $this->request->get['filter_id']
            );

            $json[] = array(
                'facebook_category_id' => 0,
                'facebook_category_name' => '--select--'
            );

            $results = $this->model_feed_facebookfeedproducts->getFacebookCategorys($data);
            foreach ($results as $result) {
                $json[] = array(
                    'facebook_category_id' => $result['facebook_category_id'],
                    'facebook_category_name' => strip_tags(html_entity_decode($result['facebook_category_name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function f_save_all_category()
    {
        $response['save'] = false;
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if (isset($this->request->post['all']) && !empty($this->request->post['all'])) {
                if (isset($this->request->post['status']) && !empty($this->request->post['status'])) {
                    $status = $this->request->post['status'];
                } else {
                    $status = 0;
                }

                $all_category = $this->request->post['all'];
                if(is_array($all_category)){
                    $this->load->model('feed/facebookfeedproducts');
                    foreach ($all_category as $item){
                        $data = [
                            'id' => $item,
                            'val' => $status
                        ];
                        $save =    $this->model_feed_facebookfeedproducts->saveCategoryF($data);
                    }
                    $response['save'] = $save;
                }
            }
        }
        echo json_encode($response);
        exit;
    }

    public function savecategory()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if(isset($this->request->post['id']) && isset($this->request->post['val'])){
                $id =(int)$this->request->post['id'];
                $val =(int)$this->request->post['val'];
                $this->load->model('feed/facebookfeedproducts');
                $data = [
                    'id' => $id,
                    'val' => $val
                ];
                $save =    $this->model_feed_facebookfeedproducts->saveCategory($data);
                echo json_encode(['save'=>$save]);
                exit;
            }
        }
        echo json_encode(['save'=>false]);

        exit;
    }

    public function save_all_category()
    {
        $response['save'] = false;
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if (isset($this->request->post['all']) && !empty($this->request->post['all'])) {
                if (isset($this->request->post['status']) && !empty($this->request->post['status'])) {
                    $status = $this->request->post['status'];
                } else {
                    $status = 0;
                }

                $all_category = $this->request->post['all'];
                if(is_array($all_category)){
                    $this->load->model('feed/facebookfeedproducts');
                    foreach ($all_category as $item){
                        $data = [
                            'id' => $item,
                            'val' => $status
                        ];
                        $save =    $this->model_feed_facebookfeedproducts->saveCategory($data);
                    }
                    $response['save'] = $save;
                }
            }
        }
        echo json_encode($response);
        exit;
    }

    public function savegooglecat()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'GET') {
            $this->load->model('feed/facebookfeedproducts');
            if (isset($this->request->get["google_main_category_id"])) {
                $data = array('category_id' => $this->request->get["category_id"], 'google_main_category_id' => $this->request->get['google_main_category_id'], 'google_category_id' => NULL);
            } else {
                $data = array('category_id' => $this->request->get["category_id"], 'google_main_category_id' => NULL, 'google_category_id' => $this->request->get['google_category_id']);
            }
            $this->model_feed_facebookfeedproducts->saveGoogleCategory($data);
        }
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('feed/facebookfeedproducts');

            $filterName = $this->request->get['filter_name'];
            $data = array(
                'google_category_name' => ($filterName != '--select--') ? $filterName : '',
                'google_category_id' => $this->request->get['filter_id']
            );

            $json[] = array(
                'google_category_id' => 0,
                'google_category_name' => '--select--'
            );

            $results = $this->model_feed_facebookfeedproducts->getGoogleCategorys($data);
            foreach ($results as $result) {
                $json[] = array(
                    'google_category_id' => $result['google_category_id'],
                    'google_category_name' => strip_tags(html_entity_decode($result['google_category_name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function removeallproductlist()
    {
        $this->load->model('feed/facebookfeedproducts');
        $result = $this->model_feed_facebookfeedproducts->removeAllProductList();
        echo $result;
    }

    public function checkallproductlist()
    {
        $this->load->model('feed/facebookfeedproducts');
        $result = $this->model_feed_facebookfeedproducts->checkAllProductList();
        echo $result;
    }

    public function updateproductlist()
    {
        $key = $this->request->post['key'];
        $val = $this->request->post['value'];

        $this->load->model('feed/facebookfeedproducts');
        $result = $this->model_feed_facebookfeedproducts->setProductsListItem($key, $val);

        if ($result) {
            echo json_encode(['status' => true]);
        } else {
            echo json_encode(['status' => false]);
        }
    }
}
