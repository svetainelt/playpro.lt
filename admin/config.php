<?php
// HTTP
define('HTTP_SERVER', 'https://playpro.lt/admin/');
define('HTTP_CATALOG', 'https://playpro.lt/');

// HTTPS
define('HTTPS_SERVER', 'https://playpro.lt/admin/');
define('HTTPS_CATALOG', 'https://playpro.lt/');

// DIR
define('DIR_APPLICATION', '/home/playpro/domains/playpro.lt/public_html/admin/');
define('DIR_SYSTEM', '/home/playpro/domains/playpro.lt/public_html/system/');
define('DIR_LANGUAGE', '/home/playpro/domains/playpro.lt/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/playpro/domains/playpro.lt/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/playpro/domains/playpro.lt/public_html/system/config/');
define('DIR_IMAGE', '/home/playpro/domains/playpro.lt/public_html/image/');
define('DIR_CACHE', '/home/playpro/domains/playpro.lt/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/playpro/domains/playpro.lt/public_html/system/storage/download/');
define('DIR_LOGS', '/home/playpro/domains/playpro.lt/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/playpro/domains/playpro.lt/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/playpro/domains/playpro.lt/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/playpro/domains/playpro.lt/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'playpro_shop');
define('DB_PASSWORD', 'KlauptinisUzraugenti');
define('DB_DATABASE', 'playpro_shop');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
