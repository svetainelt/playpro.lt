<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-5">
              <label><?php echo $text_from; ?></label>
            </div>
            <div class="col-sm-5">
              <label><?php echo $text_to; ?></label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-color"><?php echo $entry_color; ?></label>
            <div class="col-sm-4">
              <input type="text" name="journal2_color_from" value="<?php echo $journal2_color_from; ?>" placeholder="<?php echo $entry_color_placeholder; ?>" id="input-color-from" class="form-control" />
            </div>
            <div class="col-sm-1 text-center">
                <button class="btn btn-default swapInputs" title="<?php echo $text_swap; ?>" data-toggle="tooltip"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-sm-4">
              <input type="text" name="journal2_color_to" value="<?php echo $journal2_color_to; ?>" placeholder="<?php echo $entry_color_placeholder; ?>" id="input-color-to" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-google-font"><?php echo $entry_google_font; ?></label>
            <div class="col-sm-4">
              <select name="journal2_google_font_from" id="input-google-font-from" class="form-control">
                <option value=""><?php echo $text_choose_font; ?></option>
                <?php if(!empty($google_fonts)): ?>
                  <?php foreach ($google_fonts as $font): ?>
                      <?php $selected = ''; if ($journal2_google_font_from == $font['family']) $selected = 'selected'; ?>
                      <option value="<?php echo $font['family']; ?>" <?php echo $selected; ?>><?php echo $font['family']; ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
            <div class="col-sm-1 text-center">
                <button class="btn btn-default swapInputs" title="<?php echo $text_swap; ?>" data-toggle="tooltip"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-sm-4">
              <select name="journal2_google_font_to" id="input-google-font-to" class="form-control">
                <option value=""><?php echo $text_choose_font; ?></option>
                <?php if(!empty($google_fonts)): ?>
                  <?php foreach ($google_fonts as $font): ?>
                      <?php $selected = ''; if ($journal2_google_font_to == $font['family']) $selected = 'selected'; ?>
                      <option value="<?php echo $font['family']; ?>" <?php echo $selected; ?>><?php echo $font['family']; ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-system-font"><?php echo $entry_system_font; ?></label>
            <div class="col-sm-4">
              <select name="journal2_system_font_from" id="input-system-font-from" class="form-control">
                <option value=""><?php echo $text_choose_font; ?></option>
                <?php if(!empty($system_fonts)): ?>
                  <?php foreach ($system_fonts as $font): ?>
                      <?php $selected = ''; if ($journal2_system_font_from == $font['family']) $selected = 'selected'; ?>
                      <option value="<?php echo $font['family']; ?>" <?php echo $selected; ?>><?php echo $font['family']; ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
            <div class="col-sm-1 text-center">
                <button class="btn btn-default swapInputs" title="<?php echo $text_swap; ?>" data-toggle="tooltip"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-sm-4">
              <select name="journal2_system_font_to" id="input-system-font-to" class="form-control">
                <option value=""><?php echo $text_choose_font; ?></option>
                <?php if(!empty($system_fonts)): ?>
                  <?php foreach ($system_fonts as $font): ?>
                      <?php $selected = ''; if ($journal2_system_font_to == $font['family']) $selected = 'selected'; ?>
                      <option value="<?php echo $font['family']; ?>" <?php echo $selected; ?>><?php echo $font['family']; ?></option>
                  <?php endforeach; ?>
                <?php endif; ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style>
    .swapInputs {
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".swapInputs").on('click', function (e) {
            e.preventDefault();
            
            //inputs
            if($(this).closest('.form-group').find('input').length) {
                var input1 = $(this).closest('.form-group').find('input').first();
                var input2 = $(this).closest('.form-group').find('input').last();
                var inputVal1 = input1.val();
                var inputVal2 = input2.val();
                input1.val( inputVal2 );
                input2.val( inputVal1 );
            }

            //selects
            if($(this).closest('.form-group').find('select').length) {
                var select1 = $(this).closest('.form-group').find('select').first();
                var select2 = $(this).closest('.form-group').find('select').last();
                var selectVal1 = select1.val();
                var selectVal2 = select2.val();
                select1.val( selectVal2 );
                select2.val( selectVal1 );
            }
        });
    });
</script>
<?php echo $footer; ?>