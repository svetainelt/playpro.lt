<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-hide-modules" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-hide-modules" class="form-horizontal">
          <ul id="tabs" class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-module" data-toggle="tab"><?php echo $tab_module; ?></a></li>
            <li><a href="#tab-payment" data-toggle="tab"><?php echo $tab_payment; ?></a></li>
            <li><a href="#tab-shipping" data-toggle="tab"><?php echo $tab_shipping; ?></a></li>
            <li><a href="#tab-total" data-toggle="tab"><?php echo $tab_total; ?></a></li>
            <li><a href="#tab-menu" data-toggle="tab"><?php echo $tab_menu; ?></a></li>
          </ul>
            
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                  <div class="col-sm-10">
                    <select name="hide_modules_status" id="input-status" class="form-control">
                      <?php if ($hide_modules_status) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-style-status"><?php echo $entry_style_status; ?></label>
                  <div class="col-sm-10">
                    <select name="hide_modules_style_status" id="input-style-status" class="form-control">
                      <?php if ($hide_modules_style_status) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
            </div>

            <div class="tab-pane" id="tab-module">
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php echo $text_check_to_show; ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php foreach($modules as $module): ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="hide_modules[module][]" value="<?php echo $module['extension']; ?>" <?php if (isset($hide_modules['module']) && in_array($module['extension'], $hide_modules['module'])): ?>checked="checked"<? endif; ?> />
                            <?php echo $module['name']; ?>
                          </label>
                        </div>
                    <?php endforeach; ?>
                  </div>
                </div>
            </div>
              
            <div class="tab-pane" id="tab-shipping">
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php echo $text_check_to_show; ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php foreach($shippings as $module): ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="hide_modules[shipping][]" value="<?php echo $module['extension']; ?>" <?php if (isset($hide_modules['shipping']) && in_array($module['extension'], $hide_modules['shipping'])): ?>checked="checked"<? endif; ?> />
                            <?php echo $module['name']; ?>
                          </label>
                        </div>
                    <?php endforeach; ?>
                  </div>
                </div>
            </div>
              
            <div class="tab-pane" id="tab-payment">
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php echo $text_check_to_show; ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php foreach($payments as $module): ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="hide_modules[payment][]" value="<?php echo $module['extension']; ?>" <?php if (isset($hide_modules['payment']) && in_array($module['extension'], $hide_modules['payment'])): ?>checked="checked"<? endif; ?> />
                            <?php echo $module['name']; ?>
                          </label>
                        </div>
                    <?php endforeach; ?>
                  </div>
                </div>
            </div>
              
            <div class="tab-pane" id="tab-total">
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php echo $text_check_to_show; ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php foreach($totals as $module): ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" name="hide_modules[total][]" value="<?php echo $module['extension']; ?>" <?php if (isset($hide_modules['total']) && in_array($module['extension'], $hide_modules['total'])): ?>checked="checked"<? endif; ?> />
                            <?php echo $module['name']; ?>
                          </label>
                        </div>
                    <?php endforeach; ?>
                  </div>
                </div>
            </div>
              
            <div class="tab-pane" id="tab-menu">
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <?php echo $text_check_to_show; ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-11 col-sm-offset-1">
                    <div id="menu-elements-container"></div>
                  </div>
                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    var checkedMenuLinks = <?php echo json_encode((!empty($hide_modules['menu']) ? $hide_modules['menu'] : array())); ?>;
    
//        console.log(JSON.stringify(checkedMenuLinks, null, 2));

    $(document).ready(function() {
        var $tree = getRecursiveElements($('#menu > li'));
        
        printRecursiveElements($tree, [], 0);

//        console.log(JSON.stringify($tree, null, 2));
    });
    
    function getRecursiveElements($menu) {
        var $elements = [];
        
        $menu.each(function(i, el) {
            var title = $.trim($(el).find('a:first').text());

            $elements[i] = {};
            $elements[i].number = (i + 1);
            $elements[i].title = title;
         
            var $next = $(el).find('ul:first > li');
            if($next.length) {
                $elements[i].child = getRecursiveElements($next);
            }
        });
        
        return $elements;
    }
    
    function printRecursiveElements($elements, parentIds, margin) {
        for(var i = 0; i < $elements.length; i++) {
            var $el = $elements[i];
            var parent_id = $elements[i].number;
            
            if($el.title.length) {
                addCheckboxElement($el.number, $el.title, parentIds, margin);
            }
            
            if($el.child !== undefined && $el.child.length) {
                var parents = parentIds.concat([parent_id]); 
                printRecursiveElements($el.child, parents, margin + 40);
            }
        }
    }
    
    function addCheckboxElement(number, name, parentIds, margin) {
        var string = '';
        for(var i = 0; i < parentIds.length; i++) {
            string += '[' + parentIds[i] + ']';
        }

        var parents = parentIds.concat([number]); 
        var checked = checkIfChecked(parents, number);
        
        var $checkbox = '<div class="checkbox" style="margin-left: ' + margin + 'px;">' +
          '<label>' +
            '<input type="checkbox" name="hide_modules[menu]' + string + '[' + number + ']" value="' + number + '" ' + (checked ? 'checked' : '') + ' />' + name +
          '</label>' +
        '</div>';

        $('#menu-elements-container').append($checkbox);
    }
    
    function checkIfChecked(parentIds) {
        var found = false;

        if(parentIds[3] != undefined && parentIds[2] != undefined && parentIds[1] != undefined && parentIds[0] != undefined) {
            if(checkedMenuLinks[parentIds[0]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]][parentIds[2]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]][parentIds[2]][parentIds[3]] != undefined) {
                found = true;
            }
        } else if(parentIds[2] != undefined && parentIds[1] != undefined && parentIds[0] != undefined) {
            if(checkedMenuLinks[parentIds[0]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]][parentIds[2]] != undefined) {
                found = true;
            }
        } else if(parentIds[1] != undefined && parentIds[0] != undefined) {
            if(checkedMenuLinks[parentIds[0]] != undefined && checkedMenuLinks[parentIds[0]][parentIds[1]] != undefined) {
                found = true;
            }
        } else if(parentIds[0] != undefined && parentIds[0] > 0) {
            if(checkedMenuLinks[parentIds[0]] != undefined) {
                found = true;
            }
        }
        
        return found;
    }
</script>
<?php echo $footer; ?>