<div class="container-fluid">
    <h3><?php echo $text_general; ?></h3>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_enable; ?></strong></h5>
            <span class="help"><?php echo $text_cc_enable_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[CC][Enabled]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['CC']['Enabled']) && $moduleData['CC']['Enabled'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['CC']['Enabled']) || $moduleData['CC']['Enabled']== '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_link_to_pp; ?></strong></h5>
            <span class="help"><?php echo $text_cc_link_to_pp_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[CC][LinkToPP]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['CC']['LinkToPP']) && $moduleData['CC']['LinkToPP'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['CC']['LinkToPP']) || $moduleData['CC']['LinkToPP']== '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_pp_page; ?></strong></h5>
            <span class="help"><?php echo $text_cc_pp_page_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[CC][PageId]" class="form-control">
                  <option value="0"  <?php echo (empty($moduleData['CC']['PageId']) || $moduleData['CC']['PageId']== '0') ? 'selected=selected' : '' ?>><?php echo $text_cc_pp_default_page; ?></option>
                  <?php foreach ($informations as $i_page) { ?>
                      <option value="<?php echo $i_page['information_id']; ?>" <?php echo (!empty($moduleData['CC']['PageId']) && $moduleData['CC']['PageId'] == $i_page['information_id']) ? 'selected=selected' : '' ?>><?php echo $i_page['title']; ?></option>
                  <?php } ?>
             </select>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_track_pp_clicks; ?></strong></h5>
            <span class="help"><?php echo $text_cc_track_pp_clicks_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <select name="<?php echo $moduleName; ?>[CC][TrackPPClicks]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['CC']['TrackPPClicks']) && $moduleData['CC']['TrackPPClicks'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['CC']['TrackPPClicks']) || $moduleData['CC']['TrackPPClicks']== '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />

	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_default_action; ?></strong></h5>
            <span class="help"><?php echo $text_cc_default_action_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <select name="<?php echo $moduleName; ?>[CC][DefaultAction]" class="form-control">
                  <option value="analytics" <?php echo (!empty($moduleData['CC']['DefaultAction']) && $moduleData['CC']['DefaultAction'] == 'analytics') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_analytics; ?></option>
                  <option value="marketing" <?php echo (!empty($moduleData['CC']['DefaultAction']) && $moduleData['CC']['DefaultAction'] == 'marketing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_marketing; ?></option>
                  <option value="analytics_marketing" <?php echo (!empty($moduleData['CC']['DefaultAction']) && $moduleData['CC']['DefaultAction'] == 'analytics_marketing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_both; ?></option>
                  <option value="nothing"  <?php echo (empty($moduleData['CC']['DefaultAction']) || $moduleData['CC']['DefaultAction']== 'nothing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_nothing; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
            
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_close_action; ?></strong></h5>
            <span class="help"><?php echo $text_cc_close_action_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <select name="<?php echo $moduleName; ?>[CC][CloseAction]" class="form-control">
                  <option value="analytics" <?php echo (!empty($moduleData['CC']['CloseAction']) && $moduleData['CC']['CloseAction'] == 'analytics') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_analytics; ?></option>
                  <option value="marketing" <?php echo (!empty($moduleData['CC']['CloseAction']) && $moduleData['CC']['CloseAction'] == 'marketing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_marketing; ?></option>
                  <option value="analytics_marketing" <?php echo (!empty($moduleData['CC']['CloseAction']) && $moduleData['CC']['CloseAction'] == 'analytics_marketing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_both; ?></option>
                  <option value="nothing"  <?php echo (empty($moduleData['CC']['CloseAction']) || $moduleData['CC']['CloseAction']== 'nothing') ? 'selected=selected' : '' ?>><?php echo $text_cc_stop_nothing; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_always_show; ?></strong></h5>
            <span class="help"><?php echo $text_cc_always_show_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
     		<select name="<?php echo $moduleName; ?>[CC][AlwaysShow]" class="form-control">
                <option value="1" <?php echo (!empty($moduleData['CC']['AlwaysShow']) && $moduleData['CC']['AlwaysShow'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
			  	<option value="0"  <?php echo (empty($moduleData['CC']['AlwaysShow']) || $moduleData['CC']['AlwaysShow']== '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr /><br />
    
   	<h3><?php echo $text_cookie_management; ?></h3>
   	<br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_ccc_functional; ?></strong></h5>
            <span class="help"><?php echo $text_ccc_functional_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <textarea name="<?php echo $moduleName; ?>[CCC][Functional]" class="form-control" rows="4"><?php echo (isset($moduleData['CCC']['Functional'])) ? $moduleData['CCC']['Functional'] : 'PHPSESSID
cookies
language
cookieconsent_status
cookieconsent_preferences_disabled'; ?></textarea>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_ccc_analytics; ?></strong></h5>
            <span class="help"><?php echo $text_ccc_analytics_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <textarea name="<?php echo $moduleName; ?>[CCC][Analytics]" class="form-control" rows="4"><?php echo (isset($moduleData['CCC']['Analytics'])) ? $moduleData['CCC']['Analytics'] : '_ga
_gid
_gat
__atuvc
__atuvs
__utma
__cfduid'; ?></textarea>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_ccc_marketing; ?></strong></h5>
            <span class="help"><?php echo $text_ccc_marketing_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <textarea name="<?php echo $moduleName; ?>[CCC][Marketing]" class="form-control" rows="4"><?php echo (isset($moduleData['CCC']['Marketing'])) ? $moduleData['CCC']['Marketing'] : '_gads
IDE
test_cookie
fr
tr
collect
GPS
PREF
BizoID'; ?></textarea>
        </div>
    </div>
    <hr /><br />
    
    <h3><?php echo $text_cc_customize; ?></h3>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_texts; ?></strong></h5>
            <span class="help"><?php echo $text_cc_texts_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-9">
            <ul class="nav nav-tabs" id="langtabs-cc" role="tablist">
			  <?php foreach ($languages as $language) { ?>
				<li><a href="#lang-cc-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?></a></li>
			  <?php } ?>
			</ul>
			<div class="tab-content">
			  <?php foreach ($languages as $language) { ?>
				<div class="tab-pane" id="lang-cc-<?php echo $language['language_id']; ?>">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_main_message; ?></strong></h5>
							<span class="help"><?php echo $text_cc_main_message_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<textarea name="<?php echo $moduleName; ?>[CC][Message][<?php echo $language['language_id']; ?>]" class="form-control" rows="4"><?php echo (isset($moduleData['CC']['Message'][$language['language_id']])) ? $moduleData['CC']['Message'][$language['language_id']] : 'This website uses cookies to ensure you get the best experience on our website.'; ?></textarea>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_pp_message; ?></strong></h5>
							<span class="help"><?php echo $text_cc_pp_message_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<input name="<?php echo $moduleName; ?>[CC][PPText][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['CC']['PPText'][$language['language_id']])) ? $moduleData['CC']['PPText'][$language['language_id']] : 'Privacy Policy'; ?>" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_accept_text; ?></strong></h5>
							<span class="help"><?php echo $text_cc_accept_text_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<input name="<?php echo $moduleName; ?>[CC][AcceptText][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['CC']['AcceptText'][$language['language_id']])) ? $moduleData['CC']['AcceptText'][$language['language_id']] : 'Accept'; ?>" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_dismiss_text; ?></strong></h5>
							<span class="help"><?php echo $text_cc_dismiss_text_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<input name="<?php echo $moduleName; ?>[CC][DismissText][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['CC']['DismissText'][$language['language_id']])) ? $moduleData['CC']['DismissText'][$language['language_id']] : 'Close'; ?>" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_cookies_change; ?></strong></h5>
							<span class="help"><?php echo $text_cc_cookies_change_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<input name="<?php echo $moduleName; ?>[CC][CookiesText][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['CC']['CookiesText'][$language['language_id']])) ? $moduleData['CC']['CookiesText'][$language['language_id']] : 'Preferences'; ?>" />
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<h5><strong><?php echo $text_cc_cookie_button; ?></strong></h5>
							<span class="help"><?php echo $text_cc_cookie_button_helper; ?></span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
							<input name="<?php echo $moduleName; ?>[CC][ASButtonText][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['CC']['ASButtonText'][$language['language_id']])) ? $moduleData['CC']['ASButtonText'][$language['language_id']] : 'Cookie Bar'; ?>" />
						</div>
					</div>
				</div>
			  <?php } ?>
			</div>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_positioning; ?></strong></h5>
            <span class="help"><?php echo $text_cc_positioning_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[CC][Position]" class="form-control">
                  <option value="default"  <?php echo (empty($moduleData['CC']['Position']) || $moduleData['CC']['Position']== 'default') ? 'selected=selected' : '' ?>><?php echo $text_cc_banner_bottom; ?></option>
                  <option value="top" <?php echo (!empty($moduleData['CC']['Position']) && $moduleData['CC']['Position'] == 'top') ? 'selected=selected' : '' ?>><?php echo $text_cc_banner_top; ?></option>
                  <option value="top-pushdown" <?php echo (!empty($moduleData['CC']['Position']) && $moduleData['CC']['Position'] == 'top-pushdown') ? 'selected=selected' : '' ?>><?php echo $text_cc_banner_top_pushdown; ?></option>
                  <option value="bottom-left" <?php echo (!empty($moduleData['CC']['Position']) && $moduleData['CC']['Position'] == 'bottom-left') ? 'selected=selected' : '' ?>><?php echo $text_cc_banner_float_left; ?></option>
                  <option value="bottom-right" <?php echo (!empty($moduleData['CC']['Position']) && $moduleData['CC']['Position'] == 'bottom-right') ? 'selected=selected' : '' ?>><?php echo $text_cc_banner_float_right; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_cc_palette; ?></strong></h5>
            <span class="help"><?php echo $text_cc_palette_styling; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <div class="input-group">
			  <span class="input-group-addon" style="min-width:150px;"><?php echo $text_cc_bar_bg; ?></span>
			  <input type="color" name="<?php echo $moduleName; ?>[CC][Style][BannerBG]" class="form-control" value="<?php echo (isset($moduleData['CC']['Style']['BannerBG'])) ? $moduleData['CC']['Style']['BannerBG'] : '#000000'; ?>" />
			 </div>
             <br />
             <div class="input-group">
			  <span class="input-group-addon" style="min-width:150px;"><?php echo $text_cc_bar_text; ?></span>
			  <input type="color" name="<?php echo $moduleName; ?>[CC][Style][BannerText]" class="form-control" value="<?php echo (isset($moduleData['CC']['Style']['BannerText'])) ? $moduleData['CC']['Style']['BannerText'] : '#FFFFFF'; ?>" />
			 </div>
             <br />
             <div class="input-group">
			  <span class="input-group-addon" style="min-width:150px;"><?php echo $text_cc_button_bg; ?></span>
			  <input type="color" name="<?php echo $moduleName; ?>[CC][Style][ButtonBG]" class="form-control" value="<?php echo (isset($moduleData['CC']['Style']['ButtonBG'])) ? $moduleData['CC']['Style']['ButtonBG'] : '#F1D600'; ?>" />
			 </div>
             <br />
             <div class="input-group">
			  <span class="input-group-addon" style="min-width:150px;"><?php echo $text_cc_button_text; ?></span>
			  <input type="color" name="<?php echo $moduleName; ?>[CC][Style][ButtonText]" class="form-control" value="<?php echo (isset($moduleData['CC']['Style']['ButtonText'])) ? $moduleData['CC']['Style']['ButtonText'] : '#000000'; ?>" />
			 </div>
             <br />
             <div class="input-group">
			  <span class="input-group-addon" style="min-width:150px;"><?php echo $text_cc_transparency; ?></span>
			  <input step="1" min="0" max="100" type="number" name="<?php echo $moduleName; ?>[CC][Style][Opacity]" class="form-control" value="<?php echo (isset($moduleData['CC']['Style']['Opacity'])) ? $moduleData['CC']['Style']['Opacity'] : '100'; ?>" />
              <span class="input-group-addon">%</span>
			 </div>
        </div>
    </div>
    <hr /><br />
    
        <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_gdpr_cc_styles; ?></strong></h5>
            <span class="help"><?php echo $text_gdpr_cc_styles_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <textarea name="<?php echo $moduleName; ?>[CC][Styles]" class="form-control" rows="10"><?php echo (isset($moduleData['CC']['Styles'])) ? $moduleData['CC']['Styles'] : '.cc-btn {}
.cc-dismiss {}
.cc-settings {}
.cc-allow {}
.cc-message {}
.cc-link {}
.cc-window {}
.cc-close {}
.cc-revoke {}
.cc-settings-view {}
.cc-settings-dialog {}
.cc-btn-close-settings {}
.cc-btn-save-settings {}'; ?></textarea>
        </div>
    </div>
    <hr /><br />
    
	<em><?php echo $text_consent_bar_copyright; ?></em>
</div>