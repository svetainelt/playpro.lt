<div class="container-fluid">
    <h3><?php echo $text_general; ?></h3>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><span class="required">*</span> <strong><?php echo $text_gdpr_status; ?></strong></h5>
            <span class="help"><?php echo $text_gdpr_status_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
	</div>
    <hr /><br />
    <?php if ($storeId == '0') { ?>
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_show_gdpr_menu_left; ?></strong></h5>
            <span class="help"><?php echo $text_show_gdpr_menu_left_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[LinkInMenu]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['LinkInMenu']) && $moduleData['LinkInMenu'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['LinkInMenu']) || $moduleData['LinkInMenu'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
             <em><?php echo $text_show_gdpr_menu_left_default; ?></em>
        </div>
    </div>
    <hr /><br />
    <?php } ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_show_gdpr_footer; ?></strong></h5>
            <span class="help"><?php echo $text_show_gdpr_footer_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[LinkInFooter]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['LinkInFooter']) && $moduleData['LinkInFooter'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['LinkInFooter']) || $moduleData['LinkInFooter'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
             <em><?php echo $text_show_gdpr_menu_left_default; ?></em>
        </div>
    </div>
    <hr /><br />
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_right_to_be_forgotten_requests; ?></strong></h5>
            <span class="help"><?php echo $text_right_to_be_forgotten_requests_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[RTBMode]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['RTBMode']) && $moduleData['RTBMode'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_automatically; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['RTBMode']) || $moduleData['RTBMode'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_manually; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_gdpr_seo_link; ?></strong></h5>
            <span class="help"><?php echo $text_gdpr_seo_link_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
            <ul class="nav nav-tabs" id="langtabs-seo" role="tablist">
			  <?php foreach ($languages as $language) { ?>
				<li><a href="#lang-seo-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?></a></li>
			  <?php } ?>
			</ul>
			<div class="tab-content">
			  <?php foreach ($languages as $language) { ?>
				<div class="tab-pane" id="lang-seo-<?php echo $language['language_id']; ?>">
					<input name="<?php echo $moduleName; ?>[Slug][<?php echo $language['language_id']; ?>]" class="form-control" value="<?php echo (isset($moduleData['Slug'][$language['language_id']])) ? $moduleData['Slug'][$language['language_id']] : 'gdpr-tools'; ?>" />
				</div>
			  <?php } ?>
			</div>
        </div>
    </div>
    <hr /><br />
    
    <h3><?php echo $text_life_of_links; ?></h3>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_access_to_personal_data_link; ?></strong></h5>
            <span class="help"><?php echo $text_access_to_personal_data_link_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <input type="number" class="form-control" name="<?php echo $moduleName; ?>[PersonalDataLinkLife]" value="<?php if(isset($moduleData['PersonalDataLinkLife'])) { echo $moduleData['PersonalDataLinkLife']; } else { echo "5"; }?>" />
             <em><?php echo $text_access_default_hours; ?></em>
        </div>
    </div>
    <hr /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_remove_personal_data_link; ?></strong></h5>
            <span class="help"><?php echo $text_remove_personal_data_link_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <input type="number" class="form-control" name="<?php echo $moduleName; ?>[RemovePersonalDataLinkLife]" value="<?php if(isset($moduleData['RemovePersonalDataLinkLife'])) { echo $moduleData['RemovePersonalDataLinkLife']; } else { echo "2"; }?>" />
             <em><?php echo $text_delete_default_hours; ?></em>
        </div>
    </div>
    <hr /><br />

    <h3><?php echo $text_optins; ?></h3>
    <br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_contact_form_optin_link; ?></strong></h5>
            <span class="help"><?php echo $text_contact_form_optin_link_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[EnabledContactFormOptIn]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['EnabledContactFormOptIn']) && $moduleData['EnabledContactFormOptIn'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['EnabledContactFormOptIn']) || $moduleData['EnabledContactFormOptIn'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_contact_form_newsletter_link; ?></strong></h5>
            <span class="help"><?php echo $text_contact_form_newsletter_link_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[NewsletterOptIn]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['NewsletterOptIn']) && $moduleData['NewsletterOptIn'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['NewsletterOptIn']) || $moduleData['NewsletterOptIn'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_newsletter_double_optin; ?></strong></h5>
            <span class="help"><?php echo $text_newsletter_double_optin_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
             <select name="<?php echo $moduleName; ?>[NewsletterDoubleOptIn]" class="form-control">
                  <option value="1" <?php echo (!empty($moduleData['NewsletterDoubleOptIn']) && $moduleData['NewsletterDoubleOptIn'] == '1') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="0"  <?php echo (empty($moduleData['NewsletterDoubleOptIn']) || $moduleData['NewsletterDoubleOptIn'] == '0') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
             </select>
        </div>
    </div>
    <hr />
</div>