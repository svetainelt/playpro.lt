<div class="container-fluid">
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_data_security_breach; ?></strong></h5>
            <span class="help"><?php echo $text_data_security_breach_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <a href="<?php echo $data_security_breach_link; ?>" target="_blank" class="btn btn-primary"><?php echo $text_notify_customers; ?></a>
            <br />
            <em><?php echo $text_data_security_breach_helper_additional; ?></em>
        </div>
    </div>
	<hr />
	<br />
</div>