<div class="optins-filter-data well">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
           <label for="optin_id"><?php echo $column_optin_id; ?></label>
            <input name="optin_id" id="optin_id" class="form-control optins_input" value="<?php echo $filter_data['optin_id']; ?>" />
        </div>
        <div class="form-group">
            <label for="email"><?php echo $column_email; ?></label>
            <input name="email" id="email" class="form-control optins_input" value="<?php echo $filter_data['email']; ?>" />
        </div>
        <div class="form-group">
            <label for="type"><?php echo $column_type; ?></label>
            <input name="type" id="type" class="form-control optins_input" value="<?php echo $filter_data['type']; ?>" />
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="action"><?php echo $column_action; ?></label>
            <select name="action" class="form-control">
                <option value="-" <?php echo ($filter_data['action']=='-') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_all; ?></option>
                <option value="double opt-in" <?php echo ($filter_data['action']=='double opt-in') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_double_optin; ?></option>
                <option value="opt-in" <?php echo ($filter_data['action']=='opt-in') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_optin; ?></option>
                <option value="opt-out" <?php echo ($filter_data['action']=='opt-out') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_optout; ?></option>
                <option value="acceptance" <?php echo ($filter_data['action']=='acceptance') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_acceptance; ?></option>
                <option value="custom" <?php echo ($filter_data['action']=='custom') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_custom; ?></option>
            </select>
        </div>
        <div class="form-group">
            <label for="date_start"><?php echo $text_date_start; ?></label>
            <input name="date_start" id="date_start" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_start']; ?>" />
        </div>
        <div class="form-group">
            <label for="date_end"><?php echo $text_date_end; ?></label>
            <input name="date_end" id="date_end" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_end']; ?>" />
        </div>
        <div class="form-group text-right">
             <br />
             <button class="btn btn-optins-export btn-default" onClick="exportOptins(this, event);"><?php echo $text_btn_export; ?></button>
             <button class="btn btn-optins-filter btn-primary" onClick="filterOptins(this, event);"><?php echo $text_filter; ?></button>
        </div>
    </div>         
    <div class="clearfix"></div>            
</div>
<table class="table table-striped table-acceptances-results table-hover"> 
    <thead>
        <tr>
            <th><?php echo $column_optin_id; ?></th> 
            <th><?php echo $column_email; ?></th> 
            <th><?php echo $column_type; ?></th> 
            <th><?php echo $column_action; ?></th> 
            <th><?php echo $column_user_agent; ?></th>
            <th><?php echo $column_accept_language; ?></th>
            <th><?php echo $column_client_ip; ?></th>
            <th class="text-right"><?php echo $column_date; ?></th> 
            <th class="text-right"><?php echo $column_actions; ?></th> 
        </tr> 
    </thead> 
    <tbody>
        <?php if (count($sources)> 0) { ?> 
            <?php foreach ($sources as $index => $result) { ?>
            <tr> 
                <td><?php echo $result['optin_id']; ?></td> 
                <td><?php echo $result['email']; ?></td> 
                <td><?php echo $result['type']; ?></td> 
                <td><?php echo $result['action']; ?></td>
                <td style="word-break: break-word; width: 20%"><?php echo $result['user_agent']; ?></td> 
                <td><?php echo $result['accept_language']; ?></td> 
                <td><?php echo $result['client_ip']; ?></td> 
                <td class="text-right"><?php echo $result['date_added']; ?></td> 
                <td class="text-right"><a class="btn btn-small btn-default" href="<?php echo $result['link']; ?>" target="_blank" data-toggle="tooltip" title="" data-original-title="<?php echo $text_view_current; ?>"><i class="fa fa-eye"></i></a></td> 
            </tr> 
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="9" class="text-center"><?php echo $text_no_results; ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
$('.date').datetimepicker({
    pickTime: false
});
    
// input auto-send query
$('.optins_input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('.btn-optins-filter').trigger('click');
    }
});
</script>