<div class="deletions-filter-data well">
    <div class="col-xs-12 col-sm-12 col-md-6">
		<div class="form-group">
            <label for="email"><?php echo $column_email; ?></label>
            <input name="email" id="email" class="form-control deletions_input" value="<?php echo $filter_data['email']; ?>" />
        </div>
        <div class="form-group">
            <label for="status"><?php echo $entry_status; ?></label>
            <select name="status" class="form-control">
                <option value="-" <?php echo ($filter_data['status']=='-') ? 'selected="selected"' : ''; ?>><?php echo $text_filter_all; ?></option>
                <option value="0" <?php echo ($filter_data['status']=='0') ? 'selected="selected"' : ''; ?>><?php echo $text_pending_status; ?></option>
                <option value="1" <?php echo ($filter_data['status']=='1') ? 'selected="selected"' : ''; ?>><?php echo $text_cancelled_status; ?></option>
				<option value="2" <?php echo ($filter_data['status']=='2') ? 'selected="selected"' : ''; ?>><?php echo $text_awaiting_deletion_status; ?></option>
                <option value="3" <?php echo ($filter_data['status']=='3') ? 'selected="selected"' : ''; ?>><?php echo $text_deleted_status; ?></option>
            </select>
        </div>
        <div class="form-group">
            <label for="date_deletion"><?php echo $text_column_deletion_date; ?></label>
            <input name="date_deletion" id="date_deletion" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_deletion']; ?>" />
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="date_start"><?php echo $text_date_start; ?></label>
            <input name="date_start" id="date_start" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_start']; ?>" />
        </div>
        <div class="form-group">
            <label for="date_end"><?php echo $text_date_end; ?></label>
            <input name="date_end" id="date_end" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_end']; ?>" />
        </div>
        <div class="form-group text-right">
             <br />
             <button class="btn btn-deletions-filter btn-primary" onClick="filterDeletions(this, event);"><?php echo $text_filter; ?></button>
        </div>
    </div>         
    <div class="clearfix"></div>            
</div>
<table class="table table-striped table-deletions-results table-hover"> 
    <thead>
        <tr>
            <th><?php echo $column_email; ?></th> 
            <th><?php echo $entry_status; ?></th> 
            <th><?php echo $text_customer_data; ?></th>
            <th><?php echo $text_address_data; ?></th>
            <th><?php echo $text_order_data; ?></th>
            <th><?php echo $text_gdpr_data; ?></th>
			<th class="text-right"><?php echo $text_column_deletion_date; ?></th>
            <th class="text-right"><?php echo $column_date; ?></th> 
            <th class="text-right"><?php echo $column_actions; ?></th> 
        </tr> 
    </thead> 
    <tbody>
        <?php if (count($sources)> 0) { ?> 
            <?php foreach ($sources as $index => $result) { ?>
            <tr> 
                <td><?php echo $result['email']; ?></td> 
                <td><?php echo $result['status']; ?></td>
                <td><?php echo $result['customer_data']; ?></td> 
                <td><?php echo $result['address_data']; ?></td> 
                <td><?php echo $result['order_data']; ?></td> 
                <td><?php echo $result['gdpr_data']; ?></td> 
				<td class="text-right"><?php echo $result['date_deletion']; ?></td> 
                <td class="text-right"><?php echo $result['date_added']; ?></td> 
                <td class="text-right">
					<a <?php if ($result['status_code']!='0' && $result['status_code']!='1' && $result['status_code']!='3') { ?>onClick="deleteData(<?php echo $result['deletion_id']; ?>);"<?php } else { ?>disabled="disabled"<?php } ?> class="btn btn-small btn-primary" data-toggle="tooltip" title="" data-original-title="<?php echo $text_process_deletion; ?>"><i class="fa fa-trash-o"></i></a>
					<a <?php if ($result['status_code']!='3' && $result['status_code']!='1') { ?>onClick="approveRequest(<?php echo $result['deletion_id']; ?>);"<?php } else { ?>disabled="disabled"<?php } ?> class="btn btn-small btn-default" data-toggle="tooltip" title="" data-original-title="<?php echo $text_approve_deletion; ?>"><i class="fa fa-thumbs-o-up"></i></a>
					<a <?php if ($result['status_code']!='3' && $result['status_code']!='1') { ?>onClick="denyRequest(<?php echo $result['deletion_id']; ?>);"<?php } else { ?>disabled="disabled"<?php } ?> class="btn btn-small btn-danger" data-toggle="tooltip" title="" data-original-title="<?php echo $text_cancel_deletion; ?>"><i class="fa fa-minus-circle"></i></a>
           		</td> 
            </tr> 
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="9" class="text-center"><?php echo $text_no_results; ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
$('.date').datetimepicker({
    pickTime: false
});
    
// input auto-send query
$('.deletions_input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('.btn-deletions-filter').trigger('click');
    }
});
</script>