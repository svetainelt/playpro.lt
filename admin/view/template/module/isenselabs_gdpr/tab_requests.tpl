<div class="requests-filter-data well">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
           <label for="request_id"><?php echo $column_request_id; ?></label>
            <input name="request_id" id="request_id" class="form-control requests_input" value="<?php echo $filter_data['request_id']; ?>" />
        </div>
        <div class="form-group">
            <label for="type"><?php echo $column_type; ?></label>
            <input name="type" id="type" class="form-control requests_input" value="<?php echo $filter_data['type']; ?>" />
        </div>
        <div class="form-group">
            <label for="email"><?php echo $column_email; ?></label>
            <input name="email" id="email" class="form-control requests_input" value="<?php echo $filter_data['email']; ?>" />
        </div>
        <div class="form-group">
            <label for="user_agent"><?php echo $column_user_agent; ?></label>
            <input name="user_agent" id="user_agent" class="form-control requests_input" value="<?php echo $filter_data['user_agent']; ?>" />
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="server_ip"><?php echo $column_server_ip; ?></label>
            <input name="server_ip" id="server_ip" class="form-control requests_input" value="<?php echo $filter_data['server_ip']; ?>" />
        </div>
        <div class="form-group">
            <label for="client_ip"><?php echo $column_client_ip; ?></label>
            <input name="client_ip" id="client_ip" class="form-control requests_input" value="<?php echo $filter_data['client_ip']; ?>" />
        </div>
        <div class="form-group">
            <label for="date_start"><?php echo $text_date_start; ?></label>
            <input name="date_start" id="date_start" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_start']; ?>" />
        </div>
        <div class="form-group">
            <label for="date_end"><?php echo $text_date_end; ?></label>
            <input name="date_end" id="date_end" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_end']; ?>" />
        </div>
    </div>         
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-right">
             <button class="btn btn-requests-export btn-default" onClick="exportRequests(this, event);"><?php echo $text_btn_export; ?></button>
             <button class="btn btn-requests-filter btn-primary" onClick="filterRequests(this, event);"><?php echo $text_filter; ?></button>
        </div>
    </div>
    <div class="clearfix"></div>            
</div>
<table class="table table-striped table-requests-results table-hover"> 
    <thead>
        <tr>
            <th><?php echo $column_request_id; ?></th> 
            <th><?php echo $column_type; ?></th> 
            <th><?php echo $column_email; ?></th> 
            <th><?php echo $column_user_agent; ?></th>
            <th><?php echo $column_accept_language; ?></th>
            <th><?php echo $column_client_ip; ?></th>
            <th><?php echo $column_server_ip; ?></th>
            <th class="text-right"><?php echo $column_date; ?></th> 
        </tr> 
    </thead> 
    <tbody>
        <?php if (count($sources)> 0) { ?> 
            <?php foreach ($sources as $index => $result) { ?>
            <tr> 
                <td><?php echo $result['request_id']; ?></td> 
                <td><?php echo $result['type']; ?></td> 
                <td><?php echo $result['email']; ?></td> 
                <td><?php echo $result['user_agent']; ?></td> 
                <td><?php echo $result['accept_language']; ?></td> 
                <td><?php echo $result['client_ip']; ?></td> 
                <td><?php echo $result['server_ip']; ?></td> 
                <td class="text-right"><?php echo $result['request_added']; ?></td> 
            </tr> 
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="8" class="text-center"><?php echo $text_no_results; ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8">
    	        <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});
    
// input auto-send query
$('.requests_input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('.btn-requests-filter').trigger('click');
    }
});
</script>