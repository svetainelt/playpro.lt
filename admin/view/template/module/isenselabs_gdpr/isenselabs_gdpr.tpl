<?php echo $header; ?><?php echo $column_left; ?>
<?php
if (!function_exists('modification_vqmod')) {
    function modification_vqmod($file) {
        if (class_exists('VQMod')) {
            return VQMod::modCheck(modification($file), $file);
        } else {
            return modification($file);
        }
    }
}
?>
 <div id="content" class="isense_seo_module">
  <div class="page-header">
    <div class="container-fluid">
        <div class="storeSwitcherWidget">
            <div class="form-group">
                <button type="submit" form="form-filter" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary save-changes"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $store['name']; if ($store['store_id'] == '0') echo " <strong>" . $text_default . "</strong>"; ?>&nbsp;<span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-header"><?php echo $text_stores; ?></li>
                    <?php foreach ($stores as $st) { ?>
                        <li class="<?php echo ($st['store_id'] == $storeId) ? 'active' : ''; ?>"><a href="index.php?route=<?php echo $modulePath; ?>&store_id=<?php echo $st['store_id'];?>&<?php echo $token_string; ?>=<?php echo $token; ?>"><?php echo $st['name']; ?></a></li>
                    <?php } ?> 
                </ul>
            </div>
        </div>
       
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php echo $licensedData; ?>

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    
    <?php if ($success) { ?>
    <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php } ?>
    
	<div class="row row-main">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-filter" class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-body">
                   <ul class="nav nav-tabs mainMenuTabs" role="tablist">
                      <li class="active">
                          <a role="tab" data-toggle="tab" href="#controlpanel-tab"><?php echo $tab_controlpanel; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#requests-tab"><?php echo $tab_requests; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#acceptances-tab"><?php echo $tab_acceptances; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#optins-tab"><?php echo $tab_optins; ?></a>
                      </li>
                      <?php if (empty($moduleData['RTBMode']) || (isset($moduleData['RTBMode']) && $moduleData['RTBMode']=='0')) { ?>
                      <li>
                          <a role="tab" data-toggle="tab" href="#deletions-tab"><?php echo $tab_deletion_requests; ?>&nbsp;&nbsp;<span title="<?php echo $text_awaiting_action; ?>" class="label label-info"><?php echo $pending_deletions; ?></span> <span title="<?php echo $text_pending_deletion; ?>" class="label label-warning"><?php echo $awaiting_deletions; ?></span></a>
                      </li>
                      <?php } ?>
                      <li>
                          <a role="tab" data-toggle="tab" href="#personaldata-tab"><?php echo $tab_personaldata; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#datasecurity-tab"><?php echo $tab_datasecurity; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#cookieconsent-tab"><?php echo $tab_cookieconsent; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#home-tab"><?php echo $tab_home; ?></a>
                      </li>
                      <li>
                          <a role="tab" data-toggle="tab" href="#isense-support"><?php echo $tab_support; ?></a>
                      </li>
                    </ul>

                    <div class="tab-content">
                        <div id="controlpanel-tab" role="tabpanel" class="tab-pane fade in active">
							<?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_controlpanel.tpl'); ?>
                        </div>
                        <div id="requests-tab" role="tabpanel" class="tab-pane fade requests-view">
                            <br />
                            <div class="loader"></div>
                        </div>
                        <div id="acceptances-tab" role="tabpanel" class="tab-pane fade acceptances-view">
                            <br />
                            <div class="loader"></div>
                        </div>
                        <div id="optins-tab" role="tabpanel" class="tab-pane fade optins-view">
                            <br />
                            <div class="loader"></div>
                        </div>
                        <div id="deletions-tab" role="tabpanel" class="tab-pane fade deletions-view">
                            <br />
                            <div class="loader"></div>
                        </div>
                        <div id="personaldata-tab" role="tabpanel" class="tab-pane fade">
							<?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_personaldata.tpl'); ?>
                        </div>
                        <div id="datasecurity-tab" role="tabpanel" class="tab-pane fade">
							<?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_datasecurity.tpl'); ?>
                        </div>
						<div id="cookieconsent-tab" role="tabpanel" class="tab-pane fade">
							<?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_cookieconsent.tpl'); ?>
                        </div>
                        <div id="home-tab" role="tabpanel" class="tab-pane fade">
							<?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_dashboard.tpl'); ?>
                        </div>
                        <div id="isense-support" role="tabpanel" class="tab-pane fade">
                            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_support.tpl'); ?>
                        </div>
                    </div>

                </div>
            </div><!--.panel-->
        </form> 
        
    </div>
  </div>
</div>
<script type="text/javascript">
var storeId                                 = '<?php echo $storeId; ?>';
var domain                                  = '<?php echo $domain; ?>';
var domainraw                               = '<?php echo $domainRaw; ?>';
var timenow                                 = '<?php echo $timeNow; ?>';
var MID                                     = '<?php echo $mid; ?>';
var token                                   = '<?php echo $token; ?>';
var token_string                            = '<?php echo $token_string; ?>';
var moduleName                              = '<?php echo $moduleName; ?>';
var modulePath                              = '<?php echo $modulePath; ?>';
var text_loading_data						= '<?php echo $text_loading_data; ?>';
var text_cancel								= '<?php echo $text_cancel; ?>';
var text_confirm							= '<?php echo $text_confirm; ?>';
var text_confirm_action						= '<?php echo $text_confirm_text; ?>';
var text_irreversible_action				= '<?php echo $text_irreversible_action; ?>';
var text_confim_window_heading				= '<?php echo $text_confim_window_heading; ?>';
var text_data_deletion_warning				= '<?php echo $text_data_deletion_warning; ?>';
var text_deny_deletion_success				= '<?php echo $text_deny_deletion_success; ?>';
var text_unexpected_error					= '<?php echo $text_unexpected_error; ?>';
var text_data_deletion_date_warning			= '<?php echo $text_data_deletion_date_warning; ?>';
var text_successful_deletion_approval		= '<?php echo $text_successful_deletion_approval; ?>';
var text_successful_removal					= '<?php echo $text_successful_removal; ?>';
</script> 
<?php echo $footer; ?>