<div class="container-fluid">
    <h3><?php echo $text_access_to_personal_data; ?></h3>
    <p><?php echo $text_access_to_personal_data_helper; ?></p>
	<ul class="nav nav-tabs" id="langtabs" role="tablist">
	  <?php foreach ($languages as $language) { ?>
		<li><a href="#lang-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?></a></li>
	  <?php } ?>
	</ul>
	<div class="tab-content">
	  <?php foreach ($languages as $language) { ?>
		<div class="tab-pane" id="lang-<?php echo $language['language_id']; ?>">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<h5><span class="required">*</span> <strong><?php echo $text_third_party_services; ?></strong></h5>
					<span class="help"><?php echo $text_third_party_services_helper; ?></span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<textarea name="<?php echo $moduleName; ?>[ThirdPartyServices][<?php echo $language['language_id']; ?>]" class="form-control" rows="4"><?php echo (isset($moduleData['ThirdPartyServices'][$language['language_id']])) ? $moduleData['ThirdPartyServices'][$language['language_id']] : ''; ?></textarea>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<h5><strong><?php echo $text_other_services; ?></strong></h5>
					<span class="help"><?php echo $text_other_services_helper; ?></span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<textarea name="<?php echo $moduleName; ?>[OtherServices][<?php echo $language['language_id']; ?>]" class="form-control" rows="4"><?php echo (isset($moduleData['OtherServices'][$language['language_id']])) ? $moduleData['OtherServices'][$language['language_id']] : ''; ?></textarea>
				</div>
			</div>
		</div>
	  <?php } ?>
	</div>
	<hr /><br />
</div>