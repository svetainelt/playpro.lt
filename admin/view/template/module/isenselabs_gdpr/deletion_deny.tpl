<div class="container-fluid deny-form">
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="step"><?php echo $text_cancel_deletion_header; ?></h3>
		</div>
    </div>    
    <br />    
    <?php if (!$deny_form) { ?>
    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h4><?php echo $text_unexpected_error; ?></h4>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="pull-right">
					<a onclick="goBack(this);" class="btn btn-danger"><?php echo $text_go_back; ?></a>
				</div>
			</div>
		</div>  
    <?php } else { ?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h5><strong><?php echo $text_email_request; ?></strong>: <?php echo $deletion_data['email']; ?></h5>
				<h5><strong><?php echo $text_date_time_request; ?></strong>: <?php echo $deletion_data['date_added']; ?></h5>
				<h5><strong><?php echo $text_language_request; ?></strong>: <?php echo $language_data['name']; ?></h5>
				<h5><strong><?php echo $text_store_request; ?></strong>: <?php echo $store_data['name']; ?></h5>
				<br />

				<h5><?php echo $text_deletion_deny_helper; ?> <strong><?php echo $deletion_data['email']; ?></strong></h5>
				<h5><?php echo $text_data_deletion_helper; ?></h5>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<br />
				<input name="deletion_id" type="hidden" value="<?php echo $deletion_data['deletion_id']; ?>" />
				<textarea name="deny_request_text" rows="7" id="deny_request_text" placeholder="<?php echo $text_message; ?>" class="form-control"></textarea>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="pull-right">
					<br />
					<a onclick="denyDeletionRequest(this);" class="btn btn-primary"><?php echo $text_cancel_deletion; ?></a>&nbsp;&nbsp;<a onclick="goBack(this);" class="btn btn-danger"><?php echo $text_go_back; ?></a>
				</div>
			</div>
		</div>
    <?php } ?>
</div>