<div class="acceptances-filter-data well">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
           <label for="acceptance_id"><?php echo $column_acceptance_id; ?></label>
            <input name="acceptance_id" id="acceptance_id" class="form-control acceptances_input" value="<?php echo $filter_data['acceptance_id']; ?>" />
        </div>
        <div class="form-group">
            <label for="email"><?php echo $column_email; ?></label>
            <input name="email" id="email" class="form-control acceptances_input" value="<?php echo $filter_data['email']; ?>" />
        </div>
        <div class="form-group">
            <label for="name"><?php echo $column_name; ?></label>
            <input name="name" id="name" class="form-control acceptances_input" value="<?php echo $filter_data['name']; ?>" />
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <label for="date_start"><?php echo $text_date_start; ?></label>
            <input name="date_start" id="date_start" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_start']; ?>" />
        </div>
        <div class="form-group">
            <label for="date_end"><?php echo $text_date_end; ?></label>
            <input name="date_end" id="date_end" class="form-control date" data-date-format="YYYY-MM-DD" value="<?php echo $filter_data['date_end']; ?>" />
        </div>
        <div class="form-group text-right">
             <br />
             <button class="btn btn-acceptances-export btn-default" onClick="exportAcceptances(this, event);"><?php echo $text_btn_export; ?></button>
             <button class="btn btn-acceptances-filter btn-primary" onClick="filterAcceptances(this, event);"><?php echo $text_filter; ?></button>
        </div>
    </div>         
    <div class="clearfix"></div>            
</div>
<table class="table table-striped table-acceptances-results table-hover"> 
    <thead>
        <tr>
            <th><?php echo $column_acceptance_id; ?></th> 
            <th><?php echo $column_email; ?></th> 
            <th><?php echo $column_name; ?></th>
            <th><?php echo $column_content; ?></th>
            <th><?php echo $column_user_agent; ?></th>
            <th><?php echo $column_accept_language; ?></th>
            <th><?php echo $column_client_ip; ?></th>
            <th class="text-right"><?php echo $column_date; ?></th> 
            <th class="text-right"><?php echo $column_actions; ?></th> 
        </tr> 
    </thead> 
    <tbody>
        <?php if (count($sources)> 0) { ?> 
            <?php foreach ($sources as $index => $result) { ?>
            <tr> 
                <td><?php echo $result['acceptance_id']; ?></td> 
                <td><?php echo $result['email']; ?></td> 
                <td><?php echo $result['name']; ?></td> 
                <td><?php echo $result['content']; ?></td> 
                <td style="word-break: break-word; width: 20%"><?php echo $result['user_agent']; ?></td> 
                <td><?php echo $result['accept_language']; ?></td> 
                <td><?php echo $result['client_ip']; ?></td> 
                <td class="text-right"><?php echo $result['date_added']; ?></td> 
				<td class="text-right"><a class="btn btn-small btn-default" href="<?php echo $result['link']; ?>" target="_blank" data-toggle="tooltip" title="" data-original-title="<?php echo $text_view_current; ?>"><i class="fa fa-pencil"></i></a>&nbsp;<a class="btn btn-small btn-primary"  href="<?php echo $result['download']; ?>" target="_blank" data-toggle="tooltip" title="" data-original-title="<?php echo $text_download; ?>"><i class="fa fa-eye"></i></a></td> 
            </tr> 
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="9" class="text-center"><?php echo $text_no_results; ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
    	        <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});
    
// input auto-send query
$('.acceptances_input').on('keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('.btn-acceptances-filter').trigger('click');
    }
});
</script>