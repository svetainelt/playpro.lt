<div class="container-fluid">
	<h2><?php echo $text_steps_gdpr_compliant; ?></h2>
	<hr />
	<h4><?php echo $text_gdpr_checklist_helper; ?></h4>

	<ul class="checklist">
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox1]" value="1" <?php echo (!empty($moduleData['Checkbox1']) && $moduleData['Checkbox1'] == '1') ? 'checked="checked"' : '' ?> id="checkbox1"/>
				<label for="checkbox1">
					<strong><?php echo $text_gdpr_check_1; ?></strong>
					<p><?php echo $text_gdpr_check_1_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox2]" value="1" <?php echo (!empty($moduleData['Checkbox2']) && $moduleData['Checkbox2'] == '1') ? 'checked="checked"' : '' ?> id="checkbox2"/>
				<label for="checkbox2">
					<strong><?php echo $text_gdpr_check_2; ?></strong>
					<p><?php echo $text_gdpr_check_2_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox3]" value="1" <?php echo (!empty($moduleData['Checkbox3']) && $moduleData['Checkbox3'] == '1') ? 'checked="checked"' : '' ?> id="checkbox3"/>
				<label for="checkbox3">
					<strong><?php echo $text_gdpr_check_3; ?></strong>
					<p><?php echo $text_gdpr_check_3_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox4]" value="1" <?php echo (!empty($moduleData['Checkbox4']) && $moduleData['Checkbox4'] == '1') ? 'checked="checked"' : '' ?> id="checkbox4"/>
				<label for="checkbox4">
					<strong><?php echo $text_gdpr_check_4; ?></strong>
					<p><?php echo $text_gdpr_check_4_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox5]" value="1" <?php echo (!empty($moduleData['Checkbox5']) && $moduleData['Checkbox5'] == '1') ? 'checked="checked"' : '' ?> id="checkbox5"/>
				<label for="checkbox5">
					<strong><?php echo $text_gdpr_check_5; ?></strong>
					<p><?php echo $text_gdpr_check_5_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox6]" value="1" <?php echo (!empty($moduleData['Checkbox6']) && $moduleData['Checkbox6'] == '1') ? 'checked="checked"' : '' ?> id="checkbox6"/>
				<label for="checkbox6">
					<strong><?php echo $text_gdpr_check_6; ?></strong>
					<p><?php echo $text_gdpr_check_6_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox7]" value="1" <?php echo (!empty($moduleData['Checkbox7']) && $moduleData['Checkbox7'] == '1') ? 'checked="checked"' : '' ?> id="checkbox7"/>
				<label for="checkbox7">
					<strong><?php echo $text_gdpr_check_7; ?></strong>
					<p><?php echo $text_gdpr_check_7_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox8]" value="1" <?php echo (!empty($moduleData['Checkbox8']) && $moduleData['Checkbox8'] == '1') ? 'checked="checked"' : '' ?> id="checkbox8"/>
				<label for="checkbox8">
					<strong><?php echo $text_gdpr_check_8; ?></strong>
					<p><?php echo $text_gdpr_check_8_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox9]" value="1" <?php echo (!empty($moduleData['Checkbox9']) && $moduleData['Checkbox9'] == '1') ? 'checked="checked"' : '' ?> id="checkbox9"/>
				<label for="checkbox9">
					<strong><?php echo $text_gdpr_check_9; ?></strong>
					<p><?php echo $text_gdpr_check_9_helper; ?></p>
				</label>
			</div>
		</li>
		<li>
			<div class="checklist-success">
				<input type="checkbox" name="<?php echo $moduleName; ?>[Checkbox10]" value="1" <?php echo (!empty($moduleData['Checkbox10']) && $moduleData['Checkbox10'] == '1') ? 'checked="checked"' : '' ?> id="checkbox10"/>
				<label for="checkbox10">
					<strong><?php echo $text_gdpr_check_10; ?></strong>
					<p><?php echo $text_gdpr_check_10_helper; ?></p>
				</label>
			</div>
		</li>
	</ul>
</div>