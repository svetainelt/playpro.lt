<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $text_deny_deletion_subject; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000; text-align:center;">
<div style="width: 680px; text-align: center;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_main_message; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_greeting; ?></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_customer_data; ?> - <strong><?php echo $data['customer_data'] ? $text_yes : $text_no; ?></strong></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_address_data; ?> - <strong><?php echo $data['address_data'] ? $text_yes : $text_no; ?></strong></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_order_data; ?> - <strong><?php echo $data['order_data'] ? $text_yes : $text_no; ?></strong></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_gdpr_data; ?> - <strong><?php echo $data['gdpr_data'] ? $text_yes : $text_no; ?></strong></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo $text_column_deletion_date; ?> - <strong><?php echo $data['date_deletion']; ?></strong></p>
  <p style="margin-top: 0px; margin-bottom: 20px;text-align: left;"><?php echo !empty($data['message']) ? $text_custom_message . ' - <strong>' . $data['message'] . '</strong>' : ''; ?></p>
</div>
</body>
</html>