<div class="tab-pane" id="notification_message" >
	<div>
		<h3 style="margin-left:7px"><?php echo $section_teaser_bar; ?></h3>

		<table class="table gt-widget-design">
		<tr>
			<td class="col-xs-3">
				<h5><strong><?php echo $free_gifts_bar_status; ?></strong></h5>
				<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $free_gifts_bar_status_help; ?></span>
			</td>
			<td class="col-xs-9">
				<div class="col-xs-4">
					<select name="giftteaser[showFreeGift]" class="form-control">
						<option value="yes" <?php echo (empty($data['giftteaser']['showFreeGift']) || $data['giftteaser']['showFreeGift'] == 'yes') ? 'selected=selected' : ''; ?>><?php echo $text_enabled;?></option>
						<option value="no"  <?php echo (!empty($data['giftteaser']['showFreeGift']) && $data['giftteaser']['showFreeGift']== 'no') ? 'selected=selected' : ''; ?>><?php echo $text_disabled;?></option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td class="col-xs-3">
				<h5><strong><?php echo $free_gifts_bar_title; ?></strong></h5>
				<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $free_gifts_bar_title_help; ?></span>
			</td>
			<td class="col-xs-9">
				<div class="col-xs-12 form-group">
					<?php foreach ($languages as $language) { ?>
					<div class="input-group col-xs-6" style="padding-left:0px !important; margin:10px 0" >
						<span class="input-group-addon"><a href="#language_<?php echo $language["code"]; ?>" ><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>" /></a></span>
						<input class="form-control" name="giftteaser[notification_freeGift_<?php echo $language["code"]; ?>]" value="<?php if(isset($data['giftteaser']['notification_freeGift_'.$language["code"]])) { echo $data['giftteaser']['notification_freeGift_'.$language["code"]]; } else { echo "Get this product for FREE!"; } ?>" >
					</div>
					<?php } ?>
				</div>
			</td>
		</tr>
		<tr>
			<td class="col-xs-3" style="vertical-align:top; padding-top: 25px">
				<h5><strong><?php echo $free_gifts_bar_design; ?></strong></h5>
			</td>
			<td class="col-xs-9">
				<div class="col-xs-4 form-group">
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="note_font_color">
							<span class="input-group-addon"><?php echo $text_text;?></span>
							<input name="giftteaser[NoteFontColor]" class="form-control colorp" type="text" value="<?php if(!empty($data['giftteaser']['NoteFontColor'])){ echo $data['giftteaser']['NoteFontColor']; } else { echo  '#333333'; } ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="note_bg_color">
							<span class="input-group-addon"><?php echo $text_background;?></span>
							<input name="giftteaser[NoteBackgroundColor]" class="form-control colorp" type="text" value="<?php  if(!empty($data['giftteaser']['NoteBackgroundColor'])) { echo $data['giftteaser']['NoteBackgroundColor']; } else { echo  '#b0ffdf'; }  ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="note_border_color">
							<span class="input-group-addon"><?php echo $text_border;?></span>
							<input name="giftteaser[NoteBorderColor]" class="form-control colorp" type="text" value="<?php  if(!empty($data['giftteaser']['NoteBorderColor'])){ echo $data['giftteaser']['NoteBorderColor']; } else { echo '#0d955e';} ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div>
		<h3 style="margin-left:7px"><?php echo $section_widget_module; ?></h3>

		<table class="table gt-widget-design">
		<tr>
			<td class="col-xs-3">
				<h5><strong><?php echo $gift_image_size; ?></strong></h5>
				<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $gift_image_size_help; ?></span>
			</td>
			<td class="col-xs-9">
				<div class="col-xs-4">
					<div class="input-group">
						<input type="text" value="<?php if(!empty($data['giftteaser']['giftImageWidth'])) echo (int)$data['giftteaser']['giftImageWidth']; else echo 120; ?>"  name="giftteaser[giftImageWidth]" class="form-control brSmallField" style="text-align:center;">
						<span class="input-group-addon">X</span>
						<input type="text" value="<?php if(!empty($data['giftteaser']['giftImageHeight'])) echo (int)$data['giftteaser']['giftImageHeight']; else echo 120; ?>"  name="giftteaser[giftImageHeight]" class="form-control brSmallField" style="text-align:center;">
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="col-xs-3">
				<h5><strong><?php echo $wrap_in_widget; ?></strong></h5>
			</td>
			<td class="col-xs-9">
				<div class="form-group col-xs-4">
					<select name="giftteaser[widget]" class="form-control">
						<option value="yes" <?php echo (!empty($data['giftteaser']['widget']) && $data['giftteaser']['widget'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_yes;?></option>
						<option value="no"  <?php echo (empty($data['giftteaser']['widget']) || $data['giftteaser']['widget']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_no;?></option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td class="col-xs-3">
				<h5><strong><?php echo $widget_design; ?></strong></h5>
			</td>
			<td class="col-xs-9">
				<div class="form-group col-xs-4">
					<select name="giftteaser[customDesign]" class="form-control">
						<option value="custom" <?php echo (!empty($data['giftteaser']['customDesign']) && $data['giftteaser']['customDesign'] == 'custom') ? 'selected=selected' : '' ?>><?php echo $text_custom;?></option>
						<option value="default"  <?php echo (empty($data['giftteaser']['customDesign']) || $data['giftteaser']['customDesign']== 'default') ? 'selected=selected' : '' ?>><?php echo $entry_default;?></option>
					</select>
				</div>
			</td>
		</tr>
		<tr id="custom_settings" class="customCssForm hidden hiddeable">
			<td class="col-xs-3" style="vertical-align:top; padding-top: 25px">
				<h5><strong><?php echo $widget_design_custom; ?></strong></h5>
				<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $widget_design_custom_help; ?></span>
			</td>
			<td class="col-xs-9">
				<div class=" col-xs-5">
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="widget_font_color">
							<span class="input-group-addon"><?php echo $text_text;?></span>
							<input name="giftteaser[FontColor]" class="form-control colorp" type="text" value="<?php echo (isset($data['giftteaser']['FontColor']) ? $data['giftteaser']['FontColor'] : '#444444') ?> ">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="widget_bg_color">
							<span class="input-group-addon"><?php echo $text_background;?></span>
							<input name="giftteaser[BackgroundColor]" class="form-control colorp" type="text" value="<?php  if(!empty($data['giftteaser']['BackgroundColor'])) { echo $data['giftteaser']['BackgroundColor']; } else { echo  '#fbfbfb'; }  ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="widget_border_color">
							<span class="input-group-addon"><?php echo $text_border;?></span>
							<input name="giftteaser[BorderColor]" class="form-control colorp" type="text" value="<?php  if(!empty($data['giftteaser']['BorderColor'])){ echo $data['giftteaser']['BorderColor']; } else { echo '#cccccc';} ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
					<div class="customDesignColors" style="margin-bottom:10px">
						<div class="input-group" id="widget_head_bg_color">
							<span class="input-group-addon"><?php echo $heading_background;?></span>
							<input name="giftteaser[headingBackground]" class="form-control colorp" type="text" value="<?php if(!empty($data['giftteaser']['headingBackground'])){ echo $data['giftteaser']['headingBackground']; } else { echo  '#bbbbbb'; } ?>">
							<span class="input-group-addon"><i class="colorpicker_box_color"></i></span>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="col-xs-3" style="vertical-align:top; padding-top: 25px">
				<h5><strong><?php echo $widget_information; ?></strong></h5>
				<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $widget_information_help; ?></span>
			</td>
			<td class="col-xs-9">
				<div class="tabbable">
					<div class="tab-navigation">
						<div class="col-xs-12">
						<ul class="nav nav-tabs notificationMessageTabs" style="margin-top:10px;">
							<?php $class="active"; foreach ($languages as $lang) :  ?>
								<li class="<?php echo $class; $class='';?>"><a href="#lang_<?php echo $lang["code"]; ?>" data-toggle="tab"><img src="<?php echo $lang['flag_url']; ?>" title="<?php echo $lang['name']; ?>" /></a></li>
							<?php endforeach; ?>
						</ul>
						</div>
					</div><!-- /.tab-navigation -->
					<div class="tab-content">
						<?php $class="active"; foreach ($languages as $lang) : ?>
						<div class="tab-pane <?php echo $class; $class='';?> " id="lang_<?php echo $lang["code"]; ?>">
							<div class="col-xs-12">
								<input type="text" placeholder="Head title" class="form-control messageSubject"  name="giftteaser[headtitle_<?php echo $lang["code"];?>]" value="<?php if(!empty($data['giftteaser']['headtitle_'.$lang["code"]])) { echo $data['giftteaser']['headtitle_'.$lang["code"]]; } else { echo $default_heading_title; }?>"/>
								<textarea class="summernote" id="message_<?php echo $lang["code"]; ?>" name="giftteaser[notification_<?php echo $lang["code"]; ?>]"><?php if(isset($data['giftteaser']['notification_'.$lang["code"]])) { echo $data['giftteaser']['notification_'.$lang["code"]]; }?></textarea>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</td>
		</tr>
		</table>
	</div>

	<div style="margin-top:30px">
		<h3 style="margin-left:7px"><?php echo $section_design; ?></h3>

		<table class="table gt-widget-design">
		<tr class="customCssForm">
			<td class="col-xs-3" style="vertical-align:top; padding-top: 25px">
				<h5><strong><?php echo $custom_css; ?></strong></h5>
			</td>
			<td class="col-xs-9">
				<div class="col-xs-12">
					<textarea rows="12" class="form-control customCss" name="giftteaser[customCss]" placeholder="Place your custom CSS here..."><?php if(!empty($data['giftteaser']['customCss'])){ echo $data['giftteaser']['customCss']; } else { echo ".giftTeaserWidget {  }";} ?></textarea>
				</div>
			</td>
		</tr>
		</table>
	</div>
</div>

<br />
<script type="text/javascript">
if ($('select[name="giftteaser[customDesign]"]').val() == 'default') {
	$('.hiddeable').addClass('hidden');
} else {
	$('.hiddeable').removeClass('hidden');
}
$(document).on('change', 'select[name="giftteaser[customDesign]"]', function(){
	if ($('select[name="giftteaser[customDesign]"]').val() == 'default') {
		$('.hiddeable').addClass('hidden');
	} else {
		$('.hiddeable').removeClass('hidden');
	}
});

var colorpicker_change = function(input, box, color) {
	if (input !== false) {
		$(input).val(color);
	}
	$(box).css('background-color', color);
}
var colorPickersForInitialization = ['#widget_font_color','#widget_bg_color','#widget_border_color','#widget_head_bg_color','#note_font_color','#note_bg_color','#note_border_color'];
colorPickersForInitialization.forEach(function(cp) {
	$(cp).ColorPicker({
		color: $(cp).find('input').val(),
		onChange : function(hsb, hex, rgb) {
			colorpicker_change($(cp).find('input'), $(cp).find('.colorpicker_box_color'), '#' + hex);
		}
	});
});
<?php
	$colorPropsToDom = array(
		'FontColor' => '#widget_font_color',
		'BackgroundColor' => '#widget_bg_color',
		'BorderColor' => '#widget_border_color',
		'headingBackground' => '#widget_head_bg_color',
		'NoteFontColor' => '#note_font_color',
		'NoteBackgroundColor' => '#note_bg_color',
		'NoteBorderColor' => '#note_border_color'
	);
	foreach ($colorPropsToDom as $property => $selector) {
		if (!empty($data['giftteaser'][$property])) { ?>
			colorpicker_change($('<?= $selector ?>').find('input'), $('<?= $selector ?>').find('.colorpicker_box_color'), '<?php echo $data['giftteaser'][$property]; ?>');		
		<?php }
	}
?>
$('#cp_color, #cp_bgcolor').find('input').change(function() {
	colorpicker_change(false, $(this).closest('.colorpicker-component').find('.colorpicker_box_color'), $(this).val());
});
$('input, textarea').focus(function() {
	$('#cp_color, #cp_bgcolor').ColorPickerHide();
	$(this).trigger('click');
})
</script>
