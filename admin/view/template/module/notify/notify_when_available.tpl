<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit ?></h3>
        <?php if(isset($new_version_notification)){ ?>
        <span class="badge badge-danger pull-right" style="font-size:10px;font-weight:normal !important;"><?php echo $new_version_notification ?></span>
        <?php } ?>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general?></a></li>
            <li><a href="#tab_notify_product" data-toggle="tab"><?php echo $tab_notify_product?></a></li>
            <li><a href="#tab-email" id="tab3"  data-toggle="tab"><?php echo $tab_email?></a></li>
            <li><a href="#tab-support" data-toggle="tab">Support</a></li>
            <li class=""><a href="#tab_version_history" data-toggle="tab">Version History<span class="badge badge-light"><?php echo $software_version?></span></a></li>
            
          </ul>
          <div class="tab-content">

            <div class="tab-pane" id="tab_version_history">
                  <?php foreach($version_histories as $history){ ?>
                  <div class="panel panel-body">
                    <h3><?php echo $history['title'] ?></h3>
                    <hr>
                    <div><?php echo $history['description'] ?></div>
                  </div>
                  <?php } ?>

                </div>

            <div class="tab-pane" id="tab-email">

<h3><?php echo $heading_email_to_customer?></h3>
<hr>

          <ul class="nav nav-tabs" id="mobule_nwa">
                <?php foreach($languages as $language){ ?>
                <li><a href="#notify_language<?php echo $language['language_id'] ?>" data-toggle="tab"><img src="language/<?php echo $language['code'] ?>/<?php echo $language['code'] ?>.png" title="<?php echo $language['name'] ?>" /> <?php echo $language['name'] ?></a></li>
                <?php } ?>
              </ul>

              <div class="tab-content">
           
              <?php foreach($languages as $language){ ?>
                <div class="tab-pane" id="notify_language<?php echo $language['language_id'] ?>">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="module_notify_when_available_email_subject<?php echo $language['language_id']?>">
              <span data-toggle="tooltip" title="<?php echo $help_email_subject?>"><?php echo $entry_email_subject?></span></label>
            <div class="col-sm-10">
              <input type="text" name="module_notify_when_available_email_subject[<?php echo $language['language_id']?>][subject]" value="<?php echo $module_notify_when_available_email_subject[$language['language_id']]['subject']?>" placeholder="<?php echo $entry_email_subject ?>" id="module_notify_when_available_email_subject<?php echo $language['language_id']?>" class="form-control" />
             <?php if(isset($error_module_notify_when_available_email_subject[$language['language_id']]['subject'])) { ?>
              <span class="error text-danger"><?php echo $error_module_notify_when_available_email_subject[$language['language_id']]['subject'] ?></span>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
          <label class="col-sm-2 control-label" for="module_notify_when_available_email_body<?php echo $language['language_id']?>"><span data-toggle="tooltip" title="<?php echo $help_email_body?>"><?php echo $entry_email_body?></span>

          <span class="pre"><br><br>
            Below are the cheat code of dynamic value
            <p>{name} - Name<br>
            {product_url} - Product URL<br>
            {product_name} - Product Name<br>
          </p></span>

          </label>
          <div class="col-sm-10">
            <textarea name="module_notify_when_available_email_body[<?php echo $language['language_id']?>][body]" id="module_notify_when_available_email_body<?php echo $language['language_id']?>" class="form-control" ><?php echo $module_notify_when_available_email_body[$language['language_id']]['body']?></textarea>
           <?php if(isset($error_module_notify_when_available_email_body[$language['language_id']]['body'])) { ?>
              <span class="error text-danger"><?php echo $error_module_notify_when_available_email_body[$language['language_id']]['body'] ?></span>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php } ?>
    </div>

            </div>



            <div class="tab-pane " id="tab_notify_product">
              <div class="row">

                <div class="col-lg-12" id="product_body">
                </div>

         
              </div>
            </div>














            <div class="tab-pane " id="tab-support">

      <div class="panel-body">
        <div class="row">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left">Owner</td>
                  <td class="text-center">:</td>
                  <td class="text-left">Webtuning Technology</td>
                </tr>
                <tr>
                  <td class="text-left">Developer</td>
                  <td class="text-center">:</td>
                  <td class="text-left">Waseem Asgar</td>
                </tr>
                <tr>
                  <td class="text-left">Skype</td>
                  <td class="text-center">:</td>
                  <td class="text-left">er.waseem123</td>
                </tr>
                <tr>
                  <td class="text-left">Email</td>
                  <td class="text-center">:</td>
                  <td class="text-left"><a href="mailto:waseem@webtuningtechnology.com" target="_blank">waseem@webtuningtechnology.com</a></td>
                </tr>
                <tr>
                  <td class="text-left">Whatsapp</td>
                  <td class="text-center">:</td>
                  <td class="text-left">+91-956-095-9291</td>
                </tr>
                <tr>
                  <td class="text-left">Official Website</td>
                  <td class="text-center">:</td>
                  <td class="text-left"><a href="http://webtuningtechnology.com" target="_blank">webtuningtechnology.com</a></td>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>


      </div>
    </div>

    </div>
 


            <div class="tab-pane active" id="tab-general">

          <div class="form-group">
            <label class="col-sm-2 control-label" for="module_notify_when_available_status"><?php echo $entry_status ?></label>
            <div class="col-sm-10">
              <select name="module_notify_when_available_status" id="module_notify_when_available_status" class="form-control">
               <?php if($module_notify_when_available_status){ ?>
                <option value="1" selected="selected"><?php echo $text_enabled ?></option>
                <option value="0"><?php echo $text_disabled ?></option>
                <?php }else{ ?>
                <option value="1"><?php echo $text_enabled ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled ?></option>
                <?php } ?>
              </select>
            </div>
          </div>


           <div class="form-group">
            <label class="col-sm-2 control-label" for="module_notify_when_available_stock"><?php echo $entry_out_of_stock ?></label>
            <div class="col-sm-10">
              <select name="module_notify_when_available_stock" id="module_notify_when_available_stock" class="form-control">
                <?php if($module_notify_when_available_stock){ ?>}
                <option value="1" selected="selected"><?php echo $text_enabled ?></option>
                <option value="0"><?php echo $text_disabled ?></option>
                <?php }else{ ?>
                <option value="1"><?php echo $text_enabled ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="module_notify_when_available_notify"><?php echo $entry_notify_button ?></label>
            <div class="col-sm-10">
              <select name="module_notify_when_available_notify" id="module_notify_when_available_notify" class="form-control">
                <?php if($module_notify_when_available_notify){ ?>
                <option value="1" selected="selected"><?php echo $text_enabled ?></option>
                <option value="0"><?php echo $text_disabled ?></option>
                <?php }else{ ?>
                <option value="1"><?php echo $text_enabled ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

        </div>
      </div>
          
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>

<script type="text/javascript"><!--
  $('#tab_notify_product').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#product_body').load(this.href);
  });

  $('#product_body').load('index.php?route=module/notify_when_available/products&token=<?php echo $token ?>');
  //--></script> 


<?php foreach($languages as $language){ ?>
<script type="text/javascript"><!--
$("#module_notify_when_available_email_body<?php echo $language['language_id'] ?>").summernote({
  height: 200,
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
  ]
});
$("#mobule_nwa_admin_email_body<?php echo $language['language_id'] ?>").summernote({
  height: 200,
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
  ]
});
</script>
<?php } ?>

<script type="text/javascript"><!--
$('#mobule_nwa a:first').tab('show');
$('#mobule_nwa_admin a:first').tab('show');
//--></script>


<?php echo $footer; ?>