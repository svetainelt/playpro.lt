<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class="text-left"><?php echo $column_customer ?></td>
        <td class="text-left"><?php echo $column_product ?></td>  
        <td class="text-left"><?php echo $column_message ?></td>
        <td class="text-left"><?php echo $column_date_added ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if(count($products)>0){ ?>
      <?php foreach($products as $product){ ?>
      <tr>

        <td class="text-left" style="max-width:200px;">
          <table class="table table-bordered table-hover"  style="min-width:250px;float:left;">
              <tr>
                <td>
                  <?php if($product['customer_id'] > 0){ ?>
                  #<?php echo $product['customer_id']?> <a type="button" class="register btn btn-success badge badge-light"><?php echo $text_register?></a>
                  <?php }else{ ?>
                  <a type="button" class="register btn btn-success badge badge-light"><?php echo $text_unregister ?></a>
                  <?php } ?>
                </td>
              </tr>
              <tr><td><span class="table_icons"><i class="fa fa-user"></i></span> <?php echo $product['customer_name']?></td></tr>
              <tr><td><span class="table_icons"><i class="fa fa-envelope"></i></span> <?php echo $product['customer_email'] ?></td></tr>
              <tr><td><span class="table_icons"><i class="fa fa-cart"></i></span> <?php echo $product['store_name'] ?></td></tr>
              <tr><td><span class="table_icons"><i class="fa fa-cart"></i></span> <?php echo $product['language'] ?></td></tr>
              <?php if($product['customer_id'] > 0){ ?>
              <tr><td><a type="button" class="btn btn-success badge badge-light" href="<?php echo $product['customer_edit'] ?>" target="_blank"><?php echo $text_show_customer ?></a></td></tr>
              <?php } ?>
          </table>
        </td>


        <td class="text-left cart_product">
          <table class="table table-bordered table-hover" style="min-width:150px;float:left;">
            <tr>
              <td class="text-center"><a href="<?php echo $product['product_edit'] ?>" target="_blank"><?php echo $product['product_name'] ?></a>
              </td>
            </tr>
            <tr>
            
            <td class="text-center">

              <?php if($product['product_thumb']){ ?>
              <a href="<?php echo $product['product_edit'] ?>" target="_blank"><img src="<?php echo $product['product_thumb'] ?>" alt="<?php echo $product['product_name'] ?>" title="<?php echo $product['product_name'] ?>" class="img-thumbnail" /></a>
              <?php } ?>

            </td>
          </tr>
          </table>
        </td>

        <td class="text-left"><?php echo $product['message'] ?></td>
        <td class="text-left"><?php echo $product['date_added'] ?></td>
      </tr>
      <?php } ?>
      <?php }else{ ?>
      <tr>
        <td class="text-center" colspan="4"><?php echo $text_no_results ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination ?></div>
  <div class="col-sm-6 text-right"><?php echo $results ?></div>
</div>
