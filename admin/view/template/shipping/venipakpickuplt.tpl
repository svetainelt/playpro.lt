<?php
echo $header, $column_left;
?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
	   <a href="<?php echo $refresh_data_href; ?>" data-toggle="tooltip" title="<?php echo $button_refresh; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
        <button type="submit" form="form-fedex" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
<?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>  <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-fedex" class="form-horizontal">
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-key"><?php echo $entry_freelimit; ?></label>
            <div class="col-sm-10">
              <input type="text" name="venipakpickuplt_freelimit" value="<?php echo $venipakpickuplt_freelimit; ?>" placeholder="<?php echo $entry_freelimit; ?>" id="input-key" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-show-all"><?php echo $entry_show_all; ?></label>
            <div class="col-sm-10">
              <select name="venipakpickuplt_show_all" id="input-show-all" class="form-control">
                <?php if ($venipakpickuplt_show_all == "Y") { ?>
                <option value="Y" selected="selected"><?php echo $text_show_all_yes; ?></option>
                <option value="N"><?php echo $text_show_all_no; ?></option>
                <?php } else { ?>
                <option value="Y"><?php echo $text_show_all_yes; ?></option>
                <option value="N" selected="selected"><?php echo $text_show_all_no; ?></option>
                 <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
            <div class="col-sm-10">
              <select name="venipakpickuplt_tax_class_id" id="input-tax-class" class="form-control">
                <option value="0"><?php echo $text_none; ?></option>
                <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $venipakpickuplt_tax_class_id) { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                   <?php } else { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                   <?php } ?>
               <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
            <div class="col-sm-10">
              <select name="venipakpickuplt_geo_zone_id" id="input-geo-zone" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                  <?php foreach ($geo_zones as $geo_zone) { ?>
                   <?php if ($geo_zone['geo_zone_id'] == $venipakpickuplt_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                   <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                 <?php } ?>
               <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="venipakpickuplt_status" id="input-status" class="form-control">
              <?php if ($venipakpickuplt_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="venipakpickuplt_sort_order" value="<?php echo $venipakpickuplt_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
		  
       <table id="venipakpickuplt-weight-shipping" class="list table">
          <thead>
            <tr>
              <td class="left"><span data-toggle="tooltip" title="<?php echo $entry_weight_from_tooltip; ?>"><?php echo $entry_weight_from; ?></span></td>
              <td class="left"><span data-toggle="tooltip" title="<?php echo $entry_weight_to_tooltip; ?>"><?php echo $entry_weight_to; ?></span></td>
              <td class="left"><?php echo $entry_first; ?></td>
              <td class="left"><?php echo $entry_next; ?></td>
              <td></td>
            </tr>
          </thead>
             <?php $row = 0; ?>
          <?php if (is_iterable($venipakpickuplt_weight_data)) { ?>
          <?php foreach ($venipakpickuplt_weight_data as $gtwd) { ?>
	          <tbody id="venipakpickuplt-weight-row-<?php echo $row; ?>">
	            <tr>	              
	              <td>
	              	<input class="form-control" type="text" size="4" maxlength="9" name="venipakpickuplt_weight_data[<?php echo $row; ?>][weight_from]" value="<?php echo number_format($gtwd['weight_from'], 2, '.', ''); ?>" />
	              </td>
	              
	              <td>
	              	<input class="form-control" type="text" size="4" maxlength="9" name="venipakpickuplt_weight_data[<?php echo $row; ?>][weight_to]" value="<?php echo number_format($gtwd['weight_to'], 2, '.', ''); ?>" />
	              </td>

	              <td>
	              	<input class="form-control" type="text" size="4" maxlength="9" name="venipakpickuplt_weight_data[<?php echo $row; ?>][cost]" value="<?php echo number_format($gtwd['cost'], 2, '.', ''); ?>" />
	              </td>
	              
	              <td>
	              	<input class="form-control" type="text" size="4" maxlength="9" name="venipakpickuplt_weight_data[<?php echo $row; ?>][each_next]" value="<?php echo number_format($gtwd['each_next'], 2, '.', ''); ?>" />
	              </td>

	              <td class="left">
	              	<a onclick="$('#venipakpickuplt-weight-row-<?php echo $row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a>
	              </td>
	            </tr>
	          </tbody>
	          <?php $row = $row + 1 ?>
            <?php } ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="8"></td>
              <td class="left"><a onclick="addRow();" class="btn btn-success"><?php echo $button_add; ?></a></td>
            </tr>
          </tfoot>
        </table>
		  
<script type="text/javascript"><!--
var row = <?php echo $row; ?>;

function addRow() {
	html  = '<tbody id="venipakpickuplt-weight-row-' + row + '">';
	html += '<tr>';
		html += '<td><input class="form-control" size="5" maxlength="9" type="text" name="venipakpickuplt_weight_data['+row+'][weight_from]" value="0.00" />';
		html += '<td><input class="form-control" size="5" maxlength="9" type="text" name="venipakpickuplt_weight_data['+row+'][weight_to]" value="0.00" />';
		html += '<td><input class="form-control" size="5" maxlength="9" type="text" name="venipakpickuplt_weight_data['+row+'][cost]" value="0.00" />';
	    html += '<td><input class="form-control" size="5" maxlength="9" type="text" name="venipakpickuplt_weight_data['+row+'][each_next]" value="0.00" />';
		html += '<td class="left"><a onclick="$(\'#venipakpickuplt-weight-row-' + row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
	html += '</tr>';
	html += '</tbody>';
	
	$('#venipakpickuplt-weight-shipping > tfoot').before(html);
		
	row++;
}
//--></script> 
			<table class="table table-bordered table-hover">
			  <thead>
				<tr>
					<td class="text-left">Place</td>
					<td class="text-left">Region</td>
					<td class="text-left">Accept Parcels</td>
				</tr>
			  </thead>
			  <tbody>
			       <?php if ($venipakpickuplt_places) { ?>
			        <?php foreach ($venipakpickuplt_places as $place) { ?>
				  <tr>
					<td class="text-left"><?php echo $place['dickup_place']; ?></td>
					<td class="text-left"><?php echo $place['dickup_region']; ?></td>
					<td class="text-left"><?php echo $place['dickup_status']; ?></td>
				  </tr>
				   <?php } ?>
				 <?php } else { ?>
				  <tr>
					<td class="center" colspan="2">Nothing found</td>
				  </tr>
				 <?php } ?>
			  </tbody>
		  </table>  
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>