<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a onclick="$('#do_update').val('1'); $('#form-omniva').submit();" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                <button type="submit" form="form-omniva" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-omniva" class="form-horizontal">
                    <input type="hidden" name="do_update" id="do_update" value="0" />
                    <div class="row">
                        <div class="col-sm-2">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                                <?php foreach ($tab_zones as $tab_zone) { ?>
                                    <li><a href="#tab-shipping-<?php echo $tab_zone['code']; ?>" data-toggle="tab"><?php echo $tab_zone['title']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-sm-10">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-general">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><?php echo $text_module_version; ?> <?php echo SHIPPING_OMNIVA_VERSION;?></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_dimension; ?>"><?php echo $entry_dimension; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="omniva_length" value="<?php echo $omniva_length; ?>" id="input-omniva-length" class="form-control" style="width: 50px; display: inline;" /> <input type="text" name="omniva_width" value="<?php echo $omniva_width; ?>" id="input-omniva-width" class="form-control" style="width: 50px; display: inline;" /> <input type="text" name="omniva_height" value="<?php echo $omniva_height; ?>" id="input-omniva-height" class="form-control" style="width: 50px; display: inline;" />&nbsp;&nbsp;<font style="color:#FF0000;font-size:14px">(<?php echo $default_length_class; ?>)</font>
                                            <?php if ($error_omniva_length) { ?>
                                                <span class="error"><?php echo $error_omniva_length; ?></span>
                                            <?php } ?>
                                            <?php if ($error_omniva_width) { ?>
                                                <span class="error"><?php echo $error_omniva_width; ?></span>
                                            <?php } ?>
                                            <?php if ($error_omniva_height) { ?>
                                                <span class="error"><?php echo $error_omniva_height; ?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-weight"><span data-toggle="tooltip" title="<?php echo $help_weight; ?>"><?php echo $entry_weight; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="omniva_weight" value="<?php echo $omniva_weight; ?>" id="input-weight" class="form-control" style="width: 50px; display: inline;" />&nbsp;&nbsp;<font style="color:#FF0000;font-size:14px">(<?php echo $default_weight_class; ?>)</font>
                                            <?php if ($error_omniva_weight) { ?>
                                                <span class="error"><?php echo $error_omniva_weight; ?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-import-url"><?php echo $entry_import_url; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="omniva_import_url" value="<?php echo $omniva_import_url; ?>" placeholder="<?php echo $entry_import_url; ?>" id="input-import-url" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                        <div class="col-sm-10">
                                            <select name="omniva_status" id="input-status" class="form-control">
                                                <?php if ($omniva_status) { ?>
                                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                    <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php } else { ?>
                                                    <option value="1"><?php echo $text_enabled; ?></option>
                                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="omniva_sort_order" value="<?php echo $omniva_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($tab_zones as $tab_zone) { ?>
                                    <div class="tab-pane" id="tab-shipping-<?php echo $tab_zone['code']; ?>">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-geo_zone_id-<?php echo $tab_zone['code']; ?>"><?php echo $entry_geo_zone; ?></label>
                                            <div class="col-sm-10">
                                                <select name="omniva_<?php echo $tab_zone['code']; ?>_geo_zone_id" id="input-geo_zone_id-<?php echo $tab_zone['code']; ?>" class="form-control">
                                                    <option value="0"><?php echo $text_select_zone; ?></option>
                                                    <?php foreach ($geo_zones as $geo_zone) { ?>
                                                        <?php if ($geo_zone['geo_zone_id'] == ${'omniva_' . $tab_zone['code'] . '_geo_zone_id'}) { ?>
                                                            <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-tax_class_id-<?php echo $tab_zone['code']; ?>"><?php echo $entry_tax_class; ?></label>
                                            <div class="col-sm-10">
                                                <select name="omniva_<?php echo $tab_zone['code']; ?>_tax_class_id" id="input-tax_class_id-<?php echo $tab_zone['code']; ?>" class="form-control">
                                                    <option value="0"><?php echo $text_none; ?></option>
                                                    <?php foreach ($tax_classes as $tax_class) { ?>
                                                        <?php if ($tax_class['tax_class_id'] == ${'omniva_' . $tab_zone['code'] . '_tax_class_id'}) { ?>
                                                            <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-cost-<?php echo $tab_zone['code']; ?>"><?php echo $entry_cost; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="omniva_<?php echo $tab_zone['code']; ?>_cost" value="<?php echo ${'omniva_' . $tab_zone['code'] . '_cost'}; ?>" id="input-cost-<?php echo $tab_zone['code']; ?>" class="form-control" style="width: 100px; display: inline;" /> 
                                                <select name="omniva_<?php echo $tab_zone['code']; ?>_cost_option" id="input-cost_option-<?php echo $tab_zone['code']; ?>" class="form-control" style="display: inline; width: 50%;">
                                                    <?php if (${'omniva_' . $tab_zone['code'] . '_cost_option'} == 'each_product') { ?>
                                                        <option value="each_product" selected="selected"><?php echo $text_each_product; ?></option>
                                                        <option value="all_cart"><?php echo $text_all_cart; ?></option>
                                                    <?php } else { ?>
                                                        <option value="each_product"><?php echo $text_each_product; ?></option>
                                                        <option value="all_cart" selected="selected"><?php echo $text_all_cart; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-status<?php echo $tab_zone['code']; ?>"><?php echo $entry_status; ?></label>
                                            <div class="col-sm-10">
                                                <select name="omniva_<?php echo $tab_zone['code']; ?>_status" id="input-status<?php echo $tab_zone['code']; ?>" class="form-control">
                                                    <?php if (${'omniva_' . $tab_zone['code'] . '_status'}) { ?>
                                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                        <option value="0"><?php echo $text_disabled; ?></option>
                                                    <?php } else { ?>
                                                        <option value="1"><?php echo $text_enabled; ?></option>
                                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>				
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><?php echo $entry_addresses; ?></label>
                                            <div class="col-sm-10">
                                                <?php if (!empty(${'omniva_' . $tab_zone['code'] . '_addresses'})) { ?> 
                                                    <?php foreach ( ${'omniva_' . $tab_zone['code'] . '_addresses'} as $k => $v) { ?> 
                                                        <?php echo $v; ?><br />
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?> 