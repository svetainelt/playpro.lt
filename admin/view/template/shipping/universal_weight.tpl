<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-universal-weight" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-universal-weight" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                        <div class="col-sm-10">
                            <select name="universal_weight_tax_class_id" id="input-tax-class" class="form-control">
                                <option value="0"><?php echo $text_none; ?></option>
                                <?php foreach ($tax_classes as $tax_class) { ?>
                                    <?php if ($tax_class['tax_class_id'] == $universal_weight_tax_class_id) { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="universal_weight_status" id="input-status" class="form-control">
                                <?php if ($universal_weight_status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="universal_weight_sort_order" value="<?php echo $universal_weight_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="shipping-rules" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-left">
                                        <?php echo $entry_title; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_flat_cost; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_min_price; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_max_price; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_min_weight ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_max_weight; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_geo_zone; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_status; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $entry_sort_order; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php echo $entry_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php $i = 0; foreach ($universal_weight_rules as $rule): $i++; ?>
                                    <tr>
                                        <td class="text-left">
                                            <?php foreach ($languages as $language): ?>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />
                                                    </span>                                            
                                                    <input type="text" name="universal_weight_rules[<?php echo $i; ?>][title][<?php echo $language['language_id']; ?>]" value="<?php echo isset($rule['title'][$language['language_id']]) ? $rule['title'][$language['language_id']] : ''; ?>" class="form-control" />
                                                </div>
                                            <?php endforeach; ?>
                                        </td>
                                        
                                        <td class="text-left">
                                            <div class="input-group">
                                                <? if($currency['symbol_left']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_left']; ?>
                                                    </span>
                                                <? endif; ?>
                                                
                                                <input type="text" name="universal_weight_rules[<?php echo $i; ?>][cost]" value="<?php echo $rule['cost']; ?>" class="form-control" />
                                                
                                                <? if($currency['symbol_right']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_right']; ?>
                                                    </span>
                                                <? endif; ?>
                                            </div>
                                        </td>
                                        
                                        <td class="text-left">
                                            <div class="input-group">
                                                <? if($currency['symbol_left']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_left']; ?>
                                                    </span>
                                                <? endif; ?>
                                                
                                                <input type="text" name="universal_weight_rules[<?php echo $i; ?>][min_price]" value="<?php echo $rule['min_price']; ?>" class="form-control" />
                                                
                                                <? if($currency['symbol_right']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_right']; ?>
                                                    </span>
                                                <? endif; ?>
                                            </div>
                                        </td>
                                        
                                        <td class="text-left">
                                            <div class="input-group">
                                                <? if($currency['symbol_left']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_left']; ?>
                                                    </span>
                                                <? endif; ?>
                                                
                                                <input type="text" name="universal_weight_rules[<?php echo $i; ?>][max_price]" value="<?php echo $rule['max_price']; ?>" class="form-control" />
                                                
                                                <? if($currency['symbol_right']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $currency['symbol_right']; ?>
                                                    </span>
                                                <? endif; ?>
                                            </div>
                                        </td>
                                        
                                        <td class="text-left">
                                            <div class="input-group">
                                                <input type="text" name="universal_weight_rules[<?php echo $i; ?>][min_weight]" value="<?php echo $rule['min_weight']; ?>" class="form-control" />
                                                
                                                <? if($weight_class['unit']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $weight_class['unit']; ?>
                                                    </span>
                                                <? endif; ?>
                                            </div>
                                        </td>
                                        
                                        <td class="text-left">
                                            <div class="input-group">                                            
                                                <input type="text" name="universal_weight_rules[<?php echo $i; ?>][max_weight]" value="<?php echo $rule['max_weight']; ?>" class="form-control" />
                                                
                                                <? if($weight_class['unit']): ?>
                                                    <span class="input-group-addon">
                                                        <?php echo $weight_class['unit']; ?>
                                                    </span>
                                                <? endif; ?>
                                            </div>
                                        </td>

                                        <td class="text-left">
                                            <select name="universal_weight_rules[<?php echo $i; ?>][geo_zone]" class="form-control">
                                                <option value="*"><?php echo $text_all_zones; ?></option>
                                                <?php foreach ($geo_zones as $zone): ?>
                                                    <option value="<?php echo $zone['geo_zone_id']; ?>" <?php echo ($rule['geo_zone'] == $zone['geo_zone_id']) ? 'selected="selected"' : ''; ?>>
                                                        <?php echo $zone['name']; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        
                                        <td class="text-left">
                                            <select name="universal_weight_rules[<?php echo $i; ?>][status]" class="form-control">
                                                <option value="1" <?php echo ($rule['status'] == '1') ? 'selected="selected"' : ''; ?>>
                                                    <?php echo $text_enabled; ?>
                                                </option>
                                                <option value="0" <?php echo ($rule['status'] == '0') ? 'selected="selected"' : ''; ?>>
                                                    <?php echo $text_disabled; ?>
                                                </option>
                                            </select>
                                        </td>
                                        
                                        <td class="text-left">
                                            <input type="text" name="universal_weight_rules[<?php echo $i; ?>][sort_order]" value="<?php echo $rule['sort_order']; ?>" class="form-control" />
                                        </td>
                                        
                                        <td class="text-right">
                                            <button type="button" onclick="removeShippingRule(this);" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="9"></td>
                                    <td class="text-right">
                                        <button type="button" onclick="addShippingRule();" data-toggle="tooltip" title="<?php echo $button_rule_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var $symbol_left = '<?php if($currency['symbol_left']): ?><span class="input-group-addon"><?php echo $currency['symbol_left']; ?></span><? endif; ?>';
    var $symbol_right = '<?php if($currency['symbol_right']): ?><span class="input-group-addon"><?php echo $currency['symbol_right']; ?></span><? endif; ?>';
    var $weight_class = '<?php if($weight_class['unit']): ?><span class="input-group-addon"><?php echo $weight_class['unit']; ?></span><? endif; ?>';

    /* Add shipping rule */
    function addShippingRule() {
        var i = 0;
        
        /* Resort */
        $('#shipping-rules tbody tr').each(function() {
            i++;
            
            $(this).find('input, textarea').each(function() {
                name = $(this).attr('name').replace(/universal_weight_rules\[([1-9]*)\]/, 'universal_weight_rules[' + i + ']');
                $(this).attr('name', name);
            });
        });

        i++;

        var html = '<tr>';
	html += '<td class="text-left">';
        
	<?php foreach ($languages as $language): ?>
            html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><input name="universal_weight_rules[' + i + '][title][<?php echo $language['language_id']; ?>]" class="form-control" /></div>';
        <?php endforeach; ?>
            
	html += '</td>';        
        html += '<td class="text-left"><div class="input-group">' + $symbol_left + '<input type="text" name="universal_weight_rules[' + i + '][cost]" class="form-control" />' + $symbol_right + '</div></td>';
        html += '<td class="text-left"><div class="input-group">' + $symbol_left + '<input type="text" name="universal_weight_rules[' + i + '][min_price]" value="0" class="form-control" />' + $symbol_right + '</div></td>';
        html += '<td class="text-left"><div class="input-group">' + $symbol_left + '<input type="text" name="universal_weight_rules[' + i + '][max_price]" value="*" class="form-control" />' + $symbol_right + '</div></td>';
        html += '<td class="text-left"><div class="input-group"><input type="text" name="universal_weight_rules[' + i + '][min_weight]" value="0" class="form-control" />' + $weight_class + '</div></td>';
        html += '<td class="text-left"><div class="input-group"><input type="text" name="universal_weight_rules[' + i + '][max_weight]" value="*" class="form-control" />' + $weight_class + '</div></td>';
        html += '<td class="text-left"><select name="universal_weight_rules[' + i + '][geo_zone]" class="form-control">';
        html += '<option value="*"><?php echo $text_all_zones; ?></option>';
        
        <?php foreach ($geo_zones as $zone): ?>
            html += '<option value="<?php echo $zone['geo_zone_id']; ?>">';
            html +=	'<?php echo $zone['name']; ?>';
            html += '</option>';
        <?php endforeach; ?>
            
        html += '</select></td>';
        html += '<td class="text-left"><select name="universal_weight_rules[' + i + '][status]" class="form-control">';
        html += '<option value="1"><?php echo $text_enabled; ?></option><option value="0"><?php echo $text_disabled; ?></option>';
        html += '</select></td>';
        html += '<td class="text-left"><input type="text" name="universal_weight_rules[' + i + '][sort_order]" class="form-control" /></td>';
        html += '<td class="text-right"><button type="button" onclick="removeShippingRule(this);" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td';
        html += '</tr>';

        $('#shipping-rules tbody').append(html);
    }

    /* Remove shipping rule */
    function removeShippingRule(el) {
        $(el).closest('tr').remove();
    }
</script>

<?php echo $footer; ?> 