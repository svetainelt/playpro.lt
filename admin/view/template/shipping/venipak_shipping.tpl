<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit"
          form="form-shipping"
          data-toggle="tooltip"
          title="<?php echo $button_save; ?>"
          class="btn btn-primary">
          <i class="fa fa-save"></i>
        </button>
        <a href="<?php echo $cancel; ?>"
          data-toggle="tooltip"
          title="<?php echo $button_cancel; ?>"
          class="btn btn-default">
          <i class="fa fa-reply"></i>
        </a>
      </div>
      <h1>
        <?php echo $heading_title; ?>
      </h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning): ?>
      <div class="alert alert-danger alert-dismissible">
        <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">
          &times;
        </button>
      </div>
    <?php endif; ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
          <i class="fa fa-pencil"></i> <?php echo $text_edit; ?>
        </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>"
          method="post"
          enctype="multipart/form-data"
          id="form-shipping"
          class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-status">
              <?php echo $entry_status; ?>
            </label>
            <div class="col-sm-10 col-5">
              <select name="venipak_shipping_status"
                id="input-status"
                class="form-control">
                <?php if ($venipak_shipping_status): ?>
                  <option value="1" selected="selected">
                    <?php echo $text_enabled; ?>
                  </option>
                  <option value="0">
                    <?php echo $text_disabled; ?>
                  </option>
                <?php else: ?>
                  <option value="1">
                    <?php echo $text_enabled; ?>
                  </option>
                  <option value="0" selected="selected">
                    <?php echo $text_disabled; ?>
                  </option>
                <?php endif; ?>
              </select>
            </div>
          </div>
          <h4>
            <?php echo $text_venipak_connection_settings; ?>
          </h4>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_client_id">
              <?php echo $text_venipak_shipping_client_id; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_client_id"
                value="<?php echo $venipak_shipping_client_id; ?>"
                placeholder="12345"
                id="input-venipak_shipping_client_id"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_client_username">
              <?php echo $text_venipak_shipping_client_username; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_client_username"
                value="<?php echo $venipak_shipping_client_username; ?>"
                id="input-venipak_shipping_client_username"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_client_password">
              <?php echo $text_venipak_shipping_client_password; ?>
            </label>
            <div class="col-sm-10">
              <input type="password"
                name="venipak_shipping_client_password"
                value="<?php echo $venipak_shipping_client_password; ?>"
                id="input-venipak_shipping_client_password"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_test">
              <?php echo $text_venipak_shipping_test; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_test"
                id="input-venipak_shipping_test"
                class="form-control"
                <?php if ($venipak_shipping_test) {
					echo 'checked="checked"';
				}?> />
            </div>
          </div>
          <h4>
            <?php echo $text_shipping_method_settings; ?>
          </h4>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_method_title">
              <?php echo $text_venipak_shipping_method_title; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_method_title"
                value="<?php echo $venipak_shipping_method_title; ?>"
                id="input-venipak_shipping_method_title"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_method_title_courier">
              <?php echo $text_venipak_shipping_method_title_courier; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_method_title_courier"
                value="<?php echo $venipak_shipping_method_title_courier; ?>"
                id="input-venipak_shipping_method_title_courier"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_method_title_pickup">
              <?php echo $text_venipak_shipping_method_title_pickup; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_method_title_pickup"
                value="<?php echo $venipak_shipping_method_title_pickup; ?>"
                id="input-venipak_shipping_method_title_pickup"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sort_order">
              <?php echo $text_venipak_shipping_sort_order; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sort_order"
                value="<?php echo $venipak_shipping_sort_order; ?>"
                placeholder="<?php echo $venipak_shipping_sort_order; ?>"
                id="input-venipak_shipping_sort_order"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-cost">
              <?php echo $text_venipak_shipping_cost_courier; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_cost_courier"
                value="<?php echo $venipak_shipping_cost_courier; ?>"
                placeholder="<?php echo $text_venipak_shipping_cost_courier; ?>"
                id="input-cost"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-cost">
              <?php echo $text_venipak_shipping_cost_pickup; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_cost_pickup"
                value="<?php echo $venipak_shipping_cost_pickup; ?>"
                placeholder="<?php echo $text_venipak_shipping_cost_pickup; ?>"
                id="input-cost"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-venipak_shipping_free"><?php echo $text_venipak_shipping_free; ?></label>
            <div class="col-sm-10">
              <input type="text" name="venipak_shipping_free" value="<?php echo $venipak_shipping_free; ?>" id="input-venipak_shipping_free" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_products_per_pack">
              <?php echo $text_venipak_shipping_products_per_pack; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_products_per_pack"
                value="<?php echo $venipak_shipping_products_per_pack; ?>"
                id="input-venipak_shipping_products_per_pack"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_initial_tracking_number">
              <?php echo $text_venipak_shipping_initial_tracking_number; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_initial_tracking_number"
                value="<?php echo $venipak_shipping_initial_tracking_number; ?>"
                id="input-venipak_shipping_initial_tracking_number"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_is_map_enabled">
              <?php echo $text_venipak_shipping_is_map_enabled; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_is_map_enabled"
                id="input-venipak_shipping_is_map_enabled"
                class="form-control"
                <?php
				if ($venipak_shipping_is_map_enabled) {
					echo 'checked="checked"';
				}
                ?> />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_google_api_key">
              <?php echo $text_venipak_shipping_google_api_key; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_google_api_key"
                value="<?php echo $venipak_shipping_google_api_key; ?>"
                id="input-venipak_shipping_google_api_key"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_is_clusters_enabled">
              <?php echo $text_venipak_shipping_is_clusters_enabled; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_is_clusters_enabled"
                id="input-venipak_shipping_is_clusters_enabled"
                class="form-control"
                <?php
				if ($venipak_shipping_is_clusters_enabled) {
					echo 'checked="checked"';
				}
                ?> />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_geo_zone_id">
              <?php echo $text_venipak_shipping_geo_zone_id; ?>
            </label>
            <div class="col-sm-10">
              <select name="venipak_shipping_geo_zone_id"
                id="input-venipak_shipping_geo_zone_id"
                class="form-control">
                <option value="0">
                  <?php echo $text_all_zones; ?>
                </option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                  <?php if ($geo_zone['geo_zone_id'] == $venipak_shipping_geo_zone_id): ?>
                    <option value="<?php echo $geo_zone['geo_zone_id']; ?>"
                      selected="selected">
                      <?php echo $geo_zone['name']; ?>
                    </option>
                  <?php else: ?>
                    <option value="<?php echo $geo_zone['geo_zone_id']; ?>">
                      <?php echo $geo_zone['name']; ?>
                    </option>
                  <?php endif; ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_disable_courier">
              <?php echo $text_venipak_shipping_disable_courier; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_disable_courier"
                id="input-venipak_shipping_disable_courier"
                class="form-control"
                <?php
				if ($venipak_shipping_disable_courier) {
					echo 'checked="checked"';
				}
                ?> />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_disable_pickup">
              <?php echo $text_venipak_shipping_disable_pickup; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_disable_pickup"
                id="input-venipak_shipping_disable_pickup"
                class="form-control"
                <?php
				if ($venipak_shipping_disable_pickup) {
					echo 'checked="checked"';
				}
                ?> />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_disable_locker">
              <?php echo $text_venipak_shipping_disable_locker; ?>
            </label>
            <div class="col-sm-10">
              <input type="checkbox"
                name="venipak_shipping_disable_locker"
                id="input-venipak_shipping_disable_locker"
                class="form-control"
                <?php
				if ($venipak_shipping_disable_locker) {
					echo 'checked="checked"';
				}
                ?> />
            </div>
          </div>

          <h4>
            <?php echo $text_sender_settings; ?>
          </h4>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_name">
              <?php echo $text_venipak_shipping_sender_name; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_name"
                value="<?php echo $venipak_shipping_sender_name; ?>"
                id="input-venipak_shipping_sender_name"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_company_code">
              <?php echo $text_venipak_shipping_sender_company_code; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_company_code"
                value="<?php echo $venipak_shipping_sender_company_code; ?>"
                id="input-venipak_shipping_sender_company_code"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_country">
              <?php echo $text_venipak_shipping_sender_country; ?>
            </label>
            <div class="col-sm-10">
              <select name="venipak_shipping_sender_country"
                id="input-venipak_shipping_sender_country"
                class="form-control">
                <option></option>
                <option value="LT"
                  <?php
				  if ($venipak_shipping_sender_country == 'LT') {
					echo 'selected';
				}
                  ?>>
                  LT
                </option>
                <option value="LV"
                  <?php
				  if ($venipak_shipping_sender_country == 'LV') {
					echo 'selected';
				}
                  ?>>
                  LV
                </option>
                <option value="EE"
                  <?php
				  if ($venipak_shipping_sender_country == 'EE') {
					echo 'selected';
				}
                  ?>>
                  EE
                </option>
              </select>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_city">
              <?php echo $text_venipak_shipping_sender_city; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_city"
                value="<?php echo $venipak_shipping_sender_city; ?>"
                id="input-venipak_shipping_sender_city"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_address">
              <?php echo $text_venipak_shipping_sender_address; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_address"
                value="<?php echo $venipak_shipping_sender_address; ?>"
                id="input-venipak_shipping_sender_address"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_postcode">
              <?php echo $text_venipak_shipping_sender_postcode; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_postcode"
                value="<?php echo $venipak_shipping_sender_postcode; ?>"
                id="input-venipak_shipping_sender_postcode"
                class="form-control"
                minlength="4"
                maxlength="5" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_contact_person">
              <?php echo $text_venipak_shipping_sender_contact_person; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_contact_person"
                value="<?php echo $venipak_shipping_sender_contact_person; ?>"
                id="input-venipak_shipping_sender_contact_person"
                class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_contact_tel">
              <?php echo $text_venipak_shipping_sender_contact_tel; ?>
            </label>
            <div class="col-sm-10">
              <input type="text"
                name="venipak_shipping_sender_contact_tel"
                value="<?php echo $venipak_shipping_sender_contact_tel; ?>"
                id="input-venipak_shipping_sender_contact_tel"
                class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"
              for="input-venipak_shipping_sender_contact_email">
              <?php echo $text_venipak_shipping_sender_contact_email; ?>
            </label>
            <div class="col-sm-10">
              <input type="email"
                name="venipak_shipping_sender_contact_email"
                value="<?php echo $venipak_shipping_sender_contact_email; ?>"
                id="input-venipak_shipping_sender_contact_email"
                class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
