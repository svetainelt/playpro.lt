<?php
// If OpenCart version is less than 2
if(version_compare(VERSION, '2', "<")):
?>

<?php echo $header; ?>

<div id="content" class="inbank inbank--oc2">

    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>


    <div class="box">

        <div class="heading">
            <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="jQuery('#form-mokilizingas').submit();" class="button btn btn-success"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button btn btn-danger"><?php echo $button_cancel; ?></a></div>
        </div>

        <div class="content" id="content">
            <div class="page-header">
                <div class="container-fluid">

                    <h1><?php echo $heading_title; ?></h1>

                    <div class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        <?php } ?>
                    </div>

                </div>
            </div>
            <div class="container-fluid">

                <div class="mokilizingas__calculator">
                    <div class="mokilizingas__calculator-content">

                        <div class="mokilizingas__calculator-logo mokilizingas__calculator-logo--middle">
                            <img src="view/image/payment/mokilizingas.png" alt="Inbank | mokilizingas" />
                        </div>

                        <div class="mokilizingas__calculator-contact">
                            <h4>Inbank | mokilizingas</h4> <br>
                            <p>info@mokilizingas.lt </p>
                            <p>+370 700 55 888</p>
                            <p><a href="https://mokilizingas.lt" target="_blank"> mokilizingas.lt </a> </p>
                        </div>

                        <div class="mokilizingas__calculator-contact">
                            <h4>Elpresta</h4> <br>
                            <p>info@elPresta.eu </p>
                            <p>+370 6388 8816</p>
                            <p><a href="https://elPresta.eu/lt/" target="_blank"> elPresta.eu </a> </p>
                        </div>
                    </div>
                </div>

                <?php if ($module_status) { ?>
                <div class="mokilizingas__updater">
                    <?php echo $module_status; ?>
                </div>
                <?php } ?>

                <?php if ($error_warning) { ?>
                <div class="warning"><?php echo $error_warning; ?></div>
                <?php } ?>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-mokilizingas" class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_status">
                                    Modulio būsena:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_status" id="input-status" class="form-control">
                                        <?php if ($mokilizingas_status) { ?>
                                        <option value="1" selected="selected">Įjungtas</option>
                                        <option value="0">Išjungtas</option>
                                        <?php } else { ?>
                                        <option value="1">Įjungtas</option>
                                        <option value="0" selected="selected">Išjungtas</option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_mode">
                                    API rėžimas:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_mode" id="input-status" class="form-control">
                                        <?php if ($mokilizingas_mode) { ?>
                                        <option value="1" selected="selected">Live rėžimas</option>
                                        <option value="0">Testavimo rėžimas</option>
                                        <?php } else { ?>
                                        <option value="1">Live rėžimas</option>
                                        <option value="0" selected="selected">Testavimo rėžimas</option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_user">
                                    API user:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_user"
                                           value="<?php echo $mokilizingas_user; ?>"
                                           placeholder="<?php echo $mokilizingas_user; ?>"
                                           id="mokilizingas_user" class="form-control"/>

                                    <?php if ($error_user) { ?>
                                    <div class="text-danger"><?php echo $error_user; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_password">
                                    API password:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_password"
                                           value="<?php echo $mokilizingas_password; ?>"
                                           placeholder="<?php echo $mokilizingas_password; ?>"
                                           id="mokilizingas_password" class="form-control"/>

                                    <?php if ($error_password ) { ?>
                                    <div class="text-danger"><?php echo $error_password ; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_key">
                                    API key:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_key"
                                           value="<?php echo $mokilizingas_key; ?>"
                                           placeholder="<?php echo $mokilizingas_key; ?>" id="mokilizingas_key"
                                           class="form-control"/>

                                    <?php if ($error_key) { ?>
                                    <div class="text-danger"><?php echo $error_key; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_payment_name">
                                    Mokėjimo pavadinimas:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_payment_name"
                                           value="<?php echo $mokilizingas_payment_name; ?>"
                                           placeholder="<?php echo $mokilizingas_payment_name; ?>"
                                           id="mokilizingas_payment_name" class="form-control"/>

                                    <?php if ($error_payment_name) { ?>
                                    <div class="text-danger"><?php echo $error_payment_name; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_minimal_price">
                                    Minimali kaina:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_minimal_price"
                                           value="<?php echo $mokilizingas_minimal_price; ?>"
                                           placeholder="<?php echo $mokilizingas_minimal_price; ?>"
                                           id="mokilizingas_minimal_price" class="form-control"/>

                                    <?php if ($error_minimal_price) { ?>
                                    <div class="text-danger"><?php echo $error_minimal_price; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_max_price">
                                    Maksimali kaina:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_max_price"
                                           value="<?php echo $mokilizingas_max_price; ?>"
                                           placeholder="<?php echo $mokilizingas_max_price; ?>"
                                           id="mokilizingas_max_price" class="form-control"/>

                                    <?php if ($error_max_price) { ?>
                                    <div class="text-danger"><?php echo $error_max_price; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_phone">
                                    Kontaktinis telefonas:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_phone"
                                           value="<?php echo $mokilizingas_phone; ?>"
                                           placeholder="<?php echo $mokilizingas_phone; ?>"
                                           id="mokilizingas_phone" class="form-control"/>

                                    <?php if ($error_phone ) { ?>
                                    <div class="text-danger"><?php echo $error_phone; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_email">
                                    Kontaktinis el.paštas:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_email"
                                           value="<?php echo $mokilizingas_email; ?>"
                                           placeholder="<?php echo $mokilizingas_email; ?>"
                                           id="mokilizingas_email" class="form-control"/>

                                    <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_mode">
                                    Rodyti skaičiuoklę produkto puslapyje?
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_calculator" id="input-status" class="form-control">
                                        <?php if ($mokilizingas_calculator) { ?>
                                        <option value="1" selected="selected">Taip</option>
                                        <option value="0">Ne</option>
                                        <?php } else { ?>
                                        <option value="1">Taip</option>
                                        <option value="0" selected="selected">Ne</option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_mode">
                                    Skaičiuoklės stilius:
                                </label>
                                <div class="col-sm-10">
                                    <select name="mokilizingas_calculator_style" id="input-status" class="form-control">

                                        <option value="1"
                                        <?php if ($mokilizingas_calculator_style == 1) { ?>
                                        selected="selected"  <?php } ?> >ib-001</option>
                                        <option value="2"
                                        <?php if ($mokilizingas_calculator_style == 2) { ?>
                                        selected="selected"  <?php } ?> >ib-002</option>
                                        <option value="3"
                                        <?php if ($mokilizingas_calculator_style == 3) { ?>
                                        selected="selected"  <?php } ?> >ib-003</option>
                                        <option value="4"
                                        <?php if ($mokilizingas_calculator_style == 4) { ?>
                                        selected="selected"  <?php } ?> >ib-004</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_calculator_color">
                                    Skaičiuoklės spalva:
                                </label>
                                <div class="col-sm-10">
                                    <select name="mokilizingas_calculator_color" id="input-status" class="form-control">
                                        <option value="1"<?php if ($mokilizingas_calculator_color == 1) { ?>selected="selected"  <?php } ?> >1</option>
                                        <option value="2"<?php if ($mokilizingas_calculator_color == 2) { ?>selected="selected"  <?php } ?> >2</option>
                                        <option value="3"<?php if ($mokilizingas_calculator_color == 3) { ?>selected="selected"  <?php } ?> >3</option>
                                        <option value="4"<?php if ($mokilizingas_calculator_color == 4) { ?>selected="selected"  <?php } ?> >4</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_term">
                                    Standartinė lizingo trukmė:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_term"
                                           value="<?php echo $mokilizingas_term; ?>"
                                           placeholder="<?php echo $mokilizingas_term; ?>"
                                           id="mokilizingas_term" class="form-control"/>

                                    <?php if ($error_term ) { ?>
                                    <div class="text-danger"><?php echo $error_term; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_waiting_order_status_id">
                                    Laukiama patvirtinimo būsena:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_waiting_order_status_id" id="input-status" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $mokilizingas_waiting_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_accepted_order_status_id">
                                    Lizingas patvirtino mokėjimą:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_accepted_order_status_id" id="input-status"
                                            class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $mokilizingas_accepted_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"
                                                selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_canceled_order_status_id">
                                    Užsakymas atšauktas:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_canceled_order_status_id" id="input-status"
                                            class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $mokilizingas_canceled_order_status_id ) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"
                                                selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_logo_url">Logotipo URL:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_logo_url"
                                           value="<?php echo $mokilizingas_logo_url; ?>"
                                           placeholder="<?php echo $mokilizingas_logo_url; ?>"
                                           id="mokilizingas_logo_url" class="form-control"/>

                                    <?php if ($error_logo_url ) { ?>
                                    <div class="text-danger"><?php echo $error_logo_url; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_geo_zone_id">
                                    Galimos GEO zonos:
                                </label>
                                <div class="col-sm-10">

                                    <select name="mokilizingas_geo_zone_id" id="input-status" class="form-control">


                                        <option value="0"  <?php if ($mokilizingas_geo_zone_id == 0) { ?> selected="selected" <?php } ?> >
                                        Visos zonos
                                        </option>

                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                        <?php if ($geo_zone['geo_zone_id'] == $mokilizingas_geo_zone_id) { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                        <?php } ?>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="mokilizingas_logo_url">
                                    Mokėjimo rikiavimas:
                                </label>
                                <div class="col-sm-10">
                                    <input type="text" name="mokilizingas_sort_order"
                                           value="<?php echo $mokilizingas_sort_order; ?>"
                                           placeholder="<?php echo $mokilizingas_sort_order; ?>"
                                           id="mokilizingas_sort_order" class="form-control"/>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo $footer; ?>

<?php
// if greater or equal to version 2
else:
?>

<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">

            <div class="pull-right">
                <button type="submit" form="form-mokilizingas" data-toggle="tooltip" title="Išsaugoti" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                </button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Atšaukti" class="btn btn-default">
                    <i class="fa fa-reply"></i>
                </a>
            </div>

            <h1><?php echo $heading_title; ?></h1>

            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>

        </div>
    </div>
    <div class="container-fluid">

        <div class="mokilizingas__calculator">
            <div class="mokilizingas__calculator-content">

                <div class="mokilizingas__calculator-logo mokilizingas__calculator-logo--middle">
                    <img src="view/image/payment/mokilizingas.png" alt="Inbank | mokilizingas" />
                </div>

                <div class="mokilizingas__calculator-contact">
                    <h4>Inbank | mokilizingas</h4> <br>
                    <p>info@mokilizingas.lt </p>
                    <p>+370 700 55 888</p>
                    <p><a href="https://mokilizingas.lt" target="_blank"> mokilizingas.lt </a> </p>
                </div>

                <div class="mokilizingas__calculator-contact">
                    <h4>Elpresta</h4> <br>
                    <p>info@elPresta.eu </p>
                    <p>+370 6388 8816</p>
                    <p><a href="https://elPresta.eu/lt/" target="_blank"> elPresta.eu </a> </p>
                </div>
            </div>
        </div>

        <?php if ($module_status) { ?>
        <div class="mokilizingas__updater">
            <?php echo $module_status; ?>
        </div>
        <?php } ?>

        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-mokilizingas"
                      class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_status">
                            Modulio būsena
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_status" id="input-status" class="form-control">
                                <?php if ($mokilizingas_status) { ?>
                                <option value="1" selected="selected">Įjungtas</option>
                                <option value="0">Išjungtas</option>
                                <?php } else { ?>
                                <option value="1">Įjungtas</option>
                                <option value="0" selected="selected">Išjungtas</option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_mode">
                            API rėžimas
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_mode" id="input-status" class="form-control">
                                <?php if ($mokilizingas_mode) { ?>
                                <option value="1" selected="selected">Live rėžimas</option>
                                <option value="0">Testavimo rėžimas</option>
                                <?php } else { ?>
                                <option value="1">Live rėžimas</option>
                                <option value="0" selected="selected">Testavimo rėžimas</option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_user">
                            API user
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_user"
                                   value="<?php echo $mokilizingas_user; ?>"
                                   placeholder="<?php echo $mokilizingas_user; ?>"
                                   id="mokilizingas_user" class="form-control"/>

                            <?php if ($error_user) { ?>
                            <div class="text-danger"><?php echo $error_user; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_password">
                            API password
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_password"
                                   value="<?php echo $mokilizingas_password; ?>"
                                   placeholder="<?php echo $mokilizingas_password; ?>"
                                   id="mokilizingas_password" class="form-control"/>

                            <?php if ($error_password ) { ?>
                            <div class="text-danger"><?php echo $error_password ; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_key">
                            API key
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_key"
                                   value="<?php echo $mokilizingas_key; ?>"
                                   placeholder="<?php echo $mokilizingas_key; ?>" id="mokilizingas_key"
                                   class="form-control"/>

                            <?php if ($error_key) { ?>
                            <div class="text-danger"><?php echo $error_key; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_payment_name">
                            Mokėjimo pavadinimas:
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_payment_name"
                                   value="<?php echo $mokilizingas_payment_name; ?>"
                                   placeholder="<?php echo $mokilizingas_payment_name; ?>"
                                   id="mokilizingas_payment_name" class="form-control"/>

                            <?php if ($error_payment_name) { ?>
                            <div class="text-danger"><?php echo $error_payment_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_minimal_price">
                            Minimali kaina
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_minimal_price"
                                   value="<?php echo $mokilizingas_minimal_price; ?>"
                                   placeholder="<?php echo $mokilizingas_minimal_price; ?>"
                                   id="mokilizingas_minimal_price" class="form-control"/>

                            <?php if ($error_minimal_price) { ?>
                            <div class="text-danger"><?php echo $error_minimal_price; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_max_price">
                            Maksimali kaina
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_max_price"
                                   value="<?php echo $mokilizingas_max_price; ?>"
                                   placeholder="<?php echo $mokilizingas_max_price; ?>"
                                   id="mokilizingas_max_price" class="form-control"/>

                            <?php if ($error_max_price) { ?>
                            <div class="text-danger"><?php echo $error_max_price; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_phone">
                            Kontaktinis telefonas
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_phone"
                                   value="<?php echo $mokilizingas_phone; ?>"
                                   placeholder="<?php echo $mokilizingas_phone; ?>"
                                   id="mokilizingas_phone" class="form-control"/>

                            <?php if ($error_phone ) { ?>
                            <div class="text-danger"><?php echo $error_phone; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_email">
                            Kontaktinis el.paštas
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_email"
                                   value="<?php echo $mokilizingas_email; ?>"
                                   placeholder="<?php echo $mokilizingas_email; ?>"
                                   id="mokilizingas_email" class="form-control"/>

                            <?php if ($error_email) { ?>
                            <div class="text-danger"><?php echo $error_email; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_mode">
                            Rodyti skaičiuoklę produkto puslapyje?
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_calculator" id="input-status" class="form-control">
                                <?php if ($mokilizingas_calculator) { ?>
                                <option value="1" selected="selected">Taip</option>
                                <option value="0">Ne</option>
                                <?php } else { ?>
                                <option value="1">Taip</option>
                                <option value="0" selected="selected">Ne</option>
                                <?php }  ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_mode">
                            Skaičiuoklės stilius
                        </label>
                        <div class="col-sm-10">
                            <select name="mokilizingas_calculator_style" id="input-status" class="form-control">
                                <option value="1"
                                <?php if ($mokilizingas_calculator_style == 1) { ?>
                                selected="selected"  <?php } ?> >ib-001</option>
                                <option value="2"
                                <?php if ($mokilizingas_calculator_style == 2) { ?>
                                selected="selected"  <?php } ?> >ib-002</option>
                                <option value="3"
                                <?php if ($mokilizingas_calculator_style == 3) { ?>
                                selected="selected"  <?php } ?> >ib-003</option>
                                <option value="4"
                                <?php if ($mokilizingas_calculator_style == 4) { ?>
                                selected="selected"  <?php } ?> >ib-004</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_calculator_color">
                            Skaičiuoklės spalva
                        </label>
                        <div class="col-sm-10">
                            <select name="mokilizingas_calculator_color" id="input-status" class="form-control">
                                <option value="1"<?php if ($mokilizingas_calculator_color == 1) { ?>selected="selected"  <?php } ?> >1</option>
                                <option value="2"<?php if ($mokilizingas_calculator_color == 2) { ?>selected="selected"  <?php } ?> >2</option>
                                <option value="3"<?php if ($mokilizingas_calculator_color == 3) { ?>selected="selected"  <?php } ?> >3</option>
                                <option value="4"<?php if ($mokilizingas_calculator_color == 4) { ?>selected="selected"  <?php } ?> >4</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_term">
                            Standartinė lizingo trukmė
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_term"
                                   value="<?php echo $mokilizingas_term; ?>"
                                   placeholder="<?php echo $mokilizingas_term; ?>"
                                   id="mokilizingas_term" class="form-control"/>

                            <?php if ($error_term ) { ?>
                            <div class="text-danger"><?php echo $error_term; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_waiting_order_status_id">
                            Laukiama patvirtinimo būsena
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_waiting_order_status_id" id="input-status" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>

                                <?php if ($order_status['order_status_id'] == $mokilizingas_waiting_order_status_id) { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                <?php } ?>

                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_accepted_order_status_id">
                            Lizingas patvirtino mokėjimą
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_accepted_order_status_id" id="input-status"
                                    class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>

                                <?php if ($order_status['order_status_id'] == $mokilizingas_accepted_order_status_id) { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"
                                        selected="selected"><?php echo $order_status['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                <?php } ?>

                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_canceled_order_status_id">
                            Užsakymas atšauktas
                        </label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_canceled_order_status_id" id="input-status" class="form-control">
                                <?php foreach ($order_statuses as $order_status) { ?>

                                <?php if ($order_status['order_status_id'] == $mokilizingas_canceled_order_status_id ) { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"
                                        selected="selected"><?php echo $order_status['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                <?php } ?>

                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_logo_url">Logotipo URL</label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_logo_url"
                                   value="<?php echo $mokilizingas_logo_url; ?>"
                                   placeholder="<?php echo $mokilizingas_logo_url; ?>"
                                   id="mokilizingas_logo_url" class="form-control"/>

                            <?php if ($error_logo_url ) { ?>
                            <div class="text-danger"><?php echo $error_logo_url; ?></div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_geo_zone_id">Galimos GEO
                            zonos</label>
                        <div class="col-sm-10">

                            <select name="mokilizingas_geo_zone_id" id="input-status" class="form-control">

                                <option value="0"  <?php if ($mokilizingas_geo_zone_id == 0) { ?> selected="selected" <?php } ?> >
                                Visos zonos
                                </option>

                                <?php foreach ($geo_zones as $geo_zone) { ?>

                                <?php if ($geo_zone['geo_zone_id'] == $mokilizingas_geo_zone_id) { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                <?php } ?>

                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="mokilizingas_logo_url">
                            Mokėjimo rikiavimas
                        </label>
                        <div class="col-sm-10">
                            <input type="text" name="mokilizingas_sort_order"
                                   value="<?php echo $mokilizingas_sort_order; ?>"
                                   placeholder="<?php echo $mokilizingas_sort_order; ?>"
                                   id="mokilizingas_sort_order" class="form-control"/>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>

<?php
endif;
?>