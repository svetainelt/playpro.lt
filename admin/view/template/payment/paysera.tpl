<?php echo $header; ?><?php echo $column_left; ?>

<?php if ($error_warning): ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php endif; ?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-paysera" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>

            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-paysera" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-filter" data-toggle="tab"><?php echo $tab_filter_banks; ?></a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">

                            
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="paysera_project"><?php echo $entry_project; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="paysera_project" value="<?php echo $paysera_project; ?>" class="form-control" />
                                    <?php if ($error_project) { ?>
                                    <div class="text-danger"><?php echo $error_project; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="paysera_sign"><?php echo $entry_sign; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="paysera_sign" value="<?php echo $paysera_sign; ?>" class="form-control" />
                                    <?php if ($error_sign) { ?>
                                    <div class="text-danger"><?php echo $error_sign; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="paysera_lang"><span data-toggle="tooltip" title="<?php echo $help_lang; ?>"><?php echo $entry_lang; ?></span></label>
                                <div class="col-sm-10">
                                    <select name="paysera_lang" class="form-control">
                                        <?php foreach($langs as $lang => $title): ?>
                                        <option value="<?php echo $lang; ?>" <?php if($paysera_lang == $lang): ?>selected="selected"<?php endif; ?> ><?php echo $title; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br />
                                    <!--<input type="text" name="paysera_lang" value="<?php echo $paysera_lang; ?>" class="form-control" />-->
                                    <?php if ($error_lang) { ?>
                                    <div class="text-danger"><?php echo $error_lang; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="paysera_test"><?php echo $entry_test; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_test" class="form-control">
                                        <?php if($paysera_test == 0): ?>
                                        <option value="0" selected="selected"><?php echo $text_off; ?></option>
                                        <option value="100"><?php echo $text_on; ?></option>
                                        <?php else: ?>
                                        <option value="0"><?php echo $text_off; ?></option>
                                        <option value="100" selected="selected"><?php echo $text_on; ?></option>
                                        <?php endif;?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-new-order-status"><?php echo $entry_new_order_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_new_order_status_id" id="input-new-order-status" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $paysera_new_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_order_status_id" id="input-order-status" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $paysera_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_geo_zone_id" id="input-geo-zone" class="form-control">
                                        <option value="0"><?php echo $text_all_zones; ?></option>
                                        <?php foreach ($geo_zones as $geo_zone) { ?>
                                        <?php if ($geo_zone['geo_zone_id'] == $paysera_geo_zone_id) { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_status" id="input-status" class="form-control">
                                        <?php if ($paysera_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="paysera_display_payments_list"><?php echo $entry_display_payments; ?></label>
                                <div class="col-sm-10">
                                    <select name="paysera_display_payments_list" class="form-control">
                                        <?php if($paysera_display_payments_list == 1): ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                        <?php else: ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="paysera_sort_order" value="<?php echo $paysera_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>
                        </div>

                        
                        <? /* CUSTOM START */ ?>
                        <div class="tab-pane" id="tab-filter">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="paysera_use_filters"><?php echo $entry_filter_methods; ?></label>
                                <div class="col-sm-9">
                                    <select name="paysera_use_filters" class="form-control">
                                        <?php if ($paysera_use_filters): ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php else: ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="paysera_payment_countries[]"><?php echo $entry_selected_countries; ?></label>
                                <div class="col-sm-9">
                                    <?php foreach($payment_countries as $country): ?>
                                    <div class="checkbox"><label><input type="checkbox" name="paysera_payment_countries[]" value="<?php echo $country->getCode(); ?>" <?php if(in_array($country->getCode(), $selected_countries)): ?>checked<?php endif; ?>/> <?php echo $country->getTitle(); ?></label></div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $entry_select_countries; ?></label>
                                <div class="col-sm-9">
                                    <select id="w2p-cs" class="form-control">
                                        <option value="0" selected="selected"><?php echo $text_select_country; ?></option>
                                        <?php foreach($payment_countries as $country ): ?>
                                        <option value="<?php echo $country->getCode(); ?>" <?php if((!empty($default_payment_country) && $country->getCode() == $default_payment_country) || $country->getCode() == 'lt'): ?>selected<?php endif; ?>><?php echo $country->getTitle(); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="paysera_payment_methods[]"><?php echo $entry_select_methods; ?></label>
                                <div class="col-sm-6">
                                    <?php foreach ($payment_countries as $country): ?>
                                        <div rel="<?php echo $country->getCode(); ?>" class="w2p-table" style="display: <?php echo ((!empty($default_payment_country) && $country->getCode() == $default_payment_country) || $country->getCode() == 'lt') ? 'block' : 'none' ?>;">
                                            <?php foreach ($country->getGroups() as $group): ?>
                                                <div class="payment-title">
                                                    <b><?php echo $group->getTitle(); ?></b>
                                                </div>
                                                <div class="paysera-payment">
                                                    <?php foreach ($group->getPaymentMethods() as $paymentMethod): ?>
                                                        <?php if ($paymentMethod->getLogoUrl()): ?>
                                                            <div class="payment-box">
                                                                <label class="box-image">
                                                                    <?php $method_key = $paymentMethod->getKey() . '_' . $country->getCode(); ?>
                                                                    <input type="checkbox" class="radio" name="paysera_payment_methods[]" value="<?php echo $method_key; ?>" <?php if(in_array($method_key, $selected_methods)): ?>checked<?php endif; ?> />
                                                                           <img src="<?php echo $paymentMethod->getLogoUrl(); ?>" 
                                                                           title="<?php echo htmlentities($paymentMethod->getTitle()); ?>" 
                                                                           alt="<?php echo htmlentities($paymentMethod->getTitle()); ?>"
                                                                           />
                                                                </label>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                    <div style="clear:both;"></div>
                                                </div>                          
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="checkbox-buttons">
                                        <a nohref id="check_all_methods" class="btn btn-primary"><?php echo $button_check_all_methods; ?></a>
                                        <a nohref id="uncheck_all_methods" class="btn btn-primary"><?php echo $button_uncheck_all_methods; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? /* CUSTOM END */ ?>
                </form>
            </div>
        </div>

    </div>
</div>

<? /* CUSTOM START */ ?>
<style>
    .payment-title {
        margin: 5px 0;
    }
    .box-image .radio {
        margin-right: 10px;
        /* vertical center */
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }
    .box-image img {
        margin-left: 30px;
    }
    .payment-box {
        width: 250px;
        float: left;
        margin: 10px 0;
    }
    .payment-box:nth-child(3n + 1) {
        clear: both;
    }
    .box-image {
        height: 100px;
    }
    .checkbox-buttons {
        float: right;
    }
    .checkbox-buttons .button {
        margin: 10px 5px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#w2p-cs').change(function () {
            var value = $('#w2p-cs option:selected').val();

            $('.w2p-table').each(function () {
                var tid = $(this).attr('rel');
                if (tid == value) {
                    $(this).css({'display' : 'block'});
                } else {
                    $(this).css({'display' : 'none'});
                }
            });
        });

        $('#check_all_methods').click(function () {
            $('.w2p-table .radio').each(function () {
                $(this).attr('checked', true);
            });
        });

        $('#uncheck_all_methods').click(function () {
            $('.w2p-table .radio').each(function () {
                $(this).attr('checked', false);
            });
        });
    });
</script>
<? /* CUSTOM END */ ?>

<?php echo $footer; ?>
