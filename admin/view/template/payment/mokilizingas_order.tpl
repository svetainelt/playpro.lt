<h2> <?php echo $text_leasing; ?> </h2>
<div>
    <p> <?php echo $text_admin_info; ?> </p>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-xs-12">
        <p>
            <button id="button-check-mokilizingas" class="btn btn-success leasing-mb-2"> <?php echo $text_check_response; ?> </button>
        </p>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td class="text-left"><strong><?php echo $text_column_id; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_id_order; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_sessionId; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_errorMessage; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_errorCode; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_resultCode; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_resultInfo; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_resultMsg; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_advance; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_currency; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_contractNo; ?></strong></td>
        <td class="text-left"><strong><?php echo $text_column_date_add; ?></strong></td>
    </tr>
    </thead>
    <tbody>


    <?php foreach ($logs as $logLine) { ?>
    <tr>
        <td class="text-left"><?php echo $logLine['id']; ?> </td>
        <td class="text-left"><?php echo $logLine['id_order']; ?> </td>
        <td class="text-left"><?php echo $logLine['sessionId']; ?> </td>
        <td class="text-left"><?php echo $logLine['errorMessage']; ?> </td>
        <td class="text-left"><?php echo $logLine['errorCode']; ?> </td>
        <td class="text-left"><?php echo $logLine['resultCode']; ?> </td>
        <td class="text-left"><?php echo $logLine['resultInfo']; ?> </td>
        <td class="text-left"><?php echo $logLine['resultMsg']; ?> </td>
        <td class="text-left"><?php echo $logLine['advance']; ?> </td>
        <td class="text-left"><?php echo $logLine['currency']; ?> </td>
        <td class="text-left"><?php echo $logLine['contractNo']; ?> </td>
        <td class="text-left"><?php echo $logLine['date_add']; ?> </td>
    </tr>
    <?php } ?>

    </tbody>
</table>

<script type="text/javascript"><!--
    $("#button-check-mokilizingas").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {'order_id': '<?php echo $order_id; ?>', 'checkLeasingStatus': 1},
            url: 'index.php?route=payment/mokilizingas/status&token=<?php echo $user_token; ?>',
            success: function (data) {
                location.reload();
            }
        });

    });
    //--></script>
