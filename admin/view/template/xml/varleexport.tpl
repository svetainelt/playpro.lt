<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-varleexport" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $entry_heading; ?></h3>
      </div>
      <div class="panel-body">
       <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form-varleexport" class="form-horizontal">
        <div class="alert alert-info">
  			<i class="fa fa-file-code-o fw"></i> <a class="alert-link" href="<?php echo $xml_url;?>" target="_blank"><?php echo $xml_url;?></a>
		</div>
       	<div class="form-group">
            <label class="col-sm-2 control-label" for="varleexport_status">
            <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_status_info; ?>"><?php echo $entry_status; ?></span>
            </label>
            <div class="col-sm-10">
            	 <select name="varleexport_status" class="form-control">
                  <?php if ($varleexport_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
            </div>
        </div>
        
        
        <div class="form-group">
             <label class="col-sm-2 control-label" for="varleexport_language_id">
              <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_language_id_info; ?>"><?php echo $entry_language_id; ?></span>
              </label>
             <div class="col-sm-10">
            <select name="varleexport_language_id"  class="form-control">
                  <?php foreach ($languages as $language) { ?>
                  <?php if ($language['language_id'] == $varleexport_language_id) { ?>
                  <option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
          	</div>
         </div>
         
         
         <div class="form-group">
            <label class="col-sm-2 control-label" for="varleexport_min_price">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_min_price_info; ?>"><?php echo $entry_min_price; ?></span>
            </label>
            <div class="col-sm-10">
            <input type="text" name="varleexport_min_price" value="<?php echo $varleexport_min_price; ?>" class="form-control"/>
            </div> 
          </div>
         
         <div class="form-group">
            <label class="col-sm-2 control-label" for="varleexport_retail_price">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_retail_price_info; ?>"><?php echo $entry_retail_price; ?></span>
            </label>
            <div class="col-sm-10">
            
            <div class="form-group">
            	<label class="col-sm-3 control-label" for="input-operator"><?php echo $entry_operator; ?></label>
            	<div class="col-sm-9">
	            <select name="varleexport_operator"  class="form-control">
	                  <option value=""></option>
	                  <?php foreach ($operators as $operator) { ?>
	                  <?php if ($operator == $varleexport_operator) { ?>
	                  <option value="<?php echo $operator; ?>" selected="selected"><?php echo $operator; ?></option>
	                  <?php } else { ?>
	                  <option value="<?php echo $operator; ?>"><?php echo $operator; ?></option>
	                  <?php } ?>
	                  <?php } ?>
	             </select>
	             </div>
	             </div>
	             <div class="form-group">
	             <label class="col-sm-3 control-label" for="input-value"><?php echo $entry_value; ?></label>
	             <div class="col-sm-9">
	            <input type="text" name="varleexport_operator_value" value="<?php echo $varleexport_operator_value; ?>" class="form-control"/>
	            </div>
	            </div>
	            <div class="form-group">
	            <label class="col-sm-3 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                <div class="col-sm-9">
                  <select name="varleexport_tax_class_id" id="varleexport_tax_class_id" class="form-control">
                    <option value="0"><?php echo $text_none; ?></option>
                    <?php foreach ($tax_classes as $tax_class) { ?>
                    <?php if ($tax_class['tax_class_id'] == $varleexport_tax_class_id) { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
	            </div>
            
            </div> 
          </div>        
          
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="varleexport_prime_cost">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_prime_cost_info; ?>"><?php echo $entry_prime_cost; ?></span>
            </label>
            <div class="col-sm-10">
            
            <div class="form-group">
            	<label class="col-sm-3 control-label" for="input-operator"><?php echo $entry_operator; ?></label>
            	<div class="col-sm-9">
	            <select name="varleexport_operator2"  class="form-control">
	                  <option value=""></option>
	                  <?php foreach ($operators as $operator) { ?>
	                  <?php if ($operator == $varleexport_operator2) { ?>
	                  <option value="<?php echo $operator; ?>" selected="selected"><?php echo $operator; ?></option>
	                  <?php } else { ?>
	                  <option value="<?php echo $operator; ?>"><?php echo $operator; ?></option>
	                  <?php } ?>
	                  <?php } ?>
	             </select>
	             </div>
	             </div>
	             <div class="form-group">
	             <label class="col-sm-3 control-label" for="input-value"><?php echo $entry_value; ?></label>
	             <div class="col-sm-9">
	            <input type="text" name="varleexport_operator_value2" value="<?php echo $varleexport_operator_value2; ?>" class="form-control"/>
	            </div>
	            </div>
	           <?php if ($error_prime_cost) { ?>
                  <div class="text-danger"><?php echo $error_prime_cost; ?></div>
                  <?php } ?>
            
            </div> 
          </div>    
        
        <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 100px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $varleexport_stores)) { ?>
                        <input type="checkbox" name="varleexport_stores[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="varleexport_stores[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $varleexport_stores)) { ?>
                        <input type="checkbox" name="varleexport_stores[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="varleexport_stores[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
         </div>
        </div>
        
         <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_categories;?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 300px; overflow: auto;">
                    <?php foreach ($categories as $category) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($category['category_id'], $varleexport_categories)) { ?>
                        <input type="checkbox" name="varleexport_category[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                        <?php echo $category['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="varleexport_category[]" value="<?php echo $category['category_id']; ?>" />
                        <?php echo $category['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
               </div>
               <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $entry_select_all;?></a> | <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $entry_unselect_all;?></a>
         </div>
         </div>
         
         <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_manufacturers;?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 200px; overflow: auto;">
                    <?php foreach ($manufacturers as $manufacturer) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($manufacturer['manufacturer_id'], $varleexport_manufacturers)) { ?>
                        <input type="checkbox" name="varleexport_manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" checked="checked" />
                        <?php echo $manufacturer['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="varleexport_manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                        <?php echo $manufacturer['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
               </div>
               <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $entry_select_all;?></a> | <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $entry_unselect_all;?></a>
         </div>
         
      </form>
      
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>