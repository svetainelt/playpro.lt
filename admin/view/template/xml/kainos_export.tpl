<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-kaina24export" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $entry_heading; ?></h3>
      </div>
      <div class="panel-body">
       <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form-kainos_export" class="form-horizontal">
       	<div class="alert alert-info">
  		<i class="fa fa-file-code-o fw"></i> <a class="alert-link" href="<?php echo $xml_url;?>" target="_blank"><?php echo $xml_url;?></a>
		</div>
       	<div class="form-group">
            <label class="col-sm-2 control-label" for="kainos_export_status">
            <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_status_info; ?>"><?php echo $entry_status; ?></span>
            </label>
            <div class="col-sm-10">
            	 <select name="kainos_export_status" class="form-control">
                  <?php if ($kainos_export_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
            </div>
        </div>
        
        
        <div class="form-group">
             <label class="col-sm-2 control-label" for="kainos_export_language_id">
              <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_language_id_info; ?>"><?php echo $entry_language_id; ?></span>
              </label>
             <div class="col-sm-10">
            <select name="kainos_export_language_id"  class="form-control">
                  <?php foreach ($languages as $language) { ?>
                  <?php if ($language['language_id'] == $kainos_export_language_id) { ?>
                  <option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
          	</div>
         </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="kainos_export_delivery_time">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_delivery_time_info; ?>"><?php echo $entry_delivery_time; ?></span>
            </label>
            <div class="col-sm-10">
            <input type="text" name="kainos_export_delivery_time" value="<?php echo $kainos_export_delivery_time; ?>" class="form-control"/>
            </div> 
          </div>
          
         <div class="form-group">
            <label class="col-sm-2 control-label" for="kainos_export_min_price">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_min_price_info; ?>"><?php echo $entry_min_price; ?></span>
            </label>
            <div class="col-sm-10">
            <input type="text" name="kainos_export_min_price" value="<?php echo $kainos_export_min_price; ?>" class="form-control"/>
            </div> 
          </div>
         
         <div class="form-group">
            <label class="col-sm-2 control-label" for="kainos_export_operator">
             <span data-toggle="tooltip" title="" data-original-title="<?php echo $entry_operator_info; ?>"><?php echo $entry_operator; ?></span>
            </label>
            <div class="col-sm-10">
            <div class="row">
            	<div class="col-sm-4">
	            <select name="kainos_export_operator"  class="form-control">
	                  <option value=""></option>
	                  <?php foreach ($operators as $operator) { ?>
	                  <?php if ($operator == $kainos_export_operator) { ?>
	                  <option value="<?php echo $operator; ?>" selected="selected"><?php echo $operator; ?></option>
	                  <?php } else { ?>
	                  <option value="<?php echo $operator; ?>"><?php echo $operator; ?></option>
	                  <?php } ?>
	                  <?php } ?>
	             </select>
	             </div>
	             <div class="col-sm-8">
	            <input type="text" name="kainos_export_operator_value" value="<?php echo $kainos_export_operator_value; ?>" class="form-control"/>
	            </div>
            </div>
            </div> 
          </div>
          
          
         <div class="form-group">
                <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                <div class="col-sm-10">
                  <select name="kainos_export_tax_class_id" id="kainos_export_tax_class_id" class="form-control">
                    <option value="0"><?php echo $text_none; ?></option>
                    <?php foreach ($tax_classes as $tax_class) { ?>
                    <?php if ($tax_class['tax_class_id'] == $kainos_export_tax_class_id) { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
           </div>
          
        
        <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 100px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $kainos_export_stores)) { ?>
                        <input type="checkbox" name="kainos_export_stores[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="kainos_export_stores[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $kainos_export_stores)) { ?>
                        <input type="checkbox" name="kainos_export_stores[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="kainos_export_stores[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
         </div>
        </div>
        
         <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_categories;?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 300px; overflow: auto;">
                    <?php foreach ($categories as $category) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($category['category_id'], $kainos_export_categories)) { ?>
                        <input type="checkbox" name="kainos_export_category[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
                        <?php echo $category['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="kainos_export_category[]" value="<?php echo $category['category_id']; ?>" />
                        <?php echo $category['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
               </div>
               <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $entry_select_all;?></a> | <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $entry_unselect_all;?></a>
         </div>
         </div>
         
         <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_manufacturers;?></label>
                <div class="col-sm-4">
                  <div class="well well-sm" style="height: 200px; overflow: auto;">
                    <?php foreach ($manufacturers as $manufacturer) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($manufacturer['manufacturer_id'], $kainos_export_manufacturers)) { ?>
                        <input type="checkbox" name="kainos_export_manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" checked="checked" />
                        <?php echo $manufacturer['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="kainos_export_manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                        <?php echo $manufacturer['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
               </div>
               <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $entry_select_all;?></a> | <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $entry_unselect_all;?></a>
         </div>
         
      </form>
      
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>