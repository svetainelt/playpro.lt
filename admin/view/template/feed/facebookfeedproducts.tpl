<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">

                <button type="submit" form="form-feed" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i
                            class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach($breadcrumbs as $breadcrumb ){ ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if($error_warning){ ?>
        <div class="alert alert-danger alert-dismissible"><i
                    class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if($success){ ?>
        <div class="alert alert-success alert-dismissible"><i
                    class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <fieldset class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-feed"
                      class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input-status">
                            <span data-toggle="tooltip"
                                  title="<?php echo $text_toolpit_1; ?>"><?php echo $entry_status; ?></span>
                        </label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_status" id="input-status" style="width: 100%;">
                                <?php if($facebookfeedproducts_status){ ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php }else{ ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <span data-toggle="tooltip"
                                  title="<?php echo $text_customer_group_tooltip; ?>"><?php echo $text_customer_group; ?></span>
                        </label>
                        <div class="col-sm-2">

                            <select name="feed_facebookfeedproducts_customer_grup"
                                    style="width: 100%;"><?php echo $curr['code']; ?>"
                                <option value="0"
                                <?php if('0' == $feed_facebookfeedproducts_customer_grup){ ?>
                                selected="selected" <?php } ?> ><?php echo $text_select_1 ?></option>

                                <?php foreach($customer_groups as $customer_group  ){ ?>
                                <option value="<?php echo $customer_group['customer_group_id']; ?>"
                                <?php if($customer_group['customer_group_id'] == $feed_facebookfeedproducts_customer_grup){ ?>
                                selected="selected" <?php } ?> ><?php echo $customer_group['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <span data-toggle="tooltip"
                                  title="<?php echo $text_currency_toolpit; ?>"><?php echo $text_currency; ?></span>
                        </label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_currency" style="width: 100%;">
                                <?php foreach($currencies as $curr  ){ ?>
                                <option value="<?php echo $curr['code']; ?>"
                                <?php if($curr['code'] == $facebookfeedproducts_currency){ ?>
                                selected="selected" <?php } ?> ><?php echo $curr['title']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <span data-toggle="tooltip"
                                  title="<?php echo $text_language_toolpit; ?>"><?php echo $text_language; ?></span>
                        </label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_language" style="width: 100%;">
                                <?php foreach($languages as $lang ){ ?>
                                <option value="<?php echo $lang['id']; ?>"
                                <?php if($lang['id']== $facebookfeedproducts_language){ ?>
                                selected="selected" <?php } ?> ><?php echo $lang['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-sm-3 control-label"><?php echo $text_disable_image; ?></label>
                        <div class="col-sm-3">
                            <input type="hidden" name="facebookfeedproducts_image_size_off"
                                   value="0" <?php if($facebookfeedproducts_image_size_off == 0){ ?>
                            checked="checked"<?php } ?>>
                            <input type="checkbox" name="facebookfeedproducts_image_size_off"
                                   value="1" <?php if($facebookfeedproducts_image_size_off == 1){ ?>
                            checked="checked"<?php } ?>>

                        </div>

                    </div>
                    <div class="form-group <?php if($facebookfeedproducts_image_size_off == 1){ ?>hidden <?php } ?>"
                         id="resize">

                        <label class="col-sm-3 control-label"><?php echo $text_image_size; ?></label>
                        <div class="col-sm-3">
                            <input name="facebookfeedproducts_image_size"
                                   value="<?php echo $facebookfeedproducts_image_size; ?>"><?php echo $text_px; ?>

                        </div>

                    </div>
                    <div class="form-group">

                        <label class="col-sm-3 control-label"><?php echo $text_not_google_category; ?></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_google_status" id="input-google_status"
                                    style="width: 100%;">
                                <?php if($facebookfeedproducts_google_status){ ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php }else{ ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-sm-3 control-label"><span data-toggle="tooltip"
                                                                    title="<?php echo $text_image_empty; ?>"><?php echo $text_image_empty; ?></span></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_image_empty" id="input-image_empty" style="width: 100%;">
                                <?php if($facebookfeedproducts_image_empty){ ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php }else{ ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            <span data-toggle="tooltip"
                                  title="<?php echo $text_options_toolpit; ?>"><?php echo $text_options; ?></span>
                        </label>
                        <div class="col-sm-2" style="text-align: center">
                            <label class="control-label">
                                <span data-toggle="tooltip" title="<?php echo $text_select_toolpit; ?>">
                                    <?php echo $text_select; ?>
                                </span>
                            </label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $text_color; ?></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_color[]" multiple="multiple" size="5"
                                    style="height: 100%!important; width: 100%;">
                                <option value=""><?php echo $text_color_select; ?></option>
                                <?php foreach($options as $opt ){ ?>
                                <option value="<?php echo $opt['name']; ?>"
                                <?php if(is_array($facebookfeedproducts_color)){ ?>
                                <?php if(in_array($opt['name'] ,$facebookfeedproducts_color)){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php }else{ ?>
                                <?php if($opt['name'] == $facebookfeedproducts_color){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php } ?>
                                >
                                <?php echo $opt['name']; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $text_size; ?></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_size[]" multiple="multiple" size="5"
                                    style="height: 100%!important; width: 100%;">
                                <option value=""><?php echo $text_size_select; ?></option>
                                <?php foreach($options as $opt ){ ?>
                                <option value="<?php echo $opt['name']; ?>"
                                <?php if(is_array($facebookfeedproducts_size )){ ?>
                                <?php if(in_array($opt['name'] ,$facebookfeedproducts_size)){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php }else{ ?>
                                <?php if($opt['name'] == $facebookfeedproducts_size){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php } ?>
                                >
                                <?php echo $opt['name']; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $text_pattern; ?></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_pattern[]" multiple="multiple" size="5"
                                    style="height: 100%!important; width: 100%;">
                                <option value=""><?php echo $text_pattern_select; ?></option>
                                <?php foreach($options as $opt ){ ?>
                                <option value="<?php echo $opt['name']; ?>"
                                <?php if(is_array($facebookfeedproducts_pattern)){ ?>
                                <?php if(in_array($opt['name'] ,$facebookfeedproducts_pattern)){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php }else{ ?>
                                <?php if($opt['name'] == $facebookfeedproducts_pattern){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php } ?>
                                >
                                <?php echo $opt['name']; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $text_material; ?></label>
                        <div class="col-sm-2">
                            <select name="facebookfeedproducts_material[]" multiple="multiple" size="5"
                                    style="height: 100%!important; width: 100%;">
                                <option value=""><?php echo $text_material_select; ?></option>
                                <?php foreach($options as $opt ){ ?>
                                <option value="<?php echo $opt['name']; ?>"
                                <?php if(is_array($facebookfeedproducts_material)){ ?>
                                <?php if(in_array($opt['name'],$facebookfeedproducts_material)){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php }else{ ?>
                                <?php if($opt['name'] == $facebookfeedproducts_material){ ?>
                                selected="selected"
                                <?php } ?>
                                <?php } ?>
                                >
                                <?php echo $opt['name']; ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </form>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input-status">

                            <span data-toggle="tooltip"
                                  title="<?php echo $text_update_toolpit; ?>"><?php echo $entry_google_product_category; ?></span>
                            <p><a onclick="$('#form_ub').click();" class="button"><?php echo $text_update; ?></a></p>
                            <input type='submit' name="form_ub" id="form_ub" hidden></label>
                        <div class="col-sm-9">
                            <td><textarea style="width: 100%" rows="1" name="google_product_category_url"></textarea>
                                <?php echo $text_paragraf; ?>
                        </div>
                    </div>
                </form>

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input-status">

                            <span data-toggle="tooltip"
                                  title="<?php echo $text_update_toolpit; ?>"><?php echo $entry_google_product_category; ?></span>
                            <p><a onclick="$('#form_feed_ud').click();" class="button"><?php echo $text_update; ?></a>
                            </p>
                            <input type='submit' name="form_feed_ud" id="form_feed_ud" hidden></label>
                        <div class="col-sm-9">
                            <td><textarea style="width: 100%" rows="1" name="facebook_product_category_url"></textarea>
                                <?php echo $text_paragraf_facebook; ?>
                        </div>
                    </div>
                </form>

                <?php if ( empty($facebookfeedproducts_google_status)) { $facebookfeedproducts_google_status = "0";}?>

                <?php if (!$facebookfeedproducts_google_status) { ?>

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#google-feed-catigory-wrap"><?php echo $text_google_category; ?></a>
                    </li>
                    <li>
                        <a data-toggle="tab"
                           href="#facebook-feed-catigory-wrap"><?php echo $text_facebook_category; ?></a>
                    </li>
                </ul>

                <?php } ?>

                <?php if ( !$facebookfeedproducts_google_status) { ?>
                <div class="tab-content">
                    <div class="tab-pane active" id="google-feed-catigory-wrap">
                        <?php } ?>

                        <?php if(!empty($googlemaincategorys)){ ?>
                        <?php if(!empty($categories)){ ?>
                        <?php if($facebookfeedproducts_google_status){ ?>
                        <div class="row" style="padding: 10px 10px 26px 10px;">

                            <div class="col-xs-6 text-center pos-res">
                                <div class=" ">
                                    <input type="button" class="btn btn-default"
                                           name="facebookfeedproducts_all_category" data-status="1"
                                           value="<?php echo $text_category_on; ?>">
                                </div>
                                <div class=" all_category_1 pos-res"></div>
                            </div>
                            <div class="col-xs-6 text-center pos-res">
                                <div class=" ">
                                    <input type="button" class="btn btn-default"
                                           name="facebookfeedproducts_all_category" data-status="0"
                                           value="<?php echo $text_category_off; ?>">
                                </div>
                                <div class=" all_category_0 pos-res"></div>

                            </div>

                        </div>
                        <td class="save_category">
                            <table class="table">
                                <tr>
                                    <th style="padding-left: 20px"><?php echo $text_category; ?> </th>
                                    <th> <?php echo $text_status; ?></th>
                                </tr>
                                <?php foreach($categories as $category ){ ?>
                                <tr>

                                    <td style="padding-left: 20px">
                                        <?php echo $category['name']; ?>
                                    </td>
                                    <td>
                                        <div class=" text-center pos-res">
                                            <div class="onoffswitch ">
                                                <input type="checkbox"
                                                       name="facebookfeedproducts_category_<?php echo $category['category_id']; ?>"
                                                       data-id="<?php echo $category['category_id']; ?>"
                                                       class="onoffswitch-checkbox"
                                                       id="facebookfeedproducts_category_<?php echo $category['category_id']; ?>"
                                                <?php if(isset($saveCategory[$category['category_id']])){ ?>
                                                checked="checked"<?php } ?>
                                                >
                                                <label class="onoffswitch-label"
                                                       for="facebookfeedproducts_category_<?php echo $category['category_id']; ?>"></label>
                                            </div>
                                            <span class="span-block"
                                                  data-field="facebookfeedproducts_category_<?php echo $category['category_id']; ?>"></span>
                                        </div>
                                    </td>
                                </tr>

                                <?php } ?>
                            </table>

                            <?php }else{ ?>
                            <div class="google_category_true"
                                 style="font-weight:700;width:100%;display:-webkit-inline-box;">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">
                                        <span data-toggle="tooltip"
                                              title="<?php echo $text_toolpit_1; ?>"><?php echo $entry_product_category; ?></span>
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">
                                        <span data-toggle="tooltip"
                                              title="<?php echo $text_toolpit_2; ?>"><?php echo $entry_google_main_product_category; ?></span>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <label class="col-sm-12 control-label">
                                        <span data-toggle="tooltip"
                                              title="<?php echo $text_toolpit_3; ?>"><?php echo $entry_google_all_product_category; ?></span>
                                    </label>
                                </div>
                            </div>
                            <?php foreach($categories as $category ){ ?>
                            <?php $cat = ''; ?>

                            <div style="word-break: break-word;padding: 5px; width:100%;display:-webkit-inline-box;">
                                <div class="col-sm-2" style="padding: 5px;">
                                    <?php echo $category['name']; ?>
                                </div>
                                <div class="col-sm-2" style="padding: 5px;">
                                    <select name="googlemaincategory_<?php echo $category['category_id']; ?>"
                                            data-int="<?php echo $category['category_id']; ?>" style="width: 100%;">
                                        <option value=""><?php echo $text_select_1; ?></option>
                                        <?php foreach($googlemaincategorys as $googlemaincategory ){ ?>
                                        <option value="<?php echo $googlemaincategory['google_category_id']; ?>"
                                        <?php if($googlemaincategory['google_category_id'] == $category['google_category_id']){ ?>
                                        selected="selected" <?php $cat = $googlemaincategory['google_category_id'];  } ?>
                                        ><?php echo $googlemaincategory['google_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-8 max-bottom-ul" style="padding: 5px;">
                                    <input type='text' style="width: 100%;"
                                           name="googlecategory_<?php echo $category['category_id']; ?>"
                                           data-int="<?php if($cat){  echo $cat;  } ?>"
                                           data-category_id="<?php if($category['category_id']){  echo $category['category_id'];  } ?>"
                                           value="<?php echo $category['google_category_name']; ?>" <?php if(!$cat){ ?>
                                    disabled <?php } ?> >
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>

                            <?php } ?>

                            <?php if (isset($facebookfeedproducts_google_status) and !$facebookfeedproducts_google_status) { ?>
                    </div>
                    <div class="tab-pane" id="facebook-feed-catigory-wrap">

                        <?php if (isset($facebookmaincategorys) and $facebookmaincategorys) { ?>
                        <?php if ($categories) { ?>
                        <?php if ($facebookfeedproducts_google_status) { ?>
                        <div class="row" style="padding: 10px 10px 26px 10px;">

                            <div class="col-xs-6 text-center pos-res">
                                <div>
                                    <input type="button" class="btn btn-default"
                                           name="f_feed_facebookfeedproducts_all_category" data-status="1"
                                           value="<?php echo $text_category_on; ?>">
                                </div>
                                <div class=" f_all_category_1 pos-res"></div>
                            </div>
                            <div class="col-xs-6 text-center pos-res">
                                <div>
                                    <input type="button" class="btn btn-default"
                                           name="f_feed_facebookfeedproducts_all_category" data-status="0"
                                           value="<?php echo $text_category_off; ?>">
                                </div>
                                <div class="f_all_category_0 pos-res"></div>

                            </div>

                        </div>
                        <td class="f_save_category">
                            <table class="table">
                                <tr>
                                    <th style="padding-left: 20px"><?php echo $text_category; ?></th>
                                    <th><?php echo $text_status; ?></th>
                                </tr>
                                <?php foreach($categoriesf as $_key => $category) { ?>
                                <tr>

                                    <td style="padding-left: 20px">
                                        <?php echo $category['name']; ?>
                                    </td>
                                    <td>
                                        <div class=" text-center pos-res">
                                            <div class="onoffswitch ">
                                                <input type="checkbox"
                                                       name="f_feed_facebookfeedproducts_category_<?php echo $category['category_id']; ?>"
                                                       data-id="<?php echo $category['category_id']; ?>"
                                                       class="onoffswitch-checkbox"
                                                       id="f_feed_facebookfeedproducts_category_<?php echo $category['category_id']; ?>"
                                                <?php if ($saveCategoryF[$category['category_id']]) { ?>
                                                checked="checked"<?php } ?>
                                                >
                                                <label class="onoffswitch-label"
                                                       for="f_feed_facebookfeedproducts_category_<?php echo $category['category_id']; ?>"></label>
                                            </div>
                                            <span class="span-block"
                                                  data-field="f_feed_facebookfeedproducts_category_<?php echo $category['category_id']; ?>"></span>
                                        </div>
                                    </td>
                                </tr>

                                <?php } ?>
                            </table>
                        </td>
                        <?php } else { ?>

                        <div class="facebook_category_true"
                             style="font-weight:700;width:100%;display:-webkit-inline-box;">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">
                                                <span data-toggle="tooltip"
                                                      title="<?php echo $text_toolpit_1; ?>"><?php echo $entry_product_category; ?></span>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">
                                                <span data-toggle="tooltip"
                                                      title="<?php echo $text_toolpit_2_facebook; ?>"><?php echo $entry_facebook_main_product_category; ?></span>
                                </label>
                            </div>
                            <div class="col-sm-8">
                                <label class="col-sm-12 control-label">
                                                <span data-toggle="tooltip"
                                                      title="<?php echo $text_toolpit_3_facebook; ?>"><?php echo $entry_facebook_all_product_category; ?></span>
                                </label>
                            </div>
                        </div>
                        <?php foreach($categoriesf as $_key => $category) { ?>
                        <?php $cat = ''; ?>

                        <div style="word-break: break-word;padding: 5px; width:100%;display:-webkit-inline-box;">
                            <div class="col-sm-2" style="padding: 5px;">
                                <?php echo $category['name']; ?>
                            </div>
                            <div class="col-sm-2" style="padding: 5px;">
                                <select name="facebookmaincategory_<?php echo $category['category_id']; ?>"
                                        data-int="<?php echo $category['category_id']; ?>" style="width: 100%;">
                                    <option value=""><?php echo $text_select_1; ?></option>
                                    <?php foreach($facebookmaincategorys as $_key => $facebookmaincategory) { ?>
                                    <option value="<?php echo $facebookmaincategory['facebook_category_id']; ?>"
                                    <?php if ($facebookmaincategory['facebook_category_id'] == $category['facebook_category_id']) { ?>
                                    selected="selected"<?php $cat = $facebookmaincategory['facebook_category_id']; ?>
                                    <?php } ?>
                                    ><?php echo $facebookmaincategory['facebook_category_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-8 max-bottom-ul" style="padding: 5px;">
                                <input type='text' style="width: 100%;"
                                       name="facebookcategory_<?php echo $category['category_id']; ?>"
                                       data-int="<?php if ($cat) { ?>
<?php echo $cat; ?><?php } ?>
"
                                       data-category_id="<?php if ($category['category_id']) { ?>
<?php echo $category['category_id']; ?><?php } ?>
"
                                       value="<?php echo $category['facebook_category_name']; ?>"<?php if ( !$cat) { ?>
                                disabled<?php } ?>
                                >
                            </div>
                        </div>
                        <?php } ?>

                        <?php } ?>
                        <?php } ?>

                        <?php } ?>
                    </div>

                    <?php } ?>

                    <table class="table">
                        <tr>
                            <div class="alert alert-danger " id="url_feed_warning" style="display: none">
                                <i class="fa fa-exclamation-circle"></i>
                                <span id="url_feed_warning_text"></span>
                            </div>
                        </tr>
                        <tr>
                            <td>
                                <label class="col-sm-2 control-label"
                                       for="input-data-feed"><?php echo $entry_data_feed; ?></label>
                                <div class="col-sm-1" style="padding-left: 9px;">
                                    <a target="_blank" class="btn btn-primary url_button url-feed-btn" id="url_button"
                                       style="width: 100%;"
                                       href="<?php echo $link; ?>"><?php echo $text_url; ?></a>
                                </div>
                                <div class="col-sm-6">
                                    <a target="_blank" class="btn btn-primary disabled_button url-feed-btn"
                                       id="download_button"><?php echo $text_download; ?></a>
                                </div>
                            </td>
                        </tr>

                    </table>

                </div>
        </fieldset>
        <fieldset class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_cron_title; ?></h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td colspan="2">
                            <?php echo $text_cron_description; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><?php echo $text_cron_link; ?></label>
                        </td>
                        <td style="position: relative">
                            <input class="form-control copy_link" type="text"
                                   value="curl <?php echo $cron_link_generation; ?>" disabled>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><?php echo $text_file_link; ?></label>
                        </td>
                        <td style="position: relative">
                            <input class="form-control copy_link" type="text" value="<?php echo $link_file; ?>"
                                   disabled>

                        </td>
                    </tr>
                </table>

            </div>
        </fieldset>
        <fieldset class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_generator_url ; ?></h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td colspan="4">
                            <?php echo $text_generator_description ; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label"><?php echo $text_generation_link ; ?></label>
                        </td>

                        <td style="position: relative">
                            <div class="alert alert-danger alert-dismissible alert-disable"><i
                                        class="fa fa-exclamation-circle"></i> <?php echo $error_selected_fields ; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                            <select id="generation_lang" style="width: 100%;">
                                <option value="0"><?php echo $text_select ; ?></option>
                                <?php foreach( $languages as $lang ) { ?>
                                <option value="<?php echo $lang['id'] ; ?>"><?php echo $lang['name'] ; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td style="position: relative">
                            <div class="alert alert-danger alert-dismissible alert-disable"><i
                                        class="fa fa-exclamation-circle"></i> <?php echo $error_selected_fields ; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                            <select id="generation_curr" style="width: 100%;">
                                <option value="0"><?php echo $text_select ; ?></option>
                                <?php foreach( $currencies as $curr ) { ?>
                                <option value="<?php echo $curr['code'] ; ?>"><?php echo $curr['title'] ; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td style="position: relative">
                            <a href="javascript:void(0)" class="btn btn-primary" id="create_link"
                               onclick="Generation.link()"><?php echo $text_generation_btn ; ?></a>
                        </td>
                    </tr>
                    <tr>

                        <td colspan="4" style="text-align: center;">
                            <label class="" id="result_link"></label>
                        </td>

                    </tr>
                </table>

            </div>
        </fieldset>

        <fieldset class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_products; ?></h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $text_product_feed_list; ?></label>
                    <div class="col-sm-6 settings-product-list-wrap">
                        <select name="feed_facebookfeedproducts_productslist_state" class="settings settings-product-list settings-ajax">
                            <option value="0"><?php echo $text_automatically; ?></option>
                            <option <?php if (isset($feed_facebookfeedproducts_productslist_state) and $feed_facebookfeedproducts_productslist_state == 1) { ?>
                                selected="selected"<?php } ?>
                                    value="1"><?php echo $text_custom; ?></option>
                        </select>
                        <img src="view/image/facebookfeedproducts/load.gif" class="setting-save-ico setting-save-proccess-ico">
                        <i class="fa fa-check setting-save-ico setting-save-success-ico" ></i>
                    </div>
                </div>

                <div id="facebookfeedproducts_productlist_wrap">
                    <div class="form-group" style="border: 0;">
                        <label class="col-sm-3 control-label"><?php echo $text_list; ?>:</label>
                        <div class="col-sm-6 settings-product-list-wrap">
                            <select name="feed_facebookfeedproducts_producmargestate" class="settings settings-product-list settings-ajax">
                                <option value="-"> --- </option>
                                <option <?php if (isset($feed_facebookfeedproducts_producmargestate) and $feed_facebookfeedproducts_producmargestate == 0) { ?>
                                    selected="selected"<?php } ?>
                                        value="0"><?php echo $text_only_selected; ?></option>
                                <option <?php if (isset($feed_facebookfeedproducts_producmargestate) and $feed_facebookfeedproducts_producmargestate == 1) { ?>
                                    selected="selected"<?php } ?>
                                        value="1"><?php echo $text_exclude_selected_from_the_list; ?></option>
                                <option <?php if (isset($feed_facebookfeedproducts_producmargestate) and $feed_facebookfeedproducts_producmargestate == 2) { ?>
                                    selected="selected"<?php } ?>
                                        value="2"><?php echo $text_add_selected_to_list; ?></option>
                                <option <?php if (isset($feed_facebookfeedproducts_producmargestate) and $feed_facebookfeedproducts_producmargestate == 3) { ?>
                                    selected="selected"<?php } ?>
                                        value="3"><?php echo $text_except_the_chosen_one; ?></option>
                            </select>
                            <img src="view/image/facebookfeedproducts/load.gif" class="setting-save-ico setting-save-proccess-ico">
                            <i class="fa fa-check setting-save-ico setting-save-success-ico" ></i>
                        </div>
                    </div>
                    <hr>

                    <div style="margin-bottom: 20px">

                        <div class="form-group" style="border: 0;">
                            <div class="col-sm-2 settings-product-list-wrap">
                                <button class="product-feed-lest-clear"><?php echo $text_remove_all; ?></button>
                            </div>
                            <div class="col-sm-2 settings-product-list-wrap">
                                <button class="product-feed-lest-checkall"><?php echo $text_choose_all; ?></button>
                            </div>
                        </div>

                    </div>

                    <div class="product-list" style="display: grid;position: relative;">

                    </div>
                </div>
            </div>
        </fieldset>
    </div>

</div>
<script>
    var checkCategory = {
        'check': function () {
            var arr = $('select[name^=googlemaincategory]');
            var a = 0;

            for (var i = 0; i < arr.length; i++) {
                let item = arr[i];
                if (item.value) {
                    a++;
                }
            }
            var b = 0;
            var brr = new Array();
            $('input[name^=facebookfeedproducts_category_]').each(function () {
                if ($(this).is(':checked')) {
                    brr.push($(this).attr('data-id'));
                    b++;
                }
            })
            if (a || b) {
                $('#download_button').attr("href", "<?php echo $data_feed; ?>");
                $('#download_button').removeClass("disabled_button");
                $('#url_button').attr("href", "<?php echo $link; ?>");
                $('#url_button').removeClass("disabled_button");
            } else {
                $('#download_button').removeAttr("href");
                $('#download_button').addClass("disabled_button");
                $('#url_button').removeAttr("href");
                $('#url_button').addClass("disabled_button");
            }
        },
        'checkf': function () {
            var arr = $('select[name^=facebookmaincategory]');
            var a = 0;

            for (var i = 0; i < arr.length; i++) {
                let item = arr[i];
                if (item.value) {
                    a++;
                }
            }
            var b = 0;
            var brr = new Array();
            $('input[name^=f_feed_facebookfeedproducts_category_]').each(function () {
                if ($(this).is(':checked')) {
                    brr.push($(this).attr('data-id'));
                    b++;
                }
            })
            // if (a || b) {
            $('#download_button').attr("href", "<?php echo $data_feed; ?>");
            $('#download_button').removeClass("disabled_button");
            $('#url_button').attr("href", "<?php echo $link; ?>");
            $('#url_button').removeClass("disabled_button");
            //    } else {
            // $('#download_button').removeAttr("href");
            // $('#download_button').addClass("disabled_button");
            // $('#url_button').removeAttr("href");
            // $('#url_button').addClass("disabled_button");
            //  }
        }
    }
    var productSettings = {
        'updateList': function (page, status) {
            if (status) {
                $.ajax({
                    type: 'POST',
                    url: 'index.php?route=feed/facebookfeedproducts/customproductgetlist&token=<?php echo $token; ?>',
                    dataType: 'json',
                    data: {
                        page: page,
                    },
                    beforeSend: function () {
                    },
                    success: function (html, status) {
                        if (status == 'success') {
                            $('#facebookfeedproducts_productlist_wrap .product-list').html(html);
                            $('#facebookfeedproducts_productlist_wrap').css('display', 'inherit');
                        }
                    },
                    complete: function () {
                    }
                });
            } else {
                $('#facebookfeedproducts_productlist_wrap .product-list').html('');
                $('#facebookfeedproducts_productlist_wrap').css('display', 'none');
            }
        },
        'updateSettings': function (e) {
            $(this).prop('disabled', true);
            var key = $(this).attr('name');
            let val = $(this).val();

            $(this).siblings('.setting-save-ico').css('display', 'none');

            var thisSettingsProduct = this;

            $.ajax({
                type: 'POST',
                url: 'index.php?route=feed/facebookfeedproducts/updateproductsettings&token=<?php echo $token; ?>',
                dataType: 'json',
                data: {
                    key: key,
                    value: val,
                },
                beforeSend: function () {
                    $(thisSettingsProduct).siblings('.setting-save-ico.setting-save-proccess-ico').css('display', 'inline');
                },
                success: function (json) {
                    let result = json;
                    if (key == 'feed_facebookfeedproducts_productslist_state' && result.status) {
                        productSettings.updateList(1, parseInt(val));
                    }

                    $(thisSettingsProduct).siblings('.setting-save-ico').css('display', 'none');
                    $(thisSettingsProduct).siblings('.setting-save-ico.setting-save-success-ico').css('display', 'inline');
                    setTimeout(function (e) {
                        $(thisSettingsProduct).siblings('.setting-save-ico').css('display', 'none');
                    }, 2000);
                },
                complete: function (e) {
                    $(thisSettingsProduct).prop('disabled', false);
                }
            });
        },
        'updateProductList': function (e) {
            $(this).prop('disabled', true);
            var key = $(this).attr('name');

            var val = null;

            if ($(this).is(':checked') == 0) {
                val = 0;
            } else {
                val = 1;
            }

            $(this).siblings('.setting-save-ico').css('display', 'none');

            var thisSettingsProduct = this;

            $.ajax({
                type: 'POST',
                url: 'index.php?route=feed/facebookfeedproducts/updateproductlist&token=<?php echo $token; ?>',
                dataType: 'json',
                data: {
                    key: key,
                    value: val,
                },
                beforeSend: function () {
                    $(thisSettingsProduct).siblings('.setting-save-ico.setting-save-proccess-ico').css('display', 'inline');
                },
                success: function (json) {
                    let result = json;
                    if (key == 'feed_facebookfeedproducts_productslist_state' && result.status) {
                        productSettings.updateList(1, parseInt(val));
                    }

                    $(thisSettingsProduct).siblings('.setting-save-ico').css('display', 'none');
                    $(thisSettingsProduct).siblings('.setting-save-ico.setting-save-success-ico').css('display', 'inline');
                    setTimeout(function (e) {
                        $(thisSettingsProduct).siblings('.setting-save-ico').css('display', 'none');
                    }, 2000);
                },
                complete: function (e) {
                    $(thisSettingsProduct).prop('disabled', false);
                }
            });
        },
        'updatePagenation': function (e) {
            $('.my-incredible-name').addClass('disable-area');

            event.preventDefault();
            let page = $(this).attr('data-value');
            productSettings.updateList(page, 1);
            // $('.my-incredible-name').removeClass('disable-area');
        },
        'removeAll': function (e) {
            $('.my-incredible-name').addClass('disable-area');
            $.ajax({
                type: 'POST',
                url: 'index.php?route=feed/facebookfeedproducts/removeallproductlist&token=<?php echo $token; ?>',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (data) {
                    if (data == productSettings.updateList(1, 1)) {
                        productSettings.updateList(1, 1);
                    }
                },
                complete: function (e) {
                    $('.my-incredible-name').removeClass('disable-area');
                }
            });
        },
        'checkAll': function (e) {
            $('.my-incredible-name').addClass('disable-area');

            $.ajax({
                type: 'POST',
                url: 'index.php?route=feed/facebookfeedproducts/checkallproductlist&token=<?php echo $token; ?>',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (data) {
                    if (data == productSettings.updateList(1, 1)) {
                        productSettings.updateList(1, 1);
                    }
                },
                complete: function (e) {
                    $('.my-incredible-name').removeClass('disable-area');
                }
            });
        }
    }
    let Generation = {
        'link': function () {
            let host_url = '<?php echo $link; ?>';
            let cur = "&cur=";
            let lang = "&lang=";
            let customer_curr = Generation.get('generation_curr');
            let customer_lang = Generation.get('generation_lang');
            if ((customer_curr != 0) && (customer_lang != 0)) {
                host_url += lang + customer_lang + cur + customer_curr;
                let a = document.createElement('a');
                a.href = host_url;
                a.innerHTML = host_url;
                a.setAttribute('target', "_blank");

                document.getElementById('result_link').textContent = '';
                document.getElementById('result_link').appendChild(a);
            }
        },
        'get': function (name) {
            let element = document.getElementById(name);
            let val = element.value;
            if (val == 0) {
                element.parentNode.classList.add('error');
            } else {
                element.parentNode.classList.remove('error');
            }
            return val;
        }
    };
    checkCategory.check();
    checkCategory.checkf();

    productSettings.updateList(1, parseInt($('select[name="feed_facebookfeedproducts_productslist_state"]').val()));
    $('.settings.settings-product-list.settings-ajax').on('change', productSettings.updateSettings);
    $(document).on('change', '.list.settings-product-list.settings-ajax', productSettings.updateProductList);
    $(document).on('click', '.product-list-pagination-href', productSettings.updatePagenation);
    $(document).on('click', '.product-feed-lest-clear', productSettings.removeAll);
    $(document).on('click', '.product-feed-lest-checkall', productSettings.checkAll);

    $('input[name="facebookfeedproducts_all_category"]').on('click', function () {
        var checked,
            btn_save = $(this),
            status = btn_save.attr('data-status'),
            all_category = new Array();
        if (status == 1) {
            checked = true;
        } else if (status == 0) {
            checked = false;
        }

        $('input[name^=facebookfeedproducts_category_]').each(function () {
            if (status != $(this).is(':checked')) {
                all_category.push($(this).attr('data-id'))
            }
        })

        $['ajax']({
            type: 'POST',
            url: 'index.php?route=feed/facebookfeedproducts/save_all_category&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {all: all_category, status: status},
            beforeSend: function () {
                $('.all_category_' + status).addClass('lds-hourglass');
                $('.save_category').addClass('block_content');
            },
            success: function (data) {
                if (data.save) {
                    $('input[name^=facebookfeedproducts_category_]').each(function () {
                        if (status != $(this).is(':checked')) {
                            $(this).prop('checked', checked)
                            //    $('span[data-field="'+$(this).attr('name')+'"]').html('<p style="color:#49E845">save</p>');
                        }
                    })
                    $('.all_category_' + status).html('<p style="color:#49E845;position: absolute;right: 50%;">save</p>');
                    checkCategory.check();
                } else {
                    $('.all_category_' + status).html('<p style="color:red;position: absolute;right: 39%;">Category list empty</p>');
                }
            },
            complete: function () {
                $('.all_category_' + status).removeClass('lds-hourglass');
                $('.save_category').removeClass('block_content');
                setTimeout(function () {
                    $('.all_category_' + status).html('');
                }, 1500)
            }
        });
    })

    $('input[name="f_feed_facebookfeedproducts_all_category"]').on('click', function () {
        var checked,
            btn_save = $(this),
            status = btn_save.attr('data-status'),
            all_category = new Array();
        if (status == 1) {
            checked = true;
        } else if (status == 0) {
            checked = false;
        }

        $('input[name^=f_feed_facebookfeedproducts_category_]').each(function () {
            if (status != $(this).is(':checked')) {
                all_category.push($(this).attr('data-id'));
            }
        });

        $.ajax({
            type: 'POST',
            url: 'index.php?route=feed/facebookfeedproducts/f_save_all_category&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                all: all_category,
                status: status,
            },
            beforeSend: function () {
                $('.f_all_category_' + status).addClass('lds-hourglass');
                $('.f_save_category').addClass('block_content');
            },
            success: function (data) {
                if (data.save) {
                    $('input[name^=f_feed_facebookfeedproducts_category_]').each(function () {
                        if (status != $(this).is(':checked')) {
                            $(this).prop('checked', checked);
                            //    $('span[data-field="'+$(this).attr('name')+'"]').html('<p style="color:#49E845">save</p>');
                        }
                    });
                    $('.f_all_category_' + status).html('<p style="color:#49E845;position: absolute;right: 50%;">save</p>');
                    checkCategory.checkf();
                } else {
                    $('.f_all_category_' + status).html('<p style="color:red;position: absolute;right: 39%;">Category list empty</p>');
                }
            },
            complete: function () {
                $('.f_all_category_' + status).removeClass('lds-hourglass');
                $('.f_save_category').removeClass('block_content');
                setTimeout(function () {
                    $('.f_all_category_' + status).html('');
                }, 1500);
            }
        });
    });

    $('input[name^=facebookfeedproducts_category_]').on('click', function (e) {
        var checked, old_checked,
            element = $(this),
            name = element.attr('name'),
            id = element.attr('data-id');
        if (element.is(':checked')) {
            checked = 1;
            old_checked = false;
        } else {
            checked = 0;
            old_checked = true;
        }
        $['ajax']({
            type: 'POST',
            url: 'index.php?route=feed/facebookfeedproducts/savecategory&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {id: id, val: checked},
            beforeSend: function () {
                $('span[data-field="' + name + '"]').html('<div class="lds-hourglass"></div>');
            },
            success: function (data) {
                if (data.save) {
                    $('span[data-field="' + name + '"]').html('<p style="color:#49E845">save</p>');
                    checkCategory.check();
                } else {
                    $('span[data-field="' + name + '"]').html('<p style="color:red">error</p>');
                    element.prop('checked', old_checked);
                }
                setTimeout(function () {
                    $('span[data-field="' + name + '"]').html('');
                }, 2000);
            },
        });
    });

    $('input[name^=f_feed_facebookfeedproducts_category_]').on('click', function (e) {
        var checked, old_checked,
            element = $(this),
            name = element.attr('name'),
            id = element.attr('data-id');
        if (element.is(':checked')) {
            checked = 1;
            old_checked = false;
        } else {
            checked = 0;
            old_checked = true;
        }
        $.ajax({
            type: 'POST',
            url: 'index.php?route=feed/facebookfeedproducts/savecategoryf&token=<?php echo $token; ?>',
            dataType: 'json',
            data: {
                id: id,
                val: checked,
            },
            beforeSend: function () {
                $('span[data-field="' + name + '"]').html('<div class="lds-hourglass"></div>');
            },
            success: function (data) {
                if (data.save) {
                    $('span[data-field="' + name + '"]').html('<p style="color:#49E845">save</p>');
                    checkCategory.checkf();
                } else {
                    $('span[data-field="' + name + '"]').html('<p style="color:red">error</p>');
                    element.prop('checked', old_checked);
                }
                setTimeout(function () {
                    $('span[data-field="' + name + '"]').html('');
                }, 2000);
            },
        });
    });

    $('select[name^=facebookmaincategory]').change(function () {
        var button = $(this);
        var facebook_main_category_id = $('option:selected', this).val();
        var category_id = button.attr('data-int');
        button.prop('disabled', true);
        $.ajax({
            type: 'GET',
            url: 'index.php?route=feed/facebookfeedproducts/savefacebookcat&token=<?php echo $token; ?>&facebook_main_category_id=' + facebook_main_category_id + '&category_id=' + category_id,
            dataType: 'json',
            success: function () {
            },
            complete: function () {
                button.prop('disabled', false);
                $('input[name=facebookcategory_' + category_id + ']').val('');
                $('input[name=facebookcategory_' + category_id + ']').attr('data-int', facebook_main_category_id);
                $('input[name=facebookcategory_' + category_id + ']').prop('disabled', false);
                if (facebook_main_category_id == "") {
                    $('input[name=facebookcategory_' + category_id + ']').prop('disabled', true);
                }
                checkCategory.checkf();
            }
        });
    });

    $('select[name^=googlemaincategory]').change(function () {
        var button = $(this);
        var google_main_category_id = $('option:selected', this).val();
        var category_id = button.attr('data-int');
        button.prop('disabled', true);
        $['ajax']({
            type: 'GET',
            url: 'index.php?route=feed/facebookfeedproducts/savegooglecat&token=<?php echo $token; ?>&google_main_category_id=' + google_main_category_id + '&category_id=' + category_id,
            dataType: 'json',
            success: function () {
            },
            complete: function () {
                button.prop('disabled', false);
                $('input[name=googlecategory_' + category_id + ']').val('');
                $('input[name=googlecategory_' + category_id + ']').attr('data-int', google_main_category_id);
                $('input[name=googlecategory_' + category_id + ']').prop('disabled', false);
                if (google_main_category_id == "") {
                    $('input[name=googlecategory_' + category_id + ']').prop('disabled', true);
                }
                checkCategory.check();
            }
        });
    });
    var name;
    var category_id;
    $('input[name^=googlecategory]').autocomplete({
        source: function (request, response) {
            var filter_id = $(this).attr("data-int");
            name = $(this).prop("name");
            category_id = $(this).attr("data-category_id");
            $['ajax']({
                url: 'index.php?route=feed/facebookfeedproducts/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request) + '&filter_id=' + filter_id,
                dataType: 'json',
                success: function (json) {
                    response($['map'](json, function (item) {
                        return {
                            label: item['google_category_name'],
                            value: item['google_category_id']
                        }
                    }));
                }
            });
        },
        select: function (item) {
            $['ajax']({
                type: 'GET',
                url: 'index.php?route=feed/facebookfeedproducts/savegooglecat&token=<?php echo $token; ?>&google_category_id=' + item['value'] + '&category_id=' + category_id,
                dataType: 'json',
                success: function () {
                },
                complete: function () {
                    $('input[name=' + name + ']').prop('value', item['label']);
                }
            });

            return false;
        },
        focus: function (item) {
            return false;
        }
    });

    $('input[name^=facebookcategory]').autocomplete({
        source: function (request, response) {
            var filter_id = $(this).attr("data-int");
            name = $(this).prop("name");
            category_id = $(this).attr("data-category_id");
            $.ajax({
                url: 'index.php?route=feed/facebookfeedproducts/autocompletef&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request) + '&filter_id=' + filter_id,
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['facebook_category_name'],
                            value: item['facebook_category_id']
                        };
                    }));
                }
            });
        },
        select: function (item) {
            $.ajax({
                type: 'GET',
                url: 'index.php?route=feed/facebookfeedproducts/savefacebookcat&token=<?php echo $token; ?>&facebook_category_id=' + item['value'] + '&category_id=' + category_id,
                dataType: 'json',
                success: function () {
                },
                complete: function () {
                    $('input[name=' + name + ']').prop('value', item['label']);
                }
            });

            return false;
        },
        focus: function (item) {
            return false;
        }
    });

    $('input[name="facebookfeedproducts_image_size_off"]').on('click', function () {
        var block = $('#resize');
        if (block.hasClass('hidden')) {
            block.removeClass('hidden')
        } else {
            block.addClass('hidden')
        }
    })

    $('.url-feed-btn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '<?php echo HTTP_CATALOG; ?>index.php?route=feed/facebookfeedproducts/checkfeedstatus',
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (data) {
                if (data.success) {
                    window.open(e.currentTarget.href, '_blank').focus();
                } else {
                    $('#url_feed_warning_text').text(data.error);
                    $('#url_feed_warning').show();
                    setTimeout(function () {
                        $('#url_feed_warning').hide();
                        $('#url_feed_warning_text').text('');
                    }, 5000);
                }
            },
            complete: function () {
            }
        });

    });


</script>
<style>
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eee;
        opacity: 1;
    }

    .copy-btn {
        top: 13%;
        position: absolute;
        right: 1%;
    }

    .disabled_button, .disabled_button:hover, .disabled_button:focus, .disabled_button:active:focus, .disabled_button:active:hover {
        color: #fff;
        cursor: unset;
        background-color: #a8afb3;
        border-color: #a8afb3;
        outline: 0px auto -webkit-focus-ring-color;
    }

    .max-bottom-ul .dropdown-menu {
        width: 99%;
        max-height: 250px;
        overflow-y: auto;
    }

    .d_none {
        display: none !important;
    }

    .error {
        color: red;
    }
    .setting-save-proccess-ico {
        height: 27px;
        position: absolute;
        display: none;
    }
    .setting-save-success-ico {
        font-size: 18px;
        color: #0abb0a;
        display: none;
    }
    .onoffswitch {
        position: relative;
        width: 40px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .onoffswitch-checkbox {
        display: none;
    }

    .onoffswitch-label {
        display: block;
        overflow: hidden;
        cursor: pointer;
        height: 18px;
        padding: 0;
        line-height: 18px;
        border: 2px solid #E3E3E3;
        border-radius: 18px;
        background-color: #E3E3E3;
        transition: background-color 0.3s ease-in;
    }

    .onoffswitch-label:before {
        content: "";
        display: block;
        width: 18px;
        margin: 0px;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 20px;
        border: 2px solid #E3E3E3;
        border-radius: 18px;
        transition: all 0.3s ease-in 0s;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label {
        background-color: #49E845;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
        border-color: #49E845;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label:before {
        right: 0px;
    }

    .text-center {
        text-align: center;
    }

    .pos-res {
        position: relative;
    }

    .span-block {
        position: absolute;
        /*right: 20%;*/
        top: 0;
    }

    .block_content {
        pointer-events: none;
        opacity: .6;
    }

    .lds-hourglass {
        display: inline-block;
        position: absolute;
        width: 20px;
        height: 20px;
        top: 30%;
        right: 32%;
    }

    .lds-hourglass:after {
        content: " ";
        display: block;
        border-radius: 50%;
        width: 0;
        height: 0;
        /*margin: 8px;*/
        box-sizing: border-box;
        border: 7px solid #1c1c1c;
        border-color: #1c1c1c transparent #1c1c1c transparent;
        animation: lds-hourglass 1.2s infinite;
    }

    @keyframes lds-hourglass {
        0% {
            transform: rotate(0);
            animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }
        50% {
            transform: rotate(900deg);
            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        100% {
            transform: rotate(1800deg);
        }
    }

    #result_link {
        font-size: 15px;
        padding-top: 20px;
    }

    .alert-disable {
        display: none;
    }

    .error > .alert-disable {
        display: block;
    }
</style>

<?php echo $footer; ?>

</div>
