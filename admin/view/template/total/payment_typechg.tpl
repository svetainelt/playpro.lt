<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-tax" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
        </div>
        <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-typechg" class="form-horizontal">
                <div class="form-group">
                    <div class="table-responsive">
                        <label class="col-sm-2 control-label"><?php echo $entry_fix; ?></label>
                        <div class="col-sm-10">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-left"><?php echo $entry_method; ?></th>
                                        <th class="text-left"><?php echo $entry_shipping; ?></th>
                                        <th class="text-left"><span data-toggle="tooltip" title="<?php echo $help_charge; ?>"><?php echo $entry_charge; ?></span></th>
                                        <th class="text-left"><?php echo $entry_description; ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><select name="payment_typechg_method" class="form-control">
                                                <?php foreach ($payments as $payment) { ?>
                                                <?php  if ($payment['fname'] != $payment_typechg_method) { ?>
                                                <option value="<?php echo $payment['fname']; ?>"><?php echo $payment['hname']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $payment['fname']; ?>" selected="selected"><?php echo $payment['hname']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="text-left">
                                            <?php foreach ($shippings as $shipping) { ?>
                                            <?php  if (in_array($shipping['fname'], $payment_typechg_shipping)) { ?>
                                            <div><input type="checkbox" name="payment_typechg_shipping[]" value="<?php echo $shipping['fname']; ?>" checked><?php echo $shipping['hname']; ?></input></div><br/>
                                            <?php } else { ?>
                                            <div><input type="checkbox" name="payment_typechg_shipping[]" value="<?php echo $shipping['fname']; ?>"><?php echo $shipping['hname']; ?></input></div><br/>
                                            <?php } ?>
                                            <?php } ?>
                                        </td>
                                        <td class="text-left"><input name="payment_typechg_charge" value="<?php echo $payment_typechg_charge; ?>" class="form-control" /></td>
                                        <td class="text-left"><?php foreach ($languages as $language) { ?>
                                            <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['directory']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span><input class="form-control" name="payment_typechg_description_<?php echo $language['language_id']; ?>" value="<?php echo isset(${'payment_typechg_description_' . $language['language_id']}) ? ${'payment_typechg_description_' . $language['language_id']} : ''; ?>"><br /><br /><?php } ?></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                        <select name="payment_typechg_status" id="input-status" class="form-control">
                            <?php if ($payment_typechg_status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="payment_typechg_sort_order" value="<?php echo $payment_typechg_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?> 