$(document).ready(function(){
	now = new Date();
	date = now.getDate() + '.' + now.getMonth() + '.' + now.getFullYear() + ' | ' + now.getHours() + ':' + now.getMinutes();
	currentList = "index.php?route=module/giftteaser/giftList&token=" + token + "&store_id=" + store;
	giftList(currentList);	
	tofbkeCSScheckbox();
	createBinds();

	if (window.localStorage && window.localStorage['currentTab']) {
		$('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
	}
	if (window.localStorage && window.localStorage['currentSubTab']) {
		$('a[href='+window.localStorage['currentSubTab']+']').trigger('click');  
	}
	$('.fadeInOnLoad').css('visibility','visible'); 
	$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
		if (window.localStorage) {
			window.localStorage['currentTab'] = $(this).attr('href');
		}
	});

	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab'] = $(this).attr('href');
		}
	});

	$(document).on('change', 'select[name="selectStore"]', function(e) {
		$('.btn.btn-primary.save-changes').trigger('click');
		window.location = "index.php?route=module/giftteaser&store_id=" + $('select[name="selectStore"]').val() + "&token=" + token;
	});

	$(document).on('click','.pagination a', function(e) {
		e.preventDefault();
		currentList = this.href;
	});
	
	$(document).on('click','#giftListHead a', function(e) {
		e.preventDefault();
		currentList = this.href;
		giftList(currentList);
	});
});

var editors = new Array();
var tofbkeCSScheckbox = function() {
	$('input[type=checkbox][id^=buttonPosCheckbox]').each(function(index, element) {
		if ($(this).is(':checked')) {
			$($(this).attr('data-textinput')).removeAttr('disabled');
		} else {
			$($(this).attr('data-textinput')).attr('disabled','disabled');
		}
	});
}

var createBinds = function() {
	$('input[type=checkbox][id^=buttonPosCheckbox]').unbind('change').bind('change', function() {
		tofbkeCSScheckbox();
	});

	$('.widgetPositionOptionBox').unbind('change').bind('çhange', function() {
		$($(this).attr('data-checkbox')).removeAttr('checked');
		tofbkeCSScheckbox();
	});
};

function giftList(href){
	$.ajax({
		url: href,
		dataType: "html",
		success: function(data) {
			$('#giftListHead').replaceWith($(data).find('#giftListHead')); 
			$('#giftList').replaceWith($(data).find('#giftList'));
			$('.pagination').replaceWith($(data).find('.pagination'));
			tooltips = $('.giftListRow a[id^="item_"]');
			tooltips.each(function(i,l){itemTooltip($(l), $(l).attr('data-image'), $(l).text())})
			$('[data-toggle="tooltip"]').tooltip();
		}
	});
}

function removeGift(ob, refreshList) {
	row = ob.parents('tr');
	bootbox.confirm('Are you sure you want to delete this gift?', function(result){
		if(result){ 
			row.remove();
			$.ajax({
				url: "index.php?route=module/giftteaser/removeGift&token=" + token,
				data: { gift_id: ob.attr('gift-id')},
				type: 'POST',
				dataType: "html"
			});
			if(refreshList){
				giftList(currentList);
			}
		}
	});
}

function editItem(gift_id, item_id, item_name) {
	link = './index.php?route=module/giftteaser/giftForm&gift_id=' + gift_id + '&item_id=' + item_id + '&item_name=' + encodeURIComponent(item_name) + '&token=' + token;
	$('#modal').load(link, function() {	
		$('#modal').modal();
		conditionOption($('select[name="selectCondition"]').val());
		autocompleteProduct($('#some-product-selector input[name="product"]'), 'some-gift-product');
		autocompleteProduct($('#certain-product-selector input[name="product"]'), 'certain-gift-product');
		autocompleteCategory($('#category-selector input[name="category"'), 'gift-category');
		autocompleteManufacturer($('#manufacturer-selector input[name="manufacturer"]'), 'gift-manufacturer');
		$(document).on('click', '.removeIcon', function(){
			$(this).parent().remove();
		});
	});
	$(document).on('change', 'select[name="selectCondition"]', function(){
		conditionOption($('select[name="selectCondition"]').val());
	});	
}

function saveItem(ob) {
	form = ob.parents('.modal')
	type = form.find('select[name="selectCondition"] option:selected').val();
	select_total = form.find('select[name="select_total"] option:selected').val();
	total_amount = form.find('input[name="total_amount"]').val();
	total_amount_max = form.find('input[name="total_amount_max"]').val();
	someProducts = scrollboxToJson(form.find('#some-product-selector .scrollbox'));
	certainProducts = scrollboxToJson(form.find('#certain-product-selector .scrollbox'));
	categories = scrollboxToJson(form.find('#category-selector .scrollbox'));
	manufacturer = scrollboxToJson(form.find('#manufacturer-selector .scrollbox'));
	gift_id = form.find('input[type="hidden"][name="gift-parameters"]').attr('gift-id'); 
	descriptions = {};
	$('textarea[id^="desc_"]').each(function(index, element) {
		descriptions[$(element).attr('id')] = $(element).val();
	});
	
	customer_groups = new Array(); 
	$("[name*='customer_group']").each(function() {
		if(this.checked){
			customer_groups.push(this.id);
		} 
	});
	
	sort_order = form.find('input[type="number"][name="sort_order"]').val();
	certain_product_quantity = form.find('input[type="number"][name="certain_product_quantity"]').val();
	certain_product_qty_sum = form.find('select[name="certain_product_qty_sum"] option:selected').val();
	certain_product_multiple_gift = form.find('select[name="certain_product_multiple_gift"] option:selected').val();
	some_product_quantity = form.find('input[type="number"][name="some_product_quantity"]').val();
	some_product_qty_sum = form.find('select[name="some_product_qty_sum"] option:selected').val();
	some_product_min_total_price = form.find('input[name="some_product_min_total_price"]').val();
	some_product_multiple_gift = form.find('select[name="some_product_multiple_gift"] option:selected').val();
	category_include_exclude = form.find('select[name="category_include_exclude"] option:selected').val();
	category_quantity = form.find('input[type="number"][name="category_quantity"]').val();
	category_qty_sum = form.find('select[name="category_qty_sum"] option:selected').val();
	category_min_total_price = form.find('input[name="category_min_total_price"]').val();
	category_multiple_gift = form.find('select[name="category_multiple_gift"] option:selected').val();
	manufacturer_include_exclude = form.find('select[name="manufacturer_include_exclude"] option:selected').val();
	manufacturer_quantity = form.find('input[type="number"][name="manufacturer_quantity"]').val();
	manufacturer_qty_sum = form.find('select[name="manufacturer_qty_sum"] option:selected').val();
	manufacturer_min_total_price = form.find('input[name="manufacturer_min_total_price"]').val();
	manufacturer_multiple_gift = form.find('select[name="manufacturer_multiple_gift"] option:selected').val();
	reward_points_min = form.find('input[type="number"][name="reward_points_min"]').val();
	reward_points_max = form.find('input[type="number"][name="reward_points_max"]').val();
	reward_points_price = form.find('input[type="number"][name="reward_points_price"]').val();
	
	giftData = {
		'gift_id': gift_id,
		'item_id': form.find('input#itemParams').attr('item-id'),
		'type':type,
		'properties':{
			'select_total': select_total,
			'total': total_amount,
			'total_max': total_amount_max,
			'certain': certainProducts,
			'certain_product_quantity': certain_product_quantity,
			'certain_product_qty_sum': certain_product_qty_sum,
			'certain_product_multiple_gift': certain_product_multiple_gift,
			'some': someProducts,
			'some_product_quantity': some_product_quantity,
			'some_product_qty_sum': some_product_qty_sum,
			'some_product_min_total_price': some_product_min_total_price,
			'some_product_multiple_gift': some_product_multiple_gift,
			'categories': categories,
			'category_include_exclude': category_include_exclude,
			'category_quantity': category_quantity,
			'category_qty_sum': category_qty_sum,
			'category_min_total_price': category_min_total_price,
			'category_multiple_gift': category_multiple_gift,
			'customer_group':customer_groups,
			'manufacturer': manufacturer,
			'manufacturer_include_exclude': manufacturer_include_exclude,
			'manufacturer_quantity': manufacturer_quantity,
			'manufacturer_qty_sum': manufacturer_qty_sum,
			'manufacturer_min_total_price': manufacturer_min_total_price,
			'manufacturer_multiple_gift': manufacturer_multiple_gift,
			'reward_points_min': reward_points_min,
			'reward_points_max': reward_points_max,
			'reward_points_price': reward_points_price
		},
		'start_date': form.find('input[name="startDate"]').val(),
		'end_date': form.find('input[name="endDate"]').val(),
		'descriptions': descriptions,
		'sort_order': sort_order,
		'store_id': store,
	};

	$.ajax({   
		url: 'index.php?route=module/giftteaser/giftForm&token=' + token,
		type: 'post',
		dataType:'html', 
		data: giftData, 
		success: function(data) {
			giftList(currentList); 
			$('.modal').modal('hide'); 
		}
	});
}

function autocompleteProduct(ob, action) { 
	ob.autocomplete({
		delay: 500,
		source: function (request, response) {
			$.ajax({
				url: 'index.php?route=module/giftteaser/autocompleteProduct&limit=10&token=' + getURLVar('token'),
				dataType: 'json',
				type: 'POST',
				data: {filter_name: request, store_id: store},
				success: function (json) {
					response($.map(json, function (item) {
						return {
							id: item['product_id'],
							value: item['product_id'],
							label: item['name']
						}
					}));
				}
			});
		},
		select: function (item) {  
			switch(action){ 
				case 'form': console.log('selected', item); editItem(-1, item.id, item.label); break;
				case 'some-gift-product': addScrollBoxItem($('#some-gift-product'), item.id, item.label); break;
				case 'certain-gift-product': addScrollBoxItem($('#certain-gift-product'), item.id, item.label); break;
			}
			return false;
		}
	});
	
	$('#certain-gift-product').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	
	$('#some-gift-product').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	
}

function addScrollBoxItem(scrollbox, id, name) {
	scrollbox.append('<div id="' + id + '"><i class="fa fa-minus-circle"></i>' + name + '<input type="hidden" value="' + id + '" item-name="' + name + '"/></div>');
}

function autocompleteCategory(ob, action) {
	ob.autocomplete({
		delay: 500,
		source: function (request, response) {
			$.ajax({
				url: 'index.php?route=module/giftteaser/autocompleteCategory&limit=10&token=' + getURLVar('token')  + '&store_id=' + store,
				dataType: 'json',
				type: 'POST',
				data: {filter_name: request, store_id: store},
				success: function (json) { 
					response($.map(json, function (item) {
						return {
							id: item['category_id'],
							value: item['category_id'],
							label: item['name']
						}
					}));
				}
			});
		},
		select: function (item) {
			$('input[name=\'category\']').val('');
			switch(action){
				case 'addRow': addRow(item, button_add, button_remove);
				case 'gift-category': addScrollBoxItem($('#gift-category'), item.id, item.label); break;
			}
			return false;
		}		
	});
	
	$('#gift-category').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	
}

function autocompleteManufacturer(ob, action) {
	ob.autocomplete({
		delay: 500,
		source: function (request, response) {
			$.ajax({
				url: 'index.php?route=catalog/manufacturer/autocomplete&token=' + getURLVar('token')  + '&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				type: 'POST',
				success: function (json) { 
					response($.map(json, function (item) {
						return {
							id: item['manufacturer_id'],
							value: item['manufacturer_id'],
							label: item['name']
						}
					}));
				}
			});
		},
		select: function (item) { 
			//$('input[name=\'manufacturer\']').val(''); 
			switch(action){ 
				case 'form': editItem(-1, item.id); break;
				case 'gift-manufacturer': addScrollBoxItem($('#gift-manufacturer'), item.id, item.label); break;
			}
			return false;
		}

	});
	
	$('#gift-manufacturer').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	
}

function isFloat(n) {
	return n === +n && n !== (n|0);
}

function isInteger(n) {
	return n === +n && n === (n|0);
}

function isPInt(n) {
	return n > 0 && n % 1 === 0;
}

function conditionOption(option) {
	switch(option){
		case '1': $('.option-widget').hide(); $('.option-widget#total_amount').show(); $('.option-widget#total_subtotal').show();		
		break;
		case '2': $('.option-widget').hide(); $('.option-widget.opt-condition-2').show();
		break;
		case '3': $('.option-widget').hide(); $('.option-widget.opt-condition-3').show();
		break;
		case '4': $('.option-widget').hide(); $('.option-widget.opt-condition-4').show();
		break;
		case '5': $('.option-widget').hide(); $('.option-widget.opt-condition-5').show();
		break;
		case '6': $('.option-widget').hide(); $('.option-widget#reward-points-selector').show(); $('.option-widget#reward-points-selector-row2').show();
		break;
		default:  $('#option-widget').hide();
	}
}

function validateNumber(input) {  
	var regex = /[0-9]|\./; 
	if(!regex.test(input.val())) { 
		input.css({'background':'#f2dede'}); 
		return false; 
	} else { 
		input.css({'background':'#fff'}); 
		return true; 
	} 
}  

function scrollboxToJson(scrollbox){
	scrollboxElements = scrollbox.children('div');
	elements = new Array(); 
	jQuery.each(scrollboxElements, function(i, val) { 
		elements.push($(val).attr('id'));
	});
	return elements;
}
