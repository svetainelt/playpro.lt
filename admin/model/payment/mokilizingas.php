<?php

class ModelPaymentMokilizingas extends ModelExtensionPaymentMokilizingas { }

class ModelExtensionPaymentMokilizingas extends Model {
    public function getOrdersRecords($id_order)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "mokilizingas_details` WHERE `id_order` = '" . (int)$id_order . "' ORDER BY `id` DESC");
        
        return $query->rows;
    }
    
    public function getSessionIdByOrderId($id_order) {
        $query = $this->db->query("SELECT sessionId FROM `" . DB_PREFIX . "mokilizingas_details` WHERE `id_order` = '" . (int)$id_order . "' ORDER BY date_upd DESC");
        
        return $query->row;
    }
    
    public function install() {
        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "mokilizingas_details` (
			  `id` INT(11) NOT NULL AUTO_INCREMENT,
			  `stamp`  varchar(20) NOT NULL,
			  `id_order` INT(11) NOT NULL,
			  `sessionId` VARCHAR(255),
			  `errorMessage` VARCHAR(512),
			  `errorCode` VARCHAR(256),
			  `resultCode` VARCHAR(128),
			  `resultInfo` VARCHAR(256),
			  `resultMsg` VARCHAR(256),
			  `advance` VARCHAR(128),
			  `currency` VARCHAR(16),
			  `contractNo` VARCHAR(64),
			  `date_add` DATETIME NOT NULL,
			  `date_upd` DATETIME NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;
		");
    }
    
    public function uninstall() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "mokilizingas_details`;");
    }
    
    public function addQueryRecord($data)
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "mokilizingas_details` SET
                          `stamp` = '" . $this->db->escape($data['stamp']) . "',
                          `id_order` = '" . (int)$data['id_order'] . "',
                          `sessionId` = '" . $this->db->escape($data['sessionId']) . "',
                          `errorMessage` = '" . $this->db->escape($data['errorMessage']) . "',
                          `errorCode` = '" . $this->db->escape($data['errorCode']) . "',
                          `resultCode` = '" . $this->db->escape($data['resultCode']) . "',
                          `resultInfo` = '" . $this->db->escape($data['resultInfo']) . "',
                          `resultMsg` = '" . $this->db->escape($data['resultMsg']) . "',
                          `advance` = '" . $this->db->escape($data['advance']) . "',
                          `currency` = '" . $this->db->escape($data['currency']) . "',
                          `contractNo` = '" . $this->db->escape($data['contractNo']) . "',
                          `date_add` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "',
                          `date_upd` = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'
                          ");
    }
    
    public function updateOrderStatus($id_status, $id_order)
    {
        if ($id_status > 0 && $id_order > 0) {
            $this->addOrderHistory($id_order, $id_status);
            return $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `order_status_id` = " . (int)$id_status . " WHERE `order_id` = " . (int)$id_order);
        }
    }
    
    public function addOrderHistory($id_order, $id_status, $notify = 0, $comment = '')
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order_history` SET
                          `order_id` = '" . (int)$id_order . "',
                          `order_status_id` = '" . (int)$id_status . "',
                          `notify` = '" . (int)$notify . "',
                          `comment` = '" . $this->db->escape($comment) . "',
                          `date_added` = '" . date('Y-m-d H:i:s') . "'
                          ");
    }
    
    public function getLeasingResponse($url , $args) {
        $res = $this->curl($url, $args);
        
        if ($res !== false) {
            $res = json_decode($res, true);
            
            return $res;
        } else {
            return false;
        }
    }
    
    public function checkApiSettings($user, $password)
    {
        $args = array(
            'session_id' => '123456789',
            'order_id' => 991,
            'user_name' => $user,
            'user_psw' => $password,
        );
        
        $base = 'https://api.mokilizingas.lt/api2/eshop/check';
        $auth = $this->getLeasingResponse($base, $args);
        
        if (isset($auth['result']) && $auth['result'] == 'ERROR' && !empty($auth['resultInfo'])) {
            if (strpos($auth['resultInfo'], 'AUTH_FAILURE') !== false) {
                return false;
            }
        }
        
        return true;
    }
    
    public function curl($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
        
        if (0 !== $errno) {
            return false;
        }
        
        return $response;
    }
}