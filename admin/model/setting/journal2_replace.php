<?php
class ModelSettingJournal2Replace extends Model {

    public function editColorsAndFonts($data) {
        if(empty($data))
            return false;
        
        $old_color = trim($data['journal2_color_from']);
        $new_color = trim($data['journal2_color_to']);

        $old_google_font = trim($data['journal2_google_font_from']);
        $new_google_font = trim($data['journal2_google_font_to']);

        $old_system_font = trim($data['journal2_system_font_from']);
        $new_system_font = trim($data['journal2_system_font_to']);

        $this->replaceColors($old_color, $new_color);
        
        $this->replaceFontName($old_google_font, $new_google_font);
        
        $this->replaceFontFamily($old_system_font, $new_system_font);
    }
    
    private function replaceColors($old_color, $new_color) {
        if (!empty($old_color) && !empty($new_color)) {
            $old_color = str_replace('rgba', '', $old_color);
            $old_color = str_replace('rgb', '', $old_color);
            $old_color = str_replace(')', '', $old_color);
            
            $new_color = str_replace('rgba', '', $new_color);
            $new_color = str_replace('rgb', '', $new_color);
            $new_color = str_replace(')', '', $new_color);

            // ==== Change COLOR in JSON File ==== //
            $this->replaceJsonFile($old_color, $new_color);

            // ==== Change COLOR in DB ==== //
            $this->replaceDbQuery($old_color, $new_color);
        }
    }
    
    private function replaceFontName($old_font, $new_font) {
        if (!empty($old_font) && !empty($new_font)) {
            $old_font = '"font_name": "' . $old_font . '"';
            $new_font = '"font_name": "' . $new_font . '"';

            // ==== Change Font Name in JSON File ==== //
            $this->replaceJsonFile($old_font, $new_font);
  
            // ==== Change Font Name in DB ==== //
            $this->replaceDbQuery($old_font, $new_font, 'google');
            
            // naikiname tarpa ir kartojame
            $old_font = '"font_name":"' . $old_font . '"';
            $new_font = '"font_name":"' . $new_font . '"';

            // ==== Change Font Name in DB ==== //
            $this->replaceDbQuery($old_font, $new_font, 'google');
        }
    }
    
    private function replaceFontFamily($old_font, $new_font) {
        if (!empty($old_font) && !empty($new_font)) {
            $old_font = '"font_family": "' . $old_font . '"';
            $new_font = '"font_family": "' . $new_font . '"';

            // ==== Change Font Family in JSON File ==== //
            $this->replaceJsonFile($old_font, $new_font);
            
            // ==== Change Font Family in DB ==== //
            $this->replaceDbQuery($old_font, $new_font, 'system');

            // naikiname tarpa ir kartojame
            $old_font = '"font_family":"' . $old_font . '"';
            $new_font = '"font_family":"' . $new_font . '"';

            // ==== Change Font Family in DB ==== //
            $this->replaceDbQuery($old_font, $new_font, 'system');
        }
    }
    
    private function replaceDbQuery($old_value, $new_value, $like = false) {
        $this->db->query("UPDATE `" . DB_PREFIX . "journal2_settings` SET `value` = REPLACE(value, '" . $old_value . "', '" . $new_value . "') WHERE " . ($like ? "value LIKE '%" . $like . "%' AND" : "") . " value LIKE '%" . $old_value . "%'");
        $this->db->query("UPDATE `" . DB_PREFIX . "journal2_modules` SET `module_data` = REPLACE(module_data, '" . $old_value . "', '" . $new_value . "') WHERE " . ($like ? "module_data LIKE '%" . $like . "%' AND" : "") . " module_data LIKE '%" . $old_value . "%'");
    }
    
    private function replaceJsonFile($old_value, $new_value) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "journal2_config WHERE store_id = '0' AND `key` = 'active_skin'");
        $activeSkin = $query->num_rows ? $query->row['value'] : 1;
        $JSONthemePath = DIR_SYSTEM . 'journal2/data/themes/' . $activeSkin . '.json';
        
        $jsonString = file_get_contents($JSONthemePath);
        if(!empty($jsonString)) {
            $jsonString = str_replace($old_value, $new_value, $jsonString);
            file_put_contents($JSONthemePath, $jsonString);
        }
    }
}