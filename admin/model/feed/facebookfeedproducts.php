<?php
class ModelFeedFacebookFeedProducts extends Model
{
    public function install()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_product_category`;");

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebook_product_category`;");

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_category`;");

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodlist`;");

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodsettings`;");

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebook_category`;");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodlist` (
                `product_id` int(11) NOT NULL AUTO_INCREMENT,
                PRIMARY KEY `product_id` (`product_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodsettings` (
                `id` int(255) NOT NULL AUTO_INCREMENT,
                `value` int(255) NOT NULL,
                `key` varchar(255) NOT NULL,
                PRIMARY KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "google_product_category` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `google_category_id` int(11) NOT NULL,
                `google_category_name` varchar(255) NOT NULL,
                `google_category_parent` int(11) NOT NULL,
                KEY `google_category_id` (`google_category_id`),
                PRIMARY KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "facebook_product_category` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `facebook_category_id` int(11) NOT NULL,
                `facebook_category_name` varchar(255) NOT NULL,
                `facebook_category_parent` int(11) NOT NULL,
                KEY `facebook_category_id` (`facebook_category_id`),
                PRIMARY KEY `id` (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "google_category` (
                `google_category_id` int(11) NOT NULL,
                `google_main_category_id` int(11) NOT NULL,
                `category_id` int(11) NOT NULL,
                `google_category_off` TINYINT(1) NOT NULL DEFAULT '0',
                KEY `google_category_id` (`google_category_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");

        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "facebook_category` (
                `facebook_category_id` int(11) NOT NULL,
                `facebook_main_category_id` int(11) NOT NULL,
                `category_id` int(11) NOT NULL,
                `facebook_category_off` TINYINT(1) NOT NULL DEFAULT '0',
                KEY `facebook_category_id` (`facebook_category_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
        ");
    }

    public function uninstall()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_product_category`;");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebook_product_category`;");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "google_category`;");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebook_category`;");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodlist`;");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "facebookfeedproducts_prodsettings`;");
    }

    public function setProductsSV($key, $value)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "facebookfeedproducts_prodsettings
         WHERE `key` = '" . $this->db->escape($key) . "'
         ;");

        $result = $this->db->query("INSERT INTO " . DB_PREFIX . "facebookfeedproducts_prodsettings
        SET
        `key` = '" . $this->db->escape($key) . "',
        `value` = '" . $this->db->escape($value) . "';");

        return $result;
    }

    public function getProdSettings()
    {
        $sqlSelect = "SELECT * FROM " . DB_PREFIX . "facebookfeedproducts_prodsettings;";
        return $this->db->query($sqlSelect)->rows;
    }

    public function setProductsListItem($key, $value)
    {
        $sqlDelete = "DELETE FROM " . DB_PREFIX . "facebookfeedproducts_prodlist
             WHERE `product_id` = " . (int)$this->db->escape($key) . " ;";
        $sqlInsert = "INSERT INTO " . DB_PREFIX . "facebookfeedproducts_prodlist
             SET
             `product_id` = " . (int)$this->db->escape($key) . ";";
        if (intval($value)) {
            $this->db->query($sqlDelete);
            return $this->db->query($sqlInsert);
        } else {
            return $this->db->query($sqlDelete);
        }
    }

    public function getCustomProducts()
    {
        $sqlSelect = "SELECT * FROM " . DB_PREFIX . "facebookfeedproducts_prodlist;";
        $result = $this->db->query($sqlSelect);
        return $result->rows;
    }

    public function removeAllProductList()
    {
        $sqlDelete = "DELETE FROM " . DB_PREFIX . "facebookfeedproducts_prodlist";
        $result = $this->db->query($sqlDelete);
        return $result;
    }

    public function checkAllProductList()
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "facebookfeedproducts_prodlist");

        $sqlSelect = "SELECT * FROM " . DB_PREFIX . "product";
        $products = $this->db->query($sqlSelect)->rows;

        foreach ($products as $product) {
            try {
                $this->db->query("INSERT INTO " . DB_PREFIX . "facebookfeedproducts_prodlist SET product_id = " . (int)$product['product_id'] . ";");
            } catch (Exception $e) {
            }
        }

        return 1;
    }

    public function clearGoogleCategorys()
    {
        $this->db->query("TRUNCATE TABLE `" . DB_PREFIX . "google_product_category`;");
    }

    public function clearFacebookCategorys()
    {
        $this->db->query("TRUNCATE TABLE `" . DB_PREFIX . "facebook_product_category`;");
    }

    public function getGoogleCategorysByCategoryId($category_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$category_id . "' AND google_category_off='0'");

        return $query->row;
    }

    public function getFacebookCategorysByCategoryId($category_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "facebook_category WHERE category_id = '" . (int)$category_id . "' AND facebook_category_off='0'");

        return $query->row;
    }

    public function getGoogleCategoryByName($parent_name)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_name = '" . $this->db->escape($parent_name) . "'");

        return $query->row;
    }

    public function setFacebookParent($id_category, $name)
    {
        $result = $this->db->query("INSERT INTO " . DB_PREFIX . "facebook_product_category SET facebook_category_id = '" . (int)$id_category . "', facebook_category_name = '" . $this->db->escape($name) . "', facebook_category_parent = 0");
        return $result;
    }

    public function setFacebookCategory($id_category, $name, $root_name)
    {
        $query = "INSERT INTO " . DB_PREFIX . "facebook_product_category (facebook_category_id,facebook_category_name,facebook_category_parent)
              SELECT
                " . (int)$id_category . ",
                '" . $this->db->escape($name) . "',
                facebook_category_id
              FROM
                " . DB_PREFIX . "facebook_product_category
                WHERE
                facebook_category_name = '" . $this->db->escape($root_name) . "' LIMIT 1;";

        $result = $this->db->query($query);

        return $result;
    }

    public function getGoogleCategoryById($google_category_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_id = '" . $this->db->escape($google_category_id) . "'");

        return $query->row;
    }

    public function getFacbookCategoryById($google_category_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "facebook_product_category WHERE facebook_category_id = '" . $this->db->escape($google_category_id) . "'");

        return $query->row;
    }

    public function getGoogleMainCategory()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_parent = 0 ");

        return $query->rows;
    }

    public function getFacebookMainCategory()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "facebook_product_category WHERE facebook_category_parent = 0 ");

        return $query->rows;
    }

    public function getGoogleCategorys($data)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "google_product_category WHERE google_category_parent = '" . (int)$this->db->escape($data['google_category_id']) . "' AND (google_category_name LIKE '%" . $this->db->escape($data['google_category_name']) . "%' OR google_category_id LIKE '%" . $this->db->escape($data['google_category_name']) . "%')");
        return $query->rows;
    }

    public function getProductsList($excepts)
    {
        $sqlSelect = "SELECT product_id FROM " . DB_PREFIX . "product WHERE 1 ";
        foreach ($excepts as $except) {
            $sqlSelect .= "AND `product_id` != " . $except['product_id'] . " ";
        }
        return $this->db->query($sqlSelect)->rows;
    }

    public function getFacebookCategorys($data)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "facebook_product_category WHERE facebook_category_parent = '" . (int)$this->db->escape($data['facebook_category_id']) . "' AND (facebook_category_name LIKE '%" . $this->db->escape($data['facebook_category_name']) . "%' OR facebook_category_id LIKE '%" . $this->db->escape($data['facebook_category_name']) . "%')");
        return $query->rows;
    }

    public function updateGoogleCategorys($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "google_product_category SET google_category_id = '" . (int)$this->db->escape($data['google_category_id']) . "', google_category_name = '" . $this->db->escape($data['google_category_name']) . "', google_category_parent = '" . (int)$this->db->escape($data['google_category_parent']) . "'");
    }

    public function saveGoogleCategory($data)
    {
        if ($data['google_main_category_id'] != 0 ){
            $this->db->query("DELETE FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "' AND google_category_off='0'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "google_category SET category_id = '" . (int)$this->db->escape($data['category_id']) . "', google_main_category_id = '" . (int)$this->db->escape($data['google_main_category_id']) . "'");
        }elseif ($data['google_category_id'] !=0){
            $this->db->query("UPDATE " . DB_PREFIX . "google_category SET google_category_id = '" . (int)$this->db->escape($data['google_category_id']) . "' WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "' AND google_category_off='0'");
        }else{
            $this->db->query("DELETE FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "'");
        }
    }

    public function saveFacebookCategory($data)
    {
        if ($data['facebook_main_category_id'] != 0 ){
            $this->db->query("DELETE FROM " . DB_PREFIX . "facebook_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "' AND facebook_category_off='0'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "facebook_category SET category_id = '" . (int)$this->db->escape($data['category_id']) . "', facebook_main_category_id = '" . (int)$this->db->escape($data['facebook_main_category_id']) . "'");
        }elseif ($data['facebook_category_id'] !=0){
            $this->db->query("UPDATE " . DB_PREFIX . "facebook_category SET facebook_category_id = '" . (int)$this->db->escape($data['facebook_category_id']) . "' WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "' AND facebook_category_off='0'");
        }else{
            $this->db->query("DELETE FROM " . DB_PREFIX . "facebook_category WHERE category_id = '" . (int)$this->db->escape($data['category_id']) . "'");
        }
    }

    public function saveCategory($data)
    {
        if ($data['val'] !== 0) {
         return   $this->db->query("INSERT INTO " . DB_PREFIX . "google_category SET category_id = '" . (int)$this->db->escape($data['id']) . "', google_main_category_id = '0', google_category_id = '0',google_category_off='1'");
       }else{
            return  $this->db->query("DELETE FROM " . DB_PREFIX . "google_category WHERE category_id = '" . (int)$this->db->escape($data['id']) . "' AND google_category_off='1'");
        }
    }

    public function saveCategoryF($data)
    {
        if ($data['val'] !== 0) {
            return   $this->db->query("INSERT INTO " . DB_PREFIX . "facebook_category SET category_id = '" . (int)$this->db->escape($data['id']) . "', facebook_main_category_id = '0', facebook_category_id = '0',facebook_category_off='1'");
        } else {
            return  $this->db->query("DELETE FROM " . DB_PREFIX . "facebook_category WHERE category_id = '" . (int)$this->db->escape($data['id']) . "' AND facebook_category_off='1'");
        }
    }

    public function getSaveCategory()
    {
       $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "google_category WHERE google_category_off='1'");
       return $query->rows;
    }

    public function getSaveCategoryFacebook()
    {
        $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "facebook_category WHERE facebook_category_off='1'");
        return $query->rows;
    }

    public function getCategories($data = array()) {
        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.status = 1 and cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $this->db->escape($data['sort']);
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
}
