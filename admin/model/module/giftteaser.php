<?php 
class ModelModuleGiftTeaser extends Model {

	public function searchProducts($nameQuery, $storeId, $limit) {
		return $this->db->query('SELECT p.product_id, pd.name FROM `' . DB_PREFIX . 'product` p ' .
			'LEFT JOIN `' . DB_PREFIX . 'product_description` AS pd ON (pd.language_id = ' . $this->config->get('config_language_id') . ' AND p.product_id = pd.product_id) ' .
			'LEFT JOIN `' . DB_PREFIX . 'product_to_store` AS pts ON (pts.store_id = ' . $storeId . ' AND p.product_id = pts.product_id) ' .
			'WHERE pd.name LIKE "%' . $this->db->escape($nameQuery) . '%" ' .
			'ORDER BY pd.name ASC LIMIT 0, ' . $this->db->escape($limit)
		)->rows;
	}
	
	public function getProductsInIDArray($products = array()) {
		if(!empty($products)) {
			return $this->db->query('SELECT p.product_id, p.image, pd.name  FROM `' . DB_PREFIX .'product` p ' . 
				'LEFT JOIN `'  . DB_PREFIX . 'product_description` AS pd ON (pd.language_id = ' . $this->config->get('config_language_id') . ' AND p.product_id = pd.product_id) ' . 
				((count($products) == 1) ?
					'WHERE p.product_id = ' . $products[0] :
					'WHERE p.product_id IN (' . implode(',', $products) . ')'
				)
			)->rows;
		} else {
			return array();
		}
	}
	
	public function searchCategories($nameQuery, $storeId, $limit) {
		// TODO255 Add support for $storeId
		return $this->db->query(
			'SELECT * FROM (' . 
				'SELECT cp.category_id, GROUP_CONCAT(cd.name ORDER BY cp.level SEPARATOR " &gt; ") as `name` ' . 
				'FROM ' . DB_PREFIX . 'category_path cp ' . 
				'LEFT JOIN ' . DB_PREFIX . 'category_description AS cd ON (cd.language_id = ' . $this->config->get('config_language_id') . ' AND cp.path_id = cd.category_id) ' . 
				'GROUP BY cp.category_id ' . 
				'ORDER BY `name`' . 
			') AS subq WHERE subq.name LIKE "%' . $nameQuery . '%" LIMIT 0,' . $limit
		)->rows;
	}
	
	public function getCategoriesByID($categories = array()) {
		if(!empty($categories)) {
			return $this->db->query('SELECT c.category_id, name FROM ' . DB_PREFIX . 'category c ' . 
				'JOIN '  . DB_PREFIX . 'category_description AS cd on c.category_id=cd.category_id ' . 
				((count($categories) == 1) ?
					'WHERE c.category_id = ' . $categories[0] :
					'WHERE c.category_id IN (' . implode(',', $categories) . ')'
				) . 
				' AND language_id = ' . $this->config->get('config_language_id')
			)->rows;
		} else {
			return NULL;
		}
	}
	
	public function getManufacturersByID($manufacturers = array()) {
		if(!empty($manufacturers)) {
			return $this->db->query('SELECT manufacturer_id, name FROM ' . DB_PREFIX .'manufacturer ' .
				((count($manufacturers) == 1) ?
					'WHERE manufacturer_id = ' . $manufacturers[0] : 
					'WHERE manufacturer_id IN (' . implode(',', $manufacturers) . ')'
				)
			 )->rows;
		} else {
			return NULL;
		}
	}

	public function getGifts($data) {
		$sql =  "SELECT *, gt.sort_order AS sort_order
		FROM `". DB_PREFIX . "gift_teaser` AS gt 
		LEFT JOIN `" . DB_PREFIX . "product_description` pd ON gt.item_id=pd.product_id
		LEFT JOIN `" . DB_PREFIX . "product` p ON gt.item_id=p.product_id
		WHERE gt.store_id=".(int)$data['store_id'] . " 
		AND pd.language_id=" . $this->config->get('config_language_id') . "
		ORDER BY " . $this->db->escape($data['sort']) . " " . $this->db->escape($data['order']) . " 
		LIMIT " . (int)$data['start'] .", " . (int)$data['limit']; 										
		
		
		return $this->db->query($sql)->rows;
	}
	
	public function saveGift($data = array()) {
		if($this->db->query("SELECT * FROM `" . DB_PREFIX . "gift_teaser` WHERE `gift_id`=" . (int)$data['gift_id'])->num_rows > 0) {
			$this->updateGift($data);
		} else {
			$this->addGift($data);
		}
	} 
	
	public function addGift($data = array()) { 
		$this->db->query($sql = 'INSERT INTO `'. DB_PREFIX .'gift_teaser` SET ' .
			'`item_id`=' . (int)$data['item_id'] . ', ' .
			'`start_date`=' . (int)strtotime($data['start_date']) . ', ' .
			'`end_date`=' . (int)strtotime($data['end_date']) . ', ' .
			'`condition_type`=' . (int)$data['type'] . ', ' .
			'`condition_properties`="' . $this->db->escape(serialize($data['properties'])) . '", ' .
			// b64 encode the serialized descriptions to avoid collation issues with foreign language special characters
			'`description`="' . $this->db->escape(base64_encode(serialize($data['descriptions']))) . '", ' .
			'`sort_order`=' . (int)$data['sort_order'] . ', ' .
			'`store_id`=' . (int)$data['store_id']
		);
	}

	public function updateGift($data = array()) { 
		$this->db->query('UPDATE `'. DB_PREFIX .'gift_teaser` SET ' .
			'`start_date`=' . (int)strtotime($data['start_date']) . ', ' .
			'`end_date`=' . (int)strtotime($data['end_date']) . ', ' .
			'`condition_type`=' . (int)$data['type'] . ', ' .
			'`condition_properties`="' . $this->db->escape(serialize($data['properties'])) . '", ' .
			// b64 encode the serialized descriptions to avoid collation issues with foreign language special characterss
			'`description`="' . $this->db->escape(base64_encode(serialize($data['descriptions']))) . '", ' .
			'`sort_order`=' . (int)$data['sort_order'] .
			' WHERE `gift_id`=' . (int)$data['gift_id']
		);
	} 	
	
	public function removeGift($gift_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "gift_teaser` WHERE `gift_id`=" . (int)$gift_id);
	}
	
	public function getGift($gift_id) {
		return $this->db->query("SELECT * FROM `" . DB_PREFIX . "gift_teaser` WHERE `gift_id`=" . (int)$gift_id)->row;
	}  
	
	public function getTotalGifts($store_id) {
		$query = $this->db->query("SELECT COUNT(gift_id) AS count FROM `" . DB_PREFIX . "gift_teaser` WHERE `store_id`=" . (int)$store_id);
		return $query->row['count'];
	}
	
	public function install() {
		$this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX .'gift_teaser` (' .
			'`gift_id` int(11) NOT NULL AUTO_INCREMENT, ' .
			'`item_id` INT(11) NOT NULL, ' .
			'`start_date` INT(11), ' .
			'`end_date` INT(11), ' .
			'`store_id` INT(11) NOT NULL, ' .
			'`condition_type` INT(11) NOT NULL, ' .
			'`condition_properties` TEXT, ' .
			'`description` TEXT, ' .
			'`sort_order` INT, ' .
			'PRIMARY KEY (`gift_id`))'
		);

		$this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'cart_gift` (' .
			'`cart_gift_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, ' .
			'`api_id` int(11) NOT NULL, ' .
			'`customer_id` int(11) NOT NULL, ' .
			'`session_id` varchar(32) NOT NULL, ' .
			'`gift_id` int(11) NOT NULL, ' .
			'`requires_options` tinyint(1) NOT NULL, ' .
			'`option` text NOT NULL, ' .
			'`quantity` int(5) NOT NULL, ' .
			'`date_added` DATETIME NULL DEFAULT NULL, ' .
			'PRIMARY KEY (`cart_gift_id`), ' .
			'INDEX (`api_id`, `customer_id`, `session_id`, `gift_id`))'
		);

		// TODO255 Reconsider what to do about modifications on install/uninstall
		// $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%GiftTeaser by iSenseLabs%'");
		// $modifications = $this->load->controller('extension/modification/refresh');
	} 
	
	public function uninstall() {
		$this->db->query('DROP TABLE IF EXISTS `' . DB_PREFIX .'gift_teaser`');
		$this->db->query('DROP TABLE IF EXISTS `' . DB_PREFIX .'cart_gift`');
		// TODO255 Reconsider what to do about modifications on install/uninstall
		// $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%GiftTeaser by iSenseLabs%'");
		// $modifications = $this->load->controller('extension/modification/refresh');
	}

}
?>