<?php
//BULGARIA
$_['text_version']          = '';
$_['heading_title']         = 'Tag Manager (Analytics / Pixel / Google Ads)';
// Text
$_['primary']               = '';
$_['text_extension']        = 'Разширения';
$_['text_success']	        = 'Успех: Променихте Tag Manager!';
$_['text_signup']           = 'Влезте в своя <a href="https://tagmanager.google.com" target="_blank"><u>Google Tag Manager</u></a> акаунт и след създаването на контейнера за работно пространство на вашия уеб сайт копирайте и поставете кода на работното пространство i.e. GTM-XXXXX';
$_['text_default']          = 'По подразбиране';
$_['text_edit']             = 'Edit Settings';
$_['text_about']            = '<p>За поддръжка, моля, посетете нашия център за поддръжка </p><br><div class="col-sm-12"><center><div class="btn btn-success col-sm-5" style="margin:15px"><a href="https://support.aits.xyz/" target="blank" style="color:white;padding-top:10px;" >поддържа</a></div><div class="btn btn-info col-sm-5" style="margin:15px"><a href="https://billing.aits.xyz/order.php?step=1&productGroup=18&product=89" target="blank" style="color:white;padding-top:10px;" >Купете лиценз</a></div><center></div><p>Ние винаги подобряваме и добавяме нови функции. Ако необходимата ви платформа за проследяване не се поддържа от нашето разширение, не се колебайте да ни пишете на support@aits.xyz и ние ще ви помогнем.</p><p>Ние също така предлагаме персонализирана разработка и откриване на проблеми с Opencart. Ако имате въпроси, моля, изпратете ни имейл на support@aits.xyz';
$_['text_about_cookie']     = '<p>Cookie Consent ще покаже изскачащ прозорец при първото посещение на потребителя. Има два режима стандартен и приложен (EU / GDPR). </p><p>Когато режимът enfored е активиран, потребителят трябва да приема бисквитки, преди да се задейства проследяващ код на трета страна.</p>'; 

$_['heading_container']     = 'Контейнер на Google Tag Manager';
$_['text_container']        = '<p>Yможете да използвате вашия предварително изграден контейнер, за да стартирате разширението, като използвате идентификатора на контейнера <strong>%s</strong> в полето Container.<br></p>';
$_['text_order']            = '';

$_['text_cookie_title']     = 'Cookie';
$_['text_cookie_text']      = 'Ние използваме бисквитки, за да ви предоставим най-доброто онлайн изживяване. Използвайки нашия уебсайт, вие се съгласявате с използването на бисквитки в съответствие с нашата Политика за бисквитки';
$_['text_cookie_text2']     ='Изберете кои бисквитки искате да приемете';
$_['text_cookie_link']      = '';
$_['text_cookie_button1']   = 'Приемете';
$_['text_cookie_button2']   = 'Настройки';
$_['text_cookie_button3']   = 'повече информация';

// Entry
$_['entry_primary']					= 'Идентификатор на контейнера на TAG Manager' ;
$_['entry_server']					= 'TAG Manager via Server Container' ;
$_['entry_server_url']				= 'Server Container Domain' ;
$_['entry_admin']					= 'Изключете административното посещение от Анализ';
$_['entry_status']					= 'Състояние на разширението';
$_['entry_mp']						= 'Протокол за измерване';
$_['entry_product']					= 'Идентификатор на продукта';
$_['entry_ptitle']					= 'Модификатор на заглавието на продукта';
$_['entry_id_prefix']				= 'Префикс на идентификатора на продукта';
$_['entry_id_suffix']				= 'Суфикс на идентификатора на продукта';
$_['entry_cache']					= 'Активиране на кеша за ускоряване на sql';
$_['entry_debug']					= 'Съобщение за отстраняване на грешки';
$_['entry_debug_api']				= 'Съобщение за отстраняване на грешки в API в дневника за грешки';
$_['entry_route_checkout']			= 'Допълнителен маршрут за плащане';
$_['entry_route_success']			= 'Допълнителен маршрут за успех';
$_['entry_customcode']				= 'Персонализиран код на заглавката';
$_['entry_gid']						= 'Google Analytics за проследяване идентификатор';
$_['entry_ua_status']				= 'Състояние на Google Universal Analytics';
$_['entry_ga4_status']				= 'Състояние на Google GA4 Analytics';
$_['entry_ga4_mid']					= 'Google GA4 Измерване ID';
$_['entry_ga4_api']					= 'Google GA4 Измерване API Secret';
$_['entry_adword_ec']				= 'Enhanced Conversions (beta)';
$_['entry_conversion_id']			= 'Conversion ID';
$_['entry_conversion_label']		= 'Conversion Label';
$_['entry_conversion_id2']			= 'Втори Conversion ID';
$_['entry_conversion_label2']		= 'Втори Conversion Label';
$_['entry_conversion_route2']		= 'Route to Fire Втори Conversion';
$_['entry_conversion_value2']		= 'Стойност';
$_['entry_aw_optional']	        	= 'Данни за продукт по избор';
$_['entry_aw_merchant_id']	    	= 'Id Merchant Center';
$_['entry_aw_feed_country']		    = 'Страна за подаване';
$_['entry_aw_feed_language']		= 'Език за подаване';
$_['entry_adword']					= 'Проследяване на реализациите в Google Ads';
$_['entry_adword2']					= 'Вторично проследяване на реализациите';
$_['entry_userid_status']			= 'Проследяване на User-ID на Google Анализ';
$_['entry_custom_dimension']		= 'Enable Custom Dimensions';
$_['entry_custom_dimension1']		= 'Custom Dimension ';
$_['entry_custom_dimension2']		= 'Custom Dimension ';
$_['entry_custom_dimension3']		= 'Custom Dimension ';
$_['entry_custom_dimension4']		= 'Custom Dimension ';
$_['entry_custom_dimension5']		= 'Custom Dimension ';
$_['entry_custom_dimension6']		= 'Custom Dimension ';
$_['entry_custom_dimension7']		= 'Custom Dimension ';
$_['entry_custom_dimension8']		= 'Custom Dimension ';
$_['entry_custom_dimension9']		= 'Custom Dimension ';
$_['entry_remarketing']				= 'Google Ads Remarketing';
$_['entry_custom']					= 'Ремаркетинг персонализирани параметри за Анализ / Реклами';
$_['entry_dynx_itemid']				= 'dynx_itemid';
$_['entry_dynx_itemid2']			= 'dynx_itemid2';
$_['entry_dynx_pagetype']			= 'dynx_pagetype';
$_['entry_dynx_totalvalue']			= 'dynx_totalvalue';
$_['entry_ecomm_pagetype']			= 'ecomm_pagetype';
$_['entry_ecomm_prodid']			= 'ecomm_prodid';
$_['entry_ecomm_totalvalue']		= 'ecomm_totalvalue';
$_['entry_google_optimize']         = 'Google Optimize Container ID';
$_['entry_google_optimize_status']  = 'Състояние на Google Оптимизиране';
$_['entry_greview']					= 'Google Customer Satisfaction';
$_['entry_greview_badge']			= 'Google Review Badge';
$_['entry_merchant_id']				= 'Google Merchant Id';
$_['entry_pixel']					= 'Facebook Pixel Tracking';
$_['entry_pixelcode']				= 'Facebook Pixel Tracking Code (въведете само идентификатора на пиксела)';
$_['entry_fb_catalog_id']			= 'Facebook Catalog ID (for Dynamic Ads)';
$_['entry_fb_api']				    = 'Facebook Conversion API';
$_['entry_fb_token']				= 'Conversion API Access Token';
$_['entry_alt_currency']			= 'Алтернативна валута';
$_['entry_alt_currency_status']		= 'Използвайте алтернативна валута';
$_['entry_alt_currency_val']		= 'Стойност на алтернативна валута';
$_['entry_twitter_status']			= 'Twitter Analytics';
$_['entry_twitter_tag']		    	= 'Twitter Analytics Tag';
$_['entry_pinterest_status']		= 'Pinterest Analytics';
$_['entry_pinterest_tag']		    = 'Pinterest Analytics Tag';
$_['entry_glami_status']			= 'Glami Pixel';
$_['entry_glami_code']			    = 'Glami API Key';
$_['entry_hotjar_status']			= 'HotJar Tracking';
$_['entry_hotjar_siteid']			= 'HotJar Site Id';
$_['entry_luckyorange_status']		= 'Lucky Orange Tracking';
$_['entry_luckyorange_siteid']		= 'Lucky Orange Site Id';
$_['entry_clarity_status']			= 'Microsoft Clarity Heatmap';
$_['entry_clarity_siteid']			= 'Microsoft Clarity Tracking Id';
$_['entry_bing_status']				= 'Bing Tracking';
$_['entry_bing_uetid']				= 'Bing UET Id';
$_['entry_skroutz_status']			= 'Skroutz Tracking';
$_['entry_skroutz_siteid']			= 'Skroutz Shop Account ID ';
$_['entry_skroutz_manual_tax']		= 'Skroutz send manual tax  ';
$_['entry_skroutz_manual_tax_value']= 'Skroutz tax value i.e 24 without % sign';
$_['entry_skroutz_payment_fee']		= 'Skroutz Payment Processing fee ';
$_['entry_yandex_status']			= 'Yandex Metrika';
$_['entry_yandex_code']				= 'Yandex Tag ';
$_['entry_snap_pixel_status']		= 'Snapchat Pixel Status';
$_['entry_snap_pixel_id']		    = 'Snapchat Pixel ID';
$_['entry_yandex_status']			= 'Yandex Metrika';
$_['entry_zopimchat_status']		= 'Zopim Chat Status';
$_['entry_affgateway_status']		= 'Affiliate Gateway Status';
$_['entry_affgateway_code']			= 'Affiliate Gateway Campaign Code';
$_['entry_performant_status']		= '2Performant Status';
$_['entry_performant_code']			= '2Performant Campaign Code';
$_['entry_performant_confirm']		= '2Performant Confirm Code';
$_['entry_performant_tax']			= '2Performant manual tax  ';
$_['entry_performant_tax_value']	= '2Performant tax value i.e 24 without % sign';
$_['entry_performant_currency']		= 'Alternate Currency';
$_['entry_admitad_status']			= 'AdmitAd Status';
$_['entry_admitad_code']			= 'AdmitAd Campaign Code';
$_['entry_admitad_category']		= 'Tariff code';
$_['entry_admitad_additional_type']	= 'Sales (default)';
$_['entry_admitad_invoice_broker']	= 'Invoice Broker (adm default)';
$_['entry_admitad_invoice_category']= 'Invoice Category (action.code)';
$_['entry_admitad_retag_status']	= 'ReTag Status';
$_['entry_admitad_retag_code1']		= 'ReTag Code For Homepage Page i.e. 9xx0931xb';
$_['entry_admitad_retag_code2']		= 'ReTag Code For Category Pages i.e. 9xx0931xb';
$_['entry_admitad_retag_code3']		= 'ReTag Code For Product Pages i.e. 9xx0931xb';
$_['entry_admitad_retag_code4']		= 'ReTag Code For Cart Page i.e. 9xx0931xb';
$_['entry_admitad_retag_code5']		= 'ReTag Code For Thankyou Page i.e. 9xx0931xb';
$_['entry_sendinblue_status']		= 'SendinBlue Status';
$_['entry_sendinblue_code']			= 'SendinBlue Client Key';
$_['entry_zopimchat_code']			= 'Zopim ID';
$_['entry_zenchat_status']			= 'Zen Chat Status';
$_['entry_zenchat_code']			= 'Zen Chat ID';
$_['entry_freshchat_status']		= 'Fresh Chat Status';
$_['entry_freshchat_code']			= 'Fresh Chat Token ID';
$_['entry_freshchat_host']			= 'Fresh Chat Host';
$_['entry_hubspot_status']			= 'Hubspot Chat Status';
$_['entry_hubspot_code']			= 'Hubspot Chat ID';
$_['entry_smartsupp_status']		= 'Smartsupp Chat Status';
$_['entry_smartsupp_code']			= 'Smartsupp Chat ID';
$_['entry_paypal_status']			= 'Paypal Analytics Status';
$_['entry_paypal_code']			    = 'Paypal Analytics ID';
$_['entry_tiktok_status']			= 'Tiktok Pixel Status';
$_['entry_tiktok_code']			    = 'Tiktok Pixel Code';

$_['entry_pixel_test_code']			= 'Pixel API Test Code';
$_['help_pixel_test_code']			= 'Тестов код на API за преобразуване от екрана на Event Manager, оставете празно за производствени сайтове';

$_['entry_ampcode']					= 'AMP Google Tag Manager Container ID';
$_['entry_ampstatus']				= 'AMP Tagmanager Status';

$_['entry_eu_cookie']				= 'Показване на съгласието на ЕС за бисквитки';
$_['entry_eu_cookie_enforce']		= 'Прилагане на съгласието за бисквитки';
$_['entry_cookie_position']			= 'Позиция на изскачащи бисквитки';
$_['entry_cookie_title']			= 'Изскачащ заглавие на бисквитки';
$_['entry_cookie_text']				= 'Текстово съобщение за бисквитки на ЕС';
$_['entry_cookie_text2']			= 'Бисквитка Персонализиране на текст';
$_['entry_cookie_link']				= 'URL адрес на Декларацията за поверителност i.e. privacy.html';
$_['entry_cookie_button1']			= 'Текст на бутона за приемане на бисквитки';
$_['entry_cookie_button2']			= 'Текст на бисквитка Moreinfo';
$_['entry_cookie_button3']			= 'Текст на бутона за персонализиране на бисквитки';
$_['entry_cookie_bg_popup']			= 'Изскачащ цвят на фона i.e. #eaf7f7';
$_['entry_cookie_text_popup']		= 'Цвят на изскачащ текст i.e. white';
$_['entry_cookie_bg_button']		= 'Цвят на фона на бутона i.e. #56cbdb';
$_['entry_cookie_text_button']		= 'Бутон Цвят на текста i.e. white';
$_['entry_cookie_heading_color']	= 'Цвят на текста на заглавието i.e. red';
$_['entry_cookie_badge']			= 'Бутон за настройка на бисквитки';
$_['entry_cookie_badge_position']	= 'Поставяне на бутона за значка';
$_['entry_cookie_badge_color']		= 'Цвят на фона на бутона за значка';

$_['entry_customer_data']		    = 'Активиране на потребителски данни';
$_['entry_linkwise_status']		    = 'Активирайте Linkwise Marketing';
$_['entry_linkwise_code']		    = 'Linkwise проследяване ID';
$_['entry_linkwise_decimal']		= 'Linkwise десетична i.e. , or .';
$_['help_customer_data']		    = 'Събирайте и изпращайте потребителски данни, например име / телефон / адрес. Ако е забранено, ще се отменят индивидуалните настройки.';


// tabs
$_['tab_tab1']					= 'Tag Manger Общ';
$_['tab_tab2']					= 'Google';
$_['tab_tab3']					= 'Популярни анализи';
$_['tab_tab4']					= 'Партньорско проследяване';
$_['tab_tab5']					= 'Cookies';
$_['tab_tab6']					= 'Добавете Ons';
$_['tab_tab7']					= 'Доклад за поръчка';
$_['tab_tab8']					= 'Дневници';

// column
$_['column_oid']				= 'Номер на поръчка';
$_['column_status']				= 'Състояние';
$_['column_action']				= 'Ръчно действие';

// button
$_['button_send']				= 'Изпрати';
$_['button_refund']				= 'Възстановяване';


// Error
$_['error_permission']			= 'Предупреждение: Нямате разрешение да модифицирате Google Tag Manager!';
$_['error_primary']				= 'Изисква се контейнер за идентификатор на Google Tag Manager!';
$_['error_analytics']			= 'Изисква се идентификатор на Google Analytics!';
$_['error_ga4']				    = 'Изискване за измерване на Google Analytics 4!';

// Help
$_['help_ua']					= 'Използвайте универсалното проследяване на Google Analytics';
$_['help_server']				= 'Използвайте Google App Tagmanger Server Container за Tag Manager';
$_['help_server_url']			= 'Напълно квалифицирано име на домейн само с subdomain.server.com';
$_['help_gid']					= 'Моля, въведете проследяващ идентификатор на Google Analytics i.e. UA-XXXXXXX';
$_['help_ga4']					= 'Активирайте проследяването на Google Analytics 4, направете, ако използвате старо проследяване, използвайте Universal Analytics';
$_['help_ga4_api']				= 'Google Analytics 4 Measurement API Secret, който ще се използва за комуникация между сървъри (beta)';
$_['help_admin']				= 'Препоръчва се само за производство. Посетеното от администратора посещение няма да бъде изпратено до Анализ. Активиране на производствени обекти';
$_['help_custom_dim']		    = 'Използвайте персонализирано измерение, за да изпращате данни за забележки, ремаркетингът трябва да бъде активиран по-долу, за да използвате тази функция';
$_['help_aw']          		    = 'Активирайте проследяването на реализациите в Google Ads ';
$_['help_aw_ec']          		= 'Изпращайте данни за подобрени реализации в Google Ads, т.е. имейл, телефон, име, адрес, държава на реализация';
$_['help_aw_secondary']         = 'Secondary Conversion Tracking for Google Ads Optional ';
$_['help_aw_optional']          = 'По избор Настройте и тествайте отчетни реализации с данни от количката (Beta) ';
$_['help_aw_merchant']          = 'Идентификационният номер на Merchant Center, където се качват вашите елементи';
$_['help_aw_country']           = 'Държавата, свързана с емисията, в която са качени вашите елементи. Използвайте CLDR кодове на територии.';
$_['help_aw_language']          = 'Езикът, свързан с емисията, където се качват вашите елементи. Използвайте езикови кодове по ISO 639-1.';
$_['help_aw_merchant']          = 'Идентификационният номер на Merchant Center, където се качват вашите елементи';
$_['help_aw_route']             = 'Страница, където вторично преобразуване в огън';
$_['help_userid']				= 'Проследяване на потребителски идентификатор за Google Analytics, ако е активирано, изпращайте влезли уникални потребителски идентификатори на Анализ';
$_['help_conversion_id']		= 'Google Ads Conversion id Въведете без AW-, само цифри';
$_['help_conversion_label']		= 'Етикет за реализация на Google Ads от кода за проследяване на реализациите.';
$_['help_remarketing']			= 'Google Ads Remarketing / Dynamic Remarketing';
$_['help_conversion_value2']	= 'Фиксирана стойност на конверсия за страници, различни от покупка / нова поръчка';
$_['help_product']				= 'Уникалното поле, използвано за картографиране на данните за ремаркетинг с Merchant Center Feed Feed или Facebook Catalog';
$_['help_ptitle']				= 'Можете да използвате тази настройка, за да съкратите заглавието на продукта, изпратено до Анализ, за лесно идентифициране в отчетите';
$_['help_mp']					= 'Препоръчва се, използвайте Google Measurement Protocol, за да изпращате неинтерактивни посещения, препоръчва се за проследяване на IPP / възстановяване на средства и т.н. на Paypal ';
$_['help_ac']					= 'Използвайте различна валута, в случай че валутата на магазина ви не се поддържа. Трябва да се настрои в Локализация / Валута';
$_['help_ac_value']				= 'Ако искате да използвате различна стойност за преобразуване от съхраняването. Оставете черно по подразбиране';
$_['help_custom']				= 'Изпращайте персонализирани параметри до Google Ads, като използвате стари параметри.';
$_['help_cache']				= 'Препоръчва се, ако имате голям каталог и малко ресурси на сървъра, идеално за избягване на допълнителни режийни разходи на SQL сървър поради допълнителни заявки.';
$_['help_debug']				= 'Активиране / деактивиране на съобщението за регистрационния файл за отстраняване на грешки в регистрационния файл за грешки от Tag Manager';
$_['help_route']				= 'Допълнителен маршрут, т.е. бърза проверка / плащане, в случай че имате персонализиран манипулатор на Checkout или Success на линия';
$_['help_route_checkout']		= 'Един запис на ред. ако route = checkout / custom checkout добавете само checkout / custom checkout. <br> Моля, не премахвайте записите по подразбиране.';
$_['help_route_success']		= 'Един запис на ред. ако маршрут = плащане / успех на поръчката, добавете само плащане / успех на поръчката. <br> Моля, не премахвайте записите по подразбиране.';
$_['help_id_prefix']		    = 'Добавете префикс към полето за идентификатор на продукта, за да съответства на вашата емисия за продукти';
$_['help_id_suffix']		    = 'Добавете суфикс към полето за идентификатор на продукта, за да съответства на вашата емисия за продукти';

$_['help_customcode']		    = 'Персонализиран код за поставяне на заглавка може да бъде мета тагове, проследяване на javascript и т.н.';
$_['help_cenforce']		        = 'Режимът за налагане ще блокира цялото проследяване, докато потребителят не приеме съгласието на бисквитката';
$_['help_ctitle']   		    = 'Насочете се към кутията за бисквитки, оставете черно, за да премахнете заглавието';
$_['help_ctext']		        = 'Текст, който се появява, когато потребителят щракне върху Настройки или Персонализиране';
$_['help_clink']		        = 'URL за връзка за политика за бисквитки или политика за поверителност ';
$_['help_debug_api']		    = 'Показване на регистрационните файлове на API в регистрационния файл на мениджъра на маркери за всички извиквания на API i.n. Google Measurement Protocol или Conversion api';

$_['text_details']              = '';