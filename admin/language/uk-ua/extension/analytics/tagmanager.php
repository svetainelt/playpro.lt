<?php
//UK-UA Ukraine
$_['text_version']          = '';
$_['heading_title']         = 'Tag Manager (Analytics / Pixel / Google Ads)';
// Text
$_['primary']               = '';
$_['text_extension']        = 'Розширення';
$_['text_success']	        = 'Успіх: Ви змінили Менеджер тегів!';
$_['text_signup']           = 'Увійдіть у свій <a href="https://tagmanager.google.com" target="_blank"><u>Google Tag Manager</u></a> після створення контейнера робочої області веб-сайту скопіюйте та вставте код робочої області i.e. GTM-XXXXX';
$_['text_default']          = 'За замовчуванням';
$_['text_edit']             = 'Редагувати налаштування';
$_['text_about']            = '<p>Щоб отримати підтримку, відвідайте наш центр підтримки </p><br><div class="col-sm-12"><center><div class="btn btn-success col-sm-5" style="margin:15px"><a href="https://support.aits.xyz/" target="blank" style="color:white;padding-top:10px;" >Підтримка</a></div><div class="btn btn-info col-sm-5" style="margin:15px"><a href="https://billing.aits.xyz/order.php?step=1&productGroup=18&product=89" target="blank" style="color:white;padding-top:10px;" >Купити ліцензію</a></div><center></div><p>Ми завжди вдосконалюємося та додаємо нові функції. Якщо ваша розширення не підтримує вашу необхідну платформу відстеження, не соромтеся надсилати нам електронного листа на support@aits.xyz, і ми допоможемо вам. </p> <p> Ми також пропонуємо власну розробку та пошук та усунення несправностей Opencart. Якщо у вас виникли запитання, напишіть нам на support@aits.xyz';
$_['text_about_cookie']     = '<p>Згода cookie відображатиме спливаюче вікно при першому відвідуванні користувача. Існує два режими: стандартний та примусовий (EU / GDPR). </p><p>Коли ввімкнено примусовий режим, користувач повинен приймати файли cookie, перш ніж запускати будь-який сторонній код відстеження.</p>'; 

$_['heading_container']     = 'Контейнер Менеджера тегів Google';
$_['text_container']        = '<p>Ви можете використовувати наш попередньо побудований контейнер для запуску розширення за допомогою ідентифікатора контейнера <strong>%s</strong> у первинній галузі.<br></p>';
$_['text_order']            = '';

$_['text_cookie_title']     = 'Печиво';
$_['text_cookie_text']      = 'Ми використовуємо файли cookie, щоб надати вам найкращий досвід роботи в Інтернеті. Використовуючи наш веб-сайт, ви погоджуєтесь на використання нами файлів cookie відповідно до нашої Політики використання файлів cookie';
$_['text_cookie_text2']     ='Виберіть файли cookie, які ви хочете прийняти';
$_['text_cookie_link']      = '';
$_['text_cookie_button1']   = 'Прийняти';
$_['text_cookie_button2']   = 'Налаштування';
$_['text_cookie_button3']   = 'більше інформації';

// Entry
$_['entry_primary']					= 'TAG Manager Container id' ;
$_['entry_server']					= 'TAG Manager via Server Container' ;
$_['entry_server_url']				= 'Server Container Domain' ;
$_['entry_admin']					= 'Виключити відвідування адміністратора з Analytics';
$_['entry_status']					= 'Extension Status';
$_['entry_mp']						= 'Measurement Protocol';
$_['entry_product']					= 'Product Identifier';
$_['entry_ptitle']					= 'Product Title Modifier';
$_['entry_id_prefix']				= 'Product ID Prefix';
$_['entry_id_suffix']				= 'Product ID Suffix';
$_['entry_cache']					= 'Увімкнути кеш для прискорення SQL';
$_['entry_debug']					= 'Повідомлення про налагодження в журналі помилок';
$_['entry_debug_api']				= 'Повідомлення про налагодження API у журналі помилок';
$_['entry_route_checkout']			= 'Additional Checkout route';
$_['entry_route_success']			= 'Additional Success route';
$_['entry_customcode']				= 'Header Custom Code';
$_['entry_gid']						= 'Google Analytics Tracking id';
$_['entry_ua_status']				= 'Google Universal Analytics Status';
$_['entry_ga4_status']				= 'Google GA4 Analytics Status';
$_['entry_ga4_mid']					= 'Google GA4 Measurement ID';
$_['entry_ga4_api']					= 'Google GA4 Measurement API Secret';
$_['entry_adword_ec']				= 'Enhanced Conversions (beta)';
$_['entry_conversion_id']			= 'Conversion ID';
$_['entry_conversion_label']		= 'Conversion Label';
$_['entry_conversion_id2']			= 'Secondary Conversion ID';
$_['entry_conversion_label2']		= 'Secondary Conversion Label';
$_['entry_conversion_route2']		= 'Route to Fire Secondary Conversion';
$_['entry_conversion_value2']		= 'Value';
$_['entry_aw_optional']	        	= 'Product Optional Data';
$_['entry_aw_merchant_id']	    	= 'Merchant Center Id';
$_['entry_aw_feed_country']		    = 'Feed Country';
$_['entry_aw_feed_language']		= 'Feed Language';
$_['entry_adword']					= 'Google Ads Conversion Tracking';
$_['entry_adword2']					= 'Secondary Conversion Tracking';
$_['entry_userid_status']			= 'Analytics User-ID Tracking';
$_['entry_custom_dimension']		= 'Enable Custom Dimensions';
$_['entry_custom_dimension1']		= 'Custom Dimension ';
$_['entry_custom_dimension2']		= 'Custom Dimension ';
$_['entry_custom_dimension3']		= 'Custom Dimension ';
$_['entry_custom_dimension4']		= 'Custom Dimension ';
$_['entry_custom_dimension5']		= 'Custom Dimension ';
$_['entry_custom_dimension6']		= 'Custom Dimension ';
$_['entry_custom_dimension7']		= 'Custom Dimension ';
$_['entry_custom_dimension8']		= 'Custom Dimension ';
$_['entry_custom_dimension9']		= 'Custom Dimension ';
$_['entry_remarketing']				= 'Google Ads Remarketing';
$_['entry_custom']					= 'Remarketing Custom Parameters For Analytics/Ads';
$_['entry_dynx_itemid']				= 'dynx_itemid';
$_['entry_dynx_itemid2']			= 'dynx_itemid2';
$_['entry_dynx_pagetype']			= 'dynx_pagetype';
$_['entry_dynx_totalvalue']			= 'dynx_totalvalue';
$_['entry_ecomm_pagetype']			= 'ecomm_pagetype';
$_['entry_ecomm_prodid']			= 'ecomm_prodid';
$_['entry_ecomm_totalvalue']		= 'ecomm_totalvalue';
$_['entry_google_optimize']         = 'Google Optimize Container ID';
$_['entry_google_optimize_status']  = 'Google Optimize Status';
$_['entry_greview']					= 'Google Customer Satisfaction';
$_['entry_greview_badge']			= 'Google Review Badge';
$_['entry_merchant_id']				= 'Google Merchant Id';
$_['entry_pixel']					= 'Facebook Pixel Tracking';
$_['entry_pixelcode']				= 'Facebook Pixel Tracking Code (only enter the pixel id)';
$_['entry_fb_api']				    = 'Facebook Conversion API';
$_['entry_fb_token']				= 'Conversion API Access Token';
$_['entry_fb_catalog_id']			= 'Facebook Catalog ID (for Dynamic Ads)';
$_['entry_alt_currency']			= 'Alternate Currency';
$_['entry_alt_currency_status']		= 'Use Alternate Currency';
$_['entry_alt_currency_val']		= 'Alternate Currency Value';
$_['entry_twitter_status']			= 'Twitter Analytics';
$_['entry_twitter_tag']		    	= 'Twitter Analytics Tag';
$_['entry_pinterest_status']		= 'Pinterest Analytics';
$_['entry_pinterest_tag']		    = 'Pinterest Analytics Tag';
$_['entry_glami_status']			= 'Glami Pixel';
$_['entry_glami_code']			    = 'Glami API Key';
$_['entry_hotjar_status']			= 'HotJar Tracking';
$_['entry_hotjar_siteid']			= 'HotJar Site Id';
$_['entry_luckyorange_status']		= 'Lucky Orange Tracking';
$_['entry_luckyorange_siteid']		= 'Lucky Orange Site Id';
$_['entry_clarity_status']			= 'Microsoft Clarity Heatmap';
$_['entry_clarity_siteid']			= 'Microsoft Clarity Tracking Id';
$_['entry_bing_status']				= 'Bing Tracking';
$_['entry_bing_uetid']				= 'Bing UET Id';
$_['entry_skroutz_status']			= 'Skroutz Tracking';
$_['entry_skroutz_siteid']			= 'Skroutz Shop Account ID ';
$_['entry_skroutz_manual_tax']		= 'Skroutz send manual tax  ';
$_['entry_skroutz_manual_tax_value']= 'Skroutz tax value i.e 24 without % sign';
$_['entry_skroutz_payment_fee']		= 'Skroutz Payment Processing fee ';
$_['entry_yandex_status']			= 'Yandex Metrika';
$_['entry_yandex_code']				= 'Yandex Tag ';
$_['entry_snap_pixel_status']		= 'Snapchat Pixel Status';
$_['entry_snap_pixel_id']		    = 'Snapchat Pixel ID';
$_['entry_yandex_status']			= 'Yandex Metrika';
$_['entry_zopimchat_status']		= 'Zopim Chat Status';
$_['entry_affgateway_status']		= 'Affiliate Gateway Status';
$_['entry_affgateway_code']			= 'Affiliate Gateway Campaign Code';
$_['entry_performant_status']		= '2Performant Status';
$_['entry_performant_code']			= '2Performant Campaign Code';
$_['entry_performant_confirm']		= '2Performant Confirm Code';
$_['entry_performant_tax']			= '2Performant manual tax  ';
$_['entry_performant_tax_value']	= '2Performant tax value i.e 24 without % sign';
$_['entry_performant_currency']		= 'Alternate Currency';
$_['entry_admitad_status']			= 'AdmitAd Status';
$_['entry_admitad_code']			= 'AdmitAd Campaign Code';
$_['entry_admitad_category']		= 'Tariff code';
$_['entry_admitad_additional_type']	= 'Sales (default)';
$_['entry_admitad_invoice_broker']	= 'Invoice Broker (adm default)';
$_['entry_admitad_invoice_category']= 'Invoice Category (action.code)';
$_['entry_admitad_retag_status']	= 'ReTag Status';
$_['entry_admitad_retag_code1']		= 'ReTag Code For Homepage Page i.e. 9xx0931xb';
$_['entry_admitad_retag_code2']		= 'ReTag Code For Category Pages i.e. 9xx0931xb';
$_['entry_admitad_retag_code3']		= 'ReTag Code For Product Pages i.e. 9xx0931xb';
$_['entry_admitad_retag_code4']		= 'ReTag Code For Cart Page i.e. 9xx0931xb';
$_['entry_admitad_retag_code5']		= 'ReTag Code For Thankyou Page i.e. 9xx0931xb';
$_['entry_sendinblue_status']		= 'SendinBlue Status';
$_['entry_sendinblue_code']			= 'SendinBlue Client Key';
$_['entry_zopimchat_code']			= 'Zopim ID';
$_['entry_zenchat_status']			= 'Zen Chat Status';
$_['entry_zenchat_code']			= 'Zen Chat ID';
$_['entry_freshchat_status']		= 'Fresh Chat Status';
$_['entry_freshchat_code']			= 'Fresh Chat Token ID';
$_['entry_freshchat_host']			= 'Fresh Chat Host';
$_['entry_hubspot_status']			= 'Hubspot Chat Status';
$_['entry_hubspot_code']			= 'Hubspot Chat ID';
$_['entry_smartsupp_status']		= 'Smartsupp Chat Status';
$_['entry_smartsupp_code']			= 'Smartsupp Chat ID';
$_['entry_paypal_status']			= 'Paypal Analytics Status';
$_['entry_paypal_code']			    = 'Paypal Analytics ID';
$_['entry_tiktok_status']			= 'Tiktok Pixel Status';
$_['entry_tiktok_code']			    = 'Tiktok Pixel Code';

$_['entry_pixel_test_code']			= 'Pixel API Тестовий код';
$_['help_pixel_test_code']			= 'Тестовий код Conversion API на екрані Диспетчера подій, залиште порожнім для робочих сайтів.';

$_['entry_ampcode']					= 'AMP Google Tag Manager Container ID';
$_['entry_ampstatus']				= 'AMP Tagmanager Status';

$_['entry_eu_cookie']				= 'Show EU Cookie Consent';
$_['entry_eu_cookie_enforce']		= 'Enforce Cookie Consent';
$_['entry_cookie_position']			= 'Cookie Popup position';
$_['entry_cookie_title']			= 'Cookie Popup Heading';
$_['entry_cookie_text']				= 'EU Cookie Text Message';
$_['entry_cookie_text2']			= 'Cookie Customize Text';
$_['entry_cookie_link']				= 'Privacy Policy URL i.e. privacy.html';
$_['entry_cookie_button1']			= 'Cookie Accept Button Text';
$_['entry_cookie_button2']			= 'Cookie Moreinfo text';
$_['entry_cookie_button3']			= 'Cookie Customise Button Text';
$_['entry_cookie_bg_popup']			= 'Popup Background Colour i.e. #eaf7f7';
$_['entry_cookie_text_popup']		= 'Popup Text Color i.e. white';
$_['entry_cookie_bg_button']		= 'Button Background Colour i.e. #56cbdb';
$_['entry_cookie_text_button']		= 'Button Text Color i.e. white';
$_['entry_cookie_heading_color']	= 'Heading Text Color i.e. red';
$_['entry_cookie_badge']			= 'Cookie Setting Badge button';
$_['entry_cookie_badge_position']	= 'Badge button placement';
$_['entry_cookie_badge_color']		= 'Badge button background color';

$_['entry_customer_data']		    = 'Увімкнути дані користувача';
$_['entry_linkwise_status']		    = 'Увімкніть маркетинг Linkwise';
$_['entry_linkwise_code']		    = 'Ідентифікатор відстеження Linkwise';
$_['entry_linkwise_decimal']		= 'Послідовно десятковий i.e. , or .';
$_['help_customer_data']		    = 'Збирайте та надсилайте дані користувача, тобто ім’я / телефон / адресу. Якщо ця функція відключена переопределяет індивідуальні настройки.';


// tabs
$_['tab_tab1']					= 'Tag Manger General';
$_['tab_tab2']					= 'Google';
$_['tab_tab3']					= 'Popular Analytics';
$_['tab_tab4']					= 'Affiliate Tracking';
$_['tab_tab5']					= 'Cookies';
$_['tab_tab6']					= 'Add Ons';
$_['tab_tab7']					= 'Order Analytics';
$_['tab_tab8']					= 'Logs';

// column
$_['column_oid']				= 'Order Number';
$_['column_status']				= 'Status';
$_['column_action']				= 'Manual Action';

// button
$_['button_send']				= 'Send';
$_['button_refund']				= 'Refund';


// Error
$_['error_permission']			= 'Попередження: Ви не маєте дозволу змінювати Менеджер тегів Google!';
$_['error_primary']				= 'Потрібен ідентифікатор контейнера Google Tag Manager!';
$_['error_analytics']			= 'Потрібен ідентифікатор Google Analytics!';
$_['error_ga4']				    = 'Потрібен ідентифікатор вимірювання Google Analytics 4!';
$_['error_warning']             = 'Warning: Your error log file %s is %s!';

// Help
$_['help_ua']					= 'Використовуйте універсальне відстеження Google Analytics';
$_['help_server']				= 'Використовуйте Google App Tagmanger Server Container для Менеджера тегів';
$_['help_server_url']			= 'Повністю кваліфіковане доменне ім\'я, лише із субдоменом.server.com';
$_['help_gid']					= 'Введіть ідентифікатор відстеження Google Analytics, тобто UA-XXXXXXX';
$_['help_ga4']					= 'Увімкніть відстеження Google Analytics 4, якщо ви використовуєте старе відстеження, використовуйте Universal Analytics';
$_['help_ga4_api']				= 'Google Analytics 4 Measurement API Secret, який буде використовуватися для зв\'язку між серверами (бета-версія)';
$_['help_admin']				= 'Рекомендовано лише для виробництва. Відвіданий адміністратором відвідування не надсилатиметься до Analytics. Увімкнути на виробничих майданчиках';
$_['help_custom_dim']		    = 'Використовуйте спеціальний вимір для надсилання даних ремаркетингу, щоб використовувати цю функцію, слід увімкнути ремаркетинг нижче';
$_['help_aw']          		    = 'Увімкнути відстеження конверсій Google Ads';
$_['help_aw_ec']          		= 'Надсилайте дані розширених конверсій на електронну адресу Google Ads, телефон, ім’я, адресу, країну конверсії';
$_['help_aw_secondary']         = 'Вторинне відстеження конверсій для Google Ads Необов’язково ';
$_['help_aw_optional']          = 'Необов’язково Налаштування та тестування звітів про конверсії з даними кошика (бета-версія)';
$_['help_aw_merchant']          = 'Ідентифікатор Merchant Center, куди завантажуються ваші товари';
$_['help_aw_country']           = 'Країна, пов’язана з каналом, куди завантажуються ваші елементи. Використовуйте коди територій CLDR.';
$_['help_aw_language']          = 'Мова, пов’язана з каналом, куди завантажуються ваші елементи. Використовуйте мовні коди ISO 639-1.';
$_['help_aw_merchant']          = 'Ідентифікатор Merchant Center, куди завантажуються ваші товари';
$_['help_aw_route']             = 'Сторінка, де вторинне перетворення на вогонь';
$_['help_userid']				= 'Відстеження ідентифікатора користувача для Google Analytics, якщо увімкнено, надсилати унікальний ідентифікатор користувача, який увійшов у систему, до Analytics';
$_['help_conversion_id']		= 'Ідентифікатор конверсії Google Ads Ввести без AW-, лише цифри';
$_['help_conversion_label']		= 'Мітка конверсії Google Ads із коду відстеження конверсій.';
$_['help_remarketing']			= 'Ремаркетинг Google Ads / Динамічний ремаркетинг';
$_['help_conversion_value2']	= 'Виправлено значення конверсії для сторінок, відмінних від покупки / нового замовлення';
$_['help_product']				= 'Унікальне поле, що використовується для зіставлення даних ремаркетингу за допомогою Merchant Center Shopping Feed або каталогу Facebook';
$_['help_ptitle']				= 'Ви можете використовувати це налаштування, щоб скоротити назву товару, надісланого в Analytics, для зручності ідентифікації у звітах';
$_['help_mp']					= 'Рекомендовано, використовувати Google Measurement Protocol для надсилання неінтерактивних звернень, рекомендованих для відстеження IPP / відшкодування Paypal тощо';
$_['help_ac']					= 'Використовуйте іншу валюту, якщо валюта вашого магазину не підтримується. Потрібно налаштувати в локалізації / валюті';
$_['help_ac_value']				= 'Якщо ви хочете використовувати інше значення для перетворення, ніж зберігати. За замовчуванням залиште чорний';
$_['help_custom']				= 'Надсилайте користувацькі параметри в Google Ads, використовуючи старі параметри.';
$_['help_cache']				= 'Рекомендується, якщо у вас великий каталог і мало ресурсів сервера, ідеально підходить для уникнення зайвих накладних витрат на SQL-сервері через додаткові запити.';
$_['help_debug']				= 'Увімкнути / вимкнути повідомлення журналу налагодження в журналі помилок менеджером тегів';
$_['help_route']				= 'Додатковий маршрут, тобто швидка оплата / виїзд, якщо у вас є спеціальний обробник Checkout або Success On для кожного рядка.';
$_['help_route_checkout']		= 'Один запис на рядок. якщо route = checkout / custom checkout додайте лише checkout / custom checkout. <br> Будь ласка, не видаляйте записи за замовчуванням.';
$_['help_route_success']		= 'Один запис на рядок. якщо маршрут = замовлення / успішне замовлення, додайте лише оформлення замовлення / успішне замовлення. <br> Будь ласка, не видаляйте записи за замовчуванням.';
$_['help_id_prefix']		    = 'Додайте префікс до поля ідентифікатора товару, щоб він відповідав вашому каналу товару';
$_['help_id_suffix']		    = 'Додайте суфікс до поля ідентифікатора товару, щоб він відповідав вашому каналу товару';

$_['help_customcode']		    = 'Спеціальним кодом для розміщення заголовка можуть бути метатеги, відстеження javascript тощо.';
$_['help_cenforce']		        = 'Примусовий режим блокуватиме відстеження, доки користувач не прийме згоду на використання файлів cookie';
$_['help_ctitle']   		    = 'Рухаючись до вікна Cookie, залиште чорний, щоб видалити заголовок';
$_['help_ctext']		        = 'Текст, який з’являється, коли користувач натискає Налаштування або Налаштування';
$_['help_clink']		        = 'Посилання на URL-адресу політики щодо файлів cookie або політики конфіденційності ';
$_['help_debug_api']		    = 'Показувати журнали API у журналі диспетчера тегів для всіх викликів API, тобто. Google Measurement Protocol або Conversion API';

$_['text_details']              = '';