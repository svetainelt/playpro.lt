<?php
// Heading
$_['heading_title']     = 'Kainos.lt XML Export';


// Text
$_['text_success']      = 'Success: You modified kainos.lt XML Export';
$_['text_off']      = 'Export is disabled';

// Column
$_['column_action']     = 'Action';

// Entry
$_['entry_status']     = 'Status';
$_['entry_categories']     = 'Categories';
$_['entry_select_all']     = 'Select all';
$_['entry_unselect_all']     = 'Unselect all';
$_['entry_language_id']     = 'XML export language';
$_['entry_store']     = 'Export products from stores';
$_['entry_manufacturers']     = 'Manufacturers';
$_['entry_delivery_price']     = 'Delivery price (by product weight)';
$_['entry_delivery_price_info']     = 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['entry_delivery_time']     = 'Delivery time';
$_['entry_delivery_time_info']     = 'Delivery time in days. For exmple: 2';
$_['entry_min_price']     = 'Minimal product price';
$_['entry_min_price_info']     = 'Only products with greater price than this. For example: 12.99';
$_['entry_status_info']     = 'If Export is disabled, XML URL not works';
$_['entry_language_id_info']     = 'Choose which language title and description use in export';
$_['entry_operator']     = 'Change products prices in XML';
$_['entry_operator_info']     = 'You can change all prices. Leave empty if you do not want to change.';
$_['entry_heading'] = 'Kainos.lt XML export';
$_['entry_category_type'] = 'Which category show?';
$_['entry_category_type_info'] = 'Choose which product category show in XML';
$_['entry_category_type_first'] = 'First';
$_['entry_category_type_last'] = 'Last';
$_['entry_tax_class'] = 'Tax class';
$_['entry_condition'] = 'Condition';
$_['entry_condition_info'] = 'new (default) | used = used products | refurbished = refurbished products';

// Buttons
$_['button_save']     = 'Save';
$_['button_cancel']     = 'Cancel';

// Error
$_['error_permission']  = 'Attention: You do not have access to modify module!';

?>