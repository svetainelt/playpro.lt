<?php
// Heading
$_['heading_title']      = 'Paysera';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified Paysera account details!';
$_['text_on']            = 'On';
$_['text_off']           = 'Off';
$_['text_edit']          = 'Edit Paysera';

// Entry
$_['entry_project']      = 'Project ID:';
$_['entry_sign']         = 'Signature password:';
$_['entry_lang']     	 = 'Language:';
$_['help_lang']     	 = 'LIT, ENG, RUS';
$_['entry_test']         = 'Test Mode:';
$_['entry_new_order_status'] = 'New Order Status:';
$_['entry_order_status'] = 'Order Status after payment:';
$_['entry_payment_lang'] = 'Default country of payment methods:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_paymentlist']  = 'Display payment list:';

// Custom START
$_['text_paysera']   = '<a href="https://www.paysera.com" target="_blank"><img src="https://www.paysera.com/payment/m/m_images/wfiles/i1xv7h2787.png" alt="Paysera" title="Paysera" height="20px" /></a>';
$_['entry_filter_methods']  = 'Enable payment methods filtering?';
$_['text_select_country']  = '-- Choose country --';
$_['entry_select_countries']  = 'Select country and then check payment methods, which should be available to the buyer:';
$_['entry_selected_countries']  = 'Select the country for which payment methods should be available to the buyer:';
$_['button_check_all_methods']  = 'Check all payment methods';
$_['button_uncheck_all_methods']  = 'Uncheck all payment methods';
$_['tab_filter_banks'] = 'Banks';
// Custom END

// Help
$_['help_callback']      = '';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment Paysera!';
$_['error_project']      = 'Project ID Required!';
$_['error_sign']     	 = 'Signature password Required!';
$_['error_lang']     	 = 'Lang code Required!';
$_['error_refresh']      = 'Update failed!';