<?php
// Heading
$_['heading_title']    = 'Venipak Shipping';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified venipak shipping!';
$_['text_edit']        = 'Edit Venipak Shipping';

// Entry
$_['entry_total']      = 'Total';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Help
$_['help_total']       = 'Sub-Total amount needed before the free shipping becomes available.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify venipak shipping!';

$_['text_venipak_connection_settings'] = 'Venipak connection settings';
$_['text_shipping_method_settings'] = 'Shipping method settings';
$_['text_sender_settings'] = 'Sender settings';

$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';
$_['text_venipak_shipping_sort_order'] = "Sort order";
$_['text_venipak_shipping_geo_zone_id'] = "Geozone";
$_['text_venipak_shipping_method_title'] = 'Shipping method title';
$_['text_venipak_shipping_method_title_courier'] = 'Shipping method courier title';
$_['text_venipak_shipping_method_title_pickup'] = 'Shipping method pickup title';
$_['text_venipak_shipping_client_id'] = 'Venipak client ID';
$_['text_venipak_shipping_client_username'] = 'Venipak client username';
$_['text_venipak_shipping_client_password'] = 'Venipak client password';
$_['text_venipak_shipping_test'] = 'Test mode';
$_['text_venipak_shipping_cost_courier'] = 'Courier shipping fee';
$_['text_venipak_shipping_cost_pickup'] = 'Pickup shipping fee';
$_['text_venipak_shipping_free'] = 'Free shipping after order total';
$_['text_venipak_shipping_is_map_enabled'] = 'Show google map';
$_['text_venipak_shipping_google_api_key'] = 'Google API key';
$_['text_venipak_shipping_is_clusters_enabled'] = 'Enable clusters';
$_['text_venipak_shipping_disable_pickup'] = 'Disable pickup points';
$_['text_venipak_shipping_disable_locker'] = 'Disable locker points';
$_['text_venipak_shipping_disable_courier'] = 'Disable courier delivery option';

$_['text_venipak_shipping_sender_name'] = 'Sender company name';
$_['text_venipak_shipping_sender_company_code'] = 'Sender company code';
$_['text_venipak_shipping_sender_country'] = 'Sender country';
$_['text_venipak_shipping_sender_city'] = 'Sender city';
$_['text_venipak_shipping_sender_address'] = 'Sender address';
$_['text_venipak_shipping_sender_postcode'] = 'Sender postcode';
$_['text_venipak_shipping_sender_contact_person'] = 'Sender contact person';
$_['text_venipak_shipping_sender_contact_tel'] = 'Sender contact tel';
$_['text_venipak_shipping_sender_contact_email'] = 'Sender contact email';

$_['text_venipak_shipping_products_per_pack'] = 'Products per pack';
$_['text_venipak_shipping_initial_tracking_number'] = 'Initial tracking number';
