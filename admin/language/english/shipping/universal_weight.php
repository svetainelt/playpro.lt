<?php
// Heading
$_['heading_title']    = 'Infinate Shipping Rules';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Plus + Location & Weight Custom Shipping based shipping!';
$_['text_edit']        = 'Edit Infinite Shipping';
$_['text_all_zones']   = 'All zones';

$_['button_rule_add']  = 'Add shipping rule';

// Entry
$_['entry_tax_class']  = 'Tax Class:<br/>';
$_['entry_geo_zone']   = 'Geo Zone:<br/>';
$_['entry_status']     = 'Status:<br/>';
$_['entry_sort_order'] = 'Sort Order:<br/>';
$_['entry_title']      = 'Title:<br/>';
$_['entry_flat_cost']  = 'Flat Cost:<br/>';
$_['entry_min_price']  = 'Min Price:<br/>';
$_['entry_max_price']  = 'Max Price:<br/><small>Enter * for infinate max price</small>';
$_['entry_min_weight'] = 'Min Weight:<br/>';
$_['entry_max_weight'] = 'Max Weight:<br/><small>Enter * for infinate max weight</small>';
$_['entry_action']     = 'Action:<br/>';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify weight based shipping!';