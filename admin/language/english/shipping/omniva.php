<?php
// Heading
$_['heading_title']    = 'Ⓟ Omniva';

// Text
$_['text_shipping']       = 'Shipping';
$_['text_success']        = 'Success: You have modified Omniva shipping!';
$_['text_module_version'] = 'Extension version';
$_['text_select_zone']    = 'Select zone';
$_['text_each_product']   = 'per each product';
$_['text_all_cart']       = 'per shopping cart';
$_['text_edit']           = 'Edit Omniva Module';

// Tabs
$_['tab_latvia']       = 'Shipping Latvia';
$_['tab_lithuania']    = 'Shipping Lithuania';
$_['tab_estonia']      = 'Shipping Estonia';

// Entry
$_['entry_cost']       = 'Cost:';
$_['entry_dimension']  = 'Max dimensions (L x W x H):<br /><a target="_blank" href="https://www.omniva.lv/public/files/failid/standard_terms.pdf">Click to see requirements</a>';
$_['entry_weight']     = 'Max weight:<br /><a target="_blank" href="https://www.omniva.lv/public/files/failid/standard_terms.pdf">Click to see requirements</a>';
$_['entry_tax_class']  = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_import_url'] = 'URL to CSV file for import addresses:';
$_['entry_addresses']  = 'Omniva addresses:';

$_['button_update']    = 'Update addresses & Save';

$_['help_dimension']  = 'Max dimensions 38 cm (L) x 41 cm (W) x 64 cm (H)';
$_['help_weight']     = 'Max weight 30 kg';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Omniva shipping!';
$_['error_omniva_weight']  = 'Weight Required!';
$_['error_omniva_length']  = 'Length Required!';
$_['error_omniva_width']   = 'Width Required!';
$_['error_omniva_height']  = 'Height Required!';