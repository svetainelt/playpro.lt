<?php
// Heading
$_['heading_title']    = 'Venipak Pickup Lithuania';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Venipak Pickup Lithuania shipping!';
$_['text_edit']        = 'Edit PostaPont Weight Based Shipping';
$_['text_show_all_yes'] = 'Yes';
$_['text_show_all_no']  = 'No';

// Entry
$_['entry_cost']       = 'Cost:';
$_['entry_freelimit']  = 'Free shipping from sum:';
$_['entry_show_all']   = 'Show All Location:';
$_['entry_tax_class']  = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

$_['entry_weight_from']= 'Weight from:';
$_['entry_weight_from_tooltip'] = 'weight > than<br>If 0 then ignore this constrat';
$_['entry_weight_to']  = 'Weight to:';
$_['entry_weight_to_tooltip'] = 'weight <= than<br>If 0 then ignore this constrat';
$_['entry_first']      = 'First item cost:';
$_['entry_next']       = 'For each next item cost:';

// Button
$_['button_refresh']   = 'Refresh Data';
$_['button_add']       = 'Add';
$_['button_remove']    = 'Remove';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Venipak Pickup Lithuania shipping!';
?>