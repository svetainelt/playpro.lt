<?php
// Heading
$_['heading_title']    = '<img style="height:45px; width: auto" src="view/image/facebookfeedproducts/icon.png" alt="icon" border="0"/> Facebook Feed Products <span style="font-size:12px; color:#999">[1.4.5] by <a href="https://shop.pinta.pro/opencart" style="font-size:1em; color:#999" target="_blank">Pinta Webware</a></span>';

// Text
$_['text_feed']        = 'Feeds';
$_['text_success']     = 'Success: You have modified Facebook feed!';
$_['text_toolpit_1']        = 'Activate the plug-in. Without this, you cannot create a data feed.';
$_['text_currency']        = 'Currency';
$_['text_currency_toolpit']        = 'Select the currency type of your goods';
$_['text_language']        = 'Language';
$_['text_language_toolpit']        = 'Select the language of the products  in the feed according to the language of the products on your site';
$_['text_disable_image']        = 'Disable image resize';
$_['text_image_size']        = 'Image size ( min. 600x600)';
$_['text_px']        = 'px';
$_['text_not_google_category']        = 'Do not use google and facebook category';
$_['text_image_empty_toolpit']        = 'Will be used no-image picture, such products may not miss facebook';
$_['text_image_empty']        = 'Upload goods without images';
$_['text_options']        = 'Options';
$_['text_options_toolpit']        = 'Google Options';
$_['text_select']        = 'Select matching';
$_['text_select_toolpit']        = 'Please select matching options on your website with Google options';
$_['text_color']        = 'Color';
$_['text_color_select']        = 'Select options for Color';
$_['text_size']        = 'Size';
$_['text_size_select']        = 'Select options for Size';
$_['text_pattern']        = 'Pattern';
$_['text_pattern_select']        = 'Select options for Pattern';
$_['text_material']        = 'Material';
$_['text_material_select']        = 'Select options for Material';
$_['text_update']        = 'Update';
$_['text_update_toolpit']        = 'To update the list of Google categories, enter the address where the Google categories are located and click the Update button (by default, the list of Google categories already exists in the plugin)';
$_['text_paragraf']        = ' <p> Example:<i> https://www.google.com/basepages/producttype/taxonomy-with-ids.en-US.txt</i> </p>';
$_['text_paragraf_facebook']        = ' <p> Example:<i> https://www.facebook.com/products/categories/en_US.txt </i> </p>';
$_['text_category_on']        = 'All category on';
$_['text_product_feed_list']        = 'Product feed list';
$_['text_automatically']        = 'Automatically';
$_['text_category_off']        = 'All category off';
$_['text_category']        = 'Category';
$_['text_google_category']        = 'Google category';
$_['text_facebook_category']        = 'Facebook category';
$_['text_status']        = 'Status';
$_['text_toolpit_1']        = 'Category on your site';
$_['text_toolpit_2']        = 'Google category. Please select the closest match to your category on website';
$_['text_toolpit_2_facebook']        = 'Facebook category. Please select the closest match to your category on website';
$_['text_toolpit_3']        = ' Google  subcategory. Please select for more exact match to your website category  and category on Facebook';
$_['text_toolpit_3_facebook']        = ' Facebook  subcategory. Please select for more exact match to your website category  and category on Facebook';
$_['text_select_1']        = 'Select...';
$_['text_url']        = 'Url';
$_['entry_facebook_main_product_category']  = 'Facebook main product category';
$_['entry_facebook_all_product_category']  = 'Facebook all product category';
$_['text_download']        = 'Download';
$_['text_cron_title']        = 'Cron Links';
$_['text_cron_description']        = ' <p> Cron Job\'s are scheduled tasks that are run periodically. To setup your servers to use cron
                            job you can read the <a href="http://docs.opencart.com/en-gb/extension/cron/" target="_blank"> opencart documentation</a> page.
                        </p>';
$_['text_cron_link']        = 'Cron link';
$_['text_file_link']        = 'File link';
$_['text_only_selected']        = 'Only selected';
$_['text_']        = '';
$_['text_products']        = 'Products';
$_['text_list']        = 'List';
$_['text_exclude_selected_from_the_list']        = 'Everyone except selected';
$_['text_except_the_chosen_one']        = 'Except selected from list';
$_['text_add_selected_to_list']        = 'Add selected to list';
$_['text_custom']        = 'Custom';
$_['text_remove_all']        = 'Remove all';
$_['text_choose_all']        = 'Choose all';
$_['text_customer_group']        = 'Customer group';
$_['text_customer_group_tooltip']        = 'Select customer group';
$_['text_generator_url']        = 'Generator url';
$_['text_generation_btn']        = 'Create link';
$_['text_generation_link']        = 'Select fields';
$_['text_generator_url']        = 'Generator url';
$_['text_generator_description']        = 'Generation of url depending on currency and language';
$_['error_selected_fields']     = 'No field selected';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_data_feed']  = 'Data Feed Url:';
$_['entry_google_product_category']  = 'Data Google product category Url:';
$_['entry_product_category']  = 'Product category';
$_['entry_google_main_product_category']  = 'Google main product category';
$_['entry_google_all_product_category']  = 'Google all product category';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook feed!';
