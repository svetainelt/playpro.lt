<?php 
/* 
  #file: admin/language/english/total/payment_typechg.php
  #tested: Opencart v1.5.1.3
  #powered by fabiom7 - fabiome77@hotmail.it - copyright fabiom7 2012
*/

// Heading
$_['heading_title']     = 'Fixed Payment Type Charge Free Version';

// Text
$_['text_total']        = 'Order Total';
$_['text_success']      = 'Success modified Fixed Payment Type Charge!';
$_['text_edit']         = 'Edit Fixed Payment Type Charge!';

// Entry
$_['entry_fix']         = 'Fix setting:';
$_['entry_method']      = 'Payment method';
$_['entry_charge']      = 'Charge';
$_['entry_description'] = 'Description';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort order:';

$_['help_charge']      = 'Declaring first, % symbol for percentage charge. (%10 as 10%, %-10 as -10%)';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Fixed Payment Type Charge.!';
