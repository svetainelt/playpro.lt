<?php
// Heading
$_['heading_title']    = 'Journal2 theme colors and fonts change module';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Journal2 theme change module!';
$_['text_edit']        = 'Edit Journal2 theme change Module';
$_['text_from']        = 'Value from which to change';
$_['text_to']          = 'Value to which to change';
$_['text_choose_font'] = 'Choose font';
$_['text_swap']        = 'Swap places';

// Entry
$_['entry_color']                       = 'Color (RGB)';
$_['entry_google_font']                 = 'GOOGLE font name';
$_['entry_system_font']                 = 'SYSTEM font name';
$_['entry_color_placeholder']           = 'etc.: rgb(xx,xx,xx)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Journal2 theme change module!';
$_['error_color']      = 'Wrong color code';
$_['error_rgb_code']   = 'Wrong color RGB code';
$_['error_one_empty']  = 'One or more inputs are empty';