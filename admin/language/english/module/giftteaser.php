<?php
$_['heading_title']             = 'GiftTeaser';

/* Text */
$_['text_module']               = 'Modules';
$_['text_success']              = 'Success: You have modified module giftTeaser!';
$_['text_yes']                  = 'Yes';
$_['text_no']                   = 'No';
$_['text_enabled']              = 'Enabled';
$_['text_disabled']             = 'Disabled';
$_['text_product']				= 'Product';
$_['text_quantity']				= 'Quantity';
$_['text_viewed']				= 'Views';
$_['text_price']				= 'Price';
$_['text_start_date']			= 'Valid from';
$_['text_end_date']				= 'Valid to';
$_['text_added_on']				= 'Added on';
$_['text_actions']				= 'Actions';
$_['text_content_top']			= 'Content Top';
$_['text_content_bottom']		= 'Content Bottom';
$_['text_column_left']			= 'Column Left';
$_['text_column_right']			= 'Column Right';
$_['text_custom']				= 'Custom';
$_['text_text']					= 'Text';
$_['text_background']			= 'Background';
$_['text_border']				= 'Border';
$_['text_condition']			= 'Condition';
$_['text_total_subtotal']		= 'Total amount based on:';
$_['text_total_subtotal_help']	= 'Choose whether the total amount to be based on Cart Total or SubTotal';
$_['text_total']				= 'Total';
$_['text_subtotal']				= 'SubTotal';


/* Entry */
$_['entry_code']                = 'GiftTeaser status';
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_layout_options']      = 'Layout Options:';
$_['entry_position_options']    = 'Position Options:';
$_['entry_free_gift_label'] 	= 'Free Gift label';
$_['entry_gift_price_in_cart']  = 'Show Gift price in Cart';
$_['entry_customer_group']      = 'Customer Groups:';
$_['entry_manufacturer'] 		= 'Select manufacturers:';
$_['entry_category']            = 'Select Categories:';
$_['entry_scope']               = 'Scope';
$_['entry_in_selected']         = 'In selected';
$_['entry_not_in_selected']     = 'Not in selected';
$_['entry_product']             = 'Select Products:';
$_['entry_default']				= 'Default';
$_['entry_product_qty']         = 'Product Quantity:';
$_['entry_product_qty_opt_each']= 'For each product in cart';
$_['entry_product_qty_opt_sum'] = 'Sum of all product in cart';
$_['entry_minimum_total_price'] = 'Minimum Total Price:';
$_['entry_gift_multiple']       = 'Multiple Gift:';

/* Error */
$_['error_permission']          = 'Warning: You do not have permission to modify module GiftTeaser!';
$_['error_input_form']          = 'Please check the form for errors!';

/* Button */
$_['button_remove']				= 'Remove';
$_['button_save']				= 'Save';
$_['button_cancel']				= 'Cancel';
$_['button_add']				= 'Add';
$_['button_edit']				= 'Edit';
$_['button_add_module'] 		= 'Add module';
$_['button_remove']				= 'Remove';

/* Tooltip */
$_['tooltip_quantity'] 			= 'Current product quantity';
$_['tooltip_views'] 			= '';
$_['tooltip_price'] 			= '';
$_['tooltip_start_date'] 		= '';
$_['tooltip_end_date'] 			= '';
$_['tooltip_sort_order']		= '';

/* Custom */
$_['custom_colors']				= 'Custom widget design colors:';

/* Condition */
$_['condition_total']			= 'The cart total is:';
$_['condition_some']			= 'They have bought some of these products';
$_['condition_certain']			= 'They have bought the following products';
$_['condition_category']		= 'They have bought products from category';
$_['condition_manufacturer']	= 'They have bought products from manufacturer';
$_['condition_reward_points']	= 'Reward points in the cart';

/* Others */
$_['subject_text']              = 'Subject';
$_['save_changes']				= 'Save changes';
$_['add_new_gift']				= 'Add new Gift';
$_['default_heading_title'] 	= 'Get gift with your purchase!';
$_['custom_design']				= 'Widget layout';
$_['widget_help']				= 'Here you can set the text of store front widget for each language';
$_['store_front_widget']		= 'Store Front Widget';

$_['valid_from']				= 'Valid from:';
$_['valid_to']					= 'Valid to:';
$_['get_gift_when']             = 'Gift Condition:';

$_['total_amount']				= 'Total amount is between:';
$_['entry_reward_points']		= 'Reward points are between:';
$_['text_reward_points_help']	= 'Total amount of reward points the products in the cart would normally award the customer';
$_['text_reward_points_price_help']	= 'Amount of reward points the gift will subtract';
$_['entry_reward_points_price']	= 'Gift reward point price:';
$_['text_sort_order']			= 'Order';
$_['wrap_in_widget']			= 'Wrap in widget';
$_['heading_background']		= 'Widget heading background';
$_['gift_image_size']			= 'Gift image size';
$_['gift_image_size_help']		= 'The size of the image shown in the widget';

$_['gift_options']				= 'Gift Options';
$_['gift_description']			= 'Description';

$_['show_free_gifts']			= 'Show which products are set as free gifts';
$_['show_free_gifts_help']		= 'If a product is set as a Free Gift, a notification message on the product page will appear.';

$_['free_gifts_message_title']	= 'Free Gift Message Settings';
$_['free_gifts_message_title_help']	= 'The text will be set as a title of the message. The description, set in the Gift Options pop up, will be set as a description below this title.';

$_['section_teaser_bar']        = 'Teaser Bar';
$_['free_gifts_bar_status']     = 'Notification status';
$_['free_gifts_bar_status_help'] = 'Show notification bar on top of the product page set as a gift.';
$_['free_gifts_bar_title']      = 'Notification title';
$_['free_gifts_bar_title_help'] = 'The text will be set as the title of notification bar where the Gift description show below it.';
$_['free_gifts_bar_design']     = 'Notification design';

$_['section_widget_module']     = 'Widget Module';
$_['gift_image_size']           = 'Gift image size';
$_['gift_image_size_help']      = 'Set the width x height of the Gift image shown in the widget.';
$_['wrap_in_widget']            = 'Wrap in panel-box';
$_['widget_design']             = 'Panel-box design';
$_['heading_background']        = 'Heading Background';
$_['widget_design_custom']      = 'Panel-box design custom';
$_['widget_design_custom_help'] = 'Tips: To further customize, use class <code>.giftTeaserWidget.gt-custom</code> in the Custom CSS input.';
$_['widget_information']        = 'Widget Information';
$_['widget_information_help']   = 'Widget module heading and description.';

$_['section_design']            = 'Design';
$_['custom_css']                = 'Custom CSS';

$_['certain_product_help']		= '<span class="help"><i class="fa fa-info-circle"></i>  The gift will be added to the shopping cart once the customer has purchased <strong>all</strong> of the selected products with a minumim quantity equal to the selected one.</span>';
$_['some_product_help']			= '<span class="help"><i class="fa fa-info-circle"></i>  The gift will be added to the shopping cart once the customer has purchased <strong>at least one</strong> of the selected products with a minumim quantity equal to the selected one.</span>';
$_['entry_customer_group_help'] = 'Enable for the following customer groups.';
$_['entry_certain_product_help']= 'The gift will be added to the shopping cart once the customer has purchased <strong>all</strong> of the selected products.';
$_['entry_some_product_help']   = 'The gift will be added to the shopping cart once the customer has purchased <strong>at least one</strong> of the selected products.';
$_['entry_category_help']       = 'The gift will be added to the shopping cart once the customer has purchased <strong>at least one</strong> product.';
$_['entry_manufacturer_help']   = 'The gift will be added to the shopping cart once the customer has purchased <strong>at least one</strong> product.';
$_['entry_product_qty_help']    = '<b>Eligible quantity</b> for gift and validation method.';
$_['entry_gift_multiple_help']  = 'When multiple <b>eligible quantity</b> found, award multiple gift.';
$_['entry_minimum_total_price_help'] = 'Minimum total price of the products (conditions) in cart. Different currencies automatically converted.';
$_['entry_gift_price_in_cart_help'] = 'Show Gift product price in cart table and reduce it at the order totals. Make sure to enabled GiftTeaser at Extension &gt; Order Totals.';
