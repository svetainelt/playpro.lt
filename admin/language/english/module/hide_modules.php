<?php
// Heading
$_['heading_title']    = 'Hide Modules';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified hide modules module!';
$_['text_edit']        = 'Edit Hide Modules Module';
$_['text_check_to_hide'] = 'Only checked menu links will be hidden';
$_['text_check_to_show'] = 'Only checked modules will be visible';

// Tabs
$_['tab_general']        = 'General';
$_['tab_module']         = 'Modules';
$_['tab_payment']        = 'Payment modules';
$_['tab_shipping']       = 'Shipping modules';
$_['tab_total']          = 'Total modules';
$_['tab_menu']           = 'Menu';

// Entry
$_['entry_status']       = 'Status';
$_['entry_style_status'] = 'Style Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify hide modules module!';