<?php

// Heading
$_['heading_title']                     = 'Partnerystės programa';

// Text
$_['text_success']                      = 'Sėkmingai modifikavote partnerystės programą!';
$_['text_approved']                     = 'Jūs patvirtinote %s paskyras!';
$_['text_list']                         = 'Partnerių sąrašas';
$_['text_add']                          = 'Pridėti partnerį';
$_['text_edit']                         = 'Redaguoti partnerį';
$_['text_affiliate_detail']             = 'Partnerio informacija';
$_['text_affiliate_address']            = 'Partnerio adresas';
$_['text_balance']                      = 'Balansas';
$_['text_cheque']                       = 'Čekis';
$_['text_paypal']                       = 'PayPal';
$_['text_bank']                         = 'Bankinis pavedimas';

// Column
$_['column_name']                       = 'Parnerystės programos pavadinimas';
$_['column_email']                      = 'El. paštas';
$_['column_code']                       = 'Sekimo kodas';
$_['column_balance']                    = 'Likutis';
$_['column_status']                     = 'Būsena';
$_['column_approved']                   = 'Patvirtinta';
$_['column_date_added']                 = 'Data';
$_['column_description']                = 'Aprašymas';
$_['column_amount']                     = 'Kiekis';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_status']                      = 'Būsena';
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Pakartokite slaptažodį';
$_['entry_company']                     = 'Įmonė';
$_['entry_website']                     = 'Tinklalapis';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_city']                        = 'Miestas';
$_['entry_postcode']                    = 'Pašto kodas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_code']                        = 'Sekimo kodas';
$_['entry_commission']                  = 'Komisiniai (%)';
$_['entry_tax']                         = 'Įmonės/asmens kodas';
$_['entry_payment']                     = 'Mokėjimo būdas';
$_['entry_cheque']                      = 'Čekio gavėjas';
$_['entry_paypal']                      = 'PayPal paskyros el. paštas';
$_['entry_bank_name']                   = 'Banko pavadinimas';
$_['entry_bank_branch_number']          = 'ABA/BSB numeris (padalinio numeris)';
$_['entry_bank_swift_code']             = 'SWIFT kodas';
$_['entry_bank_account_name']           = 'Sąskaitos savininkas';
$_['entry_bank_account_number']         = 'Sąskaitos numeris';
$_['entry_amount']                      = 'Kiekis';
$_['entry_description']                 = 'Aprašymas';
$_['entry_name']                        = 'Partnerio vardas';
$_['entry_approved']                    = 'Ar patvirtintas?';
$_['entry_date_added']                  = 'Data';

// Help
$_['help_code']                         = 'Šis kodas naudojamas partnerio klientams stebėti.';
$_['help_commission']                   = 'Procentas, kurį partneris gauna nuo kiekvieno užsakymo.';

// Error
$_['error_warning']                     = 'Įspėjimas: Atidžiai patikrinkite formos duomenis dėl klaidų!';
$_['error_permission']                  = 'Neturite teisių partnerių informacijos atnaujinimui!';
$_['error_exists']                      = 'Įspėjimas: El. pašto adresas jau užregistruotas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas neteisingas!';
$_['error_cheque']                      = 'Čekio gavėjo vardas reikalingas!';
$_['error_paypal']                      = 'PayPal paskyros el. paštas netinkamas!';
$_['error_bank_account_name']           = 'Sąskaitos savininkas reikalingas!';
$_['error_bank_account_number']         = 'Sąskaitos numeris reikalingas!';
$_['error_telephone']                   = 'Telefonas turi būti nuo 3 iki 32 simbolių!';
$_['error_password']                    = 'Slaptažodis turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_confirm']                     = 'Naujas slaptažodis ir pakartotas slaptažodis nesutapo!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_postcode']                    = 'Pašto kodo ilgis turi būti nuo 2 iki 10 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_code']                        = 'Sekimo kodas reikalingas!';
