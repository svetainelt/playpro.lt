<?php

// Heading
$_['heading_title']                     = 'Naujienų siuntimas';

// Text
$_['text_success']                      = 'Jūsų pranešimas sėkmingai išsiųstas!';
$_['text_sent']                         = 'Jūsų pranešimas sėkmingai išsiųstas %s klientams iš %s pasirinktų!';
$_['text_list']                         = 'Pašto sąrašas';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_newsletter']                   = 'Visi, užsisakę naujienlaiškį';
$_['text_customer_all']                 = 'Visi klientai';
$_['text_customer_group']               = 'Klientų grupė';
$_['text_customer']                     = 'Klientai';
$_['text_affiliate_all']                = 'Visi partneriai';
$_['text_affiliate']                    = 'Partnerystė';
$_['text_product']                      = 'Prekės';

// Entry
$_['entry_store']                       = 'Nuo';
$_['entry_to']                          = 'Kam';
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_customer']                    = 'Klientas';
$_['entry_affiliate']                   = 'Partneris';
$_['entry_product']                     = 'Prekės';
$_['entry_subject']                     = 'Tema';
$_['entry_message']                     = 'Pranešimas';

// Help
$_['help_customer']                     = 'Automatinis užbaigimas';
$_['help_affiliate']                    = 'Automatinis užbaigimas';
$_['help_product']                      = 'Siųsti tik klientams, kurie užsisakė produktus iš sąrašo. (Automatinis užbaigimas)';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių siųsti el. laišką!';
$_['error_subject']                     = 'El. pašto tema būtina!';
$_['error_message']                     = 'El. pašto pranešimas būtinas!';
