<?php

// Heading
$_['heading_title']                     = 'Nuolaidų kuponai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti nuolaidų kuponai!';
$_['text_list']                         = 'Nuolaidų kuponų sąrašas';
$_['text_add']                          = 'Pridėti nuolaidų kuponą';
$_['text_edit']                         = 'Koreguoti nuolaidų kuponą';
$_['text_percent']                      = 'Procentai';
$_['text_amount']                       = 'Fiksuota suma';

// Column
$_['column_name']                       = 'Nuolaidų kupono pavadinimas';
$_['column_code']                       = 'Kodas';
$_['column_discount']                   = 'Nuolaida';
$_['column_date_start']                 = 'Pradžios data';
$_['column_date_end']                   = 'Pabaigos data';
$_['column_status']                     = 'Būsena';
$_['column_order_id']                   = 'Užsakymo nr.';
$_['column_customer']                   = 'Klientas';
$_['column_amount']                     = 'Kiekis';
$_['column_date_added']                 = 'Data';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Nuolaidų kupono pavadinimas';
$_['entry_code']                        = 'Kodas';
$_['entry_type']                        = 'Tipas';
$_['entry_discount']                    = 'Nuolaida';
$_['entry_logged']                      = 'Kliento prisijungimas';
$_['entry_shipping']                    = 'Nemokamas pristatymas';
$_['entry_total']                       = 'Bendra suma';
$_['entry_category']                    = 'Kategorija';
$_['entry_product']                     = 'Prekės';
$_['entry_date_start']                  = 'Pradžios data';
$_['entry_date_end']                    = 'Pabaigos data';
$_['entry_uses_total']                  = 'Nuolaidų kupono panaudojimai';
$_['entry_uses_customer']               = 'Vieno kliento nuolaidų kupono panaudojimai';
$_['entry_status']                      = 'Būsena';

// Help
$_['help_code']                         = 'Kodas kurį turi įvesti klientas, kad gautų nuolaidą.';
$_['help_type']                         = 'Procentas arba fiksuota suma.';
$_['help_logged']                       = 'Klientas turi būti prisijungęs, kad galėtų pasinaudoti nuolaidų kuponu.';
$_['help_total']                        = 'Bendra mažiausia suma, kuri turi būti pasiekta, kad nuolaidų kuponas galiotų.';
$_['help_category']                     = 'Pasirinkite visus produktus pagal pasirinktą kategoriją.';
$_['help_product']                      = 'Pasirinkite konkrečius produktus kuriems nuolaidų kuponas bus taikomas. Nepasirinkus nuolaidų kuponas bus taikomas visam krepšeliui.';
$_['help_uses_total']                   = 'Didžiausias nuolaidų kupono panaudojimų skaičius bet kuriam klientui. Jei panaudojimų skaičius neribojamas, palikite tuščią';
$_['help_uses_customer']                = 'Didžiausias nuolaidų kupono panaudojimų skaičius vienam klientui. Jei panaudojimų skaičius neribojamas, palikite tuščią';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti nuolaidų kuponus!';
$_['error_exists']                      = 'Įspėjimas: Nuolaidų kupono kodas jau egzistuoja!';
$_['error_name']                        = 'Nuolaidų kupono pavadinimas turi būti nuo 3 iki 64 simbolių ilgio!';
$_['error_code']                        = 'Kodas turi būti nuo 3 iki 10 simbolių ilgio!';
