<?php

// Heading
$_['heading_title']                     = 'Marketingo sekimas';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas merketingo sekimas!';
$_['text_list']                         = 'Marketingo sekimo sąrašas';
$_['text_add']                          = 'Pridėti marketingo sekimą';
$_['text_edit']                         = 'Koreguoti marketingo sekimą';

// Column
$_['column_name']                       = 'Kampanijos pavadinimas';
$_['column_code']                       = 'Kodas';
$_['column_clicks']                     = 'Paspaudimai';
$_['column_orders']                     = 'Užsakymai';
$_['column_date_added']                 = 'Data';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Kampanijos pavadinimas';
$_['entry_description']                 = 'Kampanijos aprašymas';
$_['entry_code']                        = 'Sekimo kodas';
$_['entry_example']                     = 'Pavyzdžiai';
$_['entry_date_added']                  = 'Data';

// Help
$_['help_code']                         = 'Sekimo kodas, kuris bus naudojamas marketingo akcijos sekimui.';
$_['help_example']                      = 'Ši sistema gali sekti perdavimus, jums reikia pridėti stebėjimo kodą į URL galą, kuris nurodo į Jūsų parduotuvę.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti marketingo sekimo!';
$_['error_name']                        = 'Akcijos pavadinimas turi būti nuo 1 iki 32 simbolių!';
$_['error_code']                        = 'Sekimo kodas reikalingas!';
