<?php

// Heading
$_['heading_title']                     = 'Baneriai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti baneriai!';
$_['text_list']                         = 'Banerių sąrašas';
$_['text_add']                          = 'Pridėti banerį';
$_['text_edit']                         = 'Koreguoti banerį';
$_['text_default']                      = 'Numatytoji parinktis';

// Column
$_['column_name']                       = 'Banerio pavadinimas';
$_['column_status']                     = 'Būsena';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Banerio pavadinimas';
$_['entry_title']                       = 'Antraštė';
$_['entry_link']                        = 'Nuoroda';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Neturite teisių banerių redagavimui!';
$_['error_name']                        = 'Banerio pavadinimo ilgis turi būti nuo 3 iki 64 simbolių!';
$_['error_title']                       = 'Banerio antraštės ilgis turi būti nuo 2 iki 64 simbolių!';
