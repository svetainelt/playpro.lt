<?php

// Heading
$_['heading_title']                     = 'Išdėstymai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotas išsidėstymas!';
$_['text_list']                         = 'Išdėstymo sąrašas';
$_['text_add']                          = 'Pridėti išdėstymą';
$_['text_edit']                         = 'Koreguoti išdėstymą';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_content_top']                  = 'Viršus';
$_['text_content_bottom']               = 'Apačia';
$_['text_column_left']                  = 'Kairė';
$_['text_column_right']                 = 'Dešinė';

// Column
$_['column_name']                       = 'Išdėstymo pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Išdėstymo pavadinimas';
$_['entry_store']                       = 'Parduotuvė';
$_['entry_route']                       = 'Kelias';
$_['entry_module']                      = 'Modulis';
$_['entry_position']                    = 'Išdėstymas';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti išdėstymus!';
$_['error_name']                        = 'Išdėstymo pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_default']                     = 'Įspėjimas: Nepavyko pašalinti išsidėstymo, nes jis yra parduotuvės numatytasis!';
$_['error_store']                       = 'Įspėjimas: Nepavyko pašalinti išsidėstymo, nes jis yra priskirtas %s parduotuvei (-ėms)!';
$_['error_product']                     = 'Įspėjimas: Nepavyko pašalinti išsidėstymo, nes jis yra priskirtas %s prekėms (-ių)!';
$_['error_category']                    = 'Įspėjimas: Nepavyko pašalinti išsidėstymo, nes jis yra priskirtas %s kategorijoms (-ų)!';
$_['error_information']                 = 'Įspėjimas: Nepavyko pašalinti išsidėstymo, nes jis yra priskirtas %s informaciniams (-ių) puslapiams (-ių)!';
