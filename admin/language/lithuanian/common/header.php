<?php

// Heading
$_['heading_title']                     = 'Administravimo panelė';

// Text
$_['text_order']                        = 'Užsakymai';
$_['text_processing_status']            = 'Apdorojami';
$_['text_complete_status']              = 'Baigti';
$_['text_customer']                     = 'Klientai';
$_['text_online']                       = 'Prisijungę klientai';
$_['text_approval']                     = 'Laukiama patvirtinimo';
$_['text_product']                      = 'Prekės';
$_['text_stock']                        = 'Išparduotos';
$_['text_review']                       = 'Atsiliepimai';
$_['text_return']                       = 'Grąžinimai';
$_['text_affiliate']                    = 'Partnerystė';
$_['text_store']                        = 'Parduotuvės';
$_['text_front']                        = 'Parduotuvės vitrina';
$_['text_help']                         = 'Pagalba';
$_['text_homepage']                     = 'Pagrindinis puslapis';
$_['text_support']                      = 'Palaikymo forumas';
$_['text_documentation']                = 'Dokumentacija';
$_['text_logout']                       = 'Atsijungti';
