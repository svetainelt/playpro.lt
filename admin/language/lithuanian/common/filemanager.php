<?php

// Heading
$_['heading_title']                     = 'Paveikslėlių tvarkyklė';

// Text
$_['text_uploaded']                     = 'Failas įkeltas!';
$_['text_directory']                    = 'Aplankas sukurtas!';
$_['text_delete']                       = 'Failas ar aplankas pašalintas!';

// Entry
$_['entry_search']                      = 'Paieška...';
$_['entry_folder']                      = 'Aplanko pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Leidimas nesuteiktas!';
$_['error_filename']                    = 'Įspėjimas: Failo pavadinimas turi būti nuo 3 iki 255 simbolių ilgio!';
$_['error_folder']                      = 'Įspėjimas: Aplanko pavadinimas turi būti nuo 3 iki 255 simbolių ilgio!';
$_['error_exists']                      = 'Įspėjimas: Failas arba katalogas tokiu pavadinimu jau yra!';
$_['error_directory']                   = 'Įspėjimas: Katalogas neegzistuoja!';
$_['error_filetype']                    = 'Įspėjimas: Netinkamas failo tipas!';
$_['error_upload']                      = 'Įspėjimas: Failas nebuvo įkeltas, dėl nežinamos pirežasties!';
$_['error_delete']                      = 'Įspėjimas: Jūs negalite pašalinti šio katalogo!';
