<?php

// Heading
$_['heading_title']                     = 'Pamiršote slaptažodį?';

// Text
$_['text_forgotten']                    = 'Pamirštas slaptažodis';
$_['text_your_email']                   = 'Jūsų el. paštas';
$_['text_email']                        = 'Įveskite el. pašto adresą, susietą su Jūsų paskyra. Naujas slaptažodis bus išsiųstas nurodytu el. pašto adresu';
$_['text_success']                      = 'El. laiškas su patvirtinimo nuoroda buvo išsiųstas į Jūsų administratoriaus el. pašto adresą.';

// Entry
$_['entry_email']                       = 'El. pašto adresas';
$_['entry_password']                    = 'Naujas slaptažodis';
$_['entry_confirm']                     = 'Pakartokite slaptažodį';

// Error
$_['error_email']                       = 'Įspėjimas: El. paštas nerastas mūsų duomenų bazėje, pabandykite dar kartą!';
$_['error_password']                    = 'Slaptažodžio ilgis turi būti nuo 3 iki 20 simbolių!';
$_['error_confirm']                     = 'Naujas slaptažodis ir pakartotas slaptažodis nesutapo!';
