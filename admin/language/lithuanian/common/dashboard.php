<?php

// Heading
$_['heading_title']                     = 'Informacinis skydelis';

// Text
$_['text_order_total']                  = 'Iš viso užsakymų';
$_['text_customer_total']               = 'Iš viso klientų';
$_['text_sale_total']                   = 'Iš viso pardavimų';
$_['text_online_total']                 = 'Prisijungę lankytojai';
$_['text_map']                          = 'Pasaulio žemėlapis';
$_['text_sale']                         = 'Pardavimų analizė';
$_['text_activity']                     = 'Naujausia veikla';
$_['text_recent']                       = 'Naujausi užsakymai';
$_['text_order']                        = 'Užsakymai';
$_['text_customer']                     = 'Klientai';
$_['text_day']                          = 'Šiandien';
$_['text_week']                         = 'Savaitė';
$_['text_month']                        = 'Mėnuo';
$_['text_year']                         = 'Metai';
$_['text_view']                         = 'Žiūrėti daugiau...';

// Error
$_['error_install']                     = 'Įspėjimas: įdiegimo aplankas vis dar egzistuoja ir turėtų būti ištrintas, dėl saugumo priežasčių.';
