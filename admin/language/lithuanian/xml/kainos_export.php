<?php
// Heading
$_['heading_title']     = 'Kainos.lt XML Eksportas';


// Text
$_['text_success']      = 'Pavyko: Jūs modifikavote kainos.lt XML Eksportą!';
$_['text_off']      = 'Exportas yra išjungtas';

// Column
$_['column_action']     = 'Veiksmas';

// Entry
$_['entry_status']     = 'Statusas';
$_['entry_categories']     = 'Kategorijos';
$_['entry_select_all']     = 'Pažymėti visus';
$_['entry_unselect_all']     = 'Atžymėti visus';
$_['entry_language_id']     = 'XML eksportavimo kalba';
$_['entry_store']     = 'Eksportuoti prekes iš šių parduotuvių';
$_['entry_manufacturers']     = 'Gamintojai';
$_['entry_delivery_price']     = 'Pristatymo kaina (pagal svorį)';
$_['entry_delivery_price_info']     = 'Pavyzdžiui: 5:10.00,7:12.00 Svoris:Kaina,Svoris:Kaina, ir t.t...';
$_['entry_delivery_time']     = 'Pristatymo terminas';
$_['entry_delivery_time_info']     = 'Pristatymo terminas dienomis. Pvz: 2';
$_['entry_min_price']     = 'Minimali produkto kaina';
$_['entry_status_info']     = 'Jeigu statusas išjungas, XML nuoroda neveiks';
$_['entry_language_id_info']     = 'Galima pasirinkti kurios kalbos pavadinimus, aprašymus kelti į XML';
$_['entry_min_price_info']     = 'Į XML bus įkelti tik brangesni produktai už nurodytą kainą. Centai atskiriami tašku, pvz.: 12.99';
$_['entry_operator']     = 'XML prekių kainų keitimas';
$_['entry_operator_info']     = 'Galite pakeisti visų prekių kainas. Norėdami nekeisti kainų, palikite tuščius laukus';
$_['entry_heading'] = 'Kainos.lt XML eksportas';
$_['entry_category_type'] = 'Kurią kategoriją įkelti?';
$_['entry_category_type_info'] = 'Pasirinkite kurią produkto kategoriją rodyti XML faile';
$_['entry_category_type_first'] = 'Pirmą';
$_['entry_category_type_last'] = 'Paskutinę';
$_['entry_tax_class'] = 'Mokesčių klasė';
$_['entry_condition'] = 'Prekės būklė';
$_['entry_condition_info'] = 'new (standartinis pasirinkimas) | used = naudotos prekės | refurbished = atnaujintos prekės';

// Buttons
$_['button_save']     = 'Išsaugoti';
$_['button_cancel']     = 'Atšaukti';

// Error
$_['error_permission']  = 'Įspėjimas: Jūs neturite teisių modifikuoti kaina24 eksporto!';

?>