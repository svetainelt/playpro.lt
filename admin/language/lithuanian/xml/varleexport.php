<?php
// Heading
$_['heading_title']     = 'Varlė XML Eksportas';


// Text
$_['text_success']      = 'Pavyko: Jūs modifikavote varlė XML Eksportą!';
$_['text_off']      = 'Exportavimas yra išjungtas';

// Column
$_['column_action']     = 'Veiksmas';

// Entry
$_['entry_status']     = 'Statusas';
$_['entry_categories']     = 'Kategorijos';
$_['entry_select_all']     = 'Pažymėti visus';
$_['entry_unselect_all']     = 'Atžymėti visus';
$_['entry_language_id']     = 'XML eksportavimo kalba';
$_['entry_store']     = 'Eksportuoti prekes iš šių parduotuvių';
$_['entry_manufacturers']     = 'Gamintojai';
$_['entry_min_price']     = 'Minimali produkto kaina';
$_['entry_status_info']     = 'Jeigu statusas išjungas, XML nuoroda neveiks';
$_['entry_language_id_info']     = 'Galima pasirinkti kurios kalbos pavadinimus, aprašymus kelti į XML';
$_['entry_min_price_info']     = 'Į XML bus įkelti tik brangesni produktai už nurodytą kainą. Centai atskiriami tašku, pvz.: 12.99';
$_['entry_retail_price']     = 'Galutinės pardavimo kainos nustatymai';
$_['entry_retail_price_info']     = 'Galite pakeisti visų prekių galutines pardavimo kainas. Norėdami nekeisti kainos, palikite tuščius laukus';
$_['entry_heading'] = 'Varlė XML eksportas';
$_['entry_tax_class'] = 'Mokesčių klasė';
$_['entry_operator'] = 'Operatorius';
$_['entry_value'] = 'Reikšmė';
$_['entry_prime_cost']     = 'Prekės savikainos nustatymai (savikaina be PVM)';
$_['entry_prime_cost_info']     = 'Jeigu paliksite tuščius laukus savikaina bus lygi galutiniai kainai be mokesčių klasės. Jeigu galutinėje kainoje nenaudojate papildomų mokesčių klasės, savikainai būtina įrašyti išskaičiavimo formulę.';


// Buttons
$_['button_save']     = 'Išsaugoti';
$_['button_cancel']     = 'Atšaukti';

// Error
$_['error_permission']  = 'Įspėjimas: Jūs neturite teisių modifikuoti varlės eksporto!';
$_['error_prime_cost']  = 'Jeigu galutinės kainos skaičiavime nenaudojate mokesčių klasės, būtina įrašyti savikainos išskaičiavimo formulę.';

?>