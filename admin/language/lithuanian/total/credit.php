<?php

// Heading
$_['heading_title']                     = 'Parduotuvės kreditai';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuoti parduotuvės kreditai!';
$_['text_edit']                         = 'Koreguoti parduotuvės kreditą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti parduotuvės kreditų!';
