<?php

// Heading
$_['heading_title']                     = 'Klarna mokestis';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuotas Klarna mokestis!';
$_['text_edit']                         = 'Koreguoti Klarna mokestį';
$_['text_sweden']                       = 'Švedija';
$_['text_norway']                       = 'Norvegija';
$_['text_finland']                      = 'Suomija';
$_['text_denmark']                      = 'Danija';
$_['text_germany']                      = 'Vokietija';
$_['text_netherlands']                  = 'Olandija';

// Entry
$_['entry_total']                       = 'Užsakymo suma';
$_['entry_fee']                         = 'Mokestis';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti Klarna mokesčio!';
