<?php 
/* 
  #file: admin/language/english/total/payment_typechg.php
  #tested: Opencart v1.5.1.3
  #powered by fabiom7 - fabiome77@hotmail.it - copyright fabiom7 2012
*/

// Heading
$_['heading_title']     = 'Apmokėjimo būdo pasirinkimo mokestis';

// Text
$_['text_total']        = 'Sąskaitų sudėtinės dalys';
$_['text_success']      = 'Atnaujinta!';
$_['text_edit']        = 'Koreguoti apmokėjimo būdo pasirinkimo mokestį';

// Entry
$_['entry_fix']         = 'Fiksuota suma:';
$_['entry_method']      = 'Apmokėjimas';
$_['entry_shipping']      = 'Pristatymas';
$_['entry_charge']      = 'Mokestis';
$_['entry_description'] = 'Aprašymas';
$_['entry_status']      = 'Būklė:';
$_['entry_sort_order']  = 'Rikiavimo tvarka:';

$_['help_charge']      = 'Nurodykite pradžioje % simbolį, jei norite kad būtų mokestis procentais. (%10 - 10%, %-10 - -10%)';

// Error
$_['error_permission']  = 'Neturite teisių redaguoti šio modulio!';