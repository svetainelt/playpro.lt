<?php

// Heading
$_['heading_title']                     = 'Tarpinė suma';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuota tarpinė suma!';
$_['text_edit']                         = 'Koreguoti tarpinę sumą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti tarpinės sumos!';
