<?php

// Heading
$_['heading_title']                     = 'Smulkaus užsakymo mokestis';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuotas smulkaus užsakymo mokestis!';
$_['text_edit']                         = 'Koreguoti smulkaus užsakymo mokestį';

// Entry
$_['entry_total']                       = 'Užsakymo suma';
$_['entry_fee']                         = 'Mokestis';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_total']                        = 'Atsiskaitymo suma turi pasiekti šią sumą, kol šis smulkaus užsakymo mokestis taps aktyvus.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti smulkaus užsakymo mokesčio!';
