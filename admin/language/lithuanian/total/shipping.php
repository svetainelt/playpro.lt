<?php

// Heading
$_['heading_title']                     = 'Pristatymas';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuotas pristatymas!';
$_['text_edit']                         = 'Koreguoti pristatymą';

// Entry
$_['entry_estimator']                   = 'Pristatymo įvertinimas';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti pristatymo!';
