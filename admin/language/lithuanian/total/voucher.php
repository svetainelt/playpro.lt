<?php

// Heading
$_['heading_title']                     = 'Dovanų čekiai';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuoti dovanų čekiai!';
$_['text_edit']                         = 'Koreguoti dovanų čekius';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti dovanų čekių!';
