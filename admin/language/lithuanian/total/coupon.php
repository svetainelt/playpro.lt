<?php

// Heading
$_['heading_title']                     = 'Nuolaidų kuponai';

// Text
$_['text_total']                        = 'Sąskaitų sudėtinės dalys';
$_['text_success']                      = 'Sėkmingai modifikuoti nuolaidų kuponai!';
$_['text_edit']                         = 'Koreguoti nuolaidų kuponą';

// Entry
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti nuolaidų kuponų!';
