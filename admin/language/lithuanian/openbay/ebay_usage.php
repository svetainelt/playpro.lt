<?php

// Heading
$_['heading_title']                     = 'Usage';

// Text
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_ebay']                         = 'eBay';
$_['text_usage']                        = 'Your account usage';

// Error
$_['error_ajax_load']                   = 'Sorry, could not get a response. Try later.';
