<?php

// Heading
$_['heading_title']                     = 'Stock updates';

// Text
$_['text_openbay']                      = 'OpenBay Pro';
$_['text_amazon']                       = 'Amazon US';
$_['text_empty']                        = 'No results!';

// Entry
$_['entry_date_start']                  = 'Date Start';
$_['entry_date_end']                    = 'Date End';

// Column
$_['column_ref']                        = 'Ref';
$_['column_date_requested']             = 'Date requested';
$_['column_date_updated']               = 'Date updated';
$_['column_status']                     = 'Būsena';
$_['column_sku']                        = 'Amazon SKU';
$_['column_stock']                      = 'Atsargos';
