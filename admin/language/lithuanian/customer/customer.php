<?php

// Heading
$_['heading_title']                     = 'Klientai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti klientai!';
$_['text_list']                         = 'Klientų sąrašas';
$_['text_add']                          = 'Pridėti klientą';
$_['text_edit']                         = 'Koreguoti klientą';
$_['text_default']                      = 'Numatytoji parinktis';
$_['text_balance']                      = 'Balansas';

// Column
$_['column_name']                       = 'Kliento vardas';
$_['column_email']                      = 'El. paštas';
$_['column_customer_group']             = 'Klientų grupė';
$_['column_status']                     = 'Būsena';
$_['column_date_added']                 = 'Data';
$_['column_comment']                    = 'Komentaras';
$_['column_description']                = 'Aprašymas';
$_['column_amount']                     = 'Suma';
$_['column_points']                     = 'Taškai';
$_['column_ip']                         = 'IP adresas';
$_['column_total']                      = 'Viso paskyrų';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_customer_group']              = 'Klientų grupė';
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_fax']                         = 'Faksas';
$_['entry_newsletter']                  = 'Naujienų prenumerata';
$_['entry_status']                      = 'Būsena';
$_['entry_approved']                    = 'Patvirtinta';
$_['entry_safe']                        = 'Saugus';
$_['entry_password']                    = 'Slaptažodis';
$_['entry_confirm']                     = 'Pakartokite slaptažodį';
$_['entry_company']                     = 'Įmonė';
$_['entry_address_1']                   = 'Adresas';
$_['entry_address_2']                   = 'Papildomas adresas';
$_['entry_city']                        = 'Miestas';
$_['entry_postcode']                    = 'Pašto kodas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Rajonas';
$_['entry_default']                     = 'Numatytasis adresas';
$_['entry_comment']                     = 'Komentaras';
$_['entry_description']                 = 'Aprašymas';
$_['entry_amount']                      = 'Kiekis';
$_['entry_points']                      = 'Taškai';
$_['entry_name']                        = 'Kliento vardas';
$_['entry_ip']                          = 'IP adresas';
$_['entry_date_added']                  = 'Data';

// Help
$_['help_safe']                         = 'Nustatykite į True, kad šis klientas išvengtų anti-fraud pagavimo.';
$_['help_points']                       = 'Naudokite minusą, kad pašalintumėte taškus.';

// Error
$_['error_warning']                     = 'Įspėjimas: Atidžiai patikrinkite formos duomenis dėl klaidų!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti klientų!';
$_['error_exists']                      = 'Įspėjimas: El. pašto adresas jau užregistruotas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas neteisingas!';
$_['error_telephone']                   = 'Telefonas turi būti nuo 3 iki 32 simbolių!';
$_['error_password']                    = 'Slaptažodis turi būti nuo 3 iki 20 simbolių ilgio!';
$_['error_confirm']                     = 'Naujas slaptažodis ir pakartotas slaptažodis nesutapo!';
$_['error_address_1']                   = 'Adresas turi būti nuo 3 iki 128 simbolių!';
$_['error_city']                        = 'Miestas turi būti nuo 2 iki 128 simbolių!';
$_['error_postcode']                    = 'Pašto kodo ilgis turi būti nuo 2 iki 10 simbolių!';
$_['error_country']                     = 'Pasirinkite šalį!';
$_['error_zone']                        = 'Pasirinkite rajoną!';
$_['error_custom_field']                = '%s reikalingas!';
