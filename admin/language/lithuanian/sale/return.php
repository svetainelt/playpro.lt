<?php

// Heading
$_['heading_title']                     = 'Produktų grąžinimai';

// Text
$_['text_success']                      = 'Sėkmingai modifikavote grąžinimus!';
$_['text_list']                         = 'Produktų grąžinimo sąrašas';
$_['text_add']                          = 'Pridėti produkto grąžinimą';
$_['text_edit']                         = 'Koreguoti produkto grąžinimą';
$_['text_opened']                       = 'Atidaryta';
$_['text_unopened']                     = 'Neatidaryta';
$_['text_order']                        = 'Užsakymo informacija';
$_['text_product']                      = 'Produkto informacija ir grąžinimo priežastis';
$_['text_history']                      = 'Pridėti grąžinimų istoriją';

// Column
$_['column_return_id']                  = 'Grąžinimo nr.';
$_['column_order_id']                   = 'Užsakymo nr.';
$_['column_customer']                   = 'Klientas';
$_['column_product']                    = 'Prekė';
$_['column_model']                      = 'Prekės kodas';
$_['column_status']                     = 'Būsena';
$_['column_date_added']                 = 'Data';
$_['column_date_modified']              = 'Modifikavimo data';
$_['column_comment']                    = 'Komentaras';
$_['column_notify']                     = 'Klientas informuotas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_customer']                    = 'Klientas';
$_['entry_order_id']                    = 'Užsakymo nr.';
$_['entry_date_ordered']                = 'Užsakymo data';
$_['entry_firstname']                   = 'Vardas';
$_['entry_lastname']                    = 'Pavardė';
$_['entry_email']                       = 'El. paštas';
$_['entry_telephone']                   = 'Telefono nr.';
$_['entry_product']                     = 'Prekė';
$_['entry_model']                       = 'Prekės kodas';
$_['entry_quantity']                    = 'Kiekis';
$_['entry_opened']                      = 'Pakuotė atidaryta';
$_['entry_comment']                     = 'Komentaras';
$_['entry_return_reason']               = 'Grąžinimo priežastis';
$_['entry_return_action']               = 'Grąžinimo veiksmas';
$_['entry_return_status']               = 'Grąžinimo būsena';
$_['entry_notify']                      = 'Informuoti klientą';
$_['entry_return_id']                   = 'Grąžinimo nr.';
$_['entry_date_added']                  = 'Data';
$_['entry_date_modified']               = 'Modifikavimo data';

// Help
$_['help_product']                      = 'Automatinis užbaigimas';

// Error
$_['error_warning']                     = 'Įspėjimas: Atidžiai patikrinkite formos duomenis dėl klaidų!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti grąžinimų!';
$_['error_order_id']                    = 'Užsakymo numeris reikalingas!';
$_['error_firstname']                   = 'Vardas turi būti nuo 1 iki 32 simbolių!';
$_['error_lastname']                    = 'Pavardė turi būti nuo 1 iki 32 simbolių!';
$_['error_email']                       = 'El. pašto adresas neteisingas!';
$_['error_telephone']                   = 'Telefonas turi būti nuo 3 iki 32 simbolių!';
$_['error_product']                     = 'Prekės pavadinimas turi būti nuo 3 iki 255 simbolių!';
$_['error_model']                       = 'Produkto modelis turi būti nuo 3 iki 64 simbolių!';
