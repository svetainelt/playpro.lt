<?php

// Heading
$_['heading_title']                     = 'Periodiniai atsiskaitymai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti periodiniai atsiskaitymai!';
$_['text_list']                         = 'Periodinių atsiskaitimų sąrašas';
$_['text_add']                          = 'Pridėti periodinį atsiskaitymą';
$_['text_edit']                         = 'Koreguoti periodinį atsiskaitymą';
$_['text_payment_profiles']             = 'Periodiniai atsiskaitymai';
$_['text_status_active']                = 'Aktyvus';
$_['text_status_inactive']              = 'Neaktyvus';
$_['text_status_cancelled']             = 'Atšauktas';
$_['text_status_suspended']             = 'Atidėtas';
$_['text_status_expired']               = 'Pasibaigęs';
$_['text_status_pending']               = 'Vykdomas';
$_['text_transactions']                 = 'Sandoriai';
$_['text_cancel_confirm']               = 'Profilio atšaukimas negali būti anuliuotas! Ar jūs tikrai norite tai padaryti?';
$_['text_transaction_date_added']       = 'Data';
$_['text_transaction_payment']          = 'Apmokėjimas';
$_['text_transaction_outstanding_payment'] = 'Neapmokėtas mokėjimas';
$_['text_transaction_skipped']          = 'Mokėjimas praleistas';
$_['text_transaction_failed']           = 'Mokėjimas nepavyko';
$_['text_transaction_cancelled']        = 'Atšauktas';
$_['text_transaction_suspended']        = 'Atidėtas';
$_['text_transaction_suspended_failed'] = 'Atidėtas iš nepavykusio apmokėjimo';
$_['text_transaction_outstanding_failed']='Neapmokėtas mokėjimas nepavyko';
$_['text_transaction_expired']          = 'Pasibaigęs';

// Entry
$_['entry_cancel_payment']              = 'Atšaukti apmokėjimą';
$_['entry_order_recurring']             = 'Nr.';
$_['entry_order_id']                    = 'Užsakymo nr.';
$_['entry_reference']                   = 'Mokėjimo rekomendacijos nuoroda';
$_['entry_customer']                    = 'Klientas';
$_['entry_date_added']                  = 'Data';
$_['entry_status']                      = 'Būsena';
$_['entry_type']                        = 'Tipas';
$_['entry_action']                      = 'Veiksmas';
$_['entry_email']                       = 'El. paštas';
$_['entry_description']                 = 'Periodinio atsiskaitymo aprašymas';
$_['entry_product']                     = 'Prekė';
$_['entry_quantity']                    = 'Kiekis';
$_['entry_amount']                      = 'Kiekis';
$_['entry_recurring']                   = 'Periodinis atsiskaitymas';
$_['entry_payment_method']              = 'Apmokėjimo metodas';

// Error
$_['error_not_cancelled']               = 'Klaida: %s';
$_['error_not_found']                   = 'Nepavyko atšaukti periodinio mokėjimo';

// Text
$_['text_cancelled']                    = 'Periodinis atsiskaitymas buvo atšauktas';
