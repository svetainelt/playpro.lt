<?php

// Heading
$_['heading_title']                     = 'Dovanų čekių temos';

// Text
$_['text_success']                      = 'Sėkmingai modifikuota dovanų čekių tema!';
$_['text_list']                         = 'Dovanų čekių temų sąrašas';
$_['text_add']                          = 'Pridėti dovanų čekio temą';
$_['text_edit']                         = 'Koreguoti dovanų čekio temą';

// Column
$_['column_name']                       = 'Temos pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Temos pavadinimas';
$_['entry_description']                 = 'Temos aprašymas';
$_['entry_image']                       = 'Paveikslėlis';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti dovanų čekių temų!';
$_['error_name']                        = 'Temos pavadinimo ilgis turi būti nuo 3 iki 32 simbolių!';
$_['error_image']                       = 'Paveikslėlis reikalingas!';
$_['error_voucher']                     = 'Įspėjimas: Šios temos negalite pašalinti, nes ji yra priskirta %s dovanų čekiams (-ių)!';
