<?php

// Heading
$_['heading_title']                     = 'Dovanų čekiai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti dovanų čekiai!';
$_['text_list']                         = 'Dovanų čekių sąrašas';
$_['text_add']                          = 'Pridėti dovanų čekį';
$_['text_edit']                         = 'Koreguoti dovanų čekį';
$_['text_sent']                         = 'Sėkmingai išsiųstas el. laiškas su dovanų čekiu!';

// Column
$_['column_name']                       = 'Dovanų čekio pavadinimas';
$_['column_code']                       = 'Kodas';
$_['column_from']                       = 'Nuo';
$_['column_to']                         = 'Kam';
$_['column_theme']                      = 'Tema';
$_['column_amount']                     = 'Suma';
$_['column_status']                     = 'Būsena';
$_['column_order_id']                   = 'Užsakymo nr.';
$_['column_customer']                   = 'Klientas';
$_['column_date_added']                 = 'Data';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_code']                        = 'Kodas';
$_['entry_from_name']                   = 'Siuntėjo vardas';
$_['entry_from_email']                  = 'Siuntėjo el. paštas';
$_['entry_to_name']                     = 'Gavėjo vardas';
$_['entry_to_email']                    = 'Gavėjo el. paštas';
$_['entry_theme']                       = 'Tema';
$_['entry_message']                     = 'Žinutė';
$_['entry_amount']                      = 'Suma';
$_['entry_status']                      = 'Būsena';

// Help
$_['help_code']                         = 'Kodas kurį klientas turi įvesti, kad aktyvuotų dovanų čekį.';

// Error
$_['error_selection']                   = 'Įspėjimas: Nepasirinktas dovanų čekis!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti dovanų čekių!';
$_['error_exists']                      = 'Įspėjimas: Dovanų čekio kodas jau naudojamas!';
$_['error_code']                        = 'Kodo ilgis turi būti nuo 3 iki 10 simbolių!';
$_['error_to_name']                     = 'Gavėjo vardo ilgis turi būti nuo 1 ir 64 simbolių!';
$_['error_from_name']                   = 'Jūsų vardo ilgis turi būti nuo 1 ir 64 simbolių!';
$_['error_email']                       = 'El. pašto adresas neteisingas!';
$_['error_amount']                      = 'Kiekis turi būti didesnis arba lygus 1!';
$_['error_order']                       = 'Įspėjimas: Šis dovanų čekis negali būti pašalintas, nes yra dalis <a href="%s">užsakymo</a>!';
