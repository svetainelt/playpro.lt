<?php

// Heading
$_['heading_title']                     = 'Naujausi užsakymai';

// Column
$_['column_order_id']                   = 'Užsakymo nr.';
$_['column_customer']                   = 'Klientas';
$_['column_status']                     = 'Būsena';
$_['column_total']                      = 'Iš viso';
$_['column_date_added']                 = 'Data';
$_['column_action']                     = 'Veiksmas';
