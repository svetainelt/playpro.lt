<?php

// Heading
$_['heading_title']                     = 'Pardavimų analizė';

// Text
$_['text_order']                        = 'Užsakymai';
$_['text_customer']                     = 'Klientai';
$_['text_day']                          = 'Šiandien';
$_['text_week']                         = 'Savaitė';
$_['text_month']                        = 'Mėnuo';
$_['text_year']                         = 'Metai';
