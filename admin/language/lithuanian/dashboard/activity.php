<?php

// Heading
$_['heading_title']                     = 'Naujausia veikla';

// Text
$_['text_customer_address_add']         = '<a href="customer_id=%d">%s</a> pridėjo naują adresą.';
$_['text_customer_address_edit']        = '<a href="customer_id=%d">%s</a> atnaujino adresą.';
$_['text_customer_address_delete']      = '<a href="customer_id=%d">%s</a> pašalino vieną iš savo adresų.';
$_['text_customer_edit']                = '<a href="customer_id=%d">%s</a> atnaujino savo paskyros informaciją.';
$_['text_customer_forgotten']           = '<a href="customer_id=%d">%s</a> paprašė naujo slaptažodžio.';
$_['text_customer_login']               = '<a href="customer_id=%d">%s</a> prisijungė.';
$_['text_customer_password']            = '<a href="customer_id=%d">%s</a> atnaujino savo paskyros slaptažodį.';
$_['text_customer_register']            = '<a href="customer_id=%d">%s</a> užsiregistravo paskyrą.';
$_['text_customer_return_account']      = '<a href="customer_id=%d">%s</a> patvirtino produkto <a href="return_id=%d">grąžinimą</a>.';
$_['text_customer_return_guest']        = '%s patvirtino produkto <a href="return_id=%d">grąžinimą</a>.';
$_['text_customer_order_account']       = '<a href="customer_id=%d">%s</a> pridėjo <a href="order_id=%d">naują užsakymą</a>.';
$_['text_customer_order_guest']         = '%s sukūrė <a href="order_id=%d">naują užsakymą</a>.';
$_['text_affiliate_edit']               = '<a href="affiliate_id=%d">%s</a> atnaujino savo paskyros informaciją.';
$_['text_affiliate_forgotten']          = '<a href="affiliate_id=%d">%s</a> paprašė naujo slaptažodžio.';
$_['text_affiliate_login']              = '<a href="affiliate_id=%d">%s</a> prisijungė.';
$_['text_affiliate_password']           = '<a href="affiliate_id=%d">%s</a> atnaujino savo paskyros slaptažodį.';
$_['text_affiliate_payment']            = '<a href="affiliate_id=%d">%s</a> atnaujino savo mokėjimo informaciją.';
$_['text_affiliate_register']           = '<a href="affiliate_id=%d">%s</a> užsiregistravo paskyrą.';
