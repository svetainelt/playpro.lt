<?php

// Heading
$_['heading_title']                     = 'Šalys';

// Text
$_['text_success']                      = 'Jūs sėkmingai modifikavote šalis!';
$_['text_list']                         = 'Šalių sąrašas';
$_['text_add']                          = 'Pridėti šalį';
$_['text_edit']                         = 'Koreguoti šalį';

// Column
$_['column_name']                       = 'Šalies pavadinimas';
$_['column_iso_code_2']                 = 'ISO kodas (2)';
$_['column_iso_code_3']                 = 'ISO kodas (3)';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Šalies pavadinimas';
$_['entry_iso_code_2']                  = 'ISO kodas (2)';
$_['entry_iso_code_3']                  = 'ISO kodas (3)';
$_['entry_address_format']              = 'Adreso formatas';
$_['entry_postcode_required']           = 'Pašto kodas privalomas';
$_['entry_status']                      = 'Būsena';

// Help
$_['help_address_format']               = 'Vardas = {firstname}<br />Pavardė = {lastname}<br />Įmonė = {company}<br />Adresas = {address_1}<br />Papildomas adresas = {address_2}<br />Miestas = {city}<br />Pašto indeksas = {postcode}<br />Rajonas = {zone}<br />Regiono kodas = {zone_code}<br />Šalis = {country}';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti šalis!';
$_['error_name']                        = 'Šalies pavadinimas turi būti nuo 3 iki 128 simbolių!';
$_['error_default']                     = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta kaip numatytoji parduotuvės šalis!';
$_['error_store']                       = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta %s parduotuvei!';
$_['error_address']                     = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta %s adresui!';
$_['error_affiliate']                   = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta %s partneriams!';
$_['error_zone']                        = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta %s zonai!';
$_['error_zone_to_geo_zone']            = 'Įspėjimas: Ši šalis negali būti pašalinta, nes ji priskirta %s zonoms/geo zonoms!';
