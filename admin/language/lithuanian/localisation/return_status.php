<?php

// Heading
$_['heading_title']                     = 'Grąžinimo būsena';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos grąžinimo būsenos!';
$_['text_list']                         = 'Grąžinimo būsenų sąrašas';
$_['text_add']                          = 'Pridėti grąžinimo būseną';
$_['text_edit']                         = 'Koreguoti grąžinimo būseną';

// Column
$_['column_name']                       = 'Grąžinimo būsenos pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Grąžinimo būsenos pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti grąžinimo būsenų!';
$_['error_name']                        = 'Grąžinimo būsenos pavadinimo ilgis turi būti nuo 3 iki 32 simbolių!';
$_['error_default']                     = 'Įspėjimas: Ši būsena negali būti pašalinta, nes ji yra numatytoji būsena!';
$_['error_return']                      = 'Įspėjimas: Ši būsena negali būti pašalinta, nes ji yra priskirta grąžinimams (%s)!';
