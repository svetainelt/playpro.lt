<?php

// Heading
$_['heading_title']                     = 'Geo Zonos';

// Text
$_['text_success']                      = 'Jūs sėkmingai modifikavote geo zonas!';
$_['text_list']                         = 'Geo zonų sąrašas';
$_['text_add']                          = 'Pridėti geo zoną';
$_['text_edit']                         = 'Koreguoti geo zoną';

// Column
$_['column_name']                       = 'Geo zonos pavadinimas';
$_['column_description']                = 'Aprašymas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Geo zonos pavadinimas';
$_['entry_description']                 = 'Aprašymas';
$_['entry_country']                     = 'Šalis';
$_['entry_zone']                        = 'Zona';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti geo zonas!';
$_['error_name']                        = 'Geo zonos pavadinimas turi būti nuo 3 iki 32 simbolių ilgio!';
$_['error_description']                 = 'Aprašymo pavadinimas turi būti nuo 3 iki 255 simbolių ilgio!';
$_['error_tax_rate']                    = 'Įspėjimas: Ši geo zona negali būti pašalinta, nes ji priskirta vienam ar keliems mokesčiams!';
