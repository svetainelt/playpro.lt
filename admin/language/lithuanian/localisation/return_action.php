<?php

// Heading
$_['heading_title']                     = 'Grąžinimo veiksmai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti grąžinimo veiksmai!';
$_['text_list']                         = 'Grąžinimo veiksmų sąrašas';
$_['text_add']                          = 'Pridėti grąžinimo veiksmą';
$_['text_edit']                         = 'Koreguoti grąžinimo veiksmą';

// Column
$_['column_name']                       = 'Grąžinimo veiksmo pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Grąžinimo veiksmo pavadinimas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti grąžinimo veiksmų!';
$_['error_name']                        = 'Grąžinimo veiksmo pavadinimo ilgis turi būti nuo 3 iki 32 simboliių!';
$_['error_return']                      = 'Įspėjimas: Šis grąžinimo veiksmas negali būti pašalintas, nes jis yra priskirtas grąžintiems produktams (%s)!';
