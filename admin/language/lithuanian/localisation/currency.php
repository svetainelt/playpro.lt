<?php

// Heading
$_['heading_title']                     = 'Valiutos';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos valiutos!';
$_['text_list']                         = 'Valiutų sąrašas';
$_['text_add']                          = 'Pridėti valiutą';
$_['text_edit']                         = 'Koreguoti valiutą';

// Column
$_['column_title']                      = 'Valiutos pavadinimas';
$_['column_code']                       = 'Kodas';
$_['column_value']                      = 'Reikšmė';
$_['column_date_modified']              = 'Paskutinį kartą atnaujinta';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_title']                       = 'Valiutos pavadinimas';
$_['entry_code']                        = 'Kodas';
$_['entry_value']                       = 'Reikšmė';
$_['entry_symbol_left']                 = 'Simbolis iš kairės';
$_['entry_symbol_right']                = 'Simbolis iš dešinės';
$_['entry_decimal_place']               = 'Pozicijos po kablelio';
$_['entry_status']                      = 'Būsena';

// Help
$_['help_code']                         = 'Nekeiskite, jei tai numatytoji valiuta. Turi galioti <a href="http://www.xe.com/iso4217.php" target="_blank">ISO kodas</a>.';
$_['help_value']                        = 'Įrašykite 1.00000, jei tai numatytoji valiuta.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti valiutas!';
$_['error_title']                       = 'Valiutos pavadinimas turi būti nuo 3 iki 32 simbolių ilgio!';
$_['error_code']                        = 'Valiutos kodas turi būti 3 simbolių ilgio!';
$_['error_default']                     = 'Įspėjimas: Šios valiutos negalima pašalinti, nes ji priskirta kaip numatytoji parduotuvės valiuta!';
$_['error_store']                       = 'Įspėjimas: Šios valiutos negalima pašalinti, nes ji priskirta %s parduotuvei!';
$_['error_order']                       = 'Įspėjimas: Šios valiutos negalima pašalinti, nes ji priskirta %s užsakymams!';
