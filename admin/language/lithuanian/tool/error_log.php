<?php

// Heading
$_['heading_title']                     = 'Klaidų žurnalas';

// Text
$_['text_success']                      = 'Sėkmingai išvalytas klaidų žurnalas!';
$_['text_list']                         = 'Klaidų sąrašas';

// Error
$_['error_warning']                     = 'Įspėjimas: Jūsų klaidų žurnalo failas %s yra %s!';
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės išvalyti klaidų žurnalo! ';
