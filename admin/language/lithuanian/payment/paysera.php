<?php
// Heading
$_['heading_title']                 = 'Paysera';

// Text
$_['text_payment']                  = 'Mokėjimai';
$_['text_success']                  = 'Sėkmingai modifikuotas paysera mokėjimo modulis!';
$_['text_on']                       = 'Įjungtas';
$_['text_off']                      = 'Išjungtas';
$_['text_edit']                     = 'Redaguoti Paysera mokėjimą';

// Entry
$_['entry_project']                 = '<span data-toggle="tooltip" title="Unikalus projekto numeris">Projekto ID:</span>';
$_['entry_sign']                    = '<span data-toggle="tooltip" title="Jį galite rasti prisijungę prie Paysera sistemos su savo naudotojo duomenimis, išsirinkus „paslaugų valdymas“ ir prie konkretaus projekto pasirinkus „bendri nustatymai“">Projekto slaptažodis:</span>';
$_['entry_lang']                    = 'Vartotojo kalba:';
$_['help_lang']                     = 'Pasirinkite kalbą, kuria bus atvaizduojami mokėjimo metodai, bei šalys';
$_['entry_test']                    = '<span data-toggle="tooltip" title="Įjunkite kol paysera nepatvirtino jūsų projekto ir išjunkite, kai projektas patvirtintas">Testinis režimas:</span>';
$_['entry_new_order_status']        = 'Naujo užsakymo statusas:';
$_['entry_order_status']            = 'Užsakymo būsena po apmokėjimo:';
$_['entry_payment_lang']            = 'Pirminė mokėjimo būdų šalis:';
$_['entry_geo_zone']                = 'Geo zona:';
$_['entry_status']                  = 'Būsena:';
$_['entry_sort_order']              = 'Rikiavimo tvarka:';
$_['entry_paymentlist']             = 'Rodyti mokėjimo būdų sąrašą:';

// Custom START
$_['tab_filter_banks']              = 'Bankų pasirinkimas';
$_['text_paysera']                  = '<a href="https://www.paysera.lt" target="_blank"><img src="https://www.paysera.com/payment/m/m_images/wfiles/i1xv7h2787.png" alt="Paysera" title="Paysera" height="20px" /></a>';
$_['entry_filter_methods']          = 'Filtruoti mokėjimo būdus pagal žemiau esančius pažymėtus pasirinkimus?';
$_['text_select_country']           = '-- Pasirinkite šalį --';
$_['entry_select_countries']        = 'Pasirinkite šalį ir tada sužymėkite mokėjimo būdus, kuriuos norite, kad matytų pirkėjas:';
$_['entry_select_methods']          = 'Sužymėkite mokėjimo būdus, pasirinktai šaliai:';
$_['entry_selected_countries']      = 'Pažymėkite šalis, kurių mokėjimo būdus bus galima matyti pirkėjui:';
$_['button_check_all_methods']      = 'Pažymėti visus metodus';
$_['button_uncheck_all_methods']    = 'Atžymėti visus metodus';
// Custom END

// Help
$_['help_callback']                 = '';

// Error
$_['error_permission']              = 'Įspėjimas: Jūs neturite teisių modifikuoti paysera mokėjimo modulio!';
$_['error_project']                 = 'Projekto ID būtinas!';
$_['error_sign']                    = 'Projekto slaptažodis būtinas!';
$_['error_lang']                    = 'Kalbos kodas būtinas!';
$_['error_refresh']                 = 'Nepavyko atnaujinti!';