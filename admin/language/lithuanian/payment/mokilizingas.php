<?php

// Heading
$_['heading_title']			= 'Išsimokėtinai su Inbank | mokilizingas';

// Text
$_['text_live']	= 'Live rėžimas';
$_['text_testing']	= 'Bandomasis rėžimas';
$_['text_leasing']	= 'Išsimokėtinai su Inbank | mokilizingas';
$_['text_payment']	= 'Inbank | mokilizingas';
$_['text_admin_info']	= 'Inbank | mokilizingas atsakymai';
$_['text_extension']		= 'Plėtinys';
$_['text_success']			= 'Sėkmingai: atnaujinote Inbank | mokilizingas modulio nustatymus!';
$_['text_edit']             = 'Keisti';
$_['text_install']             = 'Įdiegti';
$_['text_mokilizingas']		= '<a href="#" target="_blank"><img src="view/image/payment/mokilizingas.png" alt="Inbank | mokilizingas" title="Inbank | mokilizingas" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_na']				= 'N/A';
$_['text_success_action']	= 'Sėkmingai';
$_['text_error_generic']	= 'Klaida: Pastebėjome klaidų. Patikrinkite klaidų žurnalą.';
$_['text_column_id']	= 'ID';
$_['text_column_stamp']	= 'Stamp';
$_['text_column_id_order']	= 'Id užsakymo';
$_['text_column_sessionId']	= 'Sesijos ID';
$_['text_column_errorMessage']	= 'Klaidos žinutė';
$_['text_column_errorCode']	= 'Klaidos kodas';
$_['text_column_date_add']	= 'Sukurta';
$_['text_column_date_upd']	= 'Atnaujinta';
$_['text_column_resultCode']	= 'Kodas';
$_['text_column_resultInfo']	= 'Info';
$_['text_column_resultMsg']	= 'Žinutė';
$_['text_column_advance']	= 'Avansas';
$_['text_column_currency']	= 'Valiuta';
$_['text_column_contractNo']	= 'Sutarties Nr.';
$_['text_check_response']	= 'Patikrinti lizingo būseną';

// Placeholders
$_['text_placeholder_user']	= 'Įveskite vartotoją';
$_['text_placeholder_key']	= 'Įveskite API raktą skaičiuoklei';
$_['text_placeholder_password']	= 'Įveskite API slaptažodį';
$_['text_placeholder_minimal_price']	= 'Įveskite minimalią lizingo kainą';
$_['text_placeholder_phone']	= 'Kontaktinis telefonas';
$_['text_placeholder_email']	= 'El.paštas';
$_['text_placeholder_term']	= 'Standartinė lizingo trukmė (6,10,12,24)';
$_['text_placeholder_logo_url']	= 'Logotipo URL adresas';

// Settings
$_['settings_mode']	= 'API rėžimas';
$_['settings_user']	= 'API vartotojas';
$_['settings_password']	= 'API slaptažodis';
$_['settings_key']	= 'API raktas';
$_['settings_minimal_price']	= 'Minimali lizingo kaina';
$_['settings_phone']	= 'Telefonas';
$_['settings_email']	= 'El.paštas';
$_['settings_calculator']	= 'Atvaizduoti skaičiuoklę prekės puslapyje';
$_['settings_calculator_style']	= 'Skaičiuoklės šablonas';
$_['settings_term']	= 'Standartinis lizingo terminas';
$_['settings_logo_url']	= 'Logotipo URL adresas';

// Entry
$_['entry_total']			= 'Viso';
$_['waiting_order_status']	= 'Laukiamas patvirtinimas';
$_['accepted_order_status']	= 'Lizingas suteiktas';
$_['canceled_order_status']	= 'Lizingas nepatvirtintas';
$_['entry_geo_zone']		= 'Zona';
$_['entry_status']			= 'Būsena';
$_['entry_sort_order']		= 'Rikiavimas';
$_['entry_key']				= 'Key';
$_['entry_secret']			= 'Secret';
$_['entry_debug']			= 'Debug';

// Help
$_['help_debug']			= 'Aktyvuokite "debug" rėžimą ir kaupkite sistemos klaidas. Tai padeda identifikuoti problemas.';

// Error
$_['error_mode']			= 'Pasirinkite API rėžimą';
$_['error_user']			= 'Įveskite API vartotoją';
$_['error_password']		= 'Įveskite API slaptažodį';
$_['error_key']		        = 'Įveskite API raktą';
$_['error_minimal_price']  = 'Įveskite minimalią kainą';
$_['error_max_price']  = 'Įveskite maksimalią kainą';
$_['error_minimal_cart']   = 'Įveskite minimalią krepšelio kainą';
$_['error_phone']           = 'Įveskite kontaktinį telefoną';
$_['error_email']           = 'Įveskite kontaktinį el.paštą';
$_['error_logo_url']           = 'Įveskite logotipo adresą';
$_['error_php_version']		= 'Minimali PHP versija > 5.6.';
$_['error_permission']		= 'Neturite teisių koreguoti Inbank | mokilizingas modulį!';
$_['error_warning']			= 'Dėmesio: peržiūrėkite klaidas!';
$_['error_term']           = 'Įveskite standartinį lizingo terminą';
$_['error_payment_name'] = 'Įveskite mokėjimo pavadinimą. Pavyzdžiui: Išsimokėtinai su Inbank | mokilizingas';

?>