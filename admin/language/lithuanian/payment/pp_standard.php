<?php
// Heading
$_['heading_title']					 = 'PayPal Payments Standard';

// Text
$_['text_payment']					 = 'Mokėjimas';
$_['text_success']					 = 'Sėkmingai modifikuotas PayPal mokėjimo modulis!';
$_['text_edit']                      = 'Koreguoti PayPal';
$_['text_pp_standard']				 = '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Payments Standard" title="PayPal Payments Standard" /></a>';
$_['text_authorization']			 = 'Authorization';
$_['text_sale']						 = 'Sale'; 

// Entry
$_['entry_email']					 = 'El. paštas';
$_['entry_test']					 = 'Testinis režimas';
$_['entry_transaction']				 = 'Pervedimo būdas';
$_['entry_debug']					 = 'Sisteminis žurnalas';
$_['entry_total']					 = 'Suma';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status';
$_['entry_completed_status']		 = 'Completed Status';
$_['entry_denied_status']			 = 'Denied Status';
$_['entry_expired_status']			 = 'Expired Status';
$_['entry_failed_status']			 = 'Failed Status';
$_['entry_pending_status']			 = 'Pending Status';
$_['entry_processed_status']		 = 'Processed Status';
$_['entry_refunded_status']			 = 'Refunded Status';
$_['entry_reversed_status']			 = 'Reversed Status';
$_['entry_voided_status']			 = 'Voided Status';
$_['entry_geo_zone']				 = 'Geo Zona';
$_['entry_status']					 = 'Būsena';
$_['entry_sort_order']				 = 'Rikiavimo eiliškumas';

// Tab
$_['tab_general']					 = 'Bendra';
$_['tab_order_status']       		 = 'Užsakymų būsenos';

// Help
$_['help_test']						 = 'Ar naudoti testinį režimą, patikrinti ar veikia PayPal? Įjungus testinį režimą pinigai nebus nuskaičiuojami.';
$_['help_debug']			    	 = 'Bus įrašoma papildoma informacija į sisteminį žurnalą, tam, kad rasti klaidas, dėl kurių galėtų neveikti PayPal mokėjimo modulis.';
$_['help_total']					 = 'Minimali suma, kurią pasiekus, šis mokėjimo metodas taps aktyvus.';

// Error
$_['error_permission']				 = 'Įspėjimas: Jūs neturite teisių modifikuoti PayPal mokėjimo modulio!';
$_['error_email']					 = 'El. paštas reikalingas!';