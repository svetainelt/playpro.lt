<?php

// Heading
$_['heading_title']                     = 'Mokėjimas grynaisiais';

// Text
$_['text_payment']                      = 'Mokėjimas';
$_['text_success']                      = 'Sėkmingai modifikuotas mokėjimo grynaisiais mokėjimo modulis!';
$_['text_edit']                         = 'Koreguoti mokėjimą grynaisiais';

// Entry
$_['entry_total']                       = 'Suma';
$_['entry_total_max']                       = 'Maksimali suma';
$_['entry_order_status']                = 'Užsakymo būsena';
$_['entry_geo_zone']                    = 'Geo Zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_total']                        = 'Minimali suma, kurią pasiekus, šis mokėjimo metodas taps aktyvus.';
$_['help_total_max']                        = 'Maksimali suma, kurią pasiekus, šis mokėjimo metodas taps neaktyvus.';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti mokėjimo grynaisiais mokėjimo modulio!';
