<?php

// Heading
$_['heading_title']                     = 'Filtras';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas filtrų modulis!';
$_['text_edit']                         = 'Koreguoti filtro modulį';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti filtro modulio!';
