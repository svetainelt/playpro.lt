<?php

// Heading
$_['heading_title']                     = 'Baneriai';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas banerių modulis!';
$_['text_edit']                         = 'Koreguoti banerių modulį';

// Entry
$_['entry_name']                        = 'Modulio pavadinimas';
$_['entry_banner']                      = 'Baneris';
$_['entry_dimension']                   = 'Matmenys (P x A) ir transformavimo būdas';
$_['entry_width']                       = 'Plotis';
$_['entry_height']                      = 'Aukštis';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti banerių modulio!';
$_['error_name']                        = 'Modulio pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_width']                       = 'Plotis reikalingas!';
$_['error_height']                      = 'Aukštis reikalingas!';
