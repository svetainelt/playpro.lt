<?php

// Heading
$_['heading_title']                     = 'HTML turinys';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas HTML turinio modulis!';
$_['text_edit']                         = 'Koreguoti HTML turinio modulį';

// Entry
$_['entry_name']                        = 'Modulio pavadinimas';
$_['entry_title']                       = 'Eilutės pavadinimas';
$_['entry_description']                 = 'Turinys';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti HTML turinio modulio!';
$_['error_name']                        = 'Modulio pavadinimas turi būti nuo 3 iki 64 simbolių!';
