<?php

// Heading
$_['heading_title']                     = 'Journal2 temos spalvų ir šriftų keitimas';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas Journal2 temos keitimo modulis!';
$_['text_edit']                         = 'Keisti Journal2 temos spalvas / šriftus';
$_['text_from']                         = 'Pradinė reikšmė, iš kurios bus keičiama';
$_['text_to']                           = 'Galutinė reikšmė, į kurią bus pakeista';
$_['text_choose_font']                  = 'Pasirinkite šriftą';
$_['text_swap']                         = 'Sukeisti vietomis';

// Entry
$_['entry_color']                       = 'Spalvos kodas (RGB)';
$_['entry_google_font']                 = 'GOOGLE šrifto pavadinimas';
$_['entry_system_font']                 = 'SYSTEM šrifto pavadinimas';
$_['entry_color_placeholder']           = 'pvz.: rgb(xx,xx,xx)';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti Journal2 temos keitimo modulio!';
$_['error_color']                       = 'Blogai įvestas spalvos kodas';
$_['error_rgb_code']                    = 'Blogas įvestas spalvos RGB kodas';
$_['error_one_empty']                   = 'Viena iš keičiamų reikšmių iš/į nėra nurodyta.';