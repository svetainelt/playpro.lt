<?php

// Heading
$_['heading_title']                     = 'Parduotuvė';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas parduotuvės modulis!';
$_['text_edit']                         = 'Koreguoti parduotuvės modulį';

// Entry
$_['entry_admin']                       = 'Tik administratoriams';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti parduotuvės modulio!';
