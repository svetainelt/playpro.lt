<?php

// Heading
$_['heading_title']                     = 'Modulių užslėpimas';

// Text
$_['text_module']                       = 'Moduliai';
$_['text_success']                      = 'Sėkmingai modifikuotas parduotuvės modulis!';
$_['text_edit']                         = 'Koreguoti modulių užslėpimo modulį';
$_['text_check_to_hide']                = 'Sužymėkite menu punktus, kuriuos norite užslėpti';
$_['text_check_to_show']                = 'Sužymėkite modulius, kuriuos norite rodyti';

// Tabs
$_['tab_general']                       = 'Bendri nustatymai';
$_['tab_module']                        = 'Moduliai';
$_['tab_payment']                       = 'Mokėjimai';
$_['tab_shipping']                      = 'Pristatymai';
$_['tab_total']                         = 'Sąskaitų sudėtinės dalys';
$_['tab_menu']                          = 'Meniu juosta';

// Entry
$_['entry_status']                      = 'Modulių slėpimo būsena';
$_['entry_style_status']                = 'Stiliaus slėpimo būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti modulių užslėpimo modulio!';
