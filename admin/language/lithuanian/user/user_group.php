<?php

// Heading
$_['heading_title']                     = 'Vartotojų grupės';

// Text
$_['text_success']                      = 'Sėkmingai modifikuota vartotojų grupė!';
$_['text_list']                         = 'Vartotojų grupių sarašas';
$_['text_add']                          = 'Pridėti vartotojų grupę';
$_['text_edit']                         = 'Koreguoti vartotojų grupę';

// Column
$_['column_name']                       = 'Vartotojų grupės pavadinimas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Vartotojų grupės pavadinimas';
$_['entry_access']                      = 'Prieigos teisės';
$_['entry_modify']                      = 'Redagavimo teisės';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti vartotojų grupių!';
$_['error_name']                        = 'Vartotojų grupės pavadinimas turi būti nuo 3 iki 64 simbolių ilgio!';
$_['error_user']                        = 'Įspėjimas: Ši vartotojų grupė negali būti pašalinta, nes ji yra priskirta %s vartotojams!';
