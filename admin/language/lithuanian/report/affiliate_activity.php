<?php

// Heading
$_['heading_title']                     = 'Partnerystės aktyvumo ataskaita';

// Text
$_['text_list']                         = 'Partnerystės aktyvumo sąrašas';
$_['text_edit']                         = '<a href="affiliate_id=%d">%s</a> atnaujino savo paskyros informaciją.';
$_['text_forgotten']                    = '<a href="affiliate_id=%d">%s</a> paprašė naujo slaptažodžio.';
$_['text_login']                        = '<a href="affiliate_id=%d">%s</a> prisijungė.';
$_['text_password']                     = '<a href="affiliate_id=%d">%s</a> atnaujino savo paskyros slaptažodį.';
$_['text_payment']                      = '<a href="affiliate_id=%d">%s</a> atnaujino savo mokėjimo informaciją.';
$_['text_register']                     = '<a href="affiliate_id=%d">%s</a> užsiregistravo paskyrą.';

// Column
$_['column_affiliate']                  = 'Partneris';
$_['column_comment']                    = 'Komentaras';
$_['column_ip']                         = 'IP adresas';
$_['column_date_added']                 = 'Data';

// Entry
$_['entry_affiliate']                   = 'Partneris';
$_['entry_ip']                          = 'IP adresas';
$_['entry_date_start']                  = 'Pradžios data';
$_['entry_date_end']                    = 'Pabaigos data';
