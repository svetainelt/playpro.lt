<?php

// Heading
$_['heading_title']                     = 'Klientų aktyvumo ataskaita';

// Text
$_['text_list']                         = 'Klientų aktyvumo sąrašas';
$_['text_address_add']                  = '<a href="customer_id=%d">%s</a> pridėjo naują adresą.';
$_['text_address_edit']                 = '<a href="customer_id=%d">%s</a> atnaujino adresą.';
$_['text_address_delete']               = '<a href="customer_id=%d">%s</a> pašalino vieną iš savo adresų.';
$_['text_edit']                         = '<a href="customer_id=%d">%s</a> atnaujino savo paskyros informaciją.';
$_['text_forgotten']                    = '<a href="customer_id=%d">%s</a> paprašė naujo slaptažodžio.';
$_['text_login']                        = '<a href="customer_id=%d">%s</a> prisijungė.';
$_['text_password']                     = '<a href="customer_id=%d">%s</a> atnaujino savo paskyros slaptažodį.';
$_['text_register']                     = '<a href="customer_id=%d">%s</a> užsiregistravo paskyrą.';
$_['text_return_account']               = '<a href="customer_id=%d">%s</a> pateikė produkto grąžinimą.';
$_['text_return_guest']                 = '%s pateikė produkto grąžinimą.';
$_['text_order_account']                = '<a href="customer_id=%d">%s</a> sukūrė <a href="order_id=%d">naują užsakymą</a>.';
$_['text_order_guest']                  = '%s sukūrė <a href="order_id=%d">naują užsakymą</a>.';

// Column
$_['column_customer']                   = 'Klientas';
$_['column_comment']                    = 'Komentaras';
$_['column_ip']                         = 'IP adresas';
$_['column_date_added']                 = 'Data';

// Entry
$_['entry_customer']                    = 'Klientas';
$_['entry_ip']                          = 'IP adresas';
$_['entry_date_start']                  = 'Pradžios data';
$_['entry_date_end']                    = 'Pabaigos data';
