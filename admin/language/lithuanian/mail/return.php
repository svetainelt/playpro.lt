<?php

// Text
$_['text_subject']                      = '%s - grąžinimo atnaujinimas %s';
$_['text_return_id']                    = 'Grąžinimo nr.:';
$_['text_date_added']                   = 'Data:';
$_['text_return_status']                = 'Jūsų grąžinimas buvo atnaujintas į šią būseną:';
$_['text_comment']                      = 'Jūsų grąžinimo komentarai:';
$_['text_footer']                       = 'Prašome atsakyti į šį laišką, jei turite kokių nors klausimų ar neaiškumų.';
