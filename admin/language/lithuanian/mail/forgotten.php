<?php

// Text
$_['text_subject']                      = '%s - slaptažodio atūrimo prašymas';
$_['text_greeting']                     = 'Buvo pasinaudota slaptažodžio atkūrimo paslauga (%s).';
$_['text_change']                       = 'Kad atkurtumėte savo slaptažodį, paspauskite žemiau esančią nuorodą:';
$_['text_ip']                           = 'Užklausa buvo gauta iš šio IP adreso: %s';
