<?php

// Heading
$_['heading_title']                     = 'Pristatymo kaina pagal svorį';

// Text
$_['text_shipping']                     = 'Pristatymas';
$_['text_success']                      = 'Success: You have modified weight based shipping!';
$_['text_edit']                         = 'Edit Weight Based Shipping';

// Entry
$_['entry_rate']                        = 'Rates:<br /><span class="help">Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..</span>';
$_['entry_tax_class']                   = 'Mokesčių klasė';
$_['entry_geo_zone']                    = 'Geo zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Help
$_['help_rate']                         = 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';

// Error
$_['error_permission']                  = 'Warning: You do not have permission to modify weight based shipping!';
