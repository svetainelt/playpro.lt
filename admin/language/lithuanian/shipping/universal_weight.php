<?php
// Heading
$_['heading_title']    = 'Universalus pristatymo modulis';

// Text
$_['text_shipping']    = 'Pristatymo moduliai';
$_['text_success']     = 'Sėkmingai atnaujintas universalus pristatymo modulis!';
$_['text_edit']        = 'Koreguoti universalų pristatymo modulį';
$_['text_all_zones']   = 'Visos zonos';

$_['button_rule_add']  = 'Pridėti pristatymą';

// Entry
$_['entry_tax_class']  = 'Mokesčiai';
$_['entry_geo_zone']   = 'Geozona';
$_['entry_status']     = 'Būsena';
$_['entry_sort_order'] = 'Rikiavimo tvarka';
$_['entry_title']      = 'Pristatymo pavadinimas';
$_['entry_flat_cost']  = '<span data-toggle="tooltip" data-original-title="Kaina be PVM">Kaina</span>';
$_['entry_min_price']  = '<span data-toggle="tooltip" data-original-title="Kaina su PVM, nuo kurios įsigalios šis pristatymo metodas. Įveskite 0, kad neriboti kainos.">Min krepšelio kaina</span>';
$_['entry_max_price']  = '<span data-toggle="tooltip" data-original-title="Kaina su PVM, iki kurios įsigalios šis pristatymo metodas. Įveskite * , kad neriboti kainos.">Max krepšelio kaina</span>';
$_['entry_min_weight'] = '<span data-toggle="tooltip" data-original-title="Įveskite 0, kad neriboti svorio.">Min svoris</span>';
$_['entry_max_weight'] = '<span data-toggle="tooltip" data-original-title="Įveskite * , kad neriboti svorio.">Max svoris</span>';
$_['entry_action']     = 'Veiksmai';

// Error
$_['error_permission'] = 'Įspėjimas: Jūs neturite teisių modifikuoti Universalaus pristatymo modulio!';