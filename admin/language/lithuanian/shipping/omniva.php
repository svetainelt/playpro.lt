<?php
// Heading
$_['heading_title']    = 'Ⓟ Omniva';

// Text
$_['text_shipping']       = 'Pristatymas';
$_['text_success']        = 'Sėkmingai modifikuotas Omniva pristatymo modulis!';
$_['text_module_version'] = 'Versija';
$_['text_select_zone']    = '-- Pasirinkite geozoną --';
$_['text_each_product']   = 'produktui';
$_['text_all_cart']       = 'užsakymui';
$_['text_edit']           = 'Redaguoti Omniva pristatymą';

// Tabs
$_['tab_latvia']       = 'Latvijoje';
$_['tab_lithuania']    = 'Lietuvoje';
$_['tab_estonia']      = 'Estijoje';

// Entry
$_['entry_cost']       = 'Kaina:';
$_['entry_dimension']  = 'Didžiausias leistinas dydis (Ilgis x Plotis x Aukštis):<br /><a target="_blank" href="https://www.omniva.lt/public/files/failid/salygos-siunta-kurjeris-lit-lt-en-new.pdf">Standartinės sąlygos ir reikalavimai kurjerio paslaugoms</a>';
$_['entry_weight']     = 'Didžiausias leistinas svoris:<br /><a target="_blank" href="https://www.omniva.lt/public/files/failid/salygos-siunta-pastomatas-lit-lt-en-new.pdf">Standartinės paštomatų paslaugų teikimo sąlygos</a>';
$_['entry_tax_class']  = 'Mokesčiai:';
$_['entry_geo_zone']   = 'Geozona:';
$_['entry_status']     = 'Būsena:';
$_['entry_sort_order'] = 'Rikiavimo tvarka:';
$_['entry_import_url'] = 'URL adresas į CSV failą, Omniva paštomatų adresų importavimui:';
$_['entry_addresses']  = 'Omniva adresai:';

$_['button_update']    = 'Atnaujinti adresus ir išsaugoti';

$_['help_dimension']  = 'Didžiausias leistinas dydis 38 cm (Ilgis) x 41 cm (Plotis) x 64 cm (Aukštis)';
$_['help_weight']     = 'Didžiausias leistinas svoris 30 kg';

// Error
$_['error_permission'] = 'Įspėjimas: Jūs neturite teisių modifikuoti Omniva pristatymo modulio!';
$_['error_omniva_weight']  = 'Neįvestas svoris!';
$_['error_omniva_length']  = 'Neįvestas ilgis!';
$_['error_omniva_width']   = 'Neįvestas plotis!';
$_['error_omniva_height']  = 'Neįvestas aukštis!';