<?php

// Heading
$_['heading_title']                     = 'Atsiimti iš parduotuvės';

// Text
$_['text_shipping']                     = 'Pristatymas';
$_['text_success']                      = 'Sėkmingai modifikuotas atsiėmimas iš parduotuvės!';
$_['text_edit']                         = 'Koreguoti atsiėmimą iš parduotuvės';

// Entry
$_['entry_geo_zone']                    = 'Geo zona';
$_['entry_status']                      = 'Būsena';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti atsiėmimo iš parduotuvės!';
