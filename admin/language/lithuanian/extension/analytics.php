<?php

// Heading
$_['heading_title']                     = 'Analytics';

// Text
$_['text_success']                      = 'Sėkmingai modifikuota analizė';
$_['text_list']                         = 'Analytics sąrašas';

// Column
$_['column_name']                       = 'Analytics pavadinimas';
$_['column_status']                     = 'Būsena';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti analizės!';
