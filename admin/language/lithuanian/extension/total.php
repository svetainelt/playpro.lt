<?php

// Heading
$_['heading_title']                     = 'Sąskaitų sudėtinės dalys';

// Text
$_['text_success']                      = 'Sėkmingai modifikuotos sąskaitų sudėtinės dalys!';
$_['text_list']                         = 'Sąskaitų sudėtinių dalių sąrašas';

// Column
$_['column_name']                       = 'Sąskaitos sudėtinė dalis';
$_['column_status']                     = 'Būsena';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti sąskaitų sudėtinių dalių!';
