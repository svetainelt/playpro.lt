<?php

// Heading
$_['heading_title']                     = 'Modulių diegimas';

// Text
$_['text_success']                      = 'Sėkmingai įdiegtas papildinys!';
$_['text_unzip']                        = 'Išskleidžiami failai!';
$_['text_ftp']                          = 'Kopijuojami failai!';
$_['text_sql']                          = 'Paleidžiama SQL!';
$_['text_xml']                          = 'Taikomos modifikacijos!';
$_['text_php']                          = 'Paleidžiama PHP!';
$_['text_remove']                       = 'Šalinami laikinieji failai!';
$_['text_clear']                        = 'Sėkmingai pašalinti laikinieji failai!';

// Entry
$_['entry_upload']                      = 'Įkelti failą';
$_['entry_overwrite']                   = 'Failai kurie bus perrašyti';
$_['entry_progress']                    = 'Eiga';

// Help
$_['help_upload']                       = 'Reikalingas modifikacijos failas su papildiniu ".ocmod.zip" arba "ocmod.xml".';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti papildinius!';
$_['error_temporary']                   = 'Įspėjimas: yra kaikurių failų kuriuos reikia pašalinti. Paspauskite valymo mygtuką, kad išvalytų juos!';
$_['error_upload']                      = 'Failas negalėjo būti įkeltas!';
$_['error_filetype']                    = 'Netinkamas failo tipas!';
$_['error_file']                        = 'Failas nerastas!';
$_['error_unzip']                       = 'Zip failas negalėjo būti atidarytas!';
$_['error_code']                        = 'Modifikacijai reikalingas unikalus ID kodas!';
$_['error_exists']                      = 'Modifikacija %s naudoja tokį patį ID kodą kurį bandote įkelti!';
$_['error_directory']                   = 'Katalogas kuriame yra įkeliami failai, nerastas!';
$_['error_ftp_status']                  = 'FTP turi būti aktyvuotas nustatymuose';
$_['error_ftp_connection']              = 'Nepavyko prisijungti kaip %s:%s';
$_['error_ftp_login']                   = 'Nepavyko prisijungti kaip %s';
$_['error_ftp_root']                    = 'Nepavyko nustatyti šakninio katalogo kaip %s';
$_['error_ftp_directory']               = 'Nepavyko pakeisti į katalogą %s';
$_['error_ftp_file']                    = 'Nepavyko įkelti failo %s';
