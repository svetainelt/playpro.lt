<?php

// Heading
$_['heading_title']                     = 'Prekių pasirinkimai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti pasirinkimai!';
$_['text_list']                         = 'Prekių pasirinkimų sąrašas';
$_['text_add']                          = 'Pridėti pasirinkimą';
$_['text_edit']                         = 'Koreguoti pasirinkimą';
$_['text_choose']                       = 'Pasirinkimas';
$_['text_select']                       = 'Pasirinkti iš sąrašo (select)';
$_['text_radio']                        = 'Pasirinkti vieną iš kelių variantų (radio)';
$_['text_checkbox']                     = 'Pažymėti varnele (checkbox)';
$_['text_image']                        = 'Paveikslėlis';
$_['text_input']                        = 'Įvedimas';
$_['text_text']                         = 'Tekstinis laukelis (text)';
$_['text_textarea']                     = 'Didelis tekstinis laukelis (textarea)';
$_['text_file']                         = 'Failas';
$_['text_date']                         = 'Data';
$_['text_datetime']                     = 'Data ir laikas';
$_['text_time']                         = 'Laikas';

// Column
$_['column_name']                       = 'Pasirinkimo pavadinimas';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Pasirinkimo pavadinimas';
$_['entry_type']                        = 'Tipas';
$_['entry_option_value']                = 'Pasirinkimo reikšmės pavadinimas';
$_['entry_image']                       = 'Paveikslėlis';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisių modifikuoti pasirinkimų nustatymus!';
$_['error_name']                        = 'Pasirinkimo pavadinimas turi būti nuo 1 iki 128 simbolių!';
$_['error_type']                        = 'Įspėjimas: Privalote nurodyti pasirinkimo reikšmes!';
$_['error_option_value']                = 'Pasirinkimo reikšmės pavadinimas turi būti nuo 1 iki 128 simbolių!';
$_['error_product']                     = 'Įspėjimas: Šis pasirinkimas negali būti pašalintas, nes yra priskirtas %s prekėms (-ių)!';
