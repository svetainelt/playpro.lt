<?php

// Heading
$_['heading_title']                     = 'Prekių atributai';

// Text
$_['text_success']                      = 'Sėkmingai modifikuoti atributai!';
$_['text_list']                         = 'Prekių atributų sąrašas';
$_['text_add']                          = 'Pridėti atributą';
$_['text_edit']                         = 'Koreguoti atributą';

// Column
$_['column_name']                       = 'Atributo pavadinimas';
$_['column_attribute_group']            = 'Atributo grupė';
$_['column_sort_order']                 = 'Rikiavimo eiliškumas';
$_['column_action']                     = 'Veiksmas';

// Entry
$_['entry_name']                        = 'Atributo pavadinimas';
$_['entry_attribute_group']             = 'Atributo grupė';
$_['entry_sort_order']                  = 'Rikiavimo eiliškumas';

// Error
$_['error_permission']                  = 'Įspėjimas: neturite teisių atributų keitimui!';
$_['error_attribute_group']             = 'Atributo grupė reikalinga!';
$_['error_name']                        = 'Atributo pavadinimas turi būti nuo 3 iki 64 simbolių!';
$_['error_product']                     = 'Įspėjimas: šis atributas negali būti pašalintas, nes jis yra priskirtas %s prekėms (-ių)!';
