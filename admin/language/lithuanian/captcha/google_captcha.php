<?php

// Heading
$_['heading_title']                     = 'Google reCAPTCHA';

// Text
$_['text_captcha']                      = 'Captcha';
$_['text_success']                      = 'Sėkmingai modifikuotas Google reCAPTHCA!';
$_['text_edit']                         = 'Koreguoti Google reCAPTCHA';
$_['text_signup']                       = 'Eikite į <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>Google reCAPTCHA</u></a> puslapį ir užregistruokite jūsų svetainę.';

// Entry
$_['entry_key']                         = 'Puslapio raktas (Site key)';
$_['entry_secret']                      = 'Saugos raktas (Secret key)';
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti Google reCAPTCHA!';
$_['error_key']                         = 'Puslapio raktas reikalingas!';
$_['error_secret']                      = 'Saugos raktas reikalingas!';
