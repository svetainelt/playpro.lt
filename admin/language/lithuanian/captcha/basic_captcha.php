<?php

// Heading
$_['heading_title']                     = 'Standartinė (Basic) Captcha';

// Text
$_['text_captcha']                      = 'Captcha';
$_['text_success']                      = 'Sėkmingai modifikuotas Basic Captcha!';
$_['text_edit']                         = 'Koreguoti Basic Captcha';

// Entry
$_['entry_status']                      = 'Būsena';

// Error
$_['error_permission']                  = 'Įspėjimas: Jūs neturite teisės modifikuoti Basic Captcha!';
