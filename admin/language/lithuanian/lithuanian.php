<?php

// Code
$_['code']                              = 'lt';

// Direction
$_['direction']                         = 'ltr';

// Date
$_['date_format_short']                 = 'Y.m.d';
$_['date_format_long']                  = 'l Y F dS';

// Time
$_['time_format']                       = 'H:i:s ';

// Datetime
$_['datetime_format']                   = 'Y.m.d H:i:s';

// Decimal
$_['decimal_point']                     = '.';

// Thousand
$_['thousand_point']                    = ' ';

// Text
$_['text_yes']                          = 'Taip';
$_['text_no']                           = 'Ne';
$_['text_enabled']                      = '<span class="label label-success">Aktyvuota</span>';
$_['text_disabled']                     = '<span class="label label-warning">Deaktyvuota</span>';
$_['text_none']                         = ' --- Nėra --- ';
$_['text_select']                       = ' --- Pasirinkite --- ';
$_['text_select_all']                   = 'Pažymėti viską';
$_['text_unselect_all']                 = 'Nežymėti nieko';
$_['text_all_zones']                    = 'Visos zonos';
$_['text_default']                      = ' <b>(Numatytoji)</b>';
$_['text_close']                        = 'Uždaryti';
$_['text_pagination']                   = 'Rodoma nuo %d iki %d iš %d (%d psl.)';
$_['text_loading']                      = 'Įkeliama...';
$_['text_no_results']                   = 'Nieko nerasta!';
$_['text_confirm']                      = 'Ar Jūs tuo tikras?';
$_['text_home']                         = 'Pradžia';

// Button
$_['button_add']                        = 'Pridėti naują';
$_['button_delete']                     = 'Pašalinti';
$_['button_save']                       = 'Išsaugoti';
$_['button_cancel']                     = 'Atšaukti';
$_['button_cancel_recurring']           = 'Atšaukti pasikartojančius mokėjimus';
$_['button_continue']                   = 'Tęsti';
$_['button_clear']                      = 'Išvalyti įrašus';
$_['button_close']                      = 'Uždaryti';
$_['button_enable']                     = 'Aktyvuota';
$_['button_disable']                    = 'Deaktyvuota';
$_['button_filter']                     = 'Filtruoti';
$_['button_send']                       = 'Siųsti';
$_['button_edit']                       = 'Taisyti';
$_['button_copy']                       = 'Kopijuoti';
$_['button_back']                       = 'Atgal';
$_['button_remove']                     = 'Pašalinti';
$_['button_refresh']                    = 'Atnaujinti';
$_['button_export']                     = 'Eksportuoti';
$_['button_import']                     = 'Importuoti';
$_['button_download']                   = 'Parsisiųsti';
$_['button_rebuild']                    = 'Atstatyti';
$_['button_upload']                     = 'Įkelti';
$_['button_submit']                     = 'Pateikti';
$_['button_invoice_print']              = 'Spausdinti sąskaitą faktūrą';
$_['button_shipping_print']             = 'Spausdinti pristatymo sąrašą';
$_['button_address_add']                = 'Pridėti adresą';
$_['button_attribute_add']              = 'Pridėti atributą';
$_['button_banner_add']                 = 'Pridėti banerį';
$_['button_custom_field_value_add']     = 'Pridėti skirtingą lauką';
$_['button_product_add']                = 'Pridėti prekę';
$_['button_filter_add']                 = 'Pridėti filtrą';
$_['button_option_add']                 = 'Pridėti pasirinkimą';
$_['button_option_value_add']           = 'Pridėti pasirinkimo reikšmę';
$_['button_recurring_add']              = 'Pridėti periodinį mokėjimą';
$_['button_discount_add']               = 'Pridėti nuolaidą';
$_['button_special_add']                = 'Pridėti akciją';
$_['button_image_add']                  = 'Pridėti nuotrauką';
$_['button_geo_zone_add']               = 'Pridėti geo zoną';
$_['button_history_add']                = 'Pakeisti būseną';
$_['button_transaction_add']            = 'Pridėti sandorį';
$_['button_route_add']                  = 'Pridėti kelią';
$_['button_rule_add']                   = 'Pridėti taisyklę';
$_['button_module_add']                 = 'Pridėti modulį';
$_['button_link_add']                   = 'Pridėti nuorodą';
$_['button_approve']                    = 'Patvirtinti';
$_['button_reset']                      = 'Atstatyti';
$_['button_generate']                   = 'Generuoti';
$_['button_voucher_add']                = 'Pridėti čekį';
$_['button_reward_add']                 = 'Pridėti lojalumo taškus';
$_['button_reward_remove']              = 'Pašalinti lojalumo taškus';
$_['button_commission_add']             = 'Pridėti komisinius';
$_['button_commission_remove']          = 'Pašalinti komisinius';
$_['button_credit_add']                 = 'Pridėti kreditą';
$_['button_credit_remove']              = 'Pašalinti kreditą';
$_['button_ip_add']                     = 'Pridėti IP';
$_['button_parent']                     = 'Atgal';
$_['button_folder']                     = 'Naujas aplankas';
$_['button_search']                     = 'Paieška';
$_['button_view']                       = 'Žiūrėti';
$_['button_install']                    = 'Įdiegti';
$_['button_uninstall']                  = 'Pašalinti';
$_['button_login']                      = 'Prisijungti';
$_['button_unlock']                     = 'Atrakinti paskyrą';
$_['button_link']                       = 'Nuoroda';
$_['button_currency']                   = 'Atnaujinti valiutos reikšmes';
$_['button_apply']                      = 'Taikyti';
$_['button_category_add']               = 'Pridėti kategoriją';

// Tab
$_['tab_address']                       = 'Adresas';
$_['tab_additional']                    = 'Papildoma';
$_['tab_admin']                         = 'Administratorius';
$_['tab_attribute']                     = 'Atributai';
$_['tab_customer']                      = 'Kliento detalės';
$_['tab_data']                          = 'Duomenys';
$_['tab_design']                        = 'Dizainas';
$_['tab_discount']                      = 'Nuolaidos';
$_['tab_general']                       = 'Bendra';
$_['tab_history']                       = 'Užsakymo būsena';
$_['tab_ftp']                           = 'FTP';
$_['tab_ip']                            = 'IP adresai';
$_['tab_links']                         = 'Kategorijos';
$_['tab_log']                           = 'Įrašai';
$_['tab_image']                         = 'Paveikslėliai';
$_['tab_option']                        = 'Pasirinkimai';
$_['tab_server']                        = 'Serveris';
$_['tab_store']                         = 'Parduotuvė';
$_['tab_special']                       = 'Akcijos';
$_['tab_session']                       = 'Sesija';
$_['tab_local']                         = 'Vietovė';
$_['tab_mail']                          = 'Paštas';
$_['tab_module']                        = 'Modulis';
$_['tab_payment']                       = 'Mokėjimo informacija';
$_['tab_product']                       = 'Prekės';
$_['tab_reward']                        = 'Lojalumo taškai';
$_['tab_shipping']                      = 'Pristatymo informacija';
$_['tab_total']                         = 'Sumos';
$_['tab_transaction']                   = 'Sandoriai';
$_['tab_voucher']                       = 'Dovanų čekiai';
$_['tab_sale']                          = 'Pardavimai';
$_['tab_marketing']                     = 'Marketingas';
$_['tab_online']                        = 'Prisijungę lankytojai';
$_['tab_activity']                      = 'Naujausia veikla';
$_['tab_recurring']                     = 'Pasikartojimai';
$_['tab_action']                        = 'Veiksmas';
$_['tab_google']                        = 'Google';

// Error
$_['error_exception']                   = 'Klaidos kodas (%s): %s linijoje %s';
$_['error_upload_1']                    = 'Įspėjimas: Įkelto failo dydis viršija parametro upload_max_filesize reikšmę, esančią php.ini!';
$_['error_upload_2']                    = 'Įspėjimas: Įkelto failo dydis viršija parametro MAX_FILE_SIZE reikšmę, nurodytą HTML formoje!';
$_['error_upload_3']                    = 'Įspėjimas: Failas įkeltas tik dalinai!';
$_['error_upload_4']                    = 'Įspėjimas: Failas neįkeltas!';
$_['error_upload_6']                    = 'Įspėjimas: Nerastas laikinasis katalogas!';
$_['error_upload_7']                    = 'Įspėjimas: Nepavyko įrašyti failo į kaupiklį!';
$_['error_upload_8']                    = 'Įspėjimas: Failo įkėlimas sustabdytas, nes plėtinys yra neleistinas!';
$_['error_upload_999']                  = 'Įspėjimas: Nenustatytas klaidos kodas!';
