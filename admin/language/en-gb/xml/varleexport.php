<?php
// Heading
$_['heading_title']     = 'Varle XML Export';


// Text
$_['text_success']      = 'Success: You modified Varle XML Export';
$_['text_off']      = 'Export is disabled';

// Column
$_['column_action']     = 'Action';

// Entry
$_['entry_status']     = 'Status';
$_['entry_categories']     = 'Categories';
$_['entry_select_all']     = 'Select all';
$_['entry_unselect_all']     = 'Unselect all';
$_['entry_language_id']     = 'XML export language';
$_['entry_store']     = 'Export products from stores';
$_['entry_manufacturers']     = 'Manufacturers';
$_['entry_min_price']     = 'Minimal product price';
$_['entry_status_info']     = 'If Export is disabled, XML URL not works';
$_['entry_language_id_info']     = 'Choose which language title and description use in export';
$_['entry_min_price_info']     = 'Only products with greater price than this. For example: 12.99';
$_['entry_retail_price']     = 'Change retail price in XML';
$_['entry_retail_price_info']     = 'You can change all prices. Leave empty fields if you want to show the price value stored in your database.';
$_['entry_heading'] = 'Varle XML export';
$_['entry_tax_class'] = 'Tax class';
$_['entry_operator'] = 'Operator';
$_['entry_value'] = 'Value';
$_['entry_prime_cost']     = 'Change prime cost price in XML (prime cost without VAT)';
$_['entry_prime_cost_info']     = 'You can change prime cost value. If you leave empty the price will be equal to retail price without taxes. If your retail price not using taxes, you must manually add prime cost!'; 

// Buttons
$_['button_save']     = 'Save';
$_['button_cancel']     = 'Cancel';

// Error
$_['error_permission']  = 'Attention: You do not have access to modify module!';
$_['error_prime_cost']  = 'If you are not using retail price taxes please manually add prime cost formula or select retail price tax class!';

?>